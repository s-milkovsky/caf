/* global MAIN_PAGE_URL, API_URL */

import _ from 'lodash';

export class Store {
  constructor($cookies) {
    'ngInject';
    this.$cookies = $cookies;
  }

  get(key) {
    return this[key];
  }

  set(key, value) {
    this[key] = value;
  }

  getBlocks() {
    this.blocks = this.groups.map(group => (group.blocks));
    this.blocks = _.compact(_.flatten(this.blocks));
    this.set('blocks', this.blocks);
    return this.get('blocks');
  }

  getMainMenuBlock() {
    const blocks = this.getBlocks();
    return _.find(blocks, { builtin: true, title: 'Main menu' });
  }

  getMainUrl() {
    return window.location.protocol + '//' + window.location.hostname.replace('dashboard.', '');
  }

  getApiUrl() {
//    return 'https://api.chatfuel.com';
    return API_URL ? API_URL : (window.location.protocol + '//' + 'api.' + window.location.hostname.replace('dashboard.', ''));
  }

  setAutorizedCookie(val) {
    this.$cookies.put('___chatfuel_authorized', (val ? 'true' : 'false'), { path: '/', domain: 'chatfuel.com', expires: new Date(2050, 1, 1) });
  }

  setGoOutCookie(val) {
    this.$cookies.put('___chatfuel_go_out', (val ? 'true' : 'false'), { path: '/', domain: 'chatfuel.com', expires: new Date(2050, 1, 1) });
  }
}

// export function PreloaderInterceptor($rootScope) {
//   'ngInject';
//
//   return {
//     request(config) {
//       const token = localStorage.getItem('token');
//       if (token) $rootScope.showPreloader = false;
//       return config;
//     }
//   };
// }

// @see: http://www.codelord.net/2015/10/22/angular-authentication-3-step-recipe/
export function AuthInterceptor($q, $window, $rootScope, StoreService, $injector) {
  'ngInject';
  return {
    request(config) {
      const token = localStorage.getItem('token');
      config.headers.Bearer = token; // back compatibility. @TODO: remote header
      config.headers.Authorization = `Bearer ${token}`;
      return config;
    },

    responseError(rejection) {
      if (rejection.status === 401) {
        // clear token and redirect to authentication page
        localStorage.removeItem('token');
        StoreService.setAutorizedCookie(false);
        // remove trailing / and concat /auth
        let getParams = false;

        try {
          getParams = localStorage.getItem('get-params');
        } catch (err) {}

        $window.location = StoreService.getApiUrl().replace(/\/+$/, '') + '/auth' + (getParams ? getParams : '');
      } else if (rejection.status === 403 && !$rootScope.$tosShow && rejection.data.result === 'User didn\'t accept terms of service') {
        const par = window.location.hash.split('/');
        if (par.indexOf('bot') !== -1) {
          $rootScope.$afterShowTosTarget = par[par.indexOf('bot') + 1];
        } else {
          $rootScope.$afterShowTosTarget = 'bots';
        }
        $rootScope.$broadcast('$tos', true);
      }
      return $q.reject(rejection);
    }
  };
}

export function SequenceInterceptor($q) {
  'ngInject';

  let defers = [];

  return {
    request(config) {

      // if (config.async) {
      //   return config;
      // }

      if (config.method !== 'POST') {
        return config;
      }

      let previous = defers.shift();

      if (!previous) {
        previous = $q.defer();
        previous.resolve();
      }

      config.$$deferred = $q.defer();
      defers.push(config.$$deferred);

      return previous.promise.then(() => config);
    },

    response(res) {
      res.config.$$deferred && res.config.$$deferred.resolve();
      return res;
    },

    responseError(res) {
      res.config.$$deferred && res.config.$$deferred.resolve();
      return $q.reject(res);
    }
  };
}
