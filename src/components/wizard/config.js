
export default ($stateProvider) => {
  'ngInject';
  $stateProvider
    .state('wizard', {
      url: '/wizard',
      abstract: true
    });
};

