
export default class WizardPageController {
  constructor($window, $timeout) {
    'ngInject';
    $window.scrollTo(0, 0);

    // @see: /src/app.component.js
    // set class name here 'case uiView changes controller
    this.className = 'wizard';

    $timeout(() => {
      window.FB && FB.XFBML.parse();
    });
  }
}
