import ng from 'angular';
import ngRouter from 'angular-ui-router';

import config from './config';
import Component from './component';

export default ng.module('app.components.wizard.page4', [ngRouter])
  .directive('wizardPage4', Component)
  .config(config)
  .name;
