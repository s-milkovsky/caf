
export default ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('wizard.page4', {
      url: '/4/:bot_id',
      views: {
        'main@': {
          template: '<wizard-page4></wizard-page4>'
        }
      }
    });
};
