import template from './page4.template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});
