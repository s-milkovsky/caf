
export default ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('wizard.page3', {
      url: '/3/:bot_id',
      views: {
        'main@': {
          template: '<wizard-page3></wizard-page3>'
        }
      }
    });
};
