
export default class WizardPageController {
  constructor($window, $state, $stateParams, $rootScope, BotService) {
    'ngInject';

    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
    this.BotService = BotService;

    this.botReadOnly = false;

    $window.scrollTo(0, 0);

    // @see: /src/app.component.js
    // set class name here 'case uiView changes controller
    this.className = 'wizard';

    this.loadBot();
  }

  loadBot() {
    this.BotService.show()
      .then(bots => {
        this.bot = bots;
        this.botReadOnly = this.bot.status.read_only;
        if (!this.botReadOnly) {
          this.botReadOnly = false;
        }
      });
  }

  onBotReadOnlyChange() {
    this.bot.status.read_only = this.botReadOnly;

    if (this.bot.status.read_only) {
      this.bot.status.status = 'pending';
    } else
    if (!this.bot.status.read_only && this.bot.status.status === 'pending') {
      this.bot.status.status = 'published';
    }

    this.BotService.update(this.bot.id, {
      timezone_offset: this.bot.timezone_offset,
      timezone_name: this.bot.timezone_name,
      title: this.bot.title,
      read_only: this.botReadOnly,
      status: this.bot.status.status
    });

    this.$rootScope.$broadcast('$updateBotStatus', { id: this.bot.id, status: this.bot.status.status, read_only: this.bot.status.read_only });
  }

  done() {
    this.BotService.cachedList(true);

    if (this.$rootScope.backStateName) {
      this.$state.go(this.$rootScope.backStateName, { bot_id: this.bot.id });
      this.$rootScope.backStateName = null;
    } else {
      this.$state.go('bots');
    }
  }
}
