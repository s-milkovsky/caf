import template from './wizard-header.template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'whc',
  restrict: 'E',
  replace: true
});
