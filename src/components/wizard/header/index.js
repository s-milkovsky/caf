import ng from 'angular';
import Component from './component';

export default ng.module('app.common.wizard.header', [])
  .directive('wizardHeader', Component)
  .name;
