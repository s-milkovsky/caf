
export default class WizardHeaderController {
  constructor($scope, $rootScope, $state, $stateParams, BotService) {
    'ngInject';
    this.$state = $state;
    this.$rootScope = $rootScope;
    this.$stateParams = $stateParams;
    this.BotService = BotService;

    this.alow3Step = false;
    this.alowDone = false;

    $scope.$on('$stateChangeSuccess', () => {
      this.$opened = $state.includes('wizard.**');
      if (this.$opened && this.$stateParams.bot_id) {
        this.loadBot();
      }
    });

    $scope.$on('$accessTokenEvent', (e, data) => {
      this.alow3Step = data;
    });

    $scope.$on('$updateBotStatus', (e, data) => {
      this.alowDone = data.read_only;
    });
  }

  goBack(page) {
    this.$state.go('wizard.page' + page, { bot_id: this.$stateParams.bot_id });
  }

  close() {
    if (this.$rootScope.backStateName) {
      this.$state.go(this.$rootScope.backStateName, { bot_id: this.$stateParams.bot_id });
      this.$rootScope.backStateName = null;
    } else {
      this.$state.go('bots');
    }
  }

  loadBot() {
    this.BotService.show()
      .then(bots => {
        this.bot = bots;
        this.alow3Step = (this.bot.status.status !== 'draft');
        this.alowDone = this.bot.status.read_only;
      });
  }
}
