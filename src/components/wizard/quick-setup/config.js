
export default ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('wizard.quickSetup', {
      url: '/quick-setup/:bot_id',
      views: {
        'main@': {
          template: '<wizard-quick-setup></wizard-quick-setup>'
        }
      }
    });
};
