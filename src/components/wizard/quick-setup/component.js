import template from './quick-setup.template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});
