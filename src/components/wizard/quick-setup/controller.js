export default class WizardPageController {
  constructor($state, $stateParams, PageService, BotService, API_ERROR_MSG) {
    'ngInject';
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.PageService = PageService;
    this.BotService = BotService;
    this.API_ERROR_MSG = API_ERROR_MSG;

    this.loadAppParams();

    // @see: /src/app.component.js
    // set class name here 'case uiView changes controller
    this.className = 'preferences';
  }

  loadAppParams() {
    this.BotService.appParams(this.$stateParams.bot_id)
      .then(appParams => {
        this.appParams = appParams;
      })
  }

  bindBotToPage() {
    this.PageService.bindBot(this.$stateParams.bot_id, this.accessToken)
      .then(() => {
        this.$state.go('bots');
      })
      .catch(err => {
     //   alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });
  }
}
