
export default ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('wizard.page2', {
      url: '/2/:bot_id',
      views: {
        'main@': {
          template: '<wizard-page2></wizard-page2>'
        }
      }
    });
};
