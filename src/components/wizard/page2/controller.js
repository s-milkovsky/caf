
export default class WizardPageController {
  constructor($stateParams, $rootScope, $window, PageService, BotService) {
    'ngInject';
    this.$rootScope = $rootScope;
    this.$stateParams = $stateParams;
    this.PageService = PageService;
    this.BotService = BotService;

    $window.scrollTo(0, 0);
    this.loadAppParams();

    this.loadBot();

    this.className = 'wizard';
  }

  loadBot() {
    this.BotService.show()
      .then(bots => {
        this.bot = bots;
        if (this.bot.status && this.bot.status.status !== 'draft') {
          this.loadPage();
        }
      });
  }

  loadPage() {
    this.PageService.list().then(pages => {
      this.page = pages.find(page => page.id === this.bot.status.page);
      if (this.page && this.page.access_token) {
        this.PageService.getAppId(this.bot.status.page)
          .then(data => {
            if (!data.chatfuel_app) {
              this.accessToken = this.page.access_token;
              this.$result = true;
            }
          });
      }
    });
  }


  loadAppParams() {
    this.BotService.appParams(this.$stateParams.bot_id)
      .then(appParams => {
        this.appParams = appParams;
      });
  }

  bindBotToPage() {
    this.PageService.bindBot(this.$stateParams.bot_id, this.accessToken)
      .then(page => {
        this.$result = true;
        this.$pageTitle = page.title;
        this.$rootScope.$broadcast('$accessTokenEvent', true);

        this.BotService.update(this.bot.id, {
          timezone_offset: this.bot.timezone_offset,
          timezone_name: this.bot.timezone_name,
          title: this.bot.title,
          read_only: false,
          status: 'connected'
        });
      })
      .catch(err => {
        this.$result = false;
        this.$rootScope.$broadcast('$accessTokenEvent', false);
      });
  }
}
