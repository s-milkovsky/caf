import ng from 'angular';

import config from './config';
import header from './header';
import quickSetup from './quick-setup';

import page1 from './page1';
import page2 from './page2';
import page3 from './page3';
import page4 from './page4';

export default ng.module('app.components.wizard', [
  header,
  quickSetup,
  page1,
  page2,
  page3,
  page4
])
  .config(config)
  .name;
