
export default class WizardPageController {
  constructor($stateParams, $window) {
    'ngInject';
    this.$stateParams = $stateParams;

    $window.scrollTo(0, 0);

    // @see: /src/app.component.js
    // set class name here 'case uiView changes controller
    this.className = 'wizard';
  }
}
