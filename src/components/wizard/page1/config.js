
export default ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('wizard.page1', {
      url: '/1/:bot_id',
      views: {
        'main@': {
          template: '<wizard-page1></wizard-page1>'
        }
      }
    });
};
