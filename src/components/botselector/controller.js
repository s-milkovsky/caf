/* global FB */

export class BotSelectorController {
  constructor(
    $scope,
    $rootScope,
    $state,
    $stateParams,
    $sce,
    $timeout,
    $window,
    BotService,
    ModalService,
    BlockService,
    PageService,
    StoreService,
    UserService) {
    'ngInject';
    this.$sce = $sce;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$timeout = $timeout;
    this.$window = $window;
    this.$stateParams = $stateParams;
    this.BotService = BotService;
    this.ModalService = ModalService;
    this.BlockService = BlockService;
    this.PageService = PageService;
    this.UserService = UserService;
    this.StoreService = StoreService;

    this.rootLink = StoreService.getMainUrl();

    this.link = false;
    this.bots = [];
    this.currentBot = {};
    this.buttonUpdating = false;

    this.listeners = [];


    this.listeners.push($scope.$on('$stateChangeSuccess', () => {
      this.opened = $state.includes('app.**');

      if (!$state.is('auth.token')) {
        if (!this.currentBot || this.currentBot.id !== this.$stateParams.bot_id) {
          this.loadBotList();
        }
      }
      if ($state.is('app.settings')) {
        this.showConnectButton = false;
      } else {
        this.showConnectButton = true;
      }
    }));

    this.listeners.push($scope.$on('$fbClickEvent', () => {
      $timeout(() => {
        this.testPageCode = '';
        this.fbButtonLoadedEmulate = false;

        $timeout(() => {
          this._updateMessageUsButton();
        });
        this.link = false;
      }, 100);
    }));

    this.watcher = $scope.$watch(() => this.$stateParams.bot_id, () => {
      this._updateCurrentBot();
      this.UserService.show().then(res => {
        this.user = res;
        this._updateMessageUsButton();
      });
    });

    this.listeners.push($scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.selectorOpened = false;
        this.signOutOpened = false;
      });
    }));

    this.listeners.push($scope.$on('$updateBotStatus', (e, data) => {
      if (this.currentBot.id === data.id) {
        this.currentBot.status.status = data.status;
        this.currentBot.status.read_only = data.read_only;
        this.currentBot.status.page = data.pageId;

        if (data.ownerId) {
          if (!this.currentBot.status.page_info) {
            this.currentBot.status.page_info = {};
            this.currentBot.status.page_info.picture = data.picture;
          }
          if (!this.currentBot.status.page_info.owner) {
            this.currentBot.status.page_info.owner = {};
          }

          this.currentBot.status.page_info.owner.id = data.ownerId;
        } else {
          delete this.currentBot.status.page_info;
        }

        this.updateBotStatus();
      }
      this.pageLink = '';
    }));

    this.listeners.push($rootScope.$on('fb.load', () => {
      this.renderingFbButton();
    }));

    this.listeners.push($rootScope.$on('$tos', (event, status) => {
      if (!status) {
        this.UserService.show(true).then(res => {
          this.user = res;
          this.loadBotList();
        });
      }
    }));

    this.showConnectBtn = false;
  }

  $onDestroy() {
    this.closeModal();
    this.watcher();
    this.listeners.forEach(fn => fn.call());
  }

  updateBotStatus() {
    this.wizardButtonTitle = this.BotService.getWizardButtonText(this.currentBot);
  }

  loadBotList() {
        this.BotService.cachedList(true)
          .then(bots => {
            // bots.push(result.bot);
            this._update(bots);
            if (this.user && this.user.name) {
              let name = this.user.name.match(/^\S+/)[0];
              if (name.length > 13) {
                name = `${name.slice(0, 13)}...`;
                this.nameStyle12 = true;
              }
              if (name.length <= 11 && name.length >= 10) {
                this.nameStyle13 = true;
              }
              if (name.length <= 13 && name.length >= 12) {
                this.nameStyle12 = true;
              }
              this.profileName = name;
              this.profileImage = this.user.picture;
            }
          });
  }

  _update(bots) {
    this.bots = bots;
    this._updateCurrentBot();
    this._updateMessageUsButton();
  }

  _updateCurrentBot() {
    this.currentBot = this.bots.find(bot => bot.id === this.$stateParams.bot_id);

    this.updateBotStatus();

    if (this.$state.current.name === 'app.structure' || this.$state.current.name === 'app.aiSetup') {
      if (this.currentBot && this.currentBot.status && this.currentBot.status.read_only) {
        if (!this.modal) {
          this.modal = this.ModalService.readOnly();
        }
      } else {
        this.closeModal();
      }
    } else {
      this.closeModal();
    }
  }

  closeModal() {
    if (this.modal) {
      this.modal.close(null);
      this.modal = null;
    }
  }

  fbWatch() {
    this.$timeout(() => {
      this._updateMessageUsButton();
    }, 100);
  }

  _updateMessageUsButton() {
    if (!this.$window.FB) {
      this.fbWatch();
      return;
    }

    if (!this.currentBot || !this.currentBot.id || !this.user) {
      return;
    }

    if (this.buttonUpdating) {
      return;
    } else {
      this.buttonUpdating = true;
      this.$timeout(() => {
        this.buttonUpdating = false;
      }, 2000);
    }

//     if (this.currentBot.status.page && this.currentBot.status.page_info && this.currentBot.status.page_info.owner && this.user.id === this.currentBot.status.page_info.owner.id) {
    if (this.currentBot.status.page) {
      this.PageService.getAppId(this.currentBot.status.page)
        .then(data => {

          if (data.chatfuel_app) {
            this.BlockService.webPluginButton(this.currentBot.first_block.id, 'blue', 'large')
              .then(code => {
                this.testPageCode = this.$sce.trustAsHtml(code);
                this.renderingFbButton();
              });
          } else {
            this.BotService.testPageButton(this.currentBot.id, 'blue', 'large')
              .then(code => {
                this.testPageCode = this.$sce.trustAsHtml(code);
                this.renderingFbButton();
              });
          }
        });
    } else {
      this.showConnectBtn = true;
      this.BotService.testPageButton(this.currentBot.id, 'blue', 'large')
        .then(code => {
          this.testPageCode = this.$sce.trustAsHtml(code);
          this.renderingFbButton();
        });
    }
  }

  renderingFbButton() {
    if (this.testPageCode) {
      this.fbButtonLoadedEmulate = false;

      this.$timeout(() => {
        if (this.$window.FB) {
          this.$window.FB.getLoginStatus((response) => {
            if (response.status === 'connected') {
              this.$window.FB.XFBML.parse();
              this.$timeout(() => {
                   this.fbButtonLoadedEmulate = true;
              }, 1000);
              // } else if (response.status === 'not_authorized') {
            } else {
              localStorage.removeItem('token');
              this.StoreService.setAutorizedCookie(false);
              let getParams = false;
              try {
                getParams = localStorage.getItem('get-params');
              } catch (err) {}
              this.$window.location = this.StoreService.getApiUrl().replace(/\/+$/, '') + '/auth' + (getParams ? getParams : '');
            }
          }, true);
        }
      }, 100);
    }
  }

  goWizard() {
    const target = this.BotService.getWizardButtonStep(this.currentBot);
    if (target > 0) {
      this.$rootScope.backStateName = this.$state.current.name;
      this.$state.go('wizard.page' + target, { bot_id: this.currentBot.id });
    } else {
      switch (target) {
        case -1:
          this.$state.go('app.settings', { bot_id: this.currentBot.id });
          break;
        case -2:
          this.$state.go('app.structure', { bot_id: this.currentBot.id });
          break;
        default:
      }
    }
  }

  openMessenger(event) {
    event.preventDefault();
    let mBotId = '';

    if (window.cfConfig && window.cfConfig.TEST_PAGE_ID) {
      mBotId = window.cfConfig.TEST_PAGE_ID;
    } else {
      if (DEBUG) {
        mBotId = '140912872977106';
      } else {
        mBotId = '1125538687485749';
      }
    }

    window.open(`https://m.me/${this.currentBot.status.page || mBotId}`, '_blank');
  }

  logout() {
    this.StoreService.setAutorizedCookie(false);
    localStorage.removeItem('token');
    this.$window.location = this.StoreService.getMainUrl();
  }

  changeBot(botId) {
    this.$rootScope.$emit('$statEvent', { event: 'hit', data: (window.location.protocol + '//' + window.location.hostname + '/#dropdown-bot-change') });
  }

  backArrClick() {
    this.$rootScope.$emit('$statEvent', { event: 'hit', data: (window.location.protocol + '//' + window.location.hostname + '/#back-arrow-click-in-header') });
  }
  copyLink() {
    this.$rootScope.$emit('$statEvent', { event: 'hit', data: (window.location.protocol + '//' + window.location.hostname + '/#copy-bot-url') });
    document.querySelector('.input-to-copy').select();
    try {
      document.execCommand('copy');
      this.copy = true;
      this.clearSelection();
    } catch (err) {
    }
  }
  clearSelection() {
    if (document.selection) {
      document.selection.empty();
    } else {
      if (window.getSelection) {
        window.getSelection().removeAllRanges();
      }
    }
  }
  closeTooltip() {
    this.$timeout(() => {
      this.copy = false;
    }, 500);
  }
  openPage() {
    this.$rootScope.$emit('$statEvent', { event: 'hit', data: (window.location.protocol + '//' + window.location.hostname + '/#open-bot-url') });
    window.open(`https://m.me/${this.currentBot.status.page}`, '_blank');
  }

  bcClick() {
    this.$rootScope.$emit('$bcClicked');
  }
}
