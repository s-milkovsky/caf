import ng from 'angular';
import ngRouter from 'angular-ui-router';

import { Component } from './components';

import Sidebar from './sidebar';

export default ng.module('app.common.botselector', [ngRouter, Sidebar])
  .directive('botSelector', Component)
  .name;
