import template from './templates/template.html';
import { BotSelectorController as controller } from './controller';

export const Component = () => ({
  template,
  controller,
  controllerAs: 'bsc', // vm overided
  restrict: 'E',
  transclude: true,
  replace: true
});
