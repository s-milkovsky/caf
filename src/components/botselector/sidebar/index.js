import ng from 'angular';
import ngRouter from 'angular-ui-router';

import { Component } from './components';

export default ng.module('app.common.botselector.sidebar', [ngRouter])
  .directive('sidebar', Component)
  .name;
