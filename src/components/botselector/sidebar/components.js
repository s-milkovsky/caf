import template from './sidebar.html';

export const Component = () => ({
  template,
  controllerAs: 'bsc', // vm overided
  restrict: 'E',
  replace: true
});
