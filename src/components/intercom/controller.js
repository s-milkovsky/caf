export class IntercomController {
  constructor($window, $timeout, UserService) {
    'ngInject';
    this.$window = $window;
    this.$timeout = $timeout;
    this.UserService = UserService;
  }

  $onInit() {
    const wToken = () => {
      this.$timeout(() => {
        if (localStorage.getItem('token')) {
          this.UserService.show().then(res => {
            if (res) {
              window.intercomSettings = {
                app_id: 'mca3oa0n',
                name: res.name,
                email: res.email,
                user_id: res.id,
                created_at: res.date_added,
                custom_launcher_selector: '#my_intercom_link',
                hide_default_launcher: true
              };
              const s = document.createElement('script');
              s.type = 'text/javascript';
              s.async = true;
              s.src = 'https://widget.intercom.io/widget/mca3oa0n';
              const x = document.getElementsByTagName('script')[0];
              x.parentNode.insertBefore(s,x);
            }
          });
        } else {
          wToken();
        }
      }, 500);
    };

    wToken();
  }
}
