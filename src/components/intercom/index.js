
import angular from 'angular';
import Component from './component.js';


export default angular
  .module('app.common.intercom', []).directive('intercom', Component).name;
