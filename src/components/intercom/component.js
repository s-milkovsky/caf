import { IntercomController as controller } from './controller';

export default () => ({
  controller,
  restrict: 'E'
});
