import ng from 'angular';
import Logo from './logo';
import Bots from './bots';
import Aside from './aside';
import Broadcast from './broadcast';
import Structure from './structure';
import BotSelector from './botselector';
import AiSetup from './aisetup';
import Promotion from './promotion';
import Statistics from './statistics';
import Settings from './settings';
import Support from './support';
import Wizard from './wizard';
import PluginContainer from './plugin-container';
import FacebookInit from './facebook-init';
import Intercom from './intercom';
import Analytics from './analytics';
import Faq from './faq';
import Tos from './tos';
import Onboarding2 from './onboarding';

export default ng.module('app.components', [
  Logo,
  Bots,
  Aside,
  Broadcast,
  Structure,
  BotSelector,
  AiSetup,
  Promotion,
  Statistics,
  Settings,
  Support,
  Wizard,
  PluginContainer,
  FacebookInit,
  Intercom,
  Analytics,
  Faq,
  Onboarding2,
  Tos
]).name;
