import template from './bots.create.template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});
