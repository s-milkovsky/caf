
export default ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('bots.create', {
      url: '/create',
      views: {
        'main@': {
          template: '<bots-create></bots-create>'
        }
      }
    });
};
