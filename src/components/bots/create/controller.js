export default class WizardDashboardController {
  constructor($state, BotService, $timeout) {
    'ngInject';
    this.$state = $state;
    this.BotService = BotService;
    this.$timeout = $timeout;

    this.loadDraftBots();
    this.loadAllBots();

    // @see: /src/app.component.js
    // set class name here 'case uiView changes controller
    this.className = 'bot-create';
  }

  loadDraftBots() {
    this.BotService.drafts()
      .then(drafts => {
        this.draftBots = drafts;
        this.draftBots[0].$selected = true;
      });
  }

  loadAllBots() {
    this.BotService.list()
      .then(bots => {
        bots.some(bot => {
          if (!!bot.title.match(/My chatbot \d/)) {
            let i = 1;
            let title;
            while (true) {
              title = `My chatbot ${i++}`;
              if (!bots.find(item => item.title === title)) {
                break;
              }
            }
            this.botTitle = title;
            return true;
          }
          else {
            this.botTitle = 'My chatbot 1';
          }
        });
        this.$timeout(() => {
          document.execCommand('selectAll', false, null);
        });
      });
  }

  $selectBot(bot) {
    this.draftBots.forEach(item => item.$selected = false);
    bot.$selected = true;
  }

  createBot() {
    if (!this.botTitle) {
      return;
    }
    this.$loading = true;

    this.BotService.create({ title: this.botTitle })
      .then(bot => {
        let draft = this.draftBots.find(item => item.$selected);
        this.botId = bot.id;
        if (draft.id) {
          return this.BotService.cloneFrom(bot.id, draft.id);
        }
      })
      .then(() => {
        if (this.$state.is('bots.create')) {
          // this.BotService.cachedList(true)
          //   .then(res => {
          //     let actualBot = _.find(res, { 'id': this.botId });
          //     this.blockId = actualBot.first_block.id;
          //     this.$state.go('app.structure.block', { bot_id: this.botId, id: this.blockId});
          //   });
          this.$state.go('bots');
        }
      })
      .catch(err => {
    //    alert(JSON.stringify(err.data || 'Error'));
      })
      .finally(() => {
        this.$loading = false;
      });
  }
}
