import ng from 'angular';
import ngRouter from 'angular-ui-router';

import config from './config';
import Component from './component';

export default ng.module('app.components.bots.create', [ngRouter])
  .directive('botsCreate', Component)
  .config(config)
  .name;
