
export default ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('bots', {
      url: '/bots',
      views: {
        'main@': {
          template: '<bots></bots>'
        }
      }
    });
};
