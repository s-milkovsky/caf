
export default class BotsController {
  constructor($rootScope, $state, BotService, $scope) {
    'ngInject';

    this.$rootScope = $rootScope;
    this.$state = $state;
    this.BotService = BotService;
    this.$scope = $scope;
    // @see: /src/app.component.js
    // set class name here 'case uiView changes controller
    this.className = 'dashboard-v2';
    this.botCloning = false;

    angular.element(document.querySelector('html')).css('background-color', '#ffffff');

    this.listeners = [];

    this.listeners.push(this.$rootScope.$on('startCloning', () => {
      this.$rootScope.showPreloader = true;
      this.botCloning = true;
    }));

    this.listeners.push(this.$rootScope.$on('stopCloning', () => {
      this.$rootScope.showPreloader = false;
      this.botCloning = false;
    }));

    this.listeners.push(this.$scope.$on('$tos', (event, status) => {
      if (!status) {
        this.loadBots();
      }
    }));
  }

  $onInit() {
    if (!this.$rootScope.$tosShow) {
      this.loadBots();
    }
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  loadBots() {
    this.$rootScope.showPreloader = true;
    this.BotService.cachedList(true)
      .then(bots => {
        this.bots = bots;
        this.$rootScope.showPreloader = false;
        if (!this.bots.length) {
          this.goBotCreation();
        }
      });
  }

  goBotCreation() {
    this.$state.go('bots.create');
  }
}
