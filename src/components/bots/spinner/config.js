
export default ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('bots.spinner', {
      url: '/spinner',
      views: {
        'main@': {
          template: '<bots-spinner></bots-spinner>'
        }
      }
    });
};
