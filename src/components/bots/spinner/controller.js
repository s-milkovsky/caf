export default class SpinnerController {
  constructor($state, BotService, $http, StoreService, HeaderAlertService, $timeout) {
    'ngInject';
    this.$state = $state;
    this.BotService = BotService;
    this.$http = $http;
    this.StoreService = StoreService;
    this.HeaderAlertService = HeaderAlertService;
    this.$timeout = $timeout;
    this.className = 'bot-spinner';
  }
  $onInit() {
    this.url = location.hash;
    this.getParams();
    this.getBotId()
      .success(res => {
        this.$state.go('app.statistics', { bot_id: res.result });
      })
      .error(res => {
        if (res && res.bot_id) {
          this.$state.go('app.statistics', { bot_id: res.bot_id });
          this.$timeout(() => {
            this.HeaderAlertService.setText(res.message);
          }, 500);
        } else {
          this.$state.go('bots');
        }
      });
  }

  getParams() {
    this.params = this.url.match(/(?:state=)([a-zA-Z0-9]+)(?:&code=)([0-9]+)/);
  }

  getBotId() {
    return this.$http.get(`${this.StoreService.getApiUrl()}/yandex/confirm?code=${this.params[2]}&state=${this.params[1]}`);
  }
}
