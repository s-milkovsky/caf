import ng from 'angular';
import ngRouter from 'angular-ui-router';

import Config from './config';
import BotsComponent from './component';

import BotsCreate from './create';

import BotsSpinner from './spinner';

export default ng.module('app.components.bots', [ngRouter, BotsCreate, BotsSpinner])
  .directive('bots', BotsComponent)
  .config(Config)
  .name;
