import angular from 'angular';
import Component from './component.js';
import AnalyticsService from './service.js';

export default angular
  .module('app.common.analytics', []).directive('analytics', Component)
  .factory('AnalyticsService', AnalyticsService)
  .name;
