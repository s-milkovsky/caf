export default ($http, StoreService) => {
  'ngInject';

  return {
    sendAppMetrikaEvent(eventname, data) {
      return $http({
        url: `${ StoreService.getApiUrl() }/event/${ eventname }`,
        method: 'post',
        data: data
      }).then(res => {
          return res.data.result;
        });
    }
  };
};
