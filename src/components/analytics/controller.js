export class analyticsController {
  constructor($scope, $rootScope, $window, $timeout) {
    'ngInject';
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$window = $window;
    this.$timeout = $timeout;
    this.listeners = [];

    this.watchCounts = {
      ya: 0,
      ga: 0
    };


    ((i,s,o,g,r,a,m) => {i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js', 'ga');


    ((d, w, c) => {
      (w[c] = w[c] || []).push(function() {
        try {
          w.yaCounter38620685 = new Ya.Metrika({ id:38620685, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true });
        } catch(e) { }
      });

      var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); };

      s.type = "text/javascript";
      s.async = true;
      s.src = "https://mc.yandex.ru/metrika/watch.js";
      if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
      }

    )(document, window, "yandex_metrika_callbacks");

    this.$window.ga('create', 'UA-73899221-1', 'auto');
  }

  $onInit() {
    this.listeners.push(this.$scope.$on('$stateChangeSuccess', () => {
      this.$timeout(() => {
        this.$window.ga('send', 'pageview', location.pathname + location.hash);
      });
    }));

    this.listeners.push(this.$rootScope.$on('$statEvent', (e, statevent) => {

      if (statevent.event === 'hit') {
        this.hitMetrika(statevent.data);
        return;
      }


      const event = {
        hitType: 'event',
        eventCategory: statevent.event,
        eventAction: 'dashboard'
      };

      if (statevent.data) {
        event.eventLabel = statevent.data;
      }

      const allowYaMetricaEvents = ['newuser', 'fbloginok', 'createfacebookchatbot', 'authorized', 'fbloginerror', 'pagesloaded', 'pagesaccessreqbuttonshow', 'connectedpage', 'pagesaccessreqbuttonstatus', 'requestpagesaccess', 'createtelegramchatbot', 'disconnectedpage', 'createnow', 'keepmeposted', 'othermessengerslink', 'getstarted_frompopup', 'grantpagesaccess'];

      if (allowYaMetricaEvents.indexOf(statevent.event) !== -1) {
        this.sendMetrika(statevent);
      }

      this.sengGA(event);
    }));

  }

  sengGA(data) {
    if (typeof this.$window.ga === 'function') {
      this.$window.ga('send', data);
    } else {
      if (this.watchCounts.ga < 200) {
        this.watchCounts.ga ++;
        this.$timeout(() => {
          this.sengGA(data);
        }, 100);
      }
    }
  }

  sendMetrika(data) {
    if (this.$window.yaCounter38620685 && (typeof this.$window.yaCounter38620685.reachGoal === 'function')) {
      this.$window.yaCounter38620685.reachGoal(data.event, data.data);
      if (data.event === 'authorized') {
      //  console.log('YaM: authorized');
      }
    } else {
      if (this.watchCounts.ya < 200) {
        this.watchCounts.ya++;
        this.$timeout(() => {
          this.sendMetrika(data);
        }, 100);
      }
    }
  }

  hitMetrika(url) {
    if (this.$window.yaCounter38620685 && (typeof this.$window.yaCounter38620685.hit === 'function')) {
      this.$window.yaCounter38620685.hit(url);
    } else {
      if (this.watchCounts.ya < 200) {
        this.watchCounts.ya++;
        this.$timeout(() => {
          this.hitMetrika(url);
        }, 100);
      }
    }
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }
}
