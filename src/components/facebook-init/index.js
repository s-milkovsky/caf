/* global DEBUG, APP_ID */

import angular from 'angular';
import Component from './component.js';

import 'ng-facebook';

export default angular
  .module('app.common.facebookInit', ['ngFacebook']).directive('facebookInit', Component)
  .config(($facebookProvider) => {
    'ngInject';
    let appId = '';

    if (window.cfConfig && window.cfConfig.APP_ID) {
      appId = window.cfConfig.APP_ID;
    } else
    if (APP_ID) {
      appId = APP_ID;
    } else {
      if (DEBUG) {
        appId = '243378952670438';
      } else {
        appId = '1678638095724206';
      }
    }


    $facebookProvider.setCustomInit({ appId, xfbml: true, status: true, oauth: true, version: 'v2.6' });
  }).name;
