export class FacebookInitController {
  constructor($scope, $rootScope, $window, StoreService) {
    'ngInject';
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$window = $window;
    this.StoreService = StoreService;
    this.listeners = [];

  }

  $onInit() {
    this.listeners.push(this.$rootScope.$on('fb.auth.statusChange', () => {
      this.testLoginStatus();
    }));

    this.listeners.push(this.$rootScope.$on('fb.load', () => {
      this.testLoginStatus(true);
    }));

    this.listeners.push(this.$scope.$on('$stateChangeSuccess', () => {
      this.testLoginStatus();
    }));

    ((d, s, id) => {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id))
      {return;}
      js = d.createElement(s);
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');


    this.$window.fbDebug = () => {
      console.log('');
      console.log('Facebook button debug:');

      const bp = document.querySelector('.fb-button-place');

      if (bp) {
        console.log('Button code size: ', bp.innerHTML.length);

        console.log('');
        console.log('Button params:');

        if (bp.firstChild && bp.firstChild.attributes) {

          const attributes = bp.firstChild.attributes;

          for (let ind in attributes) {
            if (typeof attributes[ind].nodeValue !== 'undefined') {
              console.log(attributes[ind].nodeName + ': ' + attributes[ind].nodeValue);
            }
          }

          console.log('');
          console.log('FB aut status:');


          this.$window.FB.getLoginStatus((response) => {
            console.log('Status: ' + response.status);
            console.log('userID: ' + response.authResponse.userID);
          });
        } else {
          console.log('Button code dont loaded');
        }

      } else {
          console.log('Button not found');
      }


    }
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  testLoginStatus(sendEvent) {
    if (this.$window.FB) {
      this.$window.FB.getLoginStatus((response) => {
        if (response.status !== 'connected') {
          this.$rootScope.$emit('$statEvent', { event: 'fbloginerror', data: response.status });
          this.StoreService.setAutorizedCookie(false);
          localStorage.removeItem('token');
          let getParams = false;
          try {
            getParams = localStorage.getItem('get-params');
          } catch (err) {}
          this.$window.location = this.StoreService.getApiUrl().replace(/\/+$/, '') + '/auth' + (getParams ? getParams : '');
        } else {
          if (sendEvent) {
            this.$rootScope.$emit('$statEvent', { event: 'fbloginok' });
          }
        }
      }, true);
    }
  }
}
