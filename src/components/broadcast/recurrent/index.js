import ng from 'angular';
import component from './component';


export default ng.module('app.broadcast.recurrent', [])
  .directive('broadcastRecurrent', component)
  // .config($provide => {
  //   'ngInject';
  // })
  .filter('filterRBlockName', () => text => ((text.split('|'))[0]))
  .name;
