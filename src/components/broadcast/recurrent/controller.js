import moment from 'moment';
import _ from 'lodash';

export default class BroadcastRecurrentController {
  constructor($scope, $rootScope, $state, $stateParams, BotService, BlockService, BroadcastService) {
    'ngInject';
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
    this.BroadcastService = BroadcastService;
    this.BlockService = BlockService;
    this.BotService = BotService;


    this.listeners = [];
    this.schedules = [];


    this.listeners.push(
      $rootScope.$on('broadcast:remove', (event, id) => {
        this.deleteSchedule(id);
      })
    );

    this.listeners.push(
      $scope.$on('$pluginHasErrorValidation', (e, data) => {
        // this.updateValidateStatus(data.blockId);
      })
    );

    this.listeners.push(
      $scope.$on('$pluginUpdated', (e, data, cards) => {
        this.updateValidateStatus(data.blockId, cards);
      })
    );

    this.listeners.push(
      $scope.$on('$recurrentSchedulesLoaded', (event, id) => {
        this.schedules.forEach(item => {
          this.BlockService.show(item.block_id).then(data => {
            item.is_valid = data.is_valid;
          });
        });
      })
    );

    this.listeners.push(
      $rootScope.$on('broadcast:update', (event, id, updated) => {
        let broadcast = this.schedules.find(item => item.id === id);

        if (broadcast) {
          broadcast.block_title = updated.block_title;
          broadcast.time = updated.time;
          broadcast.use_bot_timezone = updated.use_bot_timezone;
          broadcast.repeat_days = updated.repeat_days;
        }
      })
    );

    this.loadBot();
    this.loadSchedules();
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  updateValidateStatus(blockId, cards) {
    if (!blockId) {
      blockId = this.$state.params.id;
    }
    const bc = this.schedules.find(item => item.block_id === blockId);
    if (bc) {
      let isValid = cards.every(item => {
        return item.is_valid === true;
      });
      bc.is_valid = isValid;
    }
  }

  loadSchedules() {
    this.BroadcastService.listRecurrent().then(rows => {
      rows.forEach(row => {
        if (this.schedules.find(item => item.id === row.id)) {
           return;
        }

        this.schedules.push(row);
      });

      this.$rootScope.$broadcast('$recurrentSchedulesLoaded', true);
    });
  }



  loadBot() {
    return this.BotService.show()
      .then(bots => {
        this.bot = bots;
        return this.bot;
      });
  }

  addSchedule() {
    this.loadBot()
      .then(bot => {

        const date = moment.utc({ year: 1970, month: 0, day: 0, hour: 13 });

        const broadcasts = this.$getBroadcasts()
        let title;
        let time;


        while (true) {
          title = 'Everyday ' + date.format('h:mm a')
          time = date.format('HH') * 3600 + date.format('mm') * 60;

          if (!broadcasts.find(item => (item.block_title.split('|'))[0] === title)) {
            break;
          }

          date.add(30, 'm');
        }

        const def = {
          time: time,
          repeat_days: [true, true, true, true, true, true, true],
          use_bot_timezone: false
        };

        return this.BlockService.createBroadcastBlock()
          .then(block => {
            block.title = title + '|' + Math.random();

            return this.BlockService.update(block);
          })
          .then(block => {
            return this.BlockService.broadcastRecurrent(block.id, def)
              .then(broadcastId => ({ block, broadcastId }));
          })
          .then(({ block, broadcastId }) => {

            let broadcast = _.clone(def);

            broadcast.id = broadcastId;
            broadcast.block_id = block.id;
            broadcast.block_title = block.title;

            this.schedules.push(broadcast);

            this.$state.go('app.broadcast.block', { id: block.id, broadcast_id: broadcast.id, type: 'recurrent' });
          });
      });
  }


  $getBroadcasts() {
    return _.sortBy(this.schedules, 'time');
  }

  deleteSchedule(id) {
    let broadcast = this.schedules.find(item => item.id === id);

    if (!broadcast) {
      return;
    }

    this.BroadcastService.removeRecurrent(broadcast.id)
      .then(() => {
        this.BlockService.remove(broadcast.block_id);
      })
      .then(() => {

        let i = this.schedules.findIndex(item => item.id === id);

        if (i !== -1) {
          this.schedules.splice(i, 1);
        }

        this.$state.go('app.broadcast');
      });
  }
}
