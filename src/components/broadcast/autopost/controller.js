import _ from 'lodash';

export default class BroadcastAutopostController {
  constructor($scope, $rootScope, $state, $stateParams, BotService, BlockService, BroadcastService) {
    'ngInject';
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
    this.BroadcastService = BroadcastService;
    this.BlockService = BlockService;
    this.BotService = BotService;

    this.listeners = [];
    this.autoposts = [];


    this.listeners.push(
      $rootScope.$on('broadcast:remove', (event, id) => {
        this.deleteSchedule(id);
      })
    );

    this.listeners.push(
      $scope.$on('$pluginHasErrorValidation', (e, data) => {
        // this.updateValidateStatus(data.blockId);
      })
    );

    this.listeners.push(
      $scope.$on('$pluginUpdated', (e, data, cards) => {
        this.updateValidateStatus(data.blockId, cards);
      })
    );
    this.listeners.push(
      $rootScope.$on('autopost:title', (event, data) => {
        if (!data.length) return;
        let card = data.find((item) => {
          if (item.plugin_id.indexOf('autoposting') !== -1) return item;
        });
        if (card) this.updateTitle(card);
      })
    );
    this.listeners.push(
      $scope.$on('$recurrentSchedulesLoaded', (event, id) => {
        this.autoposts.forEach(item => {
          this.BlockService.show(item.id).then(data => {
            item.is_valid = data.is_valid;
          });
        });
      })
    );
    this.listeners.push(
      $scope.$on('block:update', (event, id, title) => {
        this.autoposts.forEach(block => {
          if (block.id === id) {
            block.title = title;
          }
        });
      })
    );

    this.loadBot();
    this.loadSchedules();
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }
  updateTitle(card) {
    this.BlockService.show(this.$state.params.id)
    .then((res) => {
      let title = card.plugin_id.slice(12).slice(0, -7).replace('_', ' ');
      let titleSplited = title.split(' ');
      if (titleSplited.length > 1) {
        title = titleSplited[0].toUpperCase() + ' ' + titleSplited[1].charAt(0).toUpperCase() + titleSplited[1].slice(1);
      } else {
        title = (title === 'ifttt') ? title = title.toUpperCase() : title.charAt(0).toUpperCase() + title.slice(1);
      }
      if (res.title !== title) {
        res.title = title;
        const broadcasts = this.$getBroadcasts();

        let i = 1;
        while(true) {
          res.title = `${title} ${i++}`;
          if (!broadcasts.find(item => item.title === res.title)) {
            break;
          }
        }
        this.BlockService.update(res)
        .then((block) => {
          this.$rootScope.$broadcast('block:update', block.id, block.title);
        });
      }
    });
  }
  updateValidateStatus(blockId, cards) {
    if (!blockId) {
      blockId = this.$state.params.id;
    }
    const bc = this.autoposts.find(item => item.id === blockId);
    if (bc) {
      let isValid = cards.every(item => {
        return item.is_valid === true;
      });
      bc.is_valid = isValid;
    }
  }

  loadSchedules() {
    this.BroadcastService.listAutopost().then(rows => {
      rows.forEach(row => {
        if (this.autoposts.find(item => item.id === row.id)) {
           return;
        }

        this.autoposts.push(row);
      });

      this.$rootScope.$broadcast('$recurrentSchedulesLoaded', true);
    });
  }



  loadBot() {
    return this.BotService.show()
      .then(bots => {
        this.bot = bots;
        return this.bot;
      });
  }

  addAutopost() {
    this.loadBot()
      .then(bot => {
        let broadcasts = this.$getBroadcasts();
        let title;
        let i = 1;

        while (true) {
          title = `Autoposting ${i++}`;
          if (!broadcasts.find(item => item.title === title)) {
            break;
          }
        }
        let def = {"title": title};
        return this.BlockService.broadcastAutopost(def)
          .then(block => {
            let broadcast = _.clone(def);

            broadcast.id = block.id;
            broadcast.title = block.title;

            this.autoposts.push(broadcast);
            this.$state.go('app.broadcast.auto', { id: block.id, type: 'auto' });
          });
      });
  }


  $getBroadcasts() {
    return this.autoposts;
  }

  deleteSchedule(id) {
    let broadcast = this.autoposts.find(item => item.id === id);

    if (!broadcast) {
      return;
    }

    this.BlockService.remove(broadcast.id)
    .then(() => {

      let i = this.autoposts.findIndex(item => item.id === id);

      if (i !== -1) {
        this.autoposts.splice(i, 1);
      }

      this.$state.go('app.broadcast');
    });
  }
}
