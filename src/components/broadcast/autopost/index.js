import ng from 'angular';
import component from './component';


export default ng.module('app.broadcast.autopost', [])
  .directive('broadcastAutopost', component)
  .name;
