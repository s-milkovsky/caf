import controller from './controller';
import template from './broadcast.autopost.template.html';

export default () => ({
  scope: true,
  template,
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});
