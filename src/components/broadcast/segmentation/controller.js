

export default class SegmentationController {
  constructor($scope, $timeout, BotService, ModalService, BlockService) {
    'ngInject';

    this.$scope = $scope;
    this.$timeout = $timeout;
    this.BotService = BotService;
    this.ModalService = ModalService;
    this.BlockServise = BlockService;

    this.userFilteredCountText = 'Users';
    this.userTotalCountText = 'Users';

    this.listeners = [];

    // this.listeners.push(
    //   $rootScope.$on('broadcast:update', (event, id, updated) => {
    //   })
    // );

    this.ready = true;

    const unbindWatcher = $scope.$watch('vm.block.id', () => {
      if (this.block.id) {
        if (this.block.user_filter && this.block.user_filter.parameters && this.block.user_filter.parameters.length > 0) {
          this.ready = false;
          this.updateUserCount();
        } else {
          this.block.user_filter = { parameters: [] };
        }
        unbindWatcher();
      }
    });

    BotService.parameterStats().then(res => {
      this.parameterStats = res;
      this.userTotalCountText = this.compileUserText(res.total_users);

      const parametersTotalCounts = {};
      let localCount;

      for (const key in res.parameters) {
        localCount = 0;
        for (const subkey in res.parameters[key]) {
          if (subkey === 'NOT SET') {
            continue;
          }
          localCount += res.parameters[key][subkey];
        }
        parametersTotalCounts[key] = localCount;
      }

      this.parameterStats.parametersTotalCounts = parametersTotalCounts;

      if (!this.userFilteredCount) {
        this.userFilteredCount = res.total_users;
      }
    });

  }

  $onInit() {


  }

  $onDestroy() {
  //  this.listeners.forEach(fn => fn.call());
  }

  updateUserCount() {
    if (this.block && this.block.id) {
      this.BotService.userCount(this.block.user_filter).then(res => {
        this.userFilteredCount = Number(res);
        this.userFilteredCountText = this.compileUserText(res);
        this.ready = true;
      });
    }
  }

  compileUserText(n) {
    return String(n).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,') + ' ' + (Number(n) === 1 ? 'user' : 'users');
  }

  removeLast() {
    this.block.user_filter.parameters.pop();
    this.saveBlock();
  }

  openPopup(openHelp, index) {

    this.ModalService.userFilter(this.block.user_filter, this.userFilteredCount, openHelp, index).then(ok => {

      this.$timeout(() => {
        if (this.block.user_filter.parameters && this.block.user_filter.parameters.length > 0) {
          const res = [];

          this.block.user_filter.parameters.forEach(item => {
            if (!(item.name === '' || item.values.length === 0)) {
              res.push(item);
            }
          });

          this.block.user_filter.parameters = res;
        }

        this.saveBlock();
      });
    }, 100);
  }

  saveBlock() {
    this.BlockServise.update(this.block).then(() => {
      this.updateUserCount();
    });
  }

}
