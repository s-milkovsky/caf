import controller from './controller';
import template from './segmentation.html';

export default () => ({
  scope: {
    block: '='
  },
  template,
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true,
  bindToController: true,
  transclude: true
});
