import ng from 'angular';
import component from './component';
import './assets/less/segmentation.less';


export default ng.module('app.broadcast.segmentation', [])
  .directive('segmentation', component)
  .name;
