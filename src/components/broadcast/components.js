import contentTemplate from './templates/broadcast.content.html';
import sidebarTemplate from './templates/broadcast.sidebar.html';
import nullTemplate from './templates/broadcast.null.html';
import sentTemplate from './templates/broadcast.sent.html';

import SidebarController from './sidebar.controller';
import ContentController from './content.controller';
import NullController from './null.controller';

export const ContentComponent = () => ({
  template: contentTemplate,
  controller: ContentController,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});

export const NullComponent = () => ({
  template: nullTemplate,
  controller: NullController,
  controllerAs: 'vm',
  restrict: 'E',
  replace: false
});

export const SentComponent = () => ({
  template: sentTemplate,
  restrict: 'E',
  replace: false
});

export const SidebarComponent = () => ({
  template: sidebarTemplate,
  controller: SidebarController,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});
