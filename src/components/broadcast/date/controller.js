import _ from 'lodash';
import moment from 'moment';

export default class BroadcastDateController {
  constructor($rootScope, $scope, $state, $stateParams, $filter, BotService, BlockService, BroadcastService, API_ERROR_MSG) {
    'ngInject';
    this.$state = $state;
    this.$filter = $filter;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
    this.BroadcastService = BroadcastService;
    this.BlockService = BlockService;
    this.BotService = BotService;
    this.API_ERROR_MSG = API_ERROR_MSG;

    this.date = new Date();
    if (this.$stateParams.date) {
      this.date = this.$stateParams.date;
    }

    this.listeners = [];
    this.schedules = [];
    let startOfDay = +moment().startOf('day');
    this.$datepicker = {
      datepickerMode: 'day',
      showWeeks: false,
      maxMode: 'day',
      customClass(dt) {
        if (dt.date < startOfDay) {
          return 'prev-day';
        }
      }
    };

    this.listeners.push(
      $rootScope.$on('broadcast:remove', (event, id) => {
        this.deleteSchedule(id);
      })
    );

    this.listeners.push(
      $scope.$on('$schedulesLoaded', (event, id) => {
        this.schedules.forEach(item => {
          this.BlockService.show(item.block_id).then(data => {
            item.is_valid = data.is_valid;
          });
        });
      })
    );


    this.listeners.push(
      $scope.$on('$pluginHasErrorValidation', (e, data) => {
        // this.updateValidateStatus(data.blockId);
      })
    );

    this.listeners.push(
      $scope.$on('$pluginUpdated', (e, data, cards) => {
        this.updateValidateStatus(data.blockId, cards);
      })
    );

    this.listeners.push(
      $rootScope.$on('broadcast:update', (event, id, updated) => {
        let broadcast = this.schedules.find(item => item.id === id);

        if (broadcast) {
          broadcast.block_title = updated.block_title;
          broadcast.deadline = updated.deadline;
          broadcast.use_bot_timezone = updated.use_bot_timezone;
        }
      })
    );

    this.loadSchedules(this.date);
    this.loadBot();
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  updateValidateStatus(blockId, cards) {
    if (!blockId) {
      blockId = this.$state.params.id;
    }
    const bc = this.schedules.find(item => item.block_id === blockId);
    if (bc) {
      let isValid = cards.every(item => {
        return item.is_valid === true;
      });
      bc.is_valid = isValid;
    }
  }

  changeDate() {
    let date = this.$filter('date')(this.date, 'yyyy-MM-dd')
      , notify = false;

    if (this.$state.is('app.broadcast.block') || this.$state.is('app.broadcast.auto')) {
      notify = true;
    }

    this.$state.go('app.broadcast', { date }, { notify });

    const lCol = document.querySelector('.lcol');
    lCol.scrollTop = lCol.scrollHeight;
  }

  loadBot() {
    return this.BotService.show()
      .then(bots => {
        this.bot = bots;
        return this.bot;
      });
  }

  loadSchedules(date) {
    let from = moment.utc({ year: date.getFullYear(), month: date.getMonth(), day: date.getDate() }).startOf('month')
      , to = moment.utc({ year: date.getFullYear(), month: date.getMonth(), day: date.getDate() }).endOf('month');

    return this.BroadcastService.list(+from, +to)
      .then(rows => {
        rows.forEach(row => {
          if (this.schedules.find(item => item.id === row.id)) {
            return;
          }
          this.schedules.push(row);
        });

        this.$rootScope.$broadcast('$schedulesLoaded', true);
      });
  }

  addSchedule() {
    this.loadBot()
      .then(bot => {
        let broadcasts = this.$getBroadcasts()
          , date = moment.utc({ year: this.date.getFullYear(), month: this.date.getMonth(), day: this.date.getDate(), hour: 12 })
          , now = moment.utc(moment.utc() + bot.timezone_offset * 1000)
          , title;

        if (this.date.getFullYear() === now.year()
          && this.date.getMonth() === now.month()
          && this.date.getDate() === now.date()
          && now.hours() >= 12
        ) {
          if (now.minutes() > 30) {
            date.hours(now.hours() + 1);
          } else {
            date.hours(now.hours());
            date.minutes(30);
          }
        }

        while (true) {
          title = date.format('MMM Do h:mm a');

          if (!broadcasts.find(item => item.block_title === title)) {
            break;
          }

          // add half hour and try again
          date.add(30, 'm');
        }

        return this.BlockService.createBroadcastBlock()
          .then(block => {
            block.title = title;

            return this.BlockService.update(block);
          })
          .then(block => {
            return this.BlockService.broadcast(block.id, +date)
              .then(broadcast => ({ block, broadcast }));
          })
          .then(({ block, broadcast }) => {
            broadcast.block_id = block.id;
            broadcast.block_title = block.title;
            broadcast.deadline = +date;
            broadcast.use_bot_timezone = true;

            this.schedules.push(broadcast);
            this.$state.go('app.broadcast.block', { id: block.id, broadcast_id: broadcast.id, type: 'schedule' });
          });
      })
      .catch(err => {
    //    alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });
  }

  deleteSchedule(id) {
    let broadcast = this.schedules.find(item => item.id === id);

    if (!broadcast) {
      return;
    }

    this.BlockService.remove(broadcast.block_id)
      .then(() => {
        return this.BroadcastService.remove(broadcast.id);
      })
      .then(() => {
        let i = this.schedules.findIndex(item => item.id === id);

        if (i !== -1) {
          this.schedules.splice(i, 1);
        }

        this.$state.go('app.broadcast');
      })
      .catch(err => {
    //    alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });
  }

  $getBroadcasts() {
    return _.sortBy(this._filterBroadcasts(this.date), 'deadline');
  }

  $countSchedules(date) {
    return this._filterBroadcasts(date).length;
  }

  _filterBroadcasts(date) {
    let from = moment.utc({ year: date.getFullYear(), month: date.getMonth(), day: date.getDate(), hour: 0, minutes: 0, seconds: 0, milliseconds: 0 })
      , to = moment.utc({ year: date.getFullYear(), month: date.getMonth(), day: date.getDate(), hour: 24, minutes: 0, seconds: 0, milliseconds: 0 });

    return this.schedules.filter(item => item.deadline >= from && item.deadline < to);
  }

  $getTimeZone() {
    if (!this.bot) {
      return '';
    }
    if (this.bot.timezone_name.toLowerCase() !== 'unknown') {
      return this.bot.timezone_name;
    }
    return 'UTC+' + this.bot.timezone_offset / 3600;
  }

  $allowAddSchedule() {
    return this.date >= moment().startOf('day');
  }
}
