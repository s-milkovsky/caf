import ng from 'angular';
import component from './component';
import datepicker from 'angular-ui-bootstrap/src/datepicker';

import dayPickerTemplateUrl from './daypicker.tpl';

export default ng.module('app.broadcast.date', [datepicker])
  .directive('broadcastDate', component)
  .config($provide => {
    'ngInject';

    $provide.decorator('uibDaypickerDirective', function($delegate) {
      let directive = $delegate[0];
      directive.templateUrl = dayPickerTemplateUrl;

      return $delegate;
    });
  })
  .name;
