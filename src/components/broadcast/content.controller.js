import moment from 'moment';
import StructureContentController from '../structure/content.controller';

export default class ContentController extends StructureContentController {
  $onInit() {
    super.$onInit();

    this.$allowEdit = true;
    if (this.$stateParams.type === 'schedule') {
      this.loadBroadcast();
    } else
    if (this.$stateParams.type === 'recurrent') {
      this.loadRecurrentBroadcast();
    } else
    if (this.$stateParams.type === 'now') {
      this.$allowEdit = !window.localStorage.getItem('auto-' + this.$stateParams.id);
      this.nowCloneAllow = !this.$allowEdit;
    }

    this.days = [
      'Mon',
      'Tue',
      'Wed',
      'Thu',
      'Fri',
      'Sat',
      'Sun',
    ];

    this.daysIndex = [6, 0, 1, 2, 3, 4, 5];
  }

  loadBot() {
    return this.BotService.show()
      .then(bots => {
        this.bot = bots;
        return bots;
      });
  }

  loadBroadcast() {
    this.loadBot()
      .then(bot => {
        return this.BroadcastService.show(this.$stateParams.broadcast_id)
          .then(broadcast => ({ bot, broadcast }))
      })
      .then(({ bot, broadcast }) => {
        // botDeadline - time in bot timezone
        // deadline - time w/o bot time zone
        this.broadcast = broadcast;
        this.broadcast.botDeadline = this.broadcast.deadline + bot.timezone_offset * 1000;

        this.$allowEdit = this.broadcast.deadline >= moment.utc().startOf('day');
        this.times = this.getTimes(
          moment.utc(this.broadcast.deadline).startOf('day'),
          moment.utc(this.broadcast.botDeadline).startOf('day')
        );
      });
  }

  loadRecurrentBroadcast() {
    this.loadBot()
      .then(bot => {
        return this.BroadcastService.showRecurrent(this.$stateParams.broadcast_id)
          .then(broadcast => ({ bot, broadcast }));
      })
      .then(({ bot, broadcast }) => {
        this.broadcast = broadcast;

        this.$allowEdit = true;

        this.times = this.getTimes(
          moment.utc({ year: 1970, month: 0, day: 1, hour: 0 }),
          moment.utc({ year: 1970, month: 0, day: 1, hour: 0 })
        );
      });
  }



  // @todo: move generation title to separate method
  changeBroadcastDeadline() {
    this.$deadlineErrorShow = false;

    this.loadBot()
      .then(bot => {
        let now = moment.utc(moment.utc() + bot.timezone_offset * 1000);

        if (!('use_bot_timezone' in this.broadcast)) {
          this.broadcast.use_bot_timezone = true;
        }

        this.broadcast.botDeadline = this.broadcast.deadline + bot.timezone_offset * 1000;
        this.block.title = moment.utc(this.broadcast.deadline).format('MMM Do h:mm a');

        if (this.broadcast.deadline < now) {
          throw Error('You can not choose this time');
        }

        return this.BlockService.update({ id: this.block.id, title: this.block.title, user_filter: this.block.user_filter });
      })
      .then(() => {
        return this.BroadcastService.update(this.broadcast.id, { deadline: this.broadcast.deadline, use_bot_timezone: this.broadcast.use_bot_timezone });
      })
      .then(() => {
        this.$scope.$emit('broadcast:update', this.broadcast.id, {
          deadline: this.broadcast.deadline,
          use_bot_timezone: this.broadcast.use_bot_timezone,
          block_title: this.block.title
        });
      })
      .catch(err => {
        if (err instanceof Error) {
          this.$deadlineError = err.message;
          this.$deadlineErrorShow = true;
        } else if (err.statusText === 'CONFLICT') {
          this.$deadlineError = 'This time already scheduled, please select another one';
          this.$deadlineErrorShow = true;
        } else {
         // alert(JSON.stringify(err.data || this.API_ERROR_MSG));
        }
      });
  }

  list(id) {
    return super.list(id).then(() => {
      if (this.$stateParams.type === 'update' && this.block.cards.length === 0) {
        this.$timeout(() => {
          this.$scope.$broadcast('plugin:add', 'integration');
        });
      }
    });
  }

  /**
   * onDeleteBlock
   * @param  {Object} e
   * @return {undefined}
   */
  onDeleteBlock(e) {
    e.preventDefault();
    this.BlockService.show(this.$state.params.id)
    .then(block => {
      const context = this.$interpolate(this.ModalService.templates.block())({
        block: this.$stateParams.type === 'now' ? 'Send now' : `#${block.title}`
      });
      this.ModalService
        .confirm(context)
        .then(isConfirmed => isConfirmed && this.deleteBlock());
    });
  }

  deleteBlock() {
    switch (this.$stateParams.type) {
      case 'schedule':
      case 'recurrent': {
        this.$scope.$emit('broadcast:remove', this.$stateParams.broadcast_id);
        break;
      }
      case 'now':
      case 'update':
      case 'auto': {
        this.$scope.$emit('block:remove', this.$stateParams.id);
        break;
      }
    }
  }

  sendBroadcastNow() {
    this.BlockService.broadcastNow(this.$stateParams.id)
      .then(() => {
        this.$state.go('app.broadcast.sent');
      })
      .catch(err => {
      //  alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });
  }

  onSaveBlockTitle() {
    this.$scope.$emit('block:update', this.block.id, this.block.title);
  }

  getTimes(startOfDay, botStartOfDay) {
    let times = [];

    let secOfMid = 0;

    for (let i = 0; i < 48; i ++) {
      times.push({
        value: +startOfDay,
        label: botStartOfDay.format('h:mm a'),
        sec: secOfMid
      });

      startOfDay.add(30, 'm');
      botStartOfDay.add(30, 'm');
      secOfMid += 1800;
    }

    return times;
  }

  showPreferences() {
    if (this.broadcast) {
      if (!('use_bot_timezone' in this.broadcast)) {
        this.broadcast.use_bot_timezone = true;
      }

      this.ModalService.broadcastPreference(this.broadcast.use_bot_timezone).then(res => {
        if (res) {
          this.broadcast.use_bot_timezone = res.useBotTimezone;

          if (this.$stateParams.type === 'schedule') {
            this.BroadcastService.update(this.broadcast.id, {
              use_bot_timezone: this.broadcast.use_bot_timezone,
              deadline: this.broadcast.deadline
            }).then(() => {
              this.$scope.$emit('broadcast:update', this.broadcast.id, {
                deadline: this.broadcast.deadline,
                use_bot_timezone: this.broadcast.use_bot_timezone,
                block_title: this.block.title
              });
            });
          } else
          if (this.$stateParams.type === 'recurrent') {
            this.BroadcastService.updateRecurrent(this.broadcast.id, this.broadcast).then(() => {
              this.$scope.$emit('broadcast:update', this.broadcast.id, {
                time: this.broadcast.time,
                use_bot_timezone: this.broadcast.use_bot_timezone,
                block_title: this.block.title
              });
            });
          }



        }
      });
    }
  }

  changeRecurrentEvent() {
    const titles = {
      0: 'Never',
      127: 'Everyday',
      3: 'Weekend',
      124: 'Workdays',
      43: 'Every other day',
      84: 'Every other day'
    };

    const bitMask = parseInt(this.broadcast.repeat_days.map(item => (item ? 1 : 0)).join(''), 2);

    let title = '';

    if (titles[bitMask]) {
      title += titles[bitMask];
    } else {
      let dArr = [];
      this.daysIndex.forEach(item => {
        if (this.broadcast.repeat_days[item]) {
          dArr.push(this.days[item]);
        }
      });

      if (dArr.length > 5) {
         dArr = dArr.slice(0, 5);
         title += dArr.join(', ') + '...';
      } else {
        title += dArr.join(', ');
      }

    }

    title += ' ' + this.times.find(item => item.sec === this.broadcast.time).label;

    this.block.title = title + '|' + Math.random();


    this.BroadcastService.updateRecurrent(this.broadcast.id, this.broadcast).then(res => {
      return this.BlockService.update({ id: this.block.id, title: this.block.title, user_filter: this.block.user_filter  });
    }).then(res => {
      const q = _.clone(this.broadcast);
      q.block_title = this.block.title;

      this.$scope.$emit('broadcast:update', this.broadcast.id, q);
    });
  }

  resend() {
    const goto = bl => {
      this.BlockService.update(bl).then(() => {
        this.$state.go('app.broadcast.block', { id: bl.id, broadcast_id: 0, type: 'now' });
      });
    };

    const targetBlock = this.block;

    this.BlockService.createBroadcastNowBlock()
      .then(newBlock => {
        if (newBlock.cards && newBlock.cards.length > 0) {
          newBlock.cards.forEach(card => this.PluginCardService.remove(card));
        }

        newBlock.cards = targetBlock.cards;
        newBlock.user_filter = targetBlock.user_filter;

        let saveCardCount = 0;
        targetBlock.cards.forEach(elem => {
          elem.blockId = newBlock.id;
          delete elem.id;
          saveCardCount ++;
          this.PluginCardService.save(elem).then(() => {
            saveCardCount --;
            if (!saveCardCount) {
              goto(newBlock);
            }
          }, () => {
            saveCardCount --;
            if (!saveCardCount) {
              goto(newBlock);
            }
          });
        });

        if (saveCardCount === 0) {
          goto(newBlock);
        }
      });

  }
  $getTimeZone() {
    if (!this.bot) {
      return '';
    }
    return (this.bot.timezone_offset >= 0) ? 'UTC+' + this.bot.timezone_offset / 3600 : 'UTC' + this.bot.timezone_offset / 3600;
  }
}
