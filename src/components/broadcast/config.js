export const Config = ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('app.broadcast', {
      url: '/broadcast?{date:date}',
      views: {
        'sidebar@': {
          template: '<broadcast-sidebar></broadcast-sidebar>'
        },
        'content@': {
          template: '<broadcast-null></broadcast-null>'
        }
      }
    })
    .state('app.broadcast.sent', {
      url: '/sent',
      views: {
        'content@': {
          template: '<broadcast-sent></broadcast-sent>'
        }
      }
    })
    .state('app.broadcast.block', {
      resolve: {
        groups(GroupService, StoreService, $stateParams) {
          'ngInject';

          return GroupService.list($stateParams.bot_id).then(data => {
            StoreService.set('groups', data);
            return data;
          });
        }
      },
      url: '/:id/:broadcast_id/:type?new',
      views: {
        'content@': {
          template: '<broadcast-content></broadcast-content>'
        }
      }
    })
    .state('app.broadcast.auto', {
      resolve: {
        groups(GroupService, StoreService, $stateParams) {
          'ngInject';

          return GroupService.list($stateParams.bot_id).then(data => {
            StoreService.set('groups', data);
            return data;
          });
        }
      },
      url: '/:id/:type',
      views: {
        'content@': {
          template: '<broadcast-content></broadcast-content>'
        }
      }
    });
};
