import ng from 'angular';
import ngRouter from 'angular-ui-router';
import ngEditable from 'angular-contenteditable';
import 'angular-ui-sortable';

import { Config } from './config';
import * as components from './components';

import broadcastRecurrent from './recurrent';
import broadcastDate from './date';
import broadcastAutopost from './autopost';
import segmentation from './segmentation';
import history from './history';


export default ng.module('app.broadcast', ['ui.sortable', ngRouter, ngEditable, broadcastRecurrent, broadcastDate, broadcastAutopost, segmentation, history])
  .directive('broadcastContent', components.ContentComponent)
  .directive('broadcastSidebar', components.SidebarComponent)
  .directive('broadcastNull', components.NullComponent)
  .directive('broadcastSent', components.SentComponent)
  .config(Config)
  .name;
