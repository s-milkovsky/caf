
export default class NullController {
  constructor($state, $scope, $stateParams, $timeout) {
    'ngInject';

    this.$scope = $scope;

    this.$scope.showInfo = false;

    this.lCol = document.querySelector('.lcol');
    const broadcastDate = document.querySelector('.broadcast-date');
    const broadcastAuto = document.querySelector('.autopost-group');
    const sendNow = document.querySelector('.send-now');

    const broadcastDateMes = angular.element(document.querySelector('.broadcast-date-mes'));
    const broadcastAutoMes = angular.element(document.querySelector('.broadcast-auto-mes'));
    const sendNowMes = angular.element(document.querySelector('.send-now-mes'));


    const pos = () => {
      if (this.$scope.showInfo) {
        const st = this.lCol.scrollTop - 50;
        broadcastDateMes.css('top', - st + broadcastDate.offsetTop - broadcastDateMes.height() * 0.5);
        broadcastAutoMes.css('top', - st + broadcastAuto.offsetTop - broadcastAutoMes.height() * 0.5);
        sendNowMes.css('top', - st + sendNow.offsetTop - sendNowMes.height() * 0.5);
      }
    };

    $timeout(() => {
      pos();
    });

    angular.element(this.lCol).on('scroll', () => {
      pos();
    });

    this.listeners = [];

    this.listeners.push($scope.$on('$stateChangeSuccess', () => {
      $timeout(() => {
        pos();
      });
    }));

    this.listeners.push($scope.$on('$historyLoaded', (e, val) => {
      this.$scope.showInfo = !val;
      if (!val) {
        pos();
      }
    }));

    this.listeners.push($scope.$on('$recurrentSchedulesLoaded', () => {
      $timeout(() => {
        pos();
      });
    }));
  }

  $onDestroy() {
    angular.element(this.lCol).off('scroll');
    this.listeners.forEach(fn => fn.call());
  }
}
