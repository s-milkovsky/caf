
export default class SidebarController {
  constructor($rootScope, $state, $stateParams, BroadcastService, BlockService, BotService, API_ERROR_MSG) {
    'ngInject';
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.BotService = BotService;
    this.BlockService = BlockService;
    this.BroadcastService = BroadcastService;
    this.API_ERROR_MSG = API_ERROR_MSG;

    this.listeners = [];

    // this.loadSendOnUpdateBlockList();

    this.listeners.push(
      $rootScope.$on('block:remove', (event, id) => {
        this.deleteBlock(id);
      })
    );

    BotService.parameterStats(true);
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  addNowBlock($event) {

    if ($event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();
    }

    this.BlockService.createBroadcastNowBlock()
      .then(block => {
        this.goBlock(block.id, 0, 'now');
      });
  }

  deleteBlock(id) {
    (this.$stateParams.type === 'now' ? this.BlockService.removeBroadcastNowBlock() : this.BlockService.remove(id))
      .then(res => {
        this.$state.go('app.broadcast', {}, { reload: true });
      });
  }

  goBlock(id, broadcast_id, type = 'schedule', editTitle = false) {
    if (editTitle) {
      this.$state.go('app.broadcast.block', { id, broadcast_id, type, new: true });
    } else {
      this.$state.go('app.broadcast.block', { id, broadcast_id, type });
    }
  }
}
