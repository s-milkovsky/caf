import ng from 'angular';
import component from './component';
import './assets/less/history.less';

import { percent, generatePreview, titleCleaner } from './filters';

export default ng.module('app.broadcast.history', [])
  .directive('history', component)
  .filter('percent', percent)
  .filter('generatePreview', generatePreview)
  .filter('titleCleaner', titleCleaner)
  .name;
