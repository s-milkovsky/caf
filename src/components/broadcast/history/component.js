import controller from './controller';
import template from './history.html';

export default () => ({
  template,
  controller,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true,
  transclude: true
});
