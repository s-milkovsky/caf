export function titleCleaner() {
  return val => (val.split('|'))[0];
}

export function percent() {
  return (val, base) => (val ? Math.round(val / base * 100) : '0') + '%';
}

export function generatePreview() {
  const pluginsNames = {
    'text': 'Text card',
    'gallery': 'Gallery plugin',
    'json_plugin': 'JSON API plugin',
    'form': 'User input plugin',
    'communication': 'Live chat plugin',
    'typing': 'Typing… plugin',
    'setup_variable_plugin': 'Set up a variable plugin',
    'search/google_plugin': 'Google site search plugin',
    'search/bing_plugin': 'Bing search plugin',
    'search/swiftype_plugin': 'Swiftype search plugin',
    'search/rss_plugin': 'RSS import plugin',
    'subscribe_user_plugin': 'Subscriber plugin',
    'subscriptions_manager_plugin': 'Subscriptions list plugin',
    'subscription_broadcasting_plugin': 'Digest plugin',
    'integration/ifttt_instagram_plugin': 'Instagram plugin',
    'integration/ifttt_twitter_plugin': 'Twitter plugin',
    'integration/ifttt_youtube_plugin': 'YouTube plugin',
    'integration/zapier_plugin': 'Zapier plugin',
    'integration/zapier_rss_plugin': 'RSS plugin',
    'integration/zapier_youtube_plugin': 'YouTube plugin',
    'integration/zapier_sheets_plugin': 'Google Sheets plugin',
    'integration/zapier_calendar_plugin': 'Google Calendar plugin',
    'integration/zapier_wordpress_plugin': 'WordPress plugin',
    'integration/zapier_slack_plugin': 'Slack plugin',
    'integration/zapier_twitter_plugin': 'Twitter plugin',
    'integration/zapier_instagram_plugin': 'Instagram plugin',
    'integration/zapier_vimeo_plugin': 'Vimeo plugin',
     'autoposting/ifttt_plugin': 'IFTTT plugin',
     'autoposting/ifttt_instagram_plugin': 'Instagram plugin',
     'autoposting/ifttt_twitter_plugin': 'Twitter plugin',
     'autoposting/ifttt_youtube_plugin': 'Youtube plugin',
     'autoposting/zapier_plugin': 'Zapier plugin',
     'autoposting/zapier_rss_plugin': 'RSS plugin',
     'autoposting/zapier_youtube_plugin': 'YouTube plugin',
     'autoposting/zapier_sheets_plugin': 'Google Sheets plugin',
     'autoposting/zapier_calendar_plugin': 'Google Calendar plugin',
     'autoposting/zapier_wordpress_plugin': 'WordPress plugin',
     'autoposting/zapier_slack_plugin': 'Slack plugin',
     'autoposting/zapier_twitter_plugin': 'Twitter plugin',
     'autoposting/zapier_instagram_plugin': 'Instagram plugin',
     'autoposting/zapier_vimeo_plugin': 'Vimeo plugin'
  };

  return obj => {
    if (!obj) {
      return '';
    }
    const aCount = new Map([...new Set(obj.plugins)].map(
      x => [x, obj.plugins.filter(y => y === x).length]
    ));
    let out = '';
    aCount.forEach((cou, pId) => {
      out += cou + ' ' + pluginsNames[pId] + (cou > 1 ? 's' : '') + ' &nbsp; ';
    });
    return out;
  };
}
