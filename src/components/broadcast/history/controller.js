

export default class HistoryController {
  constructor($scope, $rootScope, $timeout, $element, $state, BotService, BlockService, BroadcastService, PluginCardService, ModalService) {
    'ngInject';

    this.$scope = $scope;
    this.$timeout = $timeout;
    this.$element = $element;
    this.$state = $state;
    this.BotService = BotService;
    this.BlockService = BlockService;
    this.BroadcastService = BroadcastService;
    this.PluginCardService = PluginCardService;
    this.ModalService = ModalService;

    this.historyArray = [];
    this.chunkHistoryAviable = true;
    this.getHistoryChunk();

    this.ttText = 'When you send a broadcast, the bot attempts delivery to all users based on the User Filter you apply. You can see the corresponding number of addressees in the “Attempts” column.<br/><br/>Some users may have deleted or blocked the bot prior to the broadcast attempt. Such instances only register when you try to send a broadcast. Those users are shown in the “Bounced” column.<br/><br/>Users who did not delete or block the bot will be show in the “Sent” column. If they read and click on a button in the broadcast message, they will show in the corresponding “Read” and “Clicked” columns.<br/><br/>Note:<br/>You can clone Send Now broadcasts.<br/>Data is refreshed every 5 minutes.<br/>We track clicks on buttons that lead to bot blocks only (not URLs).';

    this.listener = $rootScope.$on('$bcClicked', () => {
      this.chunkHistoryAviable = true;
      this.historyArray.length = 0;
      this.getHistoryChunk();
    });
  }

  $onInit() {
    this.container = angular.element(document.querySelector('.rcol'));
    this.aWindow = angular.element(window);
    this.tHead = angular.element(this.$element[0].querySelector('.thead'));
    this.tBody = angular.element(this.$element[0].querySelector('.tbody'));

    this.container.on('scroll', () => {
      this.setTHeadPos();
      this.calcShowBlock();
    });

    this.aWindow.on('resize', () => {
      this.setTHeadPos();
    });

    this.aWindow.on('scroll', () => {
      this.setTHeadPos();
    });

    this.itemEndId = -1;
  }

  $onDestroy() {
    this.container.off('scroll');
    this.aWindow.off('resize');
    this.aWindow.off('scroll');
    this.listener();
  }

  setTHeadPos() {
    this.tHead.css('left', this.getCoordsLeft(this.tBody[0]) - 2);
  }

  getCoordsLeft(elem) {
    const box = elem.getBoundingClientRect();
    const body = document.body;
    const docEl = document.documentElement;
    const clientLeft = docEl.clientLeft || body.clientLeft || 0;
    return box.left - clientLeft;
  }

  getHistoryChunk() {
    if (!this.chunkHistoryAviable || this.reqProc) {
      return false;
    }
    this.reqProc = true;

    let dateFrom = '';
    if (this.historyArray.length) {
      dateFrom = this.historyArray[this.historyArray.length - 1].date;
    }

    this.BotService.getBroadcastStats(dateFrom).then(res => {
      res.forEach(item => this.historyArray.push(item));
      this.chunkHistoryAviable = res.length > 0;
      if (!this.eventSended) {
        this.eventSended = true;
        this.historyEmpty = !this.chunkHistoryAviable;
        this.$scope.$emit('$historyLoaded', this.chunkHistoryAviable);
      }

      if (this.chunkHistoryAviable) {
        setTimeout(() => {
          this.calcShowBlock();
        }, 0);
      }
      this.reqProc = false;
    });
    return true;
  }

  calcShowBlock() {
    const itemHeight = 69;

    let itemStartId = Math.ceil(this.container[0].scrollTop / itemHeight) - 15;
    itemStartId = itemStartId < 0 ? 0 : itemStartId;
    let itemEndId = itemStartId + 40;
    if (itemEndId > this.historyArray.length - 1) {
      itemEndId = this.historyArray.length - 1;
      this.getHistoryChunk();
    }

    const lHistoryIdsArray = [];

    for (let i = itemStartId; i < (itemEndId + 1); i ++) {
      lHistoryIdsArray.push(i);
    }

    let paddingTop = 0;
    let paddingBottom = 0;

    if (itemStartId > 0) {
      paddingTop = itemStartId * itemHeight;
    }

    if (itemEndId < (this.historyArray.length - 1)) {
      paddingBottom = (this.historyArray.length - itemEndId) * itemHeight;
    }


    if (Math.abs(this.itemEndId - itemEndId) > 5 || itemEndId === (this.historyArray.length - 1) || itemStartId === 0) {
      this.itemEndId = itemEndId;

      this.$scope.$apply(() => {
        this.historyIdsArray = lHistoryIdsArray;
        this.padding = paddingTop + 'px 0px ' + paddingBottom + 'px';
      });
    }
  }

  resend(broadcastId, type, $event) {
    $event.stopPropagation();

    const goto = bl => {
      this.BlockService.update(bl).then(() => {
        this.$state.go('app.broadcast.block', { id: bl.id, broadcast_id: 0, type: 'now' });
      });
    };

    this.BroadcastService.show(broadcastId).then(res => {
      this.BlockService.show(res.block_id).then(targetBlock => {
        this.BlockService.createBroadcastNowBlock()
          .then(newBlock => {
            if (newBlock.cards && newBlock.cards.length > 0) {
              newBlock.cards.forEach(card => this.PluginCardService.remove(card));
            }

            newBlock.cards = targetBlock.cards;
            newBlock.user_filter = targetBlock.user_filter;

            let saveCardCount = 0;
            targetBlock.cards.forEach(elem => {
              elem.blockId = newBlock.id;
              delete elem.id;
              saveCardCount ++;
              this.PluginCardService.save(elem).then(() => {
                saveCardCount --;
                if (!saveCardCount) {
                  goto(newBlock);
                }
              }, () => {
                saveCardCount --;
                if (!saveCardCount) {
                  goto(newBlock);
                }
              });
            });

            if (saveCardCount === 0) {
              goto(newBlock);
            }
          });
      });
    });
  }

  goBlockByBC(broadcastId, type) {
    if (type === 'auto') {
      this.$state.go('app.broadcast.auto', { id:  broadcastId, type: 'auto' });
    } else
    if (type === 'recurrent') {
      this.BroadcastService.showRecurrent(broadcastId).then(res => {
        this.$state.go('app.broadcast.block', { id: res.block_id, broadcast_id: broadcastId, type: 'recurrent' });
      });
    } else {
      this.BroadcastService.show(broadcastId).then(res => {
        switch (type) {
          case 'send_now':
            window.localStorage.setItem('auto-' + res.block_id, 1);
            this.$state.go('app.broadcast.block', { id: res.block_id, broadcast_id: 1, type: 'now' });
            break;
          case 'scheduled':
            this.$state.go('app.broadcast.block', { id: res.block_id, broadcast_id: broadcastId, type: 'schedule' });
            break;
        }
      });
    }
  }
}
