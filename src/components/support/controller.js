export class SupportController {
  constructor($scope) {
    'ngInject';

    $scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.supportOpened = false;
      });
    });
  }
}
