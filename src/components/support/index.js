import angular from 'angular';
import Component from './component.js';

export default angular
  .module('app.common.support', []).directive('support', Component).name;
