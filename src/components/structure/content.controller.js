import _ from 'lodash';
import $ from 'jquery';

export default class ContentController {
  constructor(
    $element,
    $rootScope,
    $scope,
    $filter,
    $timeout,
    $state,
    $stateParams,
    $interpolate,
    $sce,
    BotService,
    PageService,
    ModalService,
    BlockService,
    StoreService,
    BroadcastService,
    PluginCardService,
    API_ERROR_MSG
  ) {
    'ngInject';
    this.$sce = $sce;
    this.$state = $state;
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$filter = $filter;
    this.$element = $element;
    this.$timeout = $timeout;
    this.BotService = BotService;
    this.PageService = PageService;
    this.$interpolate = $interpolate;
    this.$stateParams = $stateParams;
    this.BlockService = BlockService;
    this.StoreService = StoreService;
    this.ModalService = ModalService;
    this.BroadcastService = BroadcastService;
    this.PluginCardService = PluginCardService;
    this.block = {};
    this.blocks = [];
    this.$error = '';
    this.messageUs = '';
    this.API_ERROR_MSG = API_ERROR_MSG;
    this.groups = this.StoreService.get('groups');
    this.list(this.$stateParams.id);
    this.loadPage();
    this.loadBot();

    this.listeners = [];

    this.listeners.push($scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.$messageUs = false;
      });
    }));

    this.listeners.push($scope.$on('$onQrMouseOver', ($event, data) => {
      if (data.flag) {
        this.qrMouseOverId = data.id;
      } else {
        this.qrMouseOverId = null;
      }
    }));

    this.listeners.push($scope.$on('$onActionListOver', ($event, data) => {
      if (data.flag) {
        this.actionListOver = data.id;
      } else {
        this.actionListOver = null;
      }
    }));

    this.firstSort = true;

    this.loading = true;
  }

  $onInit() {
    if (this.$stateParams.new) this.$stateParams.new = null;

    this.$sortable = {
      handle: '.ui-sortable-handle',
      'ui-floating': true,
      start: (e, ui) => {
        if (this.firstSort) {
          $(e.target).sortable('refreshPositions');
          this.firstSort = false;
        }
      },
      axis: 'y',
      stop: (e, ui) => {

        this.block.cards = this.block.cards.map((card, i) => {
          _.extend(card, { position: i });
          return card;
        });
        _.each(this.block.cards, card => {
          card.blockId = this.block.id;
          this.PluginCardService.save(card);
        });

        this.$scope.$broadcast('$buttonUpdate');
      }
    };

    this.$sortableInGal = {
      handle: '.ui-sortable-handle-in-gal',
      'ui-floating': true,
      axis: 'x',
      tolerance: 'pointer',
      containment: 'document',
      stop: (e, ui) => {
        const card = this.block.cards.find(lcard => lcard.id === ui.item.sortable.source[0].id);
        card.blockId = this.block.id;
        this.PluginCardService.save(card);
        this.$scope.$broadcast('gallery-sort-stop');
      }
    };

  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  loadBot() {
    this.BotService.cachedList().then(bots => {
      this.bot = bots.find(item => item.id === this.$stateParams.bot_id);
    })
  }

  list(id) {
    // this.BlockService.messageUsButton(id, 'blue', 'large').then(result => {
    //   const script = result.script.replace('<script>', '').replace('</script>', '');
    //
    //   this.messageUs = {
    //     script: this.$sce.trustAsJs(script),
    //     div: this.$sce.trustAsHtml(result.div),
    //     html: this.$sce.trustAsHtml(result.html)
    //   };
    // });
    return this.BlockService.detail(id).then(data => {
      this.block = data;
      this.blocks = this.StoreService.getBlocks();


      // focus title to edit after creation
      if (this.$stateParams.new === 'true') {
        // prevent params overriding
        this.$stateParams.new = null;
        this.$titleEdited = true;
        this.block.title = this.$stateParams.title;
        this.$focusBlockTitle();
        this.saveBlockTitle();
      }

      this.$timeout(() => {
        this.$scope.$broadcast('$dataReady');
        this.$timeout(() => {
          this.loading = false;

          this.$timeout(() => {
            this.loaderRenderOff = true;
          }, 500);
        });
      }, 10);
    });
  }

  loadPage() {
    this.PageService.cachedList(true)
      .then(pages => {
        this.page = pages.find(item => item.bot_id === this.$stateParams.bot_id);
      });
  }

  toEditTitle() {
    this.$titleEdited = true;

    setTimeout(() => {
      this.$element.find('input')[0].focus();
    }, 0);
  }

  clearTitle() {
    return (this.block.title || '').replace(/( |&nbsp;)+/g, ' ').replace(/(<([^>]+)>)/ig, '').replace(/&amp;/ig, '&').trim();
  }

  saveBlockTitle() {
    const title = this.clearTitle();

    this.BlockService
      .save({
        id: this.block.id,
        title,
        parent_group: this.block.parent_group,
        referral: this.block.referral,
        referral_active: this.block.referral_active
      })
      .then(() => {
        this.$error = '';
        this.$titleEdited = false;
        this.onSaveBlockTitle();
      })
      .catch(err => {
        if (!err.data.errors) {
          this.$error = err.data.result;
        } else {
          this.$error = err.data.errors[0].replace('title ', '');
        }
      });
  }

  onSaveBlockTitle() {
    this.$scope.$emit('block:update', this.block.id, this.clearTitle(), this.block.referral, this.block.referral_active);
    if (this.$stateParams.new) {
      this.$state.go('app.structure.block', this.block);
    }

    window.localStorage.setItem('updateBlocks', 'event' + Math.random());
  }

  toggleEdit(index) {
    const card = this.block.cards[index];

    card.$saved = false;
    card.config.buttons = card.config.buttons.map(button => ({
      $edited: true,
      title: button.title,
      url: button.url
    }));
  }


  /**
   * onDeleteBlock
   * @param  {Object} e
   * @return {undefined}
   */
  onDeleteBlock(e) {
    e.preventDefault();

    const context = this.$interpolate(this.ModalService.templates.block())({
      block: `${this.block.title}`
    });

    this.ModalService
      .confirm(context)
      .then(isConfirmed => isConfirmed && this.deleteBlock());
  }

  deleteBlock() {
    this.BlockService.remove(this.block.id)
      .then(() => {
        this.$scope.$emit('block:remove', this.block.id);
        this.$state.go('app.structure');
        window.localStorage.setItem('updateBlocks', 'event' + Math.random());
      });
  }

  $remove() {
    const context = this.$interpolate(this.ModalService.templates.block())();
    this.ModalService
      .confirm(context)
      .then(isConfirmed => isConfirmed && this.deleteBlock());
  }
  onCopyBlock(e) {
    e.preventDefault();
    this.copyBlock();
  }

  copyBlock() {
    let blockTitle;
    let i = 1;

    let group = this.groups.find(item => item.id === this.block.parent_group);

    if (!group) {
      group = this.groups[this.groups.length - 1];
    }

    while (true) {
      blockTitle = `${this.block.title} Copy${i++}`;
      if (!group.blocks.find(items => items.title === blockTitle)) {
        break;
      }
    }

    this.BlockService.save({ parent_group: this.block.parent_group, title: blockTitle })
      .then(block => {
        block.cards = this.block.cards;
        this.$rootScope.$emit('block:update', block.id, blockTitle);

        let saveCardCount = 0;

        this.block.cards.forEach(elem => {
          elem.blockId = block.id;
          delete elem.id;

          saveCardCount ++;

          this.PluginCardService.save(elem).then(() => {
            saveCardCount --;
            if (!saveCardCount) {
              this.$state.go('app.structure.block', { id: block.id });
            }
          }, err => {
            saveCardCount --;
            if (!saveCardCount) {
              this.$state.go('app.structure.block', { id: block.id });
            }
          });
        });

        if (saveCardCount === 0) {
          this.$state.go('app.structure.block', { id: block.id });
        }

        group.blocks.push(block);
      });
  }

  // $showMessageUsPopup(event) {
  //   event.stopPropagation();
  //   this.$messageUs = true;
  //
  //   this.$timeout(() => {
  //     document.getElementById('structure-message-us-input').select();
  //   });
  // }

  $focusBlockTitle() {
    this.$timeout(() => {
      document.getElementById('block-title-input').focus();
      document.execCommand('selectAll', false, null);
    });
  }

  $getBlockDescription() {
    if (!this.page || !this.block) {
      return '';
    }

    // if (this.block.id === this.page.first_block.id) {
    //   return 'This is the first message that bot sends to a user.';
    // }
    // if (this.block.id === this.page.default_block.id) {
    //   return 'This block is for your bot navigation. When a user asks for a “Menu” this block is shown.';
    // }
  }

  $stripTitleTags() {
    this.block.title = this.block.title.replace(/<\S[^><]*>/g, '');
  }

  cropTitle($event) {
    if (this.block.title.replace(/<\S[^><]*>/g, '').length === 0) {
      const el = document.querySelector('h1 .text');
      el.innerHTML = '';
    }
    if (this.block.title.length > 60) {
      this.block.title = this.block.title.replace(/( |&nbsp;)+/g, ' ').replace(/(<([^>]+)>)/ig, '').trim().substring(0, 60);

      this.$timeout(() => {
        const el = document.querySelector('h1 .text');
        const range = document.createRange();
        const sel = window.getSelection();
        if (el.childNodes.length > 0) {
          const el2 = el.childNodes[el.childNodes.length - 1];
          range.setStartAfter(el2);
        } else {
          range.setStartAfter(el);
        }
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
      });
    }
  }

  closeBlock() {
    try {
      if (window.localStorage) {
        window.localStorage.removeItem('structure-params-' + this.$stateParams.bot_id);
      }
    } catch (err){}
    this.$state.go('app.structure', { bot_id: this.$stateParams.bot_id });
  }


}
