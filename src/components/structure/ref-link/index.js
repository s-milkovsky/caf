import ng from 'angular';
import './assets/less/style.less';

import { Component } from './component';

export default ng.module('app.common.structure.refLink', [])
  .directive('refLink', Component)
  .name;
