export default class NullController {
  constructor($state, $stateParams, $scope, BotService) {
    'ngInject';
    $scope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
      BotService.show()
        .then(bot => {
          let blockId;
          if (bot.first_block) {
            if (bot.first_block.id) {
              blockId = bot.first_block.id;
            } else {
              blockId = bot.first_block;
            }
            $state.go('app.structure.block', { bot_id: $stateParams.bot_id, id: blockId });
          }
        });
      this.show = true;
    });
  }
}
