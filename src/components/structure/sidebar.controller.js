
export default class SidebarController {
  constructor($scope, $rootScope, GroupService, BlockService, StoreService, API_ERROR_MSG, $state) {
    'ngInject';
    this.BlockService = BlockService;
    this.GroupService = GroupService;
    this.StoreService = StoreService;
    this.$state = $state;
    this.API_ERROR_MSG = API_ERROR_MSG;

    this.listeners = [];
    this.groups = StoreService.get('groups');



    this.listeners.push(
      $rootScope.$on('block:remove', (event, id) => {
        this.groups.forEach(group => {
          const i = group.blocks.findIndex(item => item.id === id);

          if (i !== -1) {
            group.blocks.splice(i, 1);
          }
        });
      })
    );

    this.listeners.push(
      $rootScope.$on('block:update', (event, id, title) => {
        this.groups.forEach(group => {
          group.blocks.forEach(block => {
            if (block.id === id) {
              block.title = title;
            }
          });
        });
      })
    );

    this.listeners.push(
      $scope.$on('$pluginHasErrorValidation', (e, data) => {
        // this.setBlockValidateStatus(data.blockId);
      })
    );

    this.listeners.push(
      $scope.$on('$pluginUpdated', (e, data, cards) => {
        this.setBlockValidateStatus(data.blockId, cards);
      })
    );

    this.$sortable = {
      handle: '.move',
      axis: 'y',
      stop: (e, ui) => {
        let group = ui.item.sortable.model;
        group.position = this.groups.findIndex(item => item === group);

        this.GroupService.update(group.id, group)
          .catch(err => {
            //    alert(JSON.stringify(err.data || this.API_ERROR_MSG));
          });
      }
    };
  }

  setBlockValidateStatus(blockId, cards) {
    if (!blockId) {
      blockId = this.$state.params.id;
    }
    this.groups.forEach(item => {
      const block = item.blocks.find(it => it.id === blockId);
      if (block) {
        let isValid = cards.every(item => {
          return item.is_valid === true;
        });
        block.is_valid = isValid;
      }
    });
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  addGroup() {
    let i = 1
      , title;

    while (true) {
      title = 'New Group ' + i++;

      if (!this.groups.find(item => item.title === title)) {
        break;
      }
    }

    this.GroupService.create({ title })
      .then(group => {
        group.blocks = [];
      	this.groups.push(group);
     	}, function(err) {
      // 	alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });
  }
}
