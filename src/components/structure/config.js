export const Config = ($stateProvider, $urlRouterProvider) => {
  'ngInject';

  $urlRouterProvider.otherwise('/structure');
  $stateProvider
    .state('app.structure', {
      url: '/structure',
      views: {
        'sidebar@': {
          template: '<aside></aside>'
        },
        'content@': {
          template: '<structure-null></structure-null>'
        }
      },
      resolve: {
        groups(GroupService, StoreService, $stateParams) {
          'ngInject';

          return GroupService.list($stateParams.bot_id).then(data => {
            StoreService.set('groups', data);
            return data;
          });
        }
      }
    })
    .state('app.structure.block', {
      url: '/:id?new?title',
      views: {
        'content@': {
          template: '<structure-content></structure-content>'
        }
      }
    });
};
