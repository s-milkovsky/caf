import ng from 'angular';
import ngRouter from 'angular-ui-router';
import ngEditable from 'angular-contenteditable';
import 'angular-ui-sortable';

import RefLink from './ref-link';

import { Config } from './config';
import { NullComponent, ContentComponent, SidebarComponent } from './components';

export default ng.module('app.structure', ['ui.sortable', ngRouter, ngEditable, RefLink])
  .directive('structureContent', ContentComponent)
  .directive('structureNull', NullComponent)
  .directive('structureSidebar', SidebarComponent)
  .config(Config)
  .name;
