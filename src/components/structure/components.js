import nullTemplate from './templates/structure.null.html';
import contentTemplate from './templates/structure.content.html';
import sidebarTemplate from './templates/structure.sidebar.html';

import SidebarController from './sidebar.controller';
import ContentController from './content.controller';
import NullController from './null.controller';

export const ContentComponent = () => ({
  template: contentTemplate,
  controller: ContentController,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});


export const NullComponent = () => ({
  template: nullTemplate,
  controller: NullController,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});


export const SidebarComponent = () => ({
  template: sidebarTemplate,
  controller: SidebarController,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});
