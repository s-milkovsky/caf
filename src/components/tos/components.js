import template from './templates/tos.html';
import { TosController as controller } from './controller';

export const Component = () => ({
  template,
  controller,
  controllerAs: 'tos', // vm overided
  restrict: 'E',
  replace: true
});
