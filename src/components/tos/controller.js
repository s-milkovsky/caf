export class TosController {
  constructor($scope, $state, $timeout, $rootScope, UserService) {
    'ngInject';

    this.$scope = $scope;
    this.$state = $state;
    this.$rootScope = $rootScope;
    this.$timeout = $timeout;
    this.UserService = UserService;

    this.uConfirm = false;
    this.uSubscribe = true;

    this.enabled = false;
    this.show = false;
    this.animateShow = false;

    this.currentStatus = false;

    this.body = angular.element(document.querySelector('body'));

    if (!$rootScope.$newuser) {
      $rootScope.$newuser = (window.localStorage.getItem('$newuser') === 'true');
    }

    this.$scope.$on('$tos', (event, status) => {
      if (this.currentStatus !== status) {
        this.currentStatus = status;

        if (status && !this.enabled) {
          this.enabled = true;
          this.show = true;
          this.animateShow = true;
          this.$rootScope.$tosShow = true;

          this.uConfirm = false;
          this.uSubscribe = true;

          this.goBlock();

          this.$timeout(() => {
            this.$state.go('bots');
          }, 100);
        } else {
          this.show = false;
          this.$rootScope.$tosShow = false;
          this.$timeout(() => {
            this.enabled = false;
            this.animateShow = false;
          }, 100);
        }
      }
    });
  }

  $onInit() {

  }

  $onDestroy() {
  //  this.listeners.forEach(fn => fn.call());
  }

  goBlock() {
    if (this.enabled) {
      this.body.addClass('tos-showed');
      this.$timeout(() => {
        this.goBlock();
      }, 1000);
    } else {
      this.body.removeClass('tos-showed');
    }
  }

  go() {
    if (this.uConfirm) {
      this.UserService.acceptTerms(this.uSubscribe).then(res => {
        if (res) {
          this.$rootScope.$broadcast('$tos', false);
          window.localStorage.removeItem('$newuser');

          this.$timeout(() => {
            if (this.$rootScope.$afterShowTosTarget) {
              if (this.$rootScope.$afterShowTosTarget === 'bots') {
                this.$state.go('bots');
              } else {
                if (this.$rootScope.$afterShowTosGoSettings) {
                  this.$state.go('app.settings', { bot_id: this.$rootScope.$afterShowTosTarget });
                } else {
                  this.$state.go('app.structure', { bot_id: this.$rootScope.$afterShowTosTarget });
                }
              }
              delete this.$rootScope.$afterShowTosGoSettings;
              delete this.$rootScope.$afterShowTosTarget;
              delete this.$rootScope.$newuser;
            }
          }, 100);
        }
      });
    }
  }
}
