import ng from 'angular';
import './assets/less/tos.less';
import 'angular-ui-sortable';


import { Component } from './components';

export default ng.module('app.components.tos', [])
  .directive('tos', Component)
  .name;
