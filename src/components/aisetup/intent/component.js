import template from './intent.html';
import controller from './controller';

export const Component = () => ({
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true,
  restrict: 'E',
  replace: true,
  transclude: true,
  scope: {
    index: '=',
    intents: '=',
    intent: '=',
    intentId: '@'
  }
});
