import _ from 'lodash';


export default class IntentController {
  constructor($scope, $timeout, $element, ModalService) {
    'ngInject';
    this.$scope = $scope;
    this.$timeout = $timeout;
    this.$element = $element;
    this.ModalService = ModalService;

    this.blocksTitles = [];
    this.excludeBlocks = [];
  }

  $onInit() {
    this.$scope.$on('$updateBlocksInSuggest', event => {
      event.stopPropagation();
      this.save();
    });
    this.$scope.$on('$textareaChange', event => {
      event.stopPropagation();
      this.save();
    });
    this.$scope.$on('$linesChange', event => {
      event.stopPropagation();
      this.save();
    });
    this.$scope.$on('$focusIntent', (event, index) => {
      if (this.index === index) {
        this.$timeout(() => {
          this.$scope.$broadcast('$setFocusLines');
          this.focused = true;
          this.sendFocusStatus(true);
        });
      }
    });
    this.$scope.$on('$onLineNextTab', event => {
      event.stopPropagation();
      this.$scope.$broadcast('$setFocusByIndex', 0);
    });
    this.$scope.$on('$popupValueChange', (event, data) => {
      event.stopPropagation();
      this.$timeout(() => {
        this.$scope.$broadcast('$setFocusByIndex', data.index);
      });
    });

    this.$scope.$on('document.mousedown', () => {
      this.focused = false;
      this.$timeout(() => {
        this.watchValidateIntent();
        this.sendFocusStatus();
      }, 100);
    });

    // this.$scope.$watch('vm.intent', () => {
    //   if (!this.isFirstEmptyIntent() || this.inNoFirstValid) {
    //     this.error = this.validateIntent();
    //   }
    //   this.inNoFirstValid = true;
    // }, true);


    if (!this.isFirstEmptyIntent()) {
      setTimeout(() => {
        this.watchValidateIntent();
        this.focused = false;
        this.sendFocusStatus();
      }, 10);
    }
  }

  watchValidateIntent() {
    if (!this.isFirstEmptyIntent() || this.inNoFirstValid) {
      this.error = this.validateIntent();
    }
    this.inNoFirstValid = true;
  }

  validateIntent() {
    if (!this.intent || !this.intent.lines || this.intent.lines.length === 0) {
      return true;
    }

    return !this.intent.action.items.every(item =>
    (item.item_type === 'text' && item.text && item.text.length > 0) || (item.item_type === 'block' && item.blocks && item.blocks.length > 0));
  }

  isFirstEmptyIntent() {
    return this.intents.length < 2 && this.isEmptyIntent();
  }

  isEmptyIntent() {
    return !this.intent ||
           (!this.intent.lines || this.intent.lines.length === 0) &&
           ((!this.intent.action.items || this.intent.action.items.length === 0) ||
           (this.intent.action.items.length < 2 &&
           (!this.intent.action.items[0].blocks || this.intent.action.items[0].blocks.length === 0) &&
           (!this.intent.action.items[0].text || this.intent.action.items[0].text.length === 0)));
  }

  setFocus() {
    setTimeout(() => {
      this.focused = true;
    }, 10);
  }

  sendFocusStatus(pre) {
    if (pre || this.focused === false) {
      this.$scope.$broadcast('$setFocusOutsideStatus', this.focused);
    }
  }

  removeIntent() {
    if (this.isEmptyIntent()) {
      this.intents.splice(Number(this.intentId), 1);
      this.save();
    } else {
      this.ModalService
        .confirmAdv('<p>Do You really want to delete this AI rule?</p>',
          {
            ok: 'REMOVE',
            close: 'CANCEL'
          }
        )
        .then((res) => {
          if (res) {
            this.intents.splice(Number(this.intentId), 1);
            this.save();
          }
        });
    }
  }

  addAction() {
    this.intent.action.items.push({ item_type: 'text', blocks: [], text: '' });
    this.save();

    this.$timeout(() => {
      this.$scope.$broadcast('$setFocusByIndex', this.intent.action.items.length - 1);
      this.$scope.$broadcast('$setFocusOutsideStatus', false);
    });
  }

  removeAction($index) {
    this.intent.action.items.splice($index, 1);
    this.save();
  }

  randomModeChange() {
    this.$timeout(() => {
      this.save();
    });
  }

  save() {
    setTimeout(() => {
      this.$scope.$emit('$intentChanged');
    }, 10);
  }
}
