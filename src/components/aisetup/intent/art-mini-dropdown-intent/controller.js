export default class ArtMiniDropdownController {
  constructor($scope, $rootScope, $timeout, $element, $document) {
    'ngInject';

    this.$timeout = $timeout;
    this.$scope = $scope;
    this.$element = $element;
    this.$document = $document;

    this.listeners = [];

    this.open = false;

    $document.on('click', () => {
      if (this.open) {
        $element.removeClass('open');
        this.open = false;
      }
    });

    this.listeners.push($rootScope.$on('$suggestFocusedGlobal', () => {
      $element.removeClass('open');
      this.open = false;
    }));

    this.$element.on('click', () => {
      if (!this.open) {
        setTimeout(() => {
          this.open = true;
          $element.addClass('open');
        }, 10);
      }
    });

    this.items = { block: 'BLOCK', text: 'TEXT' };
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
    this.$element.off('click');
    this.$document.off('click');
  }

  setValue(val) {
    this.value = val;
    this.$scope.$emit('$popupValueChange', { index: this.index, value: val });
  }

  getText() {
    return this.items[this.value];
  }
}
