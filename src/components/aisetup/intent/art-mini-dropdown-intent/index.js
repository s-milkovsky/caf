import './assets/less/art-mini-dropdown.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.artMiniDropdownIntent', [])
  .directive('artMiniDropdownIntent', Component)
  .name;
