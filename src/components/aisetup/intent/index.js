import ng from 'angular';

import { Component } from './component';

import artMiniDropdownIntent from './art-mini-dropdown-intent';

export default ng.module('app.components.intent', [artMiniDropdownIntent])
  .directive('intent', Component)
  .name;
