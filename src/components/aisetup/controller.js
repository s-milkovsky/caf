import _ from 'lodash';

export default class AiSetupController {
  constructor($scope, $timeout, $window, $element, BotService, BlockService, PluginCardService, StoreService) {
    'ngInject';
    this.$scope = $scope;
    this.$timeout = $timeout;
    this.$window = $window;
    this.$element = $element;
    this.BlockService = BlockService;
    this.BotService = BotService;
    this.PluginCardService = PluginCardService;
    this.StoreService = StoreService;

    this.currentLanguage = {
      name: 'English',
      code: 'en'
    };
  }

  $onInit() {
    this.className = 'ai-setup';

    this.$scope.$on('$intentChanged', event => {
      event.stopPropagation();

      this.intentsCleaner();
      this.save();

      this.$timeout(() => {
        this.calculateShowWindow(true);
      });
    });

    this.$element.on('click', () => {
      this.$scope.$apply(() => {
        this.$showSelect = false;
      });
    });

    this.loading = true;
    this.rendingPreloader = true;
    this.BotService.show()
      .then(bot => {
        this.bot = bot;
        this.BlockService.show(bot.ai_block.id ? bot.ai_block.id : bot.ai_block).then(aiBlock => {
          this.aiBlock = aiBlock;
          this.intents = _.cloneDeep(aiBlock.cards[0].config.intents);

          if (this.intents.length === 0) {
            this.$timeout(() => {
              this.addIntent();
            }, 100);
          } else {
            this.intents.forEach(item => {
              if (!item.action) {
                item.action = { random: false };
              }
              if (!item.action.items) {
                item.action.items = [];
              }

              if (item.goto_block && item.goto_block.length > 0) {
                item.action.items.push({ item_type: 'block', blocks: _.cloneDeep(item.goto_block) });
                delete item.goto_block;
              }
            });

            this.intentsCleaner();
          }

          this.BotService.getAiLanguages().then(languages => {
            this.languages = languages;
            if (aiBlock.cards[0].config.lang) {
              this.currentLanguage = this.languages.find(lBot => lBot.code === aiBlock.cards[0].config.lang);
            } else {
              this.currentLanguage = {
                name: 'English',
                code: 'en'
              };
            }
          });
          this.loading = false;
          this.$timeout(() => {
            this.rendingPreloader = false;
            this.calculateShowWindow(true);
          }, 300);
        });

      });



    //-----------------

    this.intentsListNode = angular.element(this.$element[0].querySelector('.intents-list'));
    this.intentsHeight = [];
    this.intentShowEnd = -1;

    this.aWindow = angular.element(this.$window);

    this.aWindow.on('scroll', () => {
      if (!this.scrollLock) {
        this.scrollLock = true;
        this.calculateShowWindow();
        clearTimeout(this.scrollLockTimeout);
        this.scrollLockTimeout = setTimeout(() => {
          this.scrollLock = false;
          this.calculateShowWindow();
        }, 200);
      }
    });

    this.serchField = this.$element[0].querySelector('.search-box input');

    this.aWindow.on('keydown', e => {
      if (e.keyCode === 114 || ((e.ctrlKey || e.metaKey) && e.keyCode === 70)) {
        e.preventDefault();
        this.aWindow[0].scrollTo(0, 0);
        this.serchField.focus();
      }
    });

    this.$timeout(() => {
      const inp = this.$element[0].querySelector('span.line');
      if (inp) {
        inp.focus();
      }
      window.scrollTo(1, 0);
    }, 50);
  }

  $onDestroy() {
    this.aWindow.off('scroll');
    this.aWindow.off('resize');
    this.aWindow.off('keydown');
    this.$element.off('click');
  }

  calculateShowWindow(forse) {
    if (!this.intents) {
      return;
    }

    if (this.searchText && this.searchText.length > 0) {
      const lSearchText = this.searchText.trim().toLowerCase();

      this.currentIntents = [];
      this.currentIntentsMap = [];

      this.intents.forEach((item, i) => {
        if (
          item.lines.find(line => line.toLowerCase().indexOf(lSearchText) !== -1) ||
          (item.action.random && item.action.items.find(act => (act.text && act.text.toLowerCase().indexOf(lSearchText) !== -1) || (act.blocks && _.intersection(act.blocks, this.foundBlocks).length > 0))) ||
          (!item.action.random && ((item.action.items[0].blocks && _.intersection(item.action.items[0].blocks, this.foundBlocks).length > 0) || (item.action.items[0].text && item.action.items[0].text.toLowerCase().indexOf(lSearchText) !== -1)))
        ) {
          this.currentIntents.push(item);
          this.currentIntentsMap.push(i);
        }
      });
    } else {
      this.currentIntents = this.intents;
      this.currentIntentsMap = this.intents.map((item, i) => i);
    }

    for (let i = 0; i < this.intents.length; i ++) {
      this.intentsHeight[i] = 154;
    }

    [].forEach.call(this.intentsListNode[0].querySelectorAll('.intent-item'), lIntent => {
      this.intentsHeight[Number(lIntent.getAttribute('intent-id'))] = lIntent.offsetHeight + 32;
    });

    const currentIntentsHeight = this.intentsHeight.filter((item, i) => this.currentIntentsMap.indexOf(i) !== -1);

    let iHeightSum = 308;
    let tPos = 0;
    const yScroll = window.pageYOffset;
    currentIntentsHeight.every((iHeight, i) => {
      iHeightSum += iHeight;
      if (iHeightSum > yScroll) {
        tPos = i;
        return false;
      }
      return true;
    });

    let intentShowStart = tPos - 15;
    let intentShowEnd = tPos + 20;

    if (intentShowStart < 0) {
      intentShowStart = 0;
    }

    if (intentShowEnd > (this.currentIntents.length - 1)) {
      intentShowEnd = this.currentIntents.length - 1;
    }

    const lIntentsIdsToShow = [];

    for (let i = intentShowStart; i < (intentShowEnd + 1); i ++) {
      lIntentsIdsToShow.push(i);
    }

    let paddingTop = 0;
    let paddingBottom = 0;


    if (intentShowStart > 0) {
      paddingTop = currentIntentsHeight.slice(0, intentShowStart).reduce((a, b) => (a + b));
    }

    if (intentShowEnd < (currentIntentsHeight.length - 1)) {
      paddingBottom = currentIntentsHeight.slice(intentShowEnd).reduce((a, b) => (a + b));
    }

    if (forse || Math.abs(this.intentShowEnd - intentShowEnd) > 2 || intentShowEnd === (this.currentIntents.length - 1) || intentShowStart === 0) {
      this.intentShowEnd = intentShowEnd;

      this.$scope.$apply(() => {
        this.intentsIdsToShow = lIntentsIdsToShow;
        this.padding = paddingTop + 'px 0px ' + paddingBottom + 'px';
      });
   //   this.$scope.$digest();
    }
  }

  addIntent() {
    this.searchText = null;

    this.$timeout.cancel(this.addTimeout);
    this.intents.forEach(intent => {
      intent.lines = _.compact(_.uniq(intent.lines));
    });

    this.intents.unshift({
      lines: [],
      action: {
        random: false,
        items: [
          {
            item_type: 'text',
            blocks: [],
            text: ''
          }
        ]
      }
    });

    this.addTimeout = this.$timeout(() => {
      this.intentsCleaner();
      this.save();
      this.$scope.$broadcast('$focusIntent', 0);
    }, 300);

    this.$timeout(() => {
      this.calculateShowWindow(true);
    });
  }

  intentsCleaner() {
    const lIntents = [];
    let usedBlocks = [];

    this.intents.forEach( intent => {
      const lIntent = {};

      lIntent.lines = _.compact(_.uniq(intent.lines));
      lIntent.lines.forEach((line, index) => {
        lIntent.lines[index] = this.clear(line);
      });

      lIntent.action = {
        random: intent.action.random,
        items: []
      };

      intent.action.items.every(item => {
        if (item.item_type === 'text') {
          lIntent.action.items.push({
            item_type: 'text',
            text: item.text ? item.text.trim() : ''
          });
        } else
        if (item.item_type === 'block') {
          lIntent.action.items.push({
            item_type: 'block',
            blocks: item.blocks ? item.blocks : []
          });

          usedBlocks = usedBlocks.concat(item.blocks);
        }

        return intent.action.random;
      });


      lIntents.push(lIntent);
    });

    this.aiBlock.cards[0].config.intents = lIntents;
  }

  selectLanguage(language) {
    this.$showSelect = false;
    this.currentLanguage = language;
    this.save();
  }

  save() {
    this.$timeout.cancel(this.saveTimeout);
    this.saveTimeout = this.$timeout(() => {
      this.aiBlock.cards[0].blockId = this.aiBlock.id;
      this.aiBlock.cards[0].config.custom_domains = ['talk'];
      this.aiBlock.cards[0].config.lang = this.currentLanguage.code;
      this.PluginCardService.save(this.aiBlock.cards[0]);
    }, 300);
  }

  setSearch() {
    this.foundBlocks = [];

    if (this.searchText && this.searchText.length > 0) {
      this.aWindow[0].scrollTo(0, 0);

      const st = this.searchText.trim().toLowerCase();
      const groups = this.StoreService.get('groups');
      groups.forEach(group => {
        group.blocks.forEach(block => {
          if (block.title.toLowerCase().indexOf(st) !== -1) {
            this.foundBlocks.push(block.id);
          }
        });
      });

    }


    setTimeout(() => {
      this.calculateShowWindow(true);
    }, 0);
  }

  clear(s) {
    return s.replace(/( |&nbsp;)+/g, ' ').replace(/(<([^>]+)>)/ig, '').replace(/&amp;/ig, '&').trim();
  }

}
