
export default $stateProvider => {
  'ngInject';

  $stateProvider
    .state('app.aiSetup', {
      url: '/ai-setup',
      views: {
        'main@': {
          template: '<ai-setup></ai-setup>'
        }
      },
      resolve: {
       groups(GroupService, StoreService, $stateParams) {
         'ngInject';

         return GroupService.list($stateParams.bot_id).then(data => {
           StoreService.set('groups', data);
           return data;
         });
       }
      }
    });
};
