import ng from 'angular';
import ngRouter from 'angular-ui-router';
import './assets/less/ai-setup.less';

import Intent from './intent';


import Config from './config';
import AiSetupComponent from './component';

export default ng.module('app.components.aiSetup', [ngRouter, Intent])
  .directive('aiSetup', AiSetupComponent)
  .config(Config)
  .name;
