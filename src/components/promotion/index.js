import angular from 'angular';
import ngRouter from 'angular-ui-router';
import Component from './component.js';
import Config from './config.js';

export default angular
  .module('app.common.promotion', [ngRouter])
  .directive('promotion', Component)
  .config(Config)
  .name;
