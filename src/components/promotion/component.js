import template from './promotion.html';
import controller from './controller.js';

export default () => ({
  controller,
  template,
  controllerAs: 'vm',
  restrict: 'E',
  replace: true
});
