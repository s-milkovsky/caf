export default class PromotionController {
  constructor($timeout, $state, $stateParams, $rootScope, PageService, BlockService, BotService) {
    'ngInject';
    this.$timeout = $timeout;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
    this.PageService = PageService;
    this.BlockService = BlockService;
    this.BotService = BotService;
    this.loading = true;

    this.className = 'promotion';

    this.loadBot();

    const cover1 = require('../../assets/images/covers/cover_bg_1.png');
    const cover2 = require('../../assets/images/covers/cover_bg_2.png');
  }

  loadBot() {
    this.BotService.show()
      .then(bots => {
        this.bot = bots;
        this.directLinkToBot = `https://m.me/${ this.bot.status.page }`;

        this._update();
        this.updateMessageUsCode('blue', 'standard', true);
        this.updateWebPluginCode('blue', 'standard', true);
        this.loading = false;
      });
  }

  _update() {
    if (this.bot.status.status === 'draft') {
      this.className = 'promotion no-publish';
      this.isBotConnectedToFB = false;
    } else {
      this.className = 'promotion';
      this.isBotConnectedToFB = true;
    }
  }

  goWizard(event) {
    event.preventDefault();
    this.$state.go('wizard.page1', { bot_id: this.bot.id });
  }

  goSettings() {
    this.$state.go('app.settings', { bot_id: this.bot.id });
  }

//----------------------------

  updateMessageUsCode(color, size, noFocus) {
    this.BotService.messageUsButton(this.bot.id, color, size, true)
      .then(code => {
        this.messageUsCode = code;
        if (noFocus) {
            return;
        }
        this.$focusMessageUsInput();
      });
  }

  updateWebPluginCode(color, size, noFocus) {
    this.BlockService.webPluginButton(this.bot.first_block.id, color, size, true)
      .then(code => {
        this.webPluginCode = code;
        if (noFocus) {
            return;
        }
        this.$focusMessengerInput();
      });
  }

  $focusMessageUsInput() {
    this.$timeout(() => {
      document.getElementById('promotion-message-us-input').select();
    });
  }

  $focusMessengerInput() {
    this.$timeout(() => {
      document.getElementById('promotion-messenger-input').select();
    });
  }

  goDownload(pos) {
    this.$rootScope.$emit('$statEvent', { event: 'hit', data: (window.location.href + '/cover_download/' + pos) });
  }
  copyLink(number, event) {
    this.copied = [];
    event.currentTarget.previousElementSibling.select();
    try {
      document.execCommand('copy');
      this.copied[number] = true;
      this.$timeout(() => {
        this.copied[number] = false;
      }, 1000);
      this.clearSelection();
    } catch (err) {
    }
  }
  clearSelection() {
    if (document.selection) {
      document.selection.empty();
    } else {
      if (window.getSelection) {
        window.getSelection().removeAllRanges();
      }
    }
  }
}
