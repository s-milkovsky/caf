const Config = ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('app.promotion', {
      url: '/promotion',
      views: {
        'main@': { template: '<promotion></promotion>' }
      }
    });
};

export default Config;