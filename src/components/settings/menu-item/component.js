import template from './template.html';
import controller from './controller';

export const Component = () => ({
  require: {
    SettingsController: '^^settings'
  },
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true,
  restrict: 'E',
  replace: true,
  transclude: true,
  scope: {
    item: '=',
    index: '='
  }
});
