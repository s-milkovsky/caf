import _ from 'lodash';


export default class MenuItemController {
  constructor($scope, $state, $stateParams, $timeout, BlockService) {
    'ngInject';

    this.$scope = $scope;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$timeout = $timeout;
    this.BlockService = BlockService;
    this.$showError = false;



    this.validatePatterns = {
      http: new RegExp('^(https?://)?(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\\.)+(?:[A-Z]{2,6}\\.?|[A-Z0-9-]{2,}\\.?)|localhost|\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(?::\\d+)?(?:/?|[/?][^ ]+)$', 'i')
    };

    $scope.$on('$menuSortEnd', () => {
      this.mapBlocks();
      this.$updateDiscount();
    });
  }

  $onInit() {
    this.$maxDiscount = 30;
    this.$errors = {};
    this.mapBlocks();
    this.$editOff();
    this.$timeout(() => {
      this.$scope.$on('document.click', () => {
        this.$scope.$apply(() => {
          if (this.$editedUrl || this.$editedTag) {
            this.save(null, this.$editedUrl ? 'url' : 'tag');
          }
          this.allPreValidate();
        });
      });

      this.$updateDiscount();
    }, 200);
  }

  allPreValidate() {
    if (this.item.novalidate) {
      delete this.item.novalidate;
      return;
    }

    if (this.item.url) {
      this.$validate('url');
    } else
    if (this.item.title && (this.$editedUrl || this.$editedTag)) {
      this.$showError = false;
    } else
    if (!this.item.block_ids || this.item.block_ids.length === 0) {
      this.$showError = true;
    } else
    if (this.item.block_ids && this.item.block_ids.length > 0) {
      this.$showError = false;
    } else {
      this.$showError = true;
    }

    if (!this.item.title) {
      this.$showError = true;
    }
  }


  mapBlocks() {

    if (!this.blocks || this.blocks.length === 0) {
      this.BlockService.list().then(result => {
        this.blocks = result;
        this.mapBlocks();
      });
      return;
    }


    if (this.item.block_ids && this.item.block_ids.length) {
      this.block_ids = this.item.block_ids.map(id => ({
        id,
        title: (_.find(this.blocks, { id }) || {}).title || null
      }));
      if (!this.block_ids[0].title) {
        this.item.block_ids.splice(0, 1);
        this.block_ids = null;
        this.mapBlocks();
      }
    } else {
      this.block_ids = null;
    }
  }

  $edit($event) {
    $event.preventDefault();
    this.item.url && this.$editUrl();
    this.block_ids && this.block_ids.length && this.$editTag();
  }

  $editUrl() {
    this.SettingsController.activeButton = this.item;
    this.$editedUrl = true;
    this.$editedTag = false;
    this.allPreValidate();
  }

  $editTag() {
    this.SettingsController.activeButton = this.item;
    this.$editedUrl = false;
    this.$editedTag = true;
    this.allPreValidate();
  }

  $editOff() {
    this.$editedUrl = false;
    this.$editedTag = false;
    this.$timeout(() => {
      this.allPreValidate();
    }, 100);
  }

  $updateDiscount($event) {
    this.$discount = this.$maxDiscount;
    if (this.item && this.item.title) {
      this.$discount = this.$maxDiscount - this.item.title.length;
    }
  }

  $validate(field) {
    this.$showError = false;
    switch (field) {
      case 'title':
        this.$errors.title = {};
        if (!this.item.title) {
          this.$errors.title = { message: 'Title is Empty', show: true };
          return false;
        }
        break;
      case 'url':
        this.$errors.url = {};
        if (this.item.url && !this.validatePatterns.http.test(this.item.url)) {
          this.$errors.url = { message: 'Invalid URL', show: true };
          this.$showError = true;
          return false;
        }
        break;
      default: { break; }
    }

    return true;
  }

  load($query) {
    return this.BlockService.list($query);
  }

  save($event, field) {
    $event && $event.preventDefault();

    if (!this.$validate(field)) {
      return;
    }

    this.$editedUrl && this.item.url && delete this.item.block_ids;
    this.$editedTag && delete this.item.url;
    this.$editOff();

    if (this.item.title) {
      if (!this.item.url) {
        this.item.block_ids = (this.block_ids || []).map(block => block.id);
      }

      //this.SettingsController.saveBotMenu();

      this.$scope. $emit('$saveBotMenu');
    }
  }

  remove($event) {
    $event.preventDefault();
    this.$editOff();
    this.$scope. $emit('$menuItemRemove', this.index);
  }
}
