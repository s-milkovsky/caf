import ng from 'angular';
import './assets/less/style.less';

import { Component } from './component';

export default ng.module('app.common.settings.menuItem', [])
  .directive('menuItem', Component)
  .name;
