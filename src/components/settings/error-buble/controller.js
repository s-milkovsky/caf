
export default class ErrorsController {
  constructor($scope) {
    'ngInject';
    this.$scope = $scope;
  }

  $onInit() {
    this.errorText = this.errors.errorText;
    this.buttonText = this.errors.buttonText;
    this.buttonUrl = this.errors.buttonUrl;
  }
  goToPage(buttonUrl) {
    if (buttonUrl) window.open(buttonUrl, '_blank');
  }
}
