import controller from './controller.js';

const Config = ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('app.settings', {
      url: '/settings',
      views: {
        'main@': {
          template: '<settings></settings>'
        }
      },
    });

};

export default Config;
