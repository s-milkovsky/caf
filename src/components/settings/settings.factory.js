export default ($http, StoreService) => {
  'ngInject';

  return {
    get,
    update
  };

  /**
   * get
   * @params {Number} id
   * @return {Object}
   */
  function get(id) {
    return $http({
      url: `${ StoreService.getApiUrl() }/bots/${ id }`,
      method: 'get'
    }).then(res => res.data.result);
  }

  /**
   * update
   * @params {Number} id
   * @params {Object} data
   * @return {Object}
   */
  function update(id, data) {
    return $http({
      url: `${ StoreService.getApiUrl() }/bots/${ id }`,
      method: 'post',
      data
    }).then(res => res.data.result);
  }
}
