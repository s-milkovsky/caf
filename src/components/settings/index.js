import angular from 'angular';
import ngRouter from 'angular-ui-router';
import Config from './config.js';
import SettingsFactory from './settings.factory.js';
import MenuItem from './menu-item';
import Admins from './admins';
import ErrorBuble from './error-buble';
import Settings from './component';
import PaymentsSettings from './payments';

export default angular
  .module('app.common.settings', [ngRouter, MenuItem, Admins, ErrorBuble, PaymentsSettings])
  .directive('settings', Settings)
  .factory('SettingsFactory', SettingsFactory)
  .config(Config)
  .name;
