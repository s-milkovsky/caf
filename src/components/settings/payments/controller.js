import _ from 'lodash';

export default class AdminsController {
  constructor($scope, $timeout, $element, ModalService, BotService) {
    'ngInject';

    this.$timeout = $timeout;
    this.$element = $element;
    this.ModalService = ModalService;
    this.BotService = BotService;

    this.payment_props = {
      merchant_name: '',
      merchant_emails_str: '',
      terms_of_service_url: ''
    };

    this.errors = [];

    this.validatePatterns = {
      url: new RegExp('^(https?://)?(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\\.)+(?:[A-Z]{2,6}\\.?|[A-Z0-9-]{2,}\\.?)|localhost|\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(?::\\d+)?(?:/?|[/?][^ ]+)$', 'i'),
      email: new RegExp('^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$', 'i')
    };

    $scope.$watch('vm.bot.id', () => {
      if (this.bot && this.bot.id) {
        this.botStatusPaymentEnabled = this.bot.status.payments_status === 'enabled';
   //     this.botStatusPaymentEnabled = false;
        if (this.botStatusPaymentEnabled) {
          this.getPaymentsFields();
        }
      }
    });
  }

  $onInit() {

  }

  getPaymentsFields() {
    this.BotService.getPayments(this.bot.id)
      .then(res => {
        if (!_.isEmpty(res.result)) {
          this.payment_props = res.result;

          this.payment_props.merchant_emails_str = this.payment_props.merchant_emails ? this.payment_props.merchant_emails.join(', ') : '';

          this.validate();
        } else {
          if (this.user) {
            this.payment_props =  {
              merchant_name: this.user.name,
              merchant_emails_str: this.user.email,
              terms_of_service_url: ''
            };
          }
        }
      });
  }

  openWizard() {
    this.ModalService
      .wizardPayments()
      .then(res => {
        if (res) {
          this.botStatusPaymentEnabled = true;
          this.getPaymentsFields();
        }
      });
  }

  setInpFocus() {
    if (this.advOpen) {
      this.$element[0].querySelector('input').focus();
    }
  }

  validate() {
    this.errors = [];

    this.payment_props.merchant_emails_str = this.payment_props.merchant_emails_str.trim();
    this.payment_props.terms_of_service_url = this.payment_props.terms_of_service_url.trim();

    if (this.payment_props.merchant_emails_str.length === 0 || !this.payment_props.merchant_emails_str.replace(/(^[ ,]+)|([ ,]+$)/, '').split(',').every(email => this.validatePatterns.email.test(email.trim()))) {
      this.errors.push('email');
    }

    if (this.payment_props.terms_of_service_url.length === 0 || !this.validatePatterns.url.test(this.payment_props.terms_of_service_url)) {
      this.errors.push('url');
    }

    if (this.payment_props.merchant_name.length === 0) {
      this.errors.push('name');
    }

    return this.errors.length === 0;
  }

  save() {
    this.$timeout.cancel(this.saveTimeout);
    if (this.validate()) {
      this.saveTimeout = this.$timeout(() => {
        const pProps = _.cloneDeep(this.payment_props);
        pProps.merchant_emails = _.compact(pProps.merchant_emails_str.split(/\s*,\s*/));
        delete pProps.merchant_emails_str;

        this.BotService.setPayments(this.bot.id, pProps);
      }, 500);
    }
  }
}
