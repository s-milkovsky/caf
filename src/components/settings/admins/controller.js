
export default class AdminsController {
  constructor($scope, $interpolate, $timeout, $rootScope, ModalService, BotService) {
    'ngInject';

    this.$interpolate = $interpolate;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$timeout = $timeout;

    this.ModalService = ModalService;
    this.BotService = BotService;


  }

  $onInit() {

  }

  showInvite() {
    this.BotService.getInvite().then((res) => {
      this.ModalService
        .confirmAdvInvite(
          res,
          {
            ok: 'DONE'
          }
        );
    });
  }

  remove(admin) {
    this.ModalService
      .confirmAdv('<p>Are you sure you want to remove<br/><b>' + admin.name + '</b> from chatbot admins?</p>',
        {
          ok: 'REMOVE',
          close: 'CANCEL'
        }
      )
      .then((res) => {
        if (res) {
          this.$scope.$emit('$botUnBindBegin');

          this.BotService.removeAdmin(admin.id).then(() => {
            if (this.bot.status.page_info && this.bot.status.page_info.owner && this.bot.status.page_info.owner.id === admin.id) {
              delete this.bot.status.page_info;
              this.bot.status.page = null;
              this.bot.status.status = 'draft';

              this.$rootScope.$broadcast('$updateBotStatus', { id: this.bot.id, status: this.bot.status.status, read_only: this.bot.status.read_only, pageId: this.bot.status.page, ownerId: null });
              this.$rootScope.$broadcast('$fbClickEvent', true);

              this.$scope.$emit('$botUnBindEd');
            }
          });


          this.bot.admins.forEach((item, i) => {
            if (item.id === admin.id) {
              this.bot.admins.splice(i, 1);
              return false;
            }
          });


        }
      });
  }
}
