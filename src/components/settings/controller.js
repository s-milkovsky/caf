import _ from 'lodash';
import moment from 'moment-timezone';
import confirmWizard from './templates/confirm-wizard.html';

export default class SettingsController {
  /**
   * constructor
   * @param  {Object} $stateParams
   * @param  {Object} SettingsFactory
   * @param  {Object} BotService
   * @param  {Object} data
   * @return {Object}
   */
  constructor($state, $window, $scope, $element, $stateParams, $rootScope, $timeout, $interpolate, $document, SettingsFactory, BotService, BlockService, PageService, UserService, ModalService, StoreService) {
    'ngInject';

    this.StoreService = StoreService;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$scope = $scope;
    this.$window = $window;
    this.$interpolate = $interpolate;
    this.$document = $document;
    this.$stateParams = $stateParams;
    this.$timeout = $timeout;
    this.$element = $element;

    this.data = SettingsFactory.get($stateParams.bot_id);

    this.className = 'settings';
    this.timeZones = moment.tz.names().filter(name => !name.startsWith('Etc/'));
    this.data.then((result) => {
      this.timeZone  = result.timezone_name || moment.tz.guess();
    });
    this.botId    = $stateParams.bot_id;
    this.confirmWizard = confirmWizard;

    this.SettingsFactory = SettingsFactory;
    this.BotService = BotService;
    this.PageService = PageService;
    this.UserService = UserService;
    this.ModalService = ModalService;
    this.BlockService = BlockService;

    this.selectedPage = null;

    this.pagesAccessed = false;
    this.pagesAccessReq = false;
    this.appId = -1;
    this.isChatfuelApp = true;
    this.botToPage = {};
    this.activeButton = {};

    $timeout(() => {
      this.getUserInfo();
      this.loadBotMenu();
    });

    this.tabEventHolder = this.getTabEventHolder();
    if (this.tabEventHolder) {
      this.$document.on(this.tabEventHolder.event, () => {
        if (this.needRefresh && !this.$document[0][this.tabEventHolder.state]) {
          this.pagesRefresh();
        }
      });
    }

    this.listeners = [];

    this.listeners.push($scope.$on('$destroy', () => {
      if (this.tabEventHolder) {
        this.$document.off(this.tabEventHolder.event);
      }
    }));

    this.listeners.push($scope.$on('$saveBotMenu', () => {
      this.saveBotMenu();
    }));

    this.listeners.push($scope.$on('$menuItemRemove', ($event, index) => {
      this.menuItemRemove(index);
    }));

    this.listeners.push($scope.$on('$botUnBindEd', () => {
      this.loadBot(true);
      this.alignBottom(true);
    }));

    this.listeners.push($scope.$on('$botUnBindBegin', () => {
      this.otherUserConnected = null;
    }));


    this.$sortable = {
      handle: '.ui-sortable-handle',
      axis: 'y',
      containment: '.persistent-menu',
      stop: (e, ui) => {
        this.saveBotMenu();
        this.$scope.$broadcast('$menuSortEnd');
      }
    };

    this.rootEl = angular.element(this.$element[0].firstChild);
    this.persistentMenuEl = angular.element(document.querySelector('.persistent-menu'));
    this.adminsEl = angular.element(document.querySelector('.admins'));
    this.timeZoneEl = angular.element(document.querySelector('.timezone-box'));
    this.connectToPageEl = angular.element(document.querySelector('.connect-to-page'));


    this.alignBottom();


    window.sTest = () => {
      console.log(this);
    }

  }


  $onDestroy() {
    this.$document.off(this.tabEventHolder.event);
    this.listeners.forEach(fn => fn.call());
  }

  alignBottom(silence) {
    this.$timeout(() => {

      let pos = 0;

      if (!silence) {
        this.bottomAlignLeft = (this.persistentMenuEl.height() + 60) < this.adminsEl.height();
      }

      if (this.bottomAlignLeft) {
        this.rootEl.removeClass('bottom-align-right');
        this.rootEl.addClass('bottom-align-left');
        pos = this.persistentMenuEl.height() + this.connectToPageEl.height() + 148 + 60;
      } else {
        this.rootEl.removeClass('bottom-align-left');
        this.rootEl.addClass('bottom-align-right');
        pos = this.adminsEl.height() + this.connectToPageEl.height() + 148;
      }

      // this.timeZoneEl.css({
      //   top: pos,
      //   bottom: 'auto'
      // });

    });
  }

  getUserInfo() {
    this.UserService.show(true)
      .then((res) => {
        this.user = res;



        if (res.pages_access) {
          this.pagesAccessed = true;
     //     this.pagesAccessed = false;
          if (this.pagesAccessReq) {
            this.pagesAccessReq = false;
            this.$rootScope.$emit('$statEvent', { event: 'grantpagesaccess' });
          }
        }

      //  this.pagesAccessed = false;

        this.testPagesAccessReqButton();

        this.getPagesList();
      });
  }

  getPagesList() {
    this.PageService.cachedList(Math.random())
      .then((res) => {
        this.pages = res;
    //    this.pages = [];
        this.$rootScope.$emit('$statEvent', { event: 'pagesloaded', data: this.pages ? this.pages.length : 'error' });
        this.loadBot(true);
      });
  }

  loadBot(silence) {
    this.BotService.cachedList(true)
      .then(bots => {
        this.bot = bots.find(bot => bot.id === this.botId);

    //    this.otherUserConnected = this.bot.admins[0];

        if (this.bot.status.page_info && this.bot.status.page_info.owner && this.bot.status.page_info.owner.id !== this.user.id) {
          this.otherUserConnected = this.bot.status.page_info.owner;
        } else {
          this.otherUserConnected = null;
          if (this.pages) {
            bots.forEach((lBot) => {
              let lPage;
              if (lBot.status.page) {
                lPage = this.pages.find(page => page.id === lBot.status.page);
                if (lPage) {
                  this.botToPage[lPage.id] = lBot;
                }
              }
            });
          }


          if (this.bot.status.status !== 'draft' && this.bot.status.page) {
            this.loadAppId(silence);
            this.page = this.pages.find(page => page.id === this.bot.status.page);
          } else {
            this.appId = 0;
            this.testPagesAccessReqButton();
          }


        }

        this.alignBottom(silence);
      });
  }

  loadAppId(silence) {
    this.PageService.getAppId(this.bot.status.page)
      .then(data => {
        this.appId = data.id;
        this.isChatfuelApp = data.chatfuel_app;
        if (this.isChatfuelApp) {
          this.selectedPage = this.bot.status.page;
        } else {
          this.appUrl = 'https://developers.facebook.com/sa/apps/' + data.id + '/review-status/';
        }

        this.alignBottom(silence);
      }).catch((err) => {
        this.appId = 0;
        this.testPagesAccessReqButton();
      });

  }

  bind(event, selectedPage) {
    this.showError = false;
    this.connecting = true;
    this.pageIdTemporary = selectedPage;
    this.PageService.bind(selectedPage, this.botId)
    .then(() => {
      this.botToPage[this.bot.status.page] = null;

      this.bot.status.page = selectedPage;
      this.appId = 0;
      this.isChatfuelApp = true;
      this.botToPage[this.bot.status.page] = this.bot;
      this.page = this.pages.find(page => page.id === this.bot.status.page);

      this.$rootScope.$broadcast('$fbClickEvent', true);
      this.$rootScope.$emit('$statEvent', { event: 'connectedpage' });
      this.setBotStatus('published');
      this.connecting = false;
      this.pageIdTemporary = '';
    })
    .catch((e) => {

      if (e.data.errors.length && e.data.errors[0].error_text) {
        this.showError = true;
        this.connecting = false;
        this.pageIdTemporary = '';

        this.errorData = {
          errorText: e.data.errors[0].error_text
        };
        if (e.data.errors[0].error_button) {
          this.errorData.buttonText = e.data.errors[0].error_button.title;
          this.errorData.buttonUrl = e.data.errors[0].error_button.url;
        }
      }
    });
  }

  unBind(event) {
    this.ModalService
      .confirmAdv(
        this.isChatfuelApp?
          '<p>Are you sure you want to disconnect this chatbot from <b>' + this.page.title + '</b> Page?</p>':
          '<p>Are you sure you want to disconnect this chatbot from your Facebook App?</p>',
        {
          ok: 'DISCONNECT',
          close: 'CANCEL'
        }
      )
      .then((res) => {
        if (res) {
          this.appId = 0;
          this.selectedPage = null;
          this.isChatfuelApp = true;

          this.PageService.unbind(this.bot.status.page).then(() => {
            this.$rootScope.$broadcast('$fbClickEvent', true);
            this.$rootScope.$emit('$statEvent', { event: 'disconnectedpage' });
          });
          this.botToPage[this.bot.status.page] = null;
          this.bot.status.page = null;
          this.setBotStatus('draft');
          this.testPagesAccessReqButton();
        }
      });
  }

  unBindBot() {
    this.ModalService
      .confirmAdv(
        '<p>Are you sure you want to disconnect this chatbot from <b>' + this.bot.status.page_info.title + '</b> Page?</p>',
        {
          ok: 'DISCONNECT',
          close: 'CANCEL'
        }
      )
      .then((res) => {
        if (res) {
          this.otherUserConnected = null;
          this.bot.status.page = null;
          this.setBotStatus('draft');

          this.BotService.unbindBot().then(() => {
            this.loadBot();
            this.$rootScope.$broadcast('$fbClickEvent', true);
          });
        }
      });
  }

  setBotStatus(status) {
    this.bot.status.status = status;

    this.BotService.update(this.bot.id, {
      timezone_offset: this.bot.timezone_offset,
      timezone_name: this.bot.timezone_name,
      title: this.bot.title,
      read_only: false,
      status: this.bot.status.status
    });

    this.$rootScope.$broadcast('$updateBotStatus', { id: this.bot.id, status: this.bot.status.status, read_only: this.bot.status.read_only, pageId: this.bot.status.page, ownerId: (this.bot.status.page?this.user.id:null), picture: (this.page?this.page.picture:null) });
  }

  goWizard() {
    this.ModalService
      .confirmAdv(
        this.$interpolate(this.confirmWizard())(),
        {
          ok: 'CREATE',
          close: 'CANCEL'
        }
      )
      .then((res) => {
        if (res) {
          if (this.bot.status.page) {
            this.PageService.unbind(this.bot.status.page);
          }
          this.appId = 0;
          this.selectedPage = null;
          this.isChatfuelApp = true;
          this.botToPage[this.bot.status.page] = null;
          this.bot.status.page = null;
          this.setBotStatus('draft');

          this.$rootScope.backStateName = this.$state.current.name;
          this.$state.go('wizard.page1', { bot_id: this.botId });
        }
      });
  }

  pagesRefresh() {
    this.needRefresh = false;
    this.appId = -1;
    this.getPagesList();
  }

  getTabEventHolder() {
    var stateKey, keys = {
      hidden: "visibilitychange",
      webkitHidden: "webkitvisibilitychange",
      mozHidden: "mozvisibilitychange",
      msHidden: "msvisibilitychange"
    };
    for (stateKey in keys) {
      if (stateKey in document) {
        return {
          state: stateKey,
          event: keys[stateKey]
        };
      }
    }

    return false;
  }

  setNeedRefresh() {
    this.needRefresh = true;
  }

  requestPagesAccess() {
    this.$rootScope.$emit('$statEvent', { event: 'hit', data: (window.location.protocol + '//' + window.location.hostname + '/#settings-connect-to-facebook-now-click') });

    this.pagesAccessReq = true;
    this.$rootScope.$emit('$statEvent', { event: 'requestpagesaccess' });
    this.$timeout(() => {
      this.fbWindows = window.open(this.StoreService.getApiUrl().replace(/\/+$/, '') + '/access_pages?bot=' + this.$stateParams.bot_id, '_blank');
      this.fbWindows.focus();

      this.$timeout(() => {
        this.watchUserFbAccess();
      }, 1000);
    }, 300);
  }

  watchUserFbAccess() {
    this.UserService.show(true)
      .then((res) => {
        this.user = res;
        if (res.pages_access) {
          this.fbWindows.close();
          window.focus();
          this.pagesAccessed = true;
          if (this.pagesAccessReq) {
            this.pagesAccessReq = false;
            this.$rootScope.$emit('$statEvent', { event: 'grantpagesaccess' });
          }

          this.pagesRefresh();
        } else {
          this.$timeout(() => {
           this.watchUserFbAccess();
          }, 500);
        }
      });
  }


  /**
   * updateTimeZone
   * @return undefined
   */
  updateTimeZone() {
    this.SettingsFactory.update(this.botId, {
      timezone_offset: +moment().tz(this.timeZone).utcOffset() * 60,
      timezone_name: this.timeZone
    })
      .then(() => {
        // refresh list of bots
        return this.BotService.cachedList(true);
      });
  }


  testPagesAccessReqButton() {
    if (!this.pagesAccessed && this.appId === 0) {
      this.$rootScope.$emit('$statEvent', { event: 'pagesaccessreqbuttonshow' });

      this.$timeout(() => {
        let ok = true;
        const el = document.getElementById('requestPagesAccessButton');
        if (el) {
          const cs = getComputedStyle(el);
          ok = (el.innerHTML === 'CONNECT TO FACEBOOK PAGES' && cs.display !== 'none' && cs.visibility !== 'hidden');
        } else {
          ok = false;
        }

        this.$rootScope.$emit('$statEvent', { event: 'pagesaccessreqbuttonstatus', data: ok ? 'true' : 'false' });
      }, 3000);
    }
  }

//// Persistent menu

  menuItemAdd() {
    if (!this.menu) {
      this.menu = [];
    }

    this.menu.unshift({ title: '', novalidate: true });

    this.$timeout(() => {
      this.$scope.$broadcast('$menuSortEnd');
      this.alignBottom(true);
    });
  }

  menuItemRemove(index) {
    this.menu.splice(index, 1);
    this.$timeout(() => {
      this.$scope.$broadcast('$menuSortEnd');
      this.alignBottom(true);
    });
    this.saveBotMenu();
  }

  loadBotMenu() {
    this.BotService.getMenu().then((res) => {
      this.menu = res ? res : [];
      this.alignBottom();
    });
  }

  // loadBlocks($query) {
  //   return this.BlockService.list($query);
  // }

  saveBotMenu() {
    this.$timeout(() => {
      this.BotService.setMenu(this.menu);
      this.alignBottom(true);
    });
  }
  goToPage(pageId) {
    window.open(`https://www.facebook.com/${pageId}`, '_blank');
  }
}
