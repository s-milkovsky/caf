import template from './templates/template.html';
import { AsideController as controller } from './controller';

export const Component = () => ({
  template,
  controller,
  controllerAs: 'aside', // vm overided
  restrict: 'E',
  replace: true
});
