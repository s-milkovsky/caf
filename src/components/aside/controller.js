export class AsideController {
  constructor($scope, $state, $rootScope, $timeout, BlockService, StoreService, API_ERROR_MSG, AsideGroupService) {
    'ngInject';

    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$timeout = $timeout;
    this.AsideGroupService = AsideGroupService;
    this.BlockService = BlockService;
    this.StoreService = StoreService;
    this.$state = $state;
    this.API_ERROR_MSG = API_ERROR_MSG;

    this.listeners = [];
    this.blocks = [];

    this.groups = StoreService.get('groups');




    this.listeners.push(
      $rootScope.$on('block:remove', (event, id) => {
        this.groups.forEach(group => {
          const i = group.blocks.findIndex(item => item.id === id);

          if (i !== -1) {
            group.blocks.splice(i, 1);
          }
        });
      })
    );

    this.listeners.push(
      $rootScope.$on('block:update', (event, id, title, referral, referralActive) => {
        this.groups.forEach(group => {
          group.blocks.forEach(block => {
            if (block.id === id) {
              block.title = title;
              block.referral = referral;
              block.referral_active = referralActive;
            }
          });
        });
      })
    );

    this.listeners.push(
      $scope.$on('$pluginHasErrorValidation', (e, data) => {
        // this.setBlockValidateStatus(data.blockId);
      })
    );

    this.listeners.push(
      $scope.$on('$pluginUpdated', (e, data, cards) => {
        this.setBlockValidateStatus(data.blockId, cards);
      //  console.log(data.blockId, cards);
      })
    );


    this.$sortableGroups = {
      handle: '.v-drag',
      axis: 'y',
      revert: 240,
      distance: 6,
      tolerance: 'pointer',
      animation: 480,
      forceHelperSize: true,

      stop: (e, ui) => {
        const group = ui.item.sortable.model;
        group.position = this.groups.findIndex(item => item === group);
        this.AsideGroupService.update(group.id, group);
      }
    };
  }

  $onInit() {


  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  setBlockValidateStatus(blockId, cards) {
    if (!blockId) {
      blockId = this.$state.params.id;
    }
    this.groups.forEach(item => {
      const block = item.blocks.find(it => it.id === blockId);
      if (block) {
        block.is_valid = cards.every(item => item.is_valid === true);
      }
    });
  }

  addGroup() {
    let i = 1
      , title;
    while (true) {
      title = 'New Group ' + i++;
      if (!this.groups.find(item => item.title === title)) {
        break;
      }
    }

    this.AsideGroupService.create({ title })
      .then(group => {
        console.log('aside/controller.js addGroup()', group)
        group.blocks = [];
        this.$rootScope.$emit('group.blocks:update', group.blocks);
        this.groups.push(group);

        this.$timeout(() => {
          const inp = document.querySelectorAll('.groups-list input');

          inp[inp.length - 1].select(false);
        });
      });
  }


}
