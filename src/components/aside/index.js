import ng from 'angular';
import './assets/less/style.less';
import 'angular-ui-sortable';

import asideGroup from './aside-group';

import { Component } from './components';

export default ng.module('app.components.aside', ['ui.sortable', asideGroup])
  .directive('aside', Component)
  .name;
