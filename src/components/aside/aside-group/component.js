import template from './aside-group.html';
import controller from './controller';

export default () => ({
  restrict: 'E',
  scope: {
    group: '=',
    groups: '='
  },
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true,
  replace: true
});
