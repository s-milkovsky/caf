export default class AsideGroupController {
  constructor(
    $scope,
    $rootScope,
    $element,
    $state,
    $stateParams,
    $interpolate,
    $timeout,
    $document,
    $window,
    AsideGroupService,
    BlockService,
    BotService,
    ModalService,
    PluginCardService,
    API_ERROR_MSG) {
    'ngInject';
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$element = $element;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$timeout = $timeout;
    this.$interpolate = $interpolate;
    this.$document = $document;
    this.$error = '';
    this.AsideGroupService = AsideGroupService;
    this.BlockService = BlockService;
    this.ModalService = ModalService;
    this.BotService = BotService;
    this.PluginCardService = PluginCardService;
    this.API_ERROR_MSG = API_ERROR_MSG;

    this.$element.on('click', () => {
      document.activeElement.blur();
    });

    $document.on('mousedown', () => {
      this.$scope.$apply(() => {
        this.$showContext = null;
      });
    });

    this.bigWindow = (window.innerWidth > 1340);

    this.$titleMaxLength = 25;

    this.$sortableBlock = {
      revert: 200,
      distance: 6,
      tolerance: 'pointer',
      animation: 200,
      forceHelperSize: true,
      connectWith: '.blocks-list-connect',
      containment: '.aside',
      cursor: 'grabbing',

      stop: (e, ui) => {
        let block = ui.item.sortable.model;

        for (let i = 0, l = this.groups.length; i < l; i++) {
          let group = this.groups[i]
            , index = group.blocks.findIndex(item => item === block);

          if (index !== -1) {
            block.position = index;
            block.parent_group = group.id;
            this.BlockService.update(block);
            break;
          }
        }
        window.localStorage.setItem('updateBlocks', 'event' + Math.random());
      }
    };

    this.$sortableBlockBuiltin = {
      disabled: true,
      revert: 200,
      distance: 6,
      tolerance: 'pointer',
      animation: 200,
      forceHelperSize: true,
      containment: '.aside',
      cursor: 'grabbing',

      stop: (e, ui) => {
        let block = ui.item.sortable.model;

        for (let i = 0, l = this.groups.length; i < l; i++) {
          let group = this.groups[i]
            , index = group.blocks.findIndex(item => item === block);

          if (index !== -1) {
            block.position = index;
            block.parent_group = group.id;
            this.BlockService.update(block);
            break;
          }
        }
      }
    };
  }

  $onInit() {
    this.updateDiscount();
  }

  $onDestroy() {
    this.$document.off('mousedown');
    this.$element.off('click');
  }

  updateDiscount() {
    this.$discount = this.$titleMaxLength - this.group.title.length;
  }


  go(id) {
    this.$state.go('app.structure.block', { id });
  }

  toEdit() {
    if (this.group.builtin) {
      return;
    }
    this.edited = true;
    this.$timeout(() => this.$element.find('input')[0].focus());
  }

  updateGroup() {
    this.AsideGroupService
      .update(this.group.id, {
        title: this.group.title,
        position: this.groups.findIndex(item => item === this.group)
      })
      .then(() => {
        this.edited = false;
      })
      .catch(err => {
        this.$error = err.data.message;
      });
  }

  onDeleteGroup(e) {
    e.preventDefault();

    if (!this.group.blocks || !this.group.blocks.length) {
      return this.deleteGroup();
    }
    let content;
    if (this.group.blocks.length >= 10) {
      content = this.$interpolate(this.ModalService.templates.groupMoreTen())({
        group: this.group.title
      });
    } else {
      content = this.$interpolate(this.ModalService.templates.group())({
        group: this.group.title,
        blocks: (this.group.blocks || []).map(g => g.title).join(', ')
      });
    }
    this.ModalService
      .confirm(content)
      .then(isConfirmed => isConfirmed && this.deleteGroup());
  }

  deleteGroup() {
    this.AsideGroupService
      .remove(this.group.id)
      .then(res => {
        const i = this.groups.findIndex(item => item.id === this.group.id);
        if (i !== -1) {
          this.groups.splice(i, 1);
        }

        window.localStorage.setItem('updateBlocks', 'event' + Math.random());
      });
  }

  addBlock() {
    this.BlockService.save({ parent_group: this.group.id })
      .then(block => {
        this.group.blocks || (this.group.blocks = []);
        block.title = `Untitled block ${block.title.match(/(\d+)/g)[0]}`;
        this.group.blocks.push(block);
        this.$state.go('app.structure.block', { id: block.id, new: true, title: block.title });
        window.localStorage.setItem('updateBlocks', 'event' + Math.random());

        console.log('aside/aside-group/.controlle.js - addBlock()', this.group.blocks)
      });
  }

  handleClick($event, blockId) {
    if ($event.which === 3) {
      this.$timeout(() => {
        this.$showContext = blockId;
      });
    }
  }

  keyDown(event) {
    if (event.keyCode === 13) {
      event.target.blur();
    }
  }

  duplicateBlock(blockId) {
    this.$showContext = null;

    this.BlockService.show(blockId).then(targetBlock => {

      let blockTitle;
      let i = 1;

      const lGroup = this.group;

      while (true) {
        blockTitle = `${targetBlock.title} Copy${i++}`;
        if (!lGroup.blocks.find(items => items.title === blockTitle)) {
          break;
        }
      }

      this.BlockService.save({ parent_group: this.group.id, title: blockTitle })
        .then(block => {

          block.cards = targetBlock.cards;
          this.$rootScope.$emit('block:update', block.id, blockTitle);

          let saveCardCount = 0;

          targetBlock.cards.forEach(elem => {
            elem.blockId = block.id;
            delete elem.id;

            saveCardCount ++;

            this.PluginCardService.save(elem).then(() => {
              saveCardCount --;
              if (!saveCardCount) {
                this.$state.go('app.structure.block', { id: block.id });
              }
            }, err => {
              saveCardCount --;
              if (!saveCardCount) {
                this.$state.go('app.structure.block', { id: block.id });
              }
            });
          });

          if (saveCardCount === 0) {
            this.$state.go('app.structure.block', { id: block.id });
          }

          lGroup.blocks.push(block);
          window.localStorage.setItem('updateBlocks', 'event' + Math.random());
        });
    });
  }

  deleteBlock(blockId) {
    this.$showContext = null;
    const context = this.$interpolate(this.ModalService.templates.block())();
    this.ModalService
      .confirm(context)
      .then(isConfirmed => {
        if (isConfirmed) {
          this.BlockService.remove(blockId)
            .then(() => {
              this.$scope.$emit('block:remove', blockId);

              if (this.$stateParams.id === blockId) {
                try {
                  if (window.localStorage) {
                    window.localStorage.removeItem('structure-params-' + this.$stateParams.bot_id);
                  }
                } catch (err){}

                this.BotService.show()
                  .then(bot => this.$state.go('app.structure.block', { bot_id: this.$stateParams.bot_id, id: bot.first_block.id }));
              }

              window.localStorage.setItem('updateBlocks', 'event' + Math.random());
            });
        }
      });
  }

}
