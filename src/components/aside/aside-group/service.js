
export default ($http, StoreService, $stateParams) => {
  'ngInject';

  return {
    list(botId) {
      // some problem to get bot_id in resolve section of ui.router
      if (!botId) {
        botId = $stateParams.bot_id;
      }

      return $http({
        url: `${ StoreService.getApiUrl() }/bots/${ botId }/groups`,
        method: 'get'
      })
        .then(res => {
        //  StoreService.set('groups', res.data.result);
          return res.data.result;
        });
    },

    /**
     * save
     * @param {String} groupId
     * @param {Object} data
     * @return {Object}
     */
    update(groupId, data) {
      
      return $http({
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/groups/${ groupId }`,
        method: 'post',
        data: data
      }).then(res => {
        data.id = res.data.result;
        return data;
      });
    },

    create(data) {
      return $http({
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/groups`,
        method: 'post',
        data: data
      }).then(res => {
        data.id = res.data.result;
        return data;
      });
    },

    remove(id) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/groups/${ id }/delete`
      })
        .then(response => response.data);
    }
  };
}
