import 'jquery-ui';
import ng from 'angular';
import 'angular-ui-sortable';
import AsideGroupService from './service';
import asideGroupComponent from './component';

export default ng.module('app.aside.asideGroup', ['ui.sortable'])
  .factory('AsideGroupService', AsideGroupService)
  .directive('asideGroup', asideGroupComponent)
  .name;
