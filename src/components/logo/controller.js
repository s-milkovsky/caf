export default class LogoController {
  constructor(StoreService) {
    'ngInject';
    this.StoreService = StoreService;
    this.rootLink = StoreService.getMainUrl();
  }

  setGoOutCookie() {
    this.StoreService.setGoOutCookie(true);
  }
}
