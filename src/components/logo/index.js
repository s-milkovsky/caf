import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.logo', [])
  .directive('logo', Component)
  .name;
