export default class FaqController {
  constructor($window, $scope, $timeout, $rootScope) {
    'ngInject';
    this.$window = $window;
    this.$timeout = $timeout;
    this.$rootScope = $rootScope;

    this.faqPresed = false;

    $scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.faqPresed = false;
      });
    });
  }
  goToFaqPage(path) {
    this.$timeout(() => {
      if (path === 'faq') {
        this.$window.open('https://m.me/310524615991980');
      } else
      if (path === 'community') {
        this.$window.open('https://community.chatfuel.com');
      } else
      if (path === 'tutorial') {
        this.$rootScope.$emit('$openTutorial');
      }
    }, 200);
  }
}
