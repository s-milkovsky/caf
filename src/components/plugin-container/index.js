import ng from 'angular';
import ngRouter from 'angular-ui-router';

import PluginContainerComponent from './components';

export default ng.module('app.components.pluginContainer', [ngRouter])
  .directive('pluginContainer', PluginContainerComponent)
  .name;
