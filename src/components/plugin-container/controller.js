import $ from 'jquery';

export default class PluginContainerController {
  constructor($scope, $state, $rootScope) {
    'ngInject';

    this.$scope = $scope;
    this.$state = $state;


    this.firstSort = true;


    this.$sortable = {
      //  handle: '.ui-sortable-handle',
    //  revert: 200,
      distance: 6,
      tolerance: 'pointer',
      animation: 200,
      cursor: 'ns-resize',
      containment: 'parent',
      forceHelperSize: true,
      forcePlaceholderSize: true,
      'ui-floating': true,
      start: (e, ui) => {
        if (this.firstSort) {
          $(e.target).sortable('refreshPositions');
          this.firstSort = false;
        }
      },
      axis: 'y',
      stop: (e, ui) => {
        this.$scope.$broadcast('buttons-sort-stop');
      }
    };

  }

  $onInit() {
    this.testShowQuickReplies();
    this.scopeListener = this.$scope.$on('$buttonUpdate', () => {
      this.testShowQuickReplies();
    });
  }

  $onDestroy() {
    this.scopeListener();
  }

  testShowQuickReplies() {
    // if (this.$state.current.name.indexOf('.broadcast.') !== -1) {
    //   this.showQuickReplies = false;
    //   return;
    // }

    this.showQuickReplies = true;

    if (this.plugin.config.gallery_cards) {
      this.plugin.config.gallery_cards.forEach(item => {
        if (item.buttons && item.buttons.length > 0) {
          this.showQuickReplies = false;
        }
      });

      if (this.plugin.config.quick_replies && this.plugin.config.quick_replies.buttons && this.plugin.config.quick_replies.buttons.length > 0) {
        this.height = 264;
      } else {

        if (
          this.plugin.config.gallery_cards[this.plugin.config.gallery_cards.length - 1].buttons.length === 1 &&
          this.plugin.config.gallery_cards[this.plugin.config.gallery_cards.length - 1].buttons[0].payment_summary
        ) {
          this.height = 313;
        } else {
          this.height = 313 + (this.plugin.config.gallery_cards[this.plugin.config.gallery_cards.length - 1].buttons.length) * 49;

          if (this.plugin.config.gallery_cards[this.plugin.config.gallery_cards.length - 1].buttons.length === 3) {
            this.height = this.height - 49;
          }
        }
      }


    } else

    if (this.plugin.config.buttons && this.plugin.config.buttons.length > 0) {
      this.showQuickReplies = false;
    }
  }

}
