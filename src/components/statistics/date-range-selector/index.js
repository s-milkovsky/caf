import ng from 'angular';
import './assets/less/date-range-selector.less';

import datepicker from 'angular-ui-bootstrap/src/datepicker';
import dayPickerTemplateUrl from './daypicker.tpl';

import { Component } from './component';

export default ng.module('app.common.statistics.dateRangeSelector', [datepicker])
  .directive('dateRangeSelector', Component)
  .config($provide => {
    'ngInject';
    $provide.decorator('uibDaypickerDirective', $delegate => {
      const directive = $delegate[0];
      directive.templateUrl = dayPickerTemplateUrl;
      return $delegate;
    });
  })
  .name;
