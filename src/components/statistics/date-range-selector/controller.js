import moment from 'moment';
import _ from 'lodash';

export default class DateRangeSelectorController {
  constructor($scope) {
    'ngInject';

    this.$scope = $scope;

    const endOfDay = moment().endOf('day');
    const t = this;

    const customClass = dt => {
      if (dt.date < t.minDate) {
        return 'prev-day2';
      } else
      if (dt.date > endOfDay) {
        return 'future-day';
      } else if (dt.date >= t.dateStart && dt.date <= t.dateEnd) {
        let out = 'range-day';
        if (dt.date.valueOf() === t.dateStart.valueOf()) {
          out += ' range-day-start';
        }
        if (dt.date.valueOf() === t.dateEnd.valueOf()) {
          out += ' range-day-end';
        }
        return out;
      }
    };

    this.$datepickerStart = {
      datepickerMode: 'day',
      showWeeks: false,
      maxMode: 'day',
      minDate: this.minDate,
      maxDate: endOfDay,
      customClass
    };

    this.$datepickerEnd = {
      datepickerMode: 'day',
      showWeeks: false,
      maxMode: 'day',
      minDate: this.minDate,
      maxDate: endOfDay,
      customClass
    };

    $scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.cancel();
      });
    });

    $scope.$watch('vm.minDate', () => {
      if (this.minDate) {
        this.minDate = new Date(0);
      }

      this.$datepickerStart.minDate = this.minDate;
      this.$datepickerEnd.minDate = this.minDate;
    })

    this.setRange('week');
  }

  $onInit() {
  }

  dateStartChange() {
    if (this.dateStart > this.dateEnd) {
      this.dateEnd = this.dateStart;
    } else {
      this.dateEnd = _.clone(this.dateEnd);
    }
  }

  dateEndChange() {
    if (this.dateStart > this.dateEnd) {
      this.dateStart = this.dateEnd;
    } else {
      this.dateStart = _.clone(this.dateStart);
    }
  }

  setRange(range) {
    const curDay = moment().endOf('day');
    this.dateEnd = curDay.clone().toDate();
    switch(range) {
      case 'week':
        this.dateStart = curDay.subtract(6, 'days').clone().toDate();
        break;
      case 'month':
        this.dateStart = curDay.subtract(1, 'months').clone().toDate();
        break;
      case 'quarter':
        this.dateStart = curDay.subtract(1, 'quarter').clone().toDate();
        break;
    }
    this.start = this.dateStart;
    this.end = this.dateEnd;
  }

  testRange(range) {
    const curDay = moment().endOf('day');
    if (this.dateEnd.valueOf() === curDay.clone().toDate().valueOf()) {
      switch (range) {
        case 'week':
          return (this.dateStart.valueOf() === curDay.subtract(6, 'days').clone().toDate().valueOf());
        case 'month':
          return (this.dateStart.valueOf() === curDay.subtract(1, 'month').clone().toDate().valueOf());
        case 'quarter':
          return (this.dateStart.valueOf() === curDay.subtract(1, 'quarter').clone().toDate().valueOf());
        default:
      }
    }
    return false;
  }

  testShowRange(range) {
    const curDay = moment().endOf('day');
    if (this.minDate) {
      switch (range) {
        case 'week':
          return (this.minDate.valueOf() <= curDay.subtract(6, 'days').clone().toDate().valueOf());
        case 'month':
          return (this.minDate.valueOf() <= curDay.subtract(1, 'month').clone().toDate().valueOf());
        case 'quarter':
          return (this.minDate.valueOf() <= curDay.subtract(1, 'quarter').clone().toDate().valueOf());
        default:
      }
    }
    return true;
  }

  elClick($event) {
    $event.stopPropagation();
    this.openPopup = true;
  }

  doneClick($event) {
    $event.stopPropagation();
    this.openPopup = false;

    this.start = this.dateStart;
    this.end = this.dateEnd;
  }

  cancel() {
    this.openPopup = false;
    this.dateStart = this.start;
    this.dateEnd = this.end;
  }
}
