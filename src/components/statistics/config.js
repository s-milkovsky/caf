const Config = ($stateProvider) => {
  'ngInject';

  $stateProvider
    .state('app.statistics', {
      url: '/statistics',
      views: {
        'main@': { template: '<statistics></statistics>' }
      }
    });
};

export default Config;
