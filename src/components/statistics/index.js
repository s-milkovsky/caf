import angular from 'angular';
import ngRouter from 'angular-ui-router';
import 'angular-chart.js';

import './assets/less/statistics.less';

import dateRangeSelector from './date-range-selector';


import Component from './component.js';
import Config from './config.js';

export default angular
  .module('app.common.statistics', [ngRouter, 'chart.js', dateRangeSelector])
  .directive('statistics', Component)
  .config(Config)
  .config(['ChartJsProvider', (ChartJsProvider) => {
    ChartJsProvider.setOptions({
      responsive: true,
      colors : [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
    });
  }])
  .name;
