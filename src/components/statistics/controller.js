import moment from 'moment';

export default class StatisticsController {
  constructor($scope, $timeout, $state, $stateParams, PageService, BlockService, BotService, ModalService, $http, StoreService) {
    'ngInject';
    this.$timeout = $timeout;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.PageService = PageService;
    this.BlockService = BlockService;
    this.BotService = BotService;
    this.ModalService = ModalService;
    this.StoreService = StoreService;
    this.connected = false;
    this.$http = $http;
    this.totalUsersLoading = true;
    this.userRetentionLoading = true;
    this.userActivityLoading = true;
    this.popularBlocksLoading = true;
    this.popularButtonsLoading = true;
    this.popularInputsLoading = true;
    this.referralsLoading = true;
    this.loading = true;
    this.className = 'statistics-container';

    this.startDate = new Date();
    this.endDate = new Date();

    $scope.$watchGroup(['vm.startDate', 'vm.endDate'], () => {
      if (this.has_botan_app) {
        this.totalUsersLoading = true;
        this.userRetentionLoading = true;
        this.userActivityLoading = true;
        this.popularBlocksLoading = true;
        this.popularButtonsLoading = true;
        this.popularInputsLoading = true;
        this.referralsLoading = true;

        this.getTotalUsers();
        this.getRetention();
        this.getEventUsers();
        this.getUsage();
        this.getReferrals();
      }
    });

  }

  $onInit() {
    this.grafToolTip = angular.element(document.querySelector('.custom-graf-tooltip'));
    this.genPreview()

    this.userData = [
      { activity: 'Menu', rate: '16,657,757' },
      { activity: 'Contacts', rate: '36,747' },
      { activity: 'Hot deal of the day', rate: '647' },
      { activity: 'Default', rate: '563' },
      { activity: 'Message us', rate: '71' },
      { activity: 'Francisco amonio shuppa-ruppa!', rate: '36' },
      { activity: 'The most interesting fact about George Bush', rate: '2' },
      { activity: 'Truth about Kraken', rate: '1' }
    ];
    this.userRetention = [
      { date: 'TOTAL', users: 1037, day1: '57.1%', day2: '11.7%', day3: '24.7%', day4: '2%', day5: '1%', day6: '18%', day7: '6%' },
      { date: 'OCT 14', users: 121, day1: '63.1%', day2: '18.7%', day3: '24.7%', day4: '13%', day5: '25%', day6: '62%', day7: '6%' },
      { date: 'OCT 15', users: 137, day1: '25.1%', day2: '10.7%', day3: '10.7%', day4: '9%', day5: '5%', day6: '2%', day7: '6%' },
      { date: 'OCT 16', users: 17, day1: '32.1%', day2: '10.7%', day3: '10.7%', day4: '9%', day5: '5%', day6: '8%', day7: '3%' },
      { date: 'OCT 17', users: 51, day1: '66.1%', day2: '2.7%', day3: '10.7%', day4: '19%', day5: '5%', day6: '2%', day7: '6%' },
      { date: 'OCT 18', users: 32, day1: '25.1%', day2: '10.7%', day3: '10.7%', day4: '9%', day5: '10%', day6: '9%', day7: '6%' },
      { date: 'OCT 19', users: 52, day1: '45.1%', day2: '20.7%', day3: '10.7%', day4: '9%', day5: '5%', day6: '2%', day7: '11%' },
      { date: 'OCT 20', users: 137, day1: '25.1%', day2: '10.7%', day3: '33.7%', day4: '1%', day5: '5%', day6: '2%', day7: '6%' },
      { date: 'OCT 21', users: 17, day1: '32.1%', day2: '1.7%', day3: '10.7%', day4: '9%', day5: '5%', day6: '11%', day7: '6%' },
      { date: 'OCT 22', users: 51, day1: '25.1%', day2: '10.7%', day3: '10.7%', day4: '18%', day5: '5%', day6: '2%' },
      { date: 'OCT 23', users: 32, day1: '25.1%', day2: '10.7%', day3: '31.7%', day4: '5%', day5: '5%' },
      { date: 'OCT 24', users: 52, day1: '45.1%', day2: '20.7%', day3: '10.7%', day4: '9%' },
      { date: 'OCT 25', users: 22, day1: '36.1%', day2: '10.7%', day3: '10.7%' },
      { date: 'OCT 26', users: 34, day1: '25.1%', day2: '26.7%' },
      { date: 'OCT 27', users: 111, day1: '22.1%' },
    ];
    this.userRetentionTitle = ['DATE', 'USERS', 'DAY 1', 'DAY 2', 'DAY 3', 'DAY 4', 'DAY 5', 'DAY 6', 'DAY 7'];
    this.data = {
      // dateRange: {
      //   activity: ''
      // },
      labels: {
        user: ['OCT 20', 'OCT 21', 'OCT 22', 'OCT 23', 'OCT 24', 'OCT 25', 'OCT 26', 'OCT 27', 'OCT 28', 'OCT 29', 'OCT 30', 'OCT 31'],
        popular: ['OCT 20', 'OCT 21', 'OCT 22', 'OCT 23', 'OCT 24', 'OCT 25', 'OCT 26', 'OCT 27']
      },
      points: {
        user: [[1250, 2500, 3750, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000]],
        popular: [[30, 60, 40, 70, 45, 80, 50, 77], [15, 59, 45, 91, 106, 70, 30, 54], [65, 59, 80, 98, 56, 79, 40, 61]]
      },
      datasetOverride: {
        user: [
          {
            yAxisID: 'y-axis-1',
            lineTension: 0,
            label: 'Total users',
            borderWidth: 1,
            borderColor: 'rgba(0,0,0,0)',
            pointRadius: 3,
            pointBackgroundColor: 'rgba(68, 138, 241, 0)',
            pointBorderColor: 'rgba(68, 138, 241, 0)',
            pointHoverRadius: 2,
            hitRadius: 10,
            backgroundColor: 'rgba(68, 138, 241, 1)'
          },
          {
            yAxisID: 'y-axis-1',
            lineTension: 0,
            label: 'Blocked users',
            borderWidth: 1,
            borderColor: 'rgba(0,0,0,0)',
            pointRadius: 3,
            pointBackgroundColor: 'rgba(68, 138, 241, 0)',
            pointBorderColor: 'rgba(68, 138, 241, 0)',
            pointHoverRadius: 2,
            hitRadius: 10,
            backgroundColor: 'rgba(233, 96, 74, 1)'
          }
        ]
        // popular: [{ yAxisID: 'y-axis-1', lineTension: 0, fill: false, label: 'Broadcasts sent' },
        //   { yAxisID: 'y-axis-1', lineTension: 0, fill: false, label: 'Broadcasts read' },
        //   { yAxisID: 'y-axis-1', lineTension: 0, fill: false, label: 'Messages processed' }]
      },
      options: {
        popular: {
          legend: { display: true, position: 'bottom', labels: { boxWidth: 10 } },
          tooltips: {
            mode: 'x-axis'
          },
          hover: {
            mode: 'x-axis'
          },
          scales: {
            yAxes: [
              {
                id: 'y-axis-1',
                type: 'linear',
                display: true,
                position: 'left',
                ticks: {
                  fontColor: '#767676',
                  fontSize: 13,
                  maxTicksLimit: 5
                },
                gridLines: {
                  drawBorder: false
                }
              }
            ],
            xAxes: [{
              gridLines: {
                display: false
              },
              ticks: {
                fontColor: '#767676',
                fontSize: 13,
                maxRotation: 0
              }
            }]
          }
        }
      }
    };
    this.tooltipMessage = {
      users: 'Total number of users who started a chat with your bot',
      retention: 'The left column shows date of first visit for a particular cohort of users. The percentages represent how many of those users returned (message, read message, clicked a button, wrote something) on, say, the fourth day or the fifth day',
      activity: 'Daily totals for: <br/><ul><li>All active users (includes those who read a broadcast, sent text input, pushed any button in the bot, or joined the bot for the first time - via Get Started button)</li><li>Users who read at least one broadcast message</li><li>Users who provided any input to the bot (tapped a button or typed in a text message).</li></ul><br/>Note that starting Dec 5, 2016 the definition of “active users” has changed. We now do not count those who received the broadcast but not read it as “active users”',
      blocks: 'A list of bot blocks sorted by number of users who viewed them at least once in a selected time period. Use this data to see most popular blocks, or to analyze your conversion funnel',
      buttons: 'A list of bot buttons sorted by number of users who tapped them at least once in a selected time period',
      inputs: 'A list of text inputs sorted by number of users who entered them at least once in a selected time period',
      daily_users: 'Daily totals of users subscribed and unsubscribed to/from your bot. Note that "unsubscribed" means those users who deleted or blocked your bot.',
      referrals: 'To track sources of incoming traffic, simply add <b>?ref=any_source_name</b> to your bot\'s m.me link. You can also set up specific entry blocks for such links. To do that, check out the LINK icon near the block title inside the block'
    };

    this.BotService.show().then(res => {
      this.bot = res;
      // res.find((item) => {
      if (res.id) {
        this.yandex_app = res.yandex_app;
        this.has_botan_app = res.has_botan_app;
        this.loading = false;
      }
      // })
    });

    this.$http.get(`${this.StoreService.getApiUrl()}/bots/${this.$stateParams.bot_id}/yandex/share/login`)
      .success((res) => {
        this.yandex_url = `${this.StoreService.getApiUrl()}${res.result}`;
      });

    this.totalUserParams = {
      datasetOverride: [
        {
          yAxisID: 'y-axis-1',
          lineTension: 0,
          label: 'Total users',
          borderWidth: 1,
          borderColor: 'rgba(0,0,0,0)',
          pointRadius: 3,
          pointBackgroundColor: 'rgba(68, 138, 241, 0)',
          pointBorderColor: 'rgba(68, 138, 241, 0)',
          pointHoverRadius: 4,
          hitRadius: 30,
          backgroundColor: 'rgba(143, 185, 247, 1)',
          pointHoverBackgroundColor: 'rgba(143, 185, 247, 1)',
          pointHoverBorderColor: '#ffffff',
          pointHoverBorderWidth: 1
        },
        {
          yAxisID: 'y-axis-1',
          lineTension: 0,
          label: 'Blocked users',
          borderWidth: 2,
          borderColor: 'rgba(255,255,255,0)',
          pointRadius: 3,
          pointBackgroundColor: 'rgba(68, 138, 241, 0)',
          pointBorderColor: 'rgba(68, 138, 241, 0)',
          pointHoverRadius: 4,
          hitRadius: 30,
          backgroundColor: 'rgba(239, 143, 128, 1)',
          pointHoverBackgroundColor: 'rgba(239, 143, 128, 1)',
          pointHoverBorderColor: '#ffffff',
          pointHoverBorderWidth: 1
        }
      ],
      options: {
        responsive: false,
        legend: {
          display: false
        },
        tooltips: {
          mode: 'x-axis',
          enabled: false,
          position: 'average',
          callbacks: {
            'label': () => null,
            'title': tooltipItem => String(tooltipItem[0].index),
            'footer': () => '-----'
          },
          custom: tooltip => {
            if (!tooltip || !tooltip.x || !tooltip.title) {
              this.grafToolTip.css({ opacity: 0 });
              return;
            }
            const position = angular.element(document.querySelector('.graf-total-users')).offset();


            let left;
            if (tooltip.xAlign === 'right') {
              left = position.left + tooltip.x + 39;
            } else
            if (tooltip.xAlign === 'center') {
              left = position.left + tooltip.x + 17;
            } else {
              left = position.left + tooltip.x - 7;
            }

            let top;

            // if (tooltip.yAlign === 'bottom') {
            //   top = position.top + tooltip.y + 51;
            // } else
            // if (tooltip.yAlign === 'center') {
            //   top = position.top + tooltip.y - 41;
            // } else {
            //   top = position.top + tooltip.y - 7;
            // }
            //
            // top -= 14;

            top = position.top - 5;

        //    this.grafToolTip.html(tooltip.title[0]);
            const index = Number(tooltip.title[0]);

            const total = this.totalUsersData.points[1][index];
            const unsubscribed = this.totalUsersData.points[1][index] - this.totalUsersData.points[0][index];
            const subscribed = this.totalUsersData.points[0][index];

            let out = (`
                <div><b>${moment(this.totalUsersData.datesObj[index]).format('D MMM Y')}</b></div>
                <div class="total">${this.numberFormater(total)} Total</div>
              `);

            if (total > 0) {
              out = `
                 ${out}
                 <div class="subscribed">${this.numberFormater(subscribed)} (${Math.round(subscribed / total * 100)}%) Subscribed</div>
                 <div class="unsubscribed">${this.numberFormater(unsubscribed)} (${Math.round(unsubscribed / total * 100)}%) Unsubscribed</div>
              `;
            }

            this.grafToolTip.html(out);

            this.grafToolTip.css({
              opacity: 1,
              left: left + 'px',
              top: top + 'px',
            });
          }
        },
        hover: {
          mode: 'x-axis'
        },
        scales: {
          yAxes: [
            {
              id: 'y-axis-1',
              type: 'linear',
              display: true,
              position: 'left',
              ticks: {
                fontColor: '#767676',
                fontSize: 13,
                maxTicksLimit: 5,
                beginAtZero: true,
                padding: 17,
                callback: (label, index, labels) => {
                  const num = Number(label);
                  return String(num > 1000 ? Math.round(num / 1000) : num).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,') + (num > 1000 ? 'k' : '');
                }
              },
              gridLines: {
                drawBorder: false,
                tickMarkLength: 0
              }
            }
          ],
          xAxes: [{
            gridLines: {
              display: false,
              tickMarkLength: 14
            },
            ticks: {
              fontColor: '#767676',
              fontSize: 12,
              maxRotation: 0
            }
          }]
        }
      },
      colors: ['#448af1', '#e9604a']
    };

    this.dailyUserParams = {
      datasetOverride: [
        {
          yAxisID: 'y-axis-1',
          lineTension: 0,
          label: '',
          borderWidth: 3,
          borderColor: '#448af1',
          pointRadius: 3,
          pointBackgroundColor: 'rgba(68, 138, 241, 0)',
          pointBorderColor: 'rgba(68, 138, 241, 0)',
          pointHoverRadius: 3,
          hitRadius: 30,
          backgroundColor: 'rgba(143, 185, 247, 1)',
          pointHoverBackgroundColor: '#448af1',
          pointHoverBorderColor: '#ffffff',
          pointHoverBorderWidth: 1
        },
        {
          yAxisID: 'y-axis-1',
          lineTension: 0,
          label: '',
          borderWidth: 3,
          borderColor: '#e9604a',
          pointRadius: 3,
          pointBackgroundColor: 'rgba(68, 138, 241, 0)',
          pointBorderColor: 'rgba(68, 138, 241, 0)',
          pointHoverRadius: 3,
          hitRadius: 30,
          backgroundColor: 'rgba(239, 143, 128, 1)',
          pointHoverBackgroundColor: '#e9604a',
          pointHoverBorderColor: '#ffffff',
          pointHoverBorderWidth: 1
        }
      ],
      options: {
        responsive: false,
        legend: {
          display: false
        },
        tooltips: {
          mode: 'x-axis',
          enabled: false,
          position: 'average',
          callbacks: {
            'label': () => null,
            'title': tooltipItem => String(tooltipItem[0].index),
            'footer': () => '-----'
          },
          custom: tooltip => {
            if (!tooltip || !tooltip.x || !tooltip.title) {
              this.grafToolTip.css({ opacity: 0 });
              return;
            }

            const position = angular.element(document.querySelector('.graf-daily-users')).offset();

      //      console.log(tooltip);

            let left;
            if (tooltip.xAlign === 'right') {
              left = position.left + tooltip.x + 39;
            } else
            if (tooltip.xAlign === 'center') {
              left = position.left + tooltip.x + 17;
            } else {
              left = position.left + tooltip.x - 7;
            }

            let top;
            // if (tooltip.yAlign === 'center') {
            //   top = position.top + tooltip.y - 41;
            // } else
            // if (tooltip.yAlign === 'bottom') {
            //   top = position.top + tooltip.y + 51;
            // } else {
            //   top = position.top + tooltip.y - 7;
            // }
            //
            // top -= 14;
            top = position.top - 5;

            //    this.grafToolTip.html(tooltip.title[0]);
            const index = Number(tooltip.title[0]);
            const subscribed = this.dailyUsersData.points[0][index];
            const unsubscribed = this.dailyUsersData.points[1][index];

            this.grafToolTip.html(`
   <div><b>${moment(this.dailyUsersData.datesObj[index]).format('D MMM Y')}</b></div>
   <div class="subscribed">${this.numberFormater(subscribed)} Daily new</div>
   <div class="unsubscribed">${this.numberFormater(unsubscribed)} Daily unsubscribed</div>
`);

            this.grafToolTip.css({
              opacity: 1,
              left: left + 'px',
              top: top + 'px'
            });
          }
        },
        hover: {
          mode: 'x-axis'
        },
        scales: {
          yAxes: [
            {
              id: 'y-axis-1',
              type: 'linear',
              display: true,
              position: 'left',
              scaleStepWidth: 1,
              ticks: {
                fontColor: '#767676',
                fontSize: 13,
                stepValue: 6,
                minTicksLimit: 1,
                maxTicksLimit: 5,
                beginAtZero: true,
                padding: 17,
                callback: (label, index, labels) => {
                  const num = Math.abs(Number(label));
                  if (num > 0 && num < 1) {
                    return;
                  }
                  return String(num > 1000 ? Math.round(num / 1000) : num).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,') + (num > 1000 ? 'k' : '');
                }
              },
              gridLines: {
                drawBorder: false,
                tickMarkLength: 0
              }
            }
          ],
          xAxes: [{
            gridLines: {
              display: false,
              tickMarkLength: 14
            },
            ticks: {
              fontColor: '#767676',
              fontSize: 12,
              maxRotation: 0
            }
          }]
        }
      },
      colors: ['#448af1', '#e9604a']
    };

    this.eventUserParams = {
      datasetOverride: [
        {
          yAxisID: 'y-axis-1',
          lineTension: 0,
          label: '',
          borderWidth: 3,
          borderColor: '#448af1',
          pointRadius: 3,
          pointBackgroundColor: 'rgba(68, 138, 241, 0)',
          pointBorderColor: 'rgba(68, 138, 241, 0)',
          pointHoverRadius: 3,
          hitRadius: 30,
          backgroundColor: 'rgba(143, 185, 247, 0)',
          pointHoverBackgroundColor: '#448af1',
          pointHoverBorderColor: '#ffffff',
          pointHoverBorderWidth: 1
        },
        {
          yAxisID: 'y-axis-1',
          lineTension: 0,
          label: '',
          borderWidth: 3,
          borderColor: '#e9604a',
          pointRadius: 3,
          pointBackgroundColor: 'rgba(68, 138, 241, 0)',
          pointBorderColor: 'rgba(68, 138, 241, 0)',
          pointHoverRadius: 3,
          hitRadius: 30,
          backgroundColor: 'rgba(143, 185, 247, 0)',
          pointHoverBackgroundColor: '#e9604a',
          pointHoverBorderColor: '#ffffff',
          pointHoverBorderWidth: 1
        },
        {
          yAxisID: 'y-axis-1',
          lineTension: 0,
          label: '',
          borderWidth: 3,
          borderColor: '#5bd8ac',
          pointRadius: 3,
          pointBackgroundColor: 'rgba(68, 138, 241, 0)',
          pointBorderColor: 'rgba(68, 138, 241, 0)',
          pointHoverRadius: 3,
          hitRadius: 30,
          backgroundColor: 'rgba(143, 185, 247, 0)',
          pointHoverBackgroundColor: '#5bd8ac',
          pointHoverBorderColor: '#ffffff',
          pointHoverBorderWidth: 1
        }
      ],
      options: {
        bezierCurve: false,
        responsive: false,
        legend: {
          display: false
        },
        tooltips: {
          mode: 'x-axis',
          enabled: false,
          position: 'average',
          callbacks: {
            'label': () => null,
            'title': tooltipItem => String(tooltipItem[0].index),
            'footer': () => '-----'
          },
          custom: tooltip => {
            if (!tooltip || !tooltip.x || !tooltip.title) {
              this.grafToolTip.css({ opacity: 0 });
              return;
            }

            const position = angular.element(document.querySelector('.graf-event-users')).offset();


            let left;
            if (tooltip.xAlign === 'right') {
              left = position.left + tooltip.x + 39;
            } else
            if (tooltip.xAlign === 'center') {
              left = position.left + tooltip.x + 17;
            } else {
              left = position.left + tooltip.x - 7;
            }

            let top;
            // if (tooltip.yAlign === 'center') {
            //   top = position.top + tooltip.y - 41;
            // } else
            // if (tooltip.yAlign === 'bottom') {
            //   top = position.top + tooltip.y + 51;
            // } else {
            //   top = position.top + tooltip.y - 7;
            // }
            //
            // top -= 14;
            top = position.top - 5;

            //    this.grafToolTip.html(tooltip.title[0]);
            const index = Number(tooltip.title[0]);

            this.grafToolTip.html(`
   <div><b>${moment(this.userEventData.datesObj[index]).format('D MMM Y')}</b></div>
   <div class="active-users">${this.numberFormater(this.userEventData.points[0][index])} Active users</div>
   <div class="user-message-read">${this.numberFormater(this.userEventData.points[1][index])} User broadcast read</div>
   <div class="user-input-received">${this.numberFormater(this.userEventData.points[2][index])} User input received</div>
`);

            this.grafToolTip.css({
              opacity: 1,
              left: left + 'px',
              top: top + 'px',
            });
          }
        },
        hover: {
          mode: 'x-axis'
        },
        scales: {
          yAxes: [
            {
              id: 'y-axis-1',
              type: 'linear',
              display: true,
              position: 'left',
              scaleStepWidth: 1,
              ticks: {
                fontColor: '#767676',
                fontSize: 13,
                stepValue: 6,
                minTicksLimit: 1,
                maxTicksLimit: 5,
                beginAtZero: true,
                padding: 17,
                callback: (label, index, labels) => {
                  const num = Math.abs(Number(label));
                  if (num > 0 && num < 1) {
                    return;
                  }
                  return String(num > 1000 ? Math.round(num / 1000) : num).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,') + (num > 1000 ? 'k' : '');
                }
              },
              gridLines: {
                drawBorder: false,
                tickMarkLength: 0
              }
            }
          ],
          xAxes: [{
            gridLines: {
              display: false,
              tickMarkLength: 14
            },
            ticks: {
              fontColor: '#767676',
              fontSize: 12,
              maxRotation: 0,
            }
          }]
        }
      },
      colors: ['#448af1', '#e9604a', '#5bd8ac']
    };
  }

  getTotalUsers() {
    this.$http.get(`${this.StoreService.getApiUrl()}/bots/${this.$stateParams.bot_id}/yandex/stats/total_users${this.dateReqSting()}`)
      .success((res) => {
        this.totalUsersData = {
          points: [[], []],
          dates: [],
          datesObj: []
        };

        this.dailyUsersData = {
          points: [[], []],
          dates: [],
          datesObj: []
        };

        const addLineVal = Math.max(...res.result.y['Total users']) * 0.03;
        res.result.x.forEach((date, i) => {
          if (date.indexOf('201') === -1) {
            date += ', 2016';
          }

          this.totalUsersData.dates.push(moment(date).format('MMM D').toUpperCase());
          this.totalUsersData.datesObj.push(new Date(date));
          this.totalUsersData.points[0].push(res.result.y['Total users'][i] - res.result.y['Blocked users'][i]);
          this.totalUsersData.points[1].push(res.result.y['Total users'][i]);
       //   this.totalUsersData.points[2].push(res.result.y['Total users'][i] + addLineVal);

          if (i > 0) {
            const dailyUsersUns = res.result.y['Blocked users'][i] - res.result.y['Blocked users'][i - 1];
            const dailyUsersTot = res.result.y['Total users'][i] - res.result.y['Total users'][i - 1];

            this.dailyUsersData.dates.push(moment(date).format('MMM D').toUpperCase());
            this.dailyUsersData.datesObj.push(new Date(date));
            this.dailyUsersData.points[0].push(dailyUsersTot);
            this.dailyUsersData.points[1].push(-dailyUsersUns);
          }
        });

        this.minDate = new Date(res.result.x[0]);

        this.totalUsersLoading = false;
      })
      .error(() => {
        this.totalUsersLoading = false;
        this.totalUsersLoadingError = true;
      });
  }

  getEventUsers() {
    this.$http.get(`${this.StoreService.getApiUrl()}/bots/${this.$stateParams.bot_id}/yandex/stats/event_users${this.dateReqSting()}`)
      .success((res) => {
        this.data.datasetOverride.popular = [];

        this.userEventData = {
          points: [],
          dates: [],
          datesObj: []
        };
        res.result.x.forEach(date => {
          if (date.indexOf('201') === -1) {
            date += ', 2016';
          }

          this.userEventData.dates.push(moment(date).format('MMM D').toUpperCase());
          this.userEventData.datesObj.push(new Date(date));
        });

        if (res.result.y['ym:ce:users']) {
          this.userEventData.points.push(res.result.y['ym:ce:users']);
        } else {
          this.userEventData.points.push(this.userEventData.dates.map(() => 0));
        }

        if (res.result.y['broadcast read']) {
          this.userEventData.points.push(res.result.y['broadcast read']);
        } else {
          this.userEventData.points.push(this.userEventData.dates.map(() => 0));
        }

        if (res.result.y['input from user received']) {
          this.userEventData.points.push(res.result.y['input from user received']);
        } else {
          this.userEventData.points.push(this.userEventData.dates.map(() => 0));
        }

        this.userActivityLoading = false;
      })
      .error(() => {
        this.userActivityLoading = false;
        this.userActivityLoadingError = true;
      });
  }

  mapingGrafOut(inputDate) {
    const out = {
      points: [],
      dates: [],
      datesObj: []
    };

    inputDate.dates.forEach((date, i) => {
      if (date.valueOf() >= (this.startDate.valueOf() - 3600000 * 24) && date.valueOf() <= (this.endDate.valueOf() + 1000)) {
        out.dates.push(moment(date).format('MMM D').toUpperCase());
        out.datesObj.push(date);

        inputDate.points.forEach((line, li) => {
          if (!out.points[li]) {
            out.points[li] = [];
          }
          out.points[li].push(inputDate.points[li][i]);
        });
      }
    });
    return out;
  }

  getRetention() {
    this.$http.get(`${this.StoreService.getApiUrl()}/bots/${this.$stateParams.bot_id}/yandex/stats/retention`)
      .success((res) => {
        this.userRetention = [];
        for (let i = 0; i < res.result.length; i++) {
          const row = {};
          if (i === 0) {
            row.date = 'Total';
          } else {
            if (res.result[i][0].indexOf('201') === -1) {
              res.result[i][0] += ', 2016';
            }

            row.date = moment(new Date(res.result[i][0])).format('MMM D').toUpperCase();
          }
          row.users = res.result[i][1];
          if (res.result[i][2][0] || res.result[i][2][0] === 0){
            row.day1 = `${Math.round(res.result[i][2][0] * 1000) / 10}%`;
          } else {
            row.day1 = null;
          }
          if (res.result[i][2][1] || res.result[i][2][1] === 0){
            row.day2 = `${Math.round(res.result[i][2][1] * 1000) / 10}%`;
          } else {
            row.day2 = null;
          }
          if (res.result[i][2][2] || res.result[i][2][2] === 0){
            row.day3 = `${Math.round(res.result[i][2][2] * 1000) / 10}%`;
          } else {
            row.day3 = null;
          }
          if (res.result[i][2][3] || res.result[i][2][3] === 0){
            row.day4 = `${Math.round(res.result[i][2][3] * 1000) / 10}%`;
          } else {
            row.day4 = null;
          }
          if (res.result[i][2][4] || res.result[i][2][4] === 0){
            row.day5 = `${Math.round(res.result[i][2][4] * 1000) / 10}%`;
          } else {
            row.day5 = null;
          }
          if (res.result[i][2][5] || res.result[i][2][5] === 0){
            row.day6 = `${Math.round(res.result[i][2][5] * 1000) / 10}%`;
          } else {
            row.day6 = null;
          }
          if (res.result[i][2][6] || res.result[i][2][6] === 0){
            row.day7 = `${Math.round(res.result[i][2][6] * 1000) / 10}%`;
          } else {
            row.day7 = null;
          }

          this.userRetention.push(row);
        }
        this.userRetentionLoading = false;
      })
      .error((res) => {
        this.userRetentionLoading = false;
        this.userRetentionLoadingError = true;
        // console.log(res);
      });
  }

  getUsage() {
    this.$http.get(`${this.StoreService.getApiUrl()}/bots/${this.$stateParams.bot_id}/yandex/stats/usage${this.dateReqSting()}`)
      .success((res) => {
        this.userPopularBlocks = res.result.blocks;
        this.userPopularButtons = res.result['button pressed'];
        this.userPopularInputs = res.result['user input'];
        this.popularBlocksLoading = false;
        this.popularButtonsLoading = false;
        this.popularInputsLoading = false;
      })
      .error((res) => {
        this.popularBlocksLoading = false;
        this.popularButtonsLoading = false;
        this.popularInputsLoading = false;
        this.popularBlocksLoadingError = true;
        // console.log(res);
      });
  }

  getReferrals() {
    this.$http.get(`${this.StoreService.getApiUrl()}/bots/${this.$stateParams.bot_id}/yandex/stats/referrals${this.dateReqSting()}`)
      .success((res) => {
        this.referralsData = res.result;
        this.referralsLoading = false;
        this.referralsLoadingError = false;
      })
      .error((e) => {
        this.referralsLoading = false;
        this.referralsLoadingError = true;
      });
  }

  onLogout() {
    this.ModalService
      .confirmAdv(
        '<p>Are you sure you want to sign out of Yandex account?',
        {
          ok: 'DISCONNECT',
          close: 'CANCEL'
        }
      )
      .then((res) => {
        if (res) {
          this.$http.post(`${this.StoreService.getApiUrl()}/bots/${this.$stateParams.bot_id}/yandex/share/logout`, {})
            .success((res) => {
              this.yandex_app = null;
            });
        }
      });
  }

  customOrder(item) {
    return parseInt(item.rate.replace(/,/g, ''), 10);
  }

  unBind() {
    this.ModalService
      .confirmAdv(
        '<p>Are you sure you want to sign out of Yandex account?',
        {
          ok: 'DISCONNECT',
          close: 'CANCEL'
        }
      )
      .then((res) => {
        if (res) {
          this.connected = false;
        }
      });
  }

  setBg(value) {
    if (value){
      const val = parseInt(value);
      if (!val) {
        return 0;
      }
      return (val / 100);
    }
  }

  onRefresh(value) {
    if(value === 'totalUsersLoadingError'){
      this.totalUsersLoadingError = false;
      this.totalUsersLoading = true;
      this.getTotalUsers();
    }
    if(value === 'userRetentionLoadingError'){
      this.userRetentionLoadingError = false;
      this.userRetentionLoading = true;
      this.getRetention();
    }
    if(value === 'userActivityLoadingError'){
      this.userActivityLoadingError = false;
      this.userActivityLoading = true;
      this.getEventUsers();
    }
    if(value === 'popularBlocksLoadingError'){
      this.popularBlocksLoadingError = false;
      this.popularBlocksLoading = true;
      this.popularButtonsLoading = true;
      this.popularInputsLoading = true;
      this.getUsage();
    }
    if(value === 'referralsLoadingError'){
      this.referralsLoading = true;
      this.referralsLoadingError = false;
      this.getReferrals();
    }
  }

  numberFormater(num, ksh) {
    num = Math.abs(num);
    return String((num > 1000 && ksh) ? Math.round(num / 1000) : num).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,') + ((num > 1000 && ksh) ? 'k' : '');
  }

  genPreview() {
    this.totalUsersData = {
      points: [[], []],
      dates: []
    };

    this.dailyUsersData = {
      points: [[], []],
      dates: []
    };

    let total = Math.round(Math.random() * 1000) + 1000;
    let ublock = Math.round(Math.random() * 400) + 100;

    for (let i = 0; i < 8; i++) {
      this.totalUsersData.dates.push(moment().endOf('day').subtract(i, 'days').toDate());

      total += Math.round(Math.random() * 1000) + 1000;
      ublock += Math.round(Math.random() * 400) + 100;

      this.totalUsersData.points[0].push(total);
      this.totalUsersData.points[1].push(ublock);
    }

    this.totalUsersData.dates.forEach((date, i) => {
      if (i > 0) {
        const dailyUsersUns = this.totalUsersData.points[1][i] - this.totalUsersData.points[1][i - 1];
        const dailyUsersTot = this.totalUsersData.points[0][i] - this.totalUsersData.points[0][i - 1];

        this.dailyUsersData.dates.push(this.totalUsersData.dates[i]);
        this.dailyUsersData.points[0].push(dailyUsersTot);
        this.dailyUsersData.points[1].push(-dailyUsersUns);
      }
    });
  }

  encodeLink(referralParameter) {
    return encodeURIComponent(referralParameter);
  }

  dateReqSting() {
    return `?startDate=${moment(this.startDate).format('Y-MM-DD')}&endDate=${moment(this.endDate).format('Y-MM-DD')}`;
  }
}
