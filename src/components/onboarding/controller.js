export default class Onboarding2Controller {
  constructor($scope, $rootScope, $timeout, $element, UserService, AnalyticsService) {
    'ngInject';

    this.$timeout = $timeout;
    this.$element = $element;
    this.$scope = $scope;
    this.$rootScope = $rootScope;


    this.UserService = UserService;
    this.AnalyticsService = AnalyticsService;


    this.satesTitles = [
      {
        t1: ' Quick step-by-step tutorial', t2: ''
      },
      {
        t1: 'Step 1.', t2: 'About bots, blocks and buttons'
      },
      {
        t1: 'Step 2.', t2: 'Create a block'
      },
      {
        t1: 'Step 3.', t2: 'Link blocks with buttons'
      },
      {
        t1: 'Step 4.', t2: 'Test your chatbot'
      },
      {
        t1: 'Step 5.', t2: 'Set up Artificial Intelligence'
      },
      {
        t1: 'Step 6.', t2: 'Fill up your bot'
      },
      {
        t1: 'Step 7.', t2: 'Publish your bot'
      },
      {
        t1: 'Step 8.', t2: 'Get your first users'
      },
      {
        t1: '', t2: 'You did it!'
      }
    ];

    this.eventsNames = {
      getStarted: 'onboarding2-get-started',
      step: 'onboarding2-set-step-',
      done: 'onboarding2-done',
      fromTutorialButton: 'onboarding2-from-tutorial-button',
      newUser: 'onboarding2-newuser',
      expandCountInStep: 'onboarding2-expand-count-in-step-',
      collapseCountInStep: 'onboarding2-collapse-count-in-step-'
    };

    this.close = true;
    this.hide = true;

    this.currentStep = 0;
    this.oldStep = null;
    this.directionStep = 0;
    this.directionStepEnd = 1;
    this.progressValue = 0;
    this.expandCount = 0;
    this.collapseCount = 0;

    this.listeners = [];
    this.listeners.push($rootScope.$on('$openTutorial', () => {
      this.onbShow(true);
      this.sendStatEvent(this.eventsNames.fromTutorialButton);
    }));
    this.listeners.push($rootScope.$on('$newuser', () => {
      this.onbShow(true);
      this.sendStatEvent(this.eventsNames.newUser);
      this.UserService.setFlags('ab_onboarding_enabled');
//      this.AnalyticsService.sendAppMetrikaEvent('ab', { onboarding: true });
    }));
  }

  $onInit() {
    this.body = angular.element(document.querySelector('body'));
    this.minimise = (window.localStorage.getItem('onboarding2-minimise') === '1');

    if (window.localStorage.getItem('onboarding2-show') === '1') {
      this.currentStep = parseInt(window.localStorage.getItem('onboarding2-step'), 10);
      if (isNaN(this.currentStep)) {
        this.currentStep = 0;
      }
      this.onbShow();
    }
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  getStarted() {
    this.sendStatEvent(this.eventsNames.getStarted);
    this.nextStep();
  }

  onbDone() {
    this.onbHide();
    this.sendStatEvent(this.eventsNames.done);
  }

  setStep(n, dir) {
    this.oldStep = this.currentStep;
    this.currentStep = n;
    this.calcProgress();
    this.directionStepEnd = 0;

    this.scrollBox.scrollTop = 0;

    if (dir === 1 || dir === -1) {
      this.directionStep = dir;
    }

    window.localStorage.setItem('onboarding2-step', String(this.currentStep));

    this.$timeout(() => {
      this.directionStepEnd = this.directionStep;
      this.directionStep = 0;

      this.$timeout(() => {
        this.oldStep = null;
      }, 300);
      this.$scope.$digest();
    }, 50);

    if (this.oldStep > 0 && this.oldStep < 9) {
      this.sendStatEvent(this.eventsNames.expandCountInStep + this.oldStep + '--value-' + this.expandCount);
      this.sendStatEvent(this.eventsNames.collapseCountInStep + this.oldStep + '--value-' + this.collapseCount);
    }

    this.expandCount = 0;
    this.collapseCount = 0;
  }

  calcProgress() {
    this.progressValue = Math.floor(this.currentStep / 9 * 100);
  }

  nextStep($event) {
    if ($event) {
      $event.preventDefault();
      $event.stopPropagation();
    }

    this.setStep(this.currentStep + 1, 1);

    this.sendStatEvent(this.eventsNames.step + this.currentStep);
  }

  prevStep() {
    this.setStep(this.currentStep - 1, -1);
    this.sendStatEvent(this.eventsNames.step + this.currentStep);
  }

  onbHide() {
    this.hide = true;

    this.body.removeClass('onbording2-show');

    window.localStorage.setItem('onboarding2-show', '0');

    this.$timeout(() => {
      this.close = true;
    }, 100);
  }

  onbShow(max) {
    if (!this.body) {
      this.$timeout(() => {
        this.onbShow(max);
      }, 50);
    }


    this.close = false;
    this.hide = false;

    this.body.addClass('onbording2-show');

    window.localStorage.setItem('onboarding2-show', '1');
    window.localStorage.setItem('onboarding2-step', String(this.currentStep));
    if (max) {
      this.minimiseToggle(false);

      if (this.currentStep === 9) {
        this.currentStep = 0;
        this.oldStep = null;
        window.localStorage.setItem('onboarding2-step', '0');
      }
    }

    this.calcProgress();

    this.$timeout(() => {
      this.scrollBox = document.querySelector('.onboarding2 .content');
    }, 100);

  }

  minimiseToggle(val) {
    if (typeof val !== 'undefined') {
      this.minimise = val;
    } else {
      this.minimise = !Boolean(this.minimise);

      if (this.minimise) {
        this.expandCount ++;
      } else {
        this.collapseCount ++;
      }
    }
    window.localStorage.setItem('onboarding2-minimise', this.minimise ? '1' : '0');
  }

  minimiseOff($event) {
    if (this.minimise) {
      this.minimiseToggle(false);
      $event.stopPropagation();
    }
  }

  minimiseOn($event) {
    if (!this.minimise) {
      this.minimiseToggle(true);
      $event.stopPropagation();
    }
  }

  sendStatEvent(eventName) {
    this.$rootScope.$emit('$statEvent', { event: 'hit', data: (window.location.protocol + '//' + window.location.hostname + '/#' + eventName) });
  }
}
