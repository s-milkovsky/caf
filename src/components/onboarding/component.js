import controller from './controller';
import template from './onboarding2.html';

export default () => ({
  controller,
  template,
  restrict: 'E',
  controllerAs: 'onb',
  replace: true
});
