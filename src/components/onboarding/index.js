
import angular from 'angular';
import Component from './component.js';
import './assets/less/onboarding2.less';

export default angular
  .module('app.common.onboarding2', []).directive('onboarding2', Component).name;
