import ng from 'angular';

export default ng.module('app.common.textareaVExpand', [])
  .directive('textareaVExpand', ($timeout) => {
    'ngInject';
    return (scope, element) => {

      $timeout(() => {
        const mask = element.parent().find('.wrap-mask');

        const listeners = [];

        listeners.push(scope.$on('update-mask', () => {
          $timeout(() => {
            element.css({ height: mask.height() + 12 });
          });
        }));

        listeners.push(scope.$on('$destroy', () => {
          listeners.forEach(fn => fn.call());
        }));

      });
    };
  })
  .name;
