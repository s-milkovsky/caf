import ng from 'angular';
import TutorialService from './service';
import TutorialComponent from './component';

export default ng.module('app.common.tutorial', [])
  .service('TutorialService', TutorialService)
  .directive('tutorial', TutorialComponent)
  .name;
