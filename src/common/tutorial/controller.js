export default class TutorialController {
  constructor($state, TutorialService, TUTORIAL) {
    'ngInject';

    this.TutorialService = TutorialService;
    this.TUTORIAL = TUTORIAL;
    // this.isBotsState = $state.is('bots');
    this.isBotsState = false;
  }

  $onInit() {
    this.TutorialService.get().then(data => {
      this.title = data[this.page].title || data.title;
      this.items = data[this.page].links || [];
    });
  }
}
