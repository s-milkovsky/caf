import template from './tutorial.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    page: '@'
  }
});
