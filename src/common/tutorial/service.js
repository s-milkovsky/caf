export default class TutorialService {
  constructor($http, $q, TUTORIAL) {
    'ngInject';
    this.$http = $http;
    this.$q = $q;
    this.TUTORIAL = TUTORIAL;
    this.tutorial = null;
  }

  get() {
    if (!this.tutorial) {
      this.tutorial = this.fetch();
    }
    return this.tutorial;
  }

  fetch() {
    return this.$http({
      url: '/tutorials.json'
    }).then(res => res.data)
    .catch(() => {
      const deffered = this.$q.defer();
      deffered.resolve(this.TUTORIAL);
      this.tutorial = deffered.promise;
      return this.tutorial;
    });
  }
}
