import template from './art-mini-dropdown.html';
import controller from './controller';

export default () => ({
  template,
  controllerAs: '$artMiniDropdown',
  controller,
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    ngModel: '=',
    items: '=',
    index: '='
  }
});
