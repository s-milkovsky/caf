
export default ($timeout) => {
  'ngInject';
  return {
    errorText: null,
    buttonText: null,
    buttonUrl: null,

    setError(errorText, buttonText, buttonUrl) {
      this.errorText = errorText;
      this.buttonText = buttonText;
      this.buttonUrl = buttonUrl;
    }
  };
};
