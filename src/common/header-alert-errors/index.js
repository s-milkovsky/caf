import ng from 'angular';

import component from './component';
import service from './service';

export default ng.module('app.common.headerAlertErrors', [])
  .factory('HeaderAlertErrorsService', service)
  .directive('headerAlertErrors', component)
  .name;
