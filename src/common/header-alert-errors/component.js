import template from './template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'hae',
  restrict: 'E',
  replace: true
});
