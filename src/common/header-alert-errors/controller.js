
export default class HeaderAlertController {
  constructor($timeout, $scope, $rootScope, HeaderAlertErrorsService, BotService) {
    'ngInject';
    this.HeaderAlertErrorsService = HeaderAlertErrorsService;
    this.BotService = BotService;

    $rootScope.$on('$stateChangeSuccess', (event, toState) => {
      this.BotService.getSettingNotifications().then(res => {
        if (res.length && res[0].error_text) {
          const bodyElement = angular.element('body');
          bodyElement.addClass('header-alert-error');

          if (res[0].error_button) {
            this.HeaderAlertErrorsService.setError(res[0].error_text, res[0].error_button.title, res[0].error_button.url);
          } else {
            this.HeaderAlertErrorsService.setError(res[0].error_text, '', '');
          }
        } else {
          this.HeaderAlertErrorsService.setError('');
        }
      });
    });

    $timeout(() => {
      this.showAlert = true;
    }, 500);
  }
  goToPage(buttonUrl) {
    if (buttonUrl) window.open(buttonUrl, '_blank');
  }
}
