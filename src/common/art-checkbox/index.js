import './assets/less/art-checkbox.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.artCheckbox', [])
  .directive('artCheckbox', Component)
  .name;
