import ng from 'angular';
import suggestTemplate from './component';
import './assets/less/style.less';


export default ng.module('app.common.suggestTemplate', [])
  .directive('suggestTemplate', suggestTemplate)
  .name;
