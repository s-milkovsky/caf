export default ($timeout, BotService) => {
  'ngInject';
  return (scope, element) => {
    let input = element.find('input');

    if (!input.length) {
      input = element.find('textarea');
    }

    const wrapMask = angular.element('<div class="wrap-mask"></div>');
    const mask = angular.element('<div class="mask"></div>');
    const box = angular.element('<ul class="auto-box"><li>test</li><li>test</li></ul>');
    const reg = new RegExp('\{\{([^{}]+?)\}\}', 'gi');
    const sk = { l: '{{', r: '}}' };

    let botParamsCache = [];
    let currentSubstring = '';
    let currentParam = '';
    let currentNTagParam = 0;
    let caretPosition = 0;
    let popupShowed = false;

    const showPopup = (query) => {
      currentSubstring = query;
      query = query.toLowerCase();

      let botParams = [];

      if (query === '') {
        botParams = botParamsCache;
      } else {
        botParamsCache.forEach(item => {
          if (item.toLowerCase().indexOf(query) !== -1) {
            botParams.push(item);
          }
        });
      }

      if (botParams.length === 0) {
        return;
      }

//      botParams = botParams.sort();

      const marker = mask.find('b');
      if (marker) {
        box.css('left', marker.prop('offsetLeft') - input.scrollLeft());

        let out = '';
        botParams.forEach((item, i) => {
          if (i === 0) {
            currentParam = item;
            out += '<li class="act"><span>' + item + '</span></li>';
          } else {
            out += '<li><span>' + item + '</span></li>';
          }
        });
        box.html(out);
      }
      if (!popupShowed) {
        popupShowed = true;
        box.addClass('act');
      }
    };

    const hidePopup = () => {
      if (popupShowed) {
        popupShowed = false;
        box.removeClass('act');
        botParamsCache = [];
      }
    };

    const addParam = () => {
      if (currentParam) {
        const dividPos = input.val().substring(0, caretPosition).lastIndexOf(sk.l) + 2;
        input.val(input.val().substring(0, dividPos) + input.val().substring(dividPos, input.val().length).replace(currentSubstring, currentParam + sk.r).replace(new RegExp(currentParam + '\}\}[^}]+?\}\}', 'ig'), currentParam + sk.r));
        input.trigger('input');
        currentParam = '';
        render();
      }
    };

    const load = (query) => {
      if (botParamsCache.length) {
        showPopup(query);
      } else {
        BotService.getParams().then(res => {
          botParamsCache = res;
          showPopup(query);
        });
      }
    };

    const htmlspecialchars = (str) => {
      if (typeof(str) === 'string') {
        str = str.replace(/&/g, 'U');
        str = str.replace(/"/g, '\"');
        str = str.replace(/'/g, '\'');
        str = str.replace(/</g, '1');
        str = str.replace(/>/g, '1');
      }
      return str;
    }

    const showAllSpan = () => {
      const spans = mask.find('span');
      angular.forEach(spans, span => {
        angular.element(span).attr('style', '');
      });
    };

    const render = (event) => {
      const val = htmlspecialchars(input.val());
      let maskVal = val;
      caretPosition = input[0].selectionStart;
      if (!event || event.type !== 'keydown') {
        if (
          (val.lastIndexOf(sk.l, caretPosition - 2) !== -1 && (val.lastIndexOf(sk.r, caretPosition - 1) === -1 || (val.lastIndexOf(sk.r, caretPosition - 1) < val.lastIndexOf(sk.l, caretPosition - 2))))
        ) {
          const substr = val.substring(val.lastIndexOf(sk.l, caretPosition - 2) + 2, caretPosition);
          if (substr.indexOf('{') === -1 && substr.indexOf('}') === -1) {
            const dividPos = input.val().substring(0, caretPosition).lastIndexOf(sk.l);
            maskVal = maskVal.substring(0, dividPos) + '<b></b>' + maskVal.substring(dividPos, maskVal.length);
            mask.html(maskVal);
            load(substr);
          } else {
            hidePopup();
          }
        } else {
          hidePopup();
        }
      }
      maskVal = maskVal.replace(reg, '<span>$1</span>');
      mask.html(maskVal);

      if (caretPosition) {
        if (
          (val.lastIndexOf(sk.l, caretPosition) !== -1 && (val.lastIndexOf(sk.r, caretPosition - 2) === -1 || (val.lastIndexOf(sk.r, caretPosition - 2) < val.lastIndexOf(sk.l, caretPosition)))) &&
          (val.indexOf(sk.r, caretPosition - 1) !== -1 && (val.indexOf(sk.l, caretPosition + 1) === -1 || (val.indexOf(sk.r, caretPosition - 1) < val.indexOf(sk.l, caretPosition + 1))))
        ) {
          let pos = val.substring(0, caretPosition - 1).match(reg);
          pos = pos ? pos.length : 0;
          showAllSpan();
          angular.element(mask.find('span')[pos]).css('opacity', '0');
        }
      }
      mask.css('transform', 'translateX(-' + input.scrollLeft() + 'px)');
    };

    wrapMask.append(mask);
    element.append(wrapMask);
    element.append(box);

    $timeout(() => {
      render();
    });

    box.on('mousedown', (event) => {
      if (event.target.tagName.toLowerCase() === 'li') {
        currentParam = event.target.firstChild.innerHTML;
        addParam();
        hidePopup();
      }
      if (event.target.tagName.toLowerCase() === 'span') {
        currentParam = event.target.innerHTML;
        addParam();
        hidePopup();
      }
    });

    input.on('keyup keydown mouseup', (event) => {
      if ([13, 38, 40].indexOf(event.keyCode) !== -1 && popupShowed) {
        event.preventDefault();
        if (event.type === 'keyup') {
          return;
        }
        switch (event.keyCode) {
          case 13:
            addParam();
            hidePopup();
            return;
          case 38:
          case 40:
            const lis = box.find('li');
            currentNTagParam += (event.keyCode === 38 ? -1 : 1);
            if (currentNTagParam < 0) {
              currentNTagParam = 0;
            } else
            if (currentNTagParam >= lis.length) {
              currentNTagParam = lis.length - 1;
            }
            angular.forEach(lis, (li, i) => {
              const el = angular.element(li);
              if (i === currentNTagParam) {
                el.addClass('act');
                currentParam = el.text();
              } else {
                el.removeClass('act');
              }
            });
            return;
          default:
        }
      }
      render(event);
    });

    input.on('blur', (event) => {
      $timeout(() => {
        if (input[0] !== document.activeElement) {
          caretPosition = null;
        }
        render(event);
        showAllSpan();
      });
    });

    const listeners = [];

    listeners.push(scope.$on('document.click', () => {
      scope.$apply(() => {
        hidePopup();
      });
    }));

    scope.$on('$destroy', () => {
      input.off('keyup keydown mouseup');
      input.off('blur');
      box.off('click');
      listeners.forEach(fn => fn.call());
    });
  };
};
