
export default ($q, $timeout, $http, $stateParams, StoreService) => {
  'ngInject';
  let cachedListPromise;

  return {
    list(refresh) {
      let params = {};
      if (refresh) {
        params = { refresh }
      }

      return $http({
        url: `${ StoreService.getApiUrl() }/pages`,
        method: 'get',
        params
      }).then(res => res.data.result);
    },

    cachedList(refresh) {
      if (refresh) {
        cachedListPromise = null;
      }
      if (!cachedListPromise) {
        cachedListPromise = this.list(refresh);
      }

      return cachedListPromise;
    },

    show() {
      return $http({
        url: `${ StoreService.getApiUrl() }/pages/${ $stateParams.page_id }`,
        method: 'get'
      }).then(res => res.data.result);
    },


    // enable(id) {
    //   return $http({
    //     method: 'post',
    //     url: `${ StoreService.getApiUrl() }/pages/${ id }/enable`
    //   }).then(res => res.data.result);
    // },
    //
    // disable(id) {
    //   return $http({
    //     method: 'post',
    //     url: `${ StoreService.getApiUrl() }/pages/${ id }/disable`
    //   }).then(res => res.data.result);
    // },

    bindBot(botId, access_token) {
      return $http({
        method: 'post',
        data: { access_token },
        url: `${ StoreService.getApiUrl() }/pages/new/bind/${ botId }`
      }).then(res => res.data.result);
    },

    save(data) {
      return $http({
        url: `${ StoreService.getApiUrl() }/pages/${ $stateParams.page_id }`,
        method: 'post',
        data: data
      }).then(res => {
        data.id = res.data.result;
        return data;
      });
    },

    getAppId(pageId) {
      return $http({
        url: `${ StoreService.getApiUrl() }/pages/${ pageId }/apps`,
        method: 'get'
      }).then(res => {
        if (res.data.result.length) {
          return {
            id: res.data.result[0].id,
            chatfuel_app: res.data.result[0].chatfuel_app?true:false
          };
        } else {
          return {
            id: 0,
            chatfuel_app: true
          };
        }
      });
    },

    getAccessPages(){
      return $http({
        url: `${ StoreService.getApiUrl() }/access_pages`,
        method: 'get'
      }).then(res => {
        return res;
      });
    },

    bind(pageId, botId){
      return $http({
        url: `${ StoreService.getApiUrl() }/pages/${ pageId }/bind/${ botId }`,
        method: 'post'
      }).then(res => {
        return res;
      });
    },

    unbind(pageId){
      return $http({
        url: `${ StoreService.getApiUrl() }/pages/${ pageId }/unbind`,
        method: 'post'
      }).then(res => {
        return res;
      });
    }
  };
}
