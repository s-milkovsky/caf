import ng from 'angular';
import PageService from './service';

export default ng.module('app.common.page', [])
  .factory('PageService', PageService)
  .name;
