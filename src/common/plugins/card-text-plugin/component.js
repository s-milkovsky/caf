import template from './template.html';
import controller from './controller';

export default () => ({
  require: {
    PluginController: '^^plugin',
    CardController: '^^pluginCard'
  },
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true,
  restrict: 'E',
  replace: true,
  transclude: true,
  scope: {
    item: '=',
    index: '='
  }
});
