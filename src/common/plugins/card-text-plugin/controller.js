export default class CardTextController {
  constructor($scope, $element, $timeout, ValidationService, $rootScope) {
    'ngInject';
    this.ValidationService = ValidationService;
    this.$timeout = $timeout;
    this.$element = $element;
    this.$scope = $scope;
    this.$rootScope = $rootScope;

    $scope.$on('$buttonUpdate', () => {
      this.validate();
      if ('resizeCard' in $scope) {
        $scope.resizeCard();
      }
    });
    this.init();
  }

  init() {
    this.type = 'text';
    this.field = 'text';
    this.$errors = { message: 'Text is Empty' };
  }

  $onInit() {
    this.ValidationService.get().then(data => {
      this.validation = data;
      this.setDiscount(this.validation[this.type][this.field].max);
      this.updateDiscount();
    });

    if (this.PluginController && this.PluginController.item.plugin_id === 'text') {

      if (!this.PluginController.item.id) {
        this.save();
        this.$timeout(() => {
          this.$element.find('textarea')[0].focus();
        });
      }
      if (this.PluginController.item.is_valid === false) {

        if (this.PluginController.item.validation_details) {
          this.PluginController.item.validation_details = {};
        }

        this.data = this.PluginController.item.validation_details || {};
        this.data.card_id = this.PluginController.item.id;
        this.$rootScope.$broadcast('$pluginSaveResultError', { data: this.data });
        this.validate();
      } else {
        this.PluginController.item.is_valid = true;
        this.$rootScope.$broadcast('$pluginSaveResultOk', this.PluginController.item);
        this.PluginController.item.is_valid = true;
      }
    }


  }


  setDiscount(max) {
    this.discount = max;
  }

  updateDiscount(e) {
    this.leftSymbols = this.discount;
    if (this.item) {
      this.leftSymbols = this.discount - this.item.length;
    }
  }

  validate() {
    const status = Boolean(this.item);
    this.$errors.show = !status;
    return status;
  }

  save() {
    if (!this.validate()) {
     // return;
    }
    this.PluginController.save(this.item, this.index);
  }
}
