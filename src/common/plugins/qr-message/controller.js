export default class QrMessageController {
  constructor($scope, UserService) {
    'ngInject';

    this.$scope = $scope;
    this.UserService = UserService;
  }

  $onInit() {
    this.UserService.show()
    .then((res) => {
      this.isFirstQRAdd = false;
      if (res.flags.indexOf('isFirstQRAdd') === -1) {
        this.isFirstQRAdd = true;
        this.UserService.setFlags('isFirstQRAdd')
        .then(() => {
          this.UserService.show(true);
        });
      }
    });
  }
}
