import template from './template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true,
  restrict: 'E',
  replace: true,
  transclude: true,
  scope: {
    plugin: '='
  }
});
