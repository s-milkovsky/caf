import CardPluginBaseController from '../card-plugin-base-controller/controller';
export default class CardAudioController extends CardPluginBaseController {
  $onInit() {
    if (!this.item.id) {
      this.save();
    }
    if (this.item.is_valid === false) {
      this.data = this.item.validation_details || {};
      this.data.card_id = this.item.id;
      this.$rootScope.$broadcast('$pluginSaveResultError', { data: this.data });
    } else {
      this.$rootScope.$broadcast('$pluginSaveResultOk', this.item);
    }
  }
}
