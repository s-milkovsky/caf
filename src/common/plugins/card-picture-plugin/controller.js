

export default class CardPictureController {
  constructor($timeout, Upload, ModalService, StoreService) {
    'ngInject';

    this.$timeout = $timeout;
    this.Upload = Upload;
    this.ModalService = ModalService;
    this.StoreService = StoreService;
  }

  $onInit() {
    this.$timeout(() => {
      if (!this.item.id) {
        this.save();
      }
    });
  }

  edit() {
    this.ModalService.imageResize(this.item.config.url).then(result => {
      if (result) {
        this.item.config.url = `http://${result}`;
        this.save();
      }
    });
  }

  upload(file, err) {
    if (err.length) {
   //   console.log(err);
      return this.ModalService.imageSize();
    }
    if (file) {
      this.$loading = true;
      this.Upload.upload({
        url: `${this.StoreService.getApiUrl()}/imgupload?max_size=1000`,
       // url: `${this.StoreService.getApiUrl()}/imgupload`,
        data: { file }
      }).then(response => {
        this.$timeout(() => {
          this.item.config.url = `http://${response.data.result}`;
          this.save();
          this.$loading = false;
        });
      }).catch(err => {
     //   console.log(err);
      });
    }
  }

  save() {
    this.PluginListController.savePlugin(this.item);
  }
}
