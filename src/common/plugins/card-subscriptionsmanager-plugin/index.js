import ng from 'angular';

import Component from './component';

export default ng.module('app.common.plugins.cardSubscriptionsmanager', [])
  .directive('pluginCardSubscriptionsmanager', Component).name;
