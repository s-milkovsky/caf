import ng from 'angular';

import Component from './component';

export default ng.module('app.common.plugins.cardCommunicationPlugin', [])
  .directive('pluginCardCommunication', Component).name;
