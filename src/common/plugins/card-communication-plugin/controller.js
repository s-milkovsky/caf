import CardPluginBaseController from '../card-plugin-base-controller/controller';
export default class CardCommunicationController extends CardPluginBaseController {

  $onInit() {
    this._showAdminChatTestButton();
    this.$timeout(() => {
      if (!this.item.id) {
        this.save();
      }
      if (!this.item.validation_details) {
        this.item.validation_details = {};
      }
      if (this.item.is_valid === false) {
        this.data = this.item.validation_details || {};
        this.data.card_id = this.item.id;
        this.$rootScope.$broadcast('$pluginSaveResultError', { data: this.data });
      } else {
        this.$rootScope.$broadcast('$pluginSaveResultOk', this.item);
      }
    });
  }

  _showAdminChatTestButton() {
    this.BotService.show()
      .then(bots => {
        this.bot = bots;
        if (this.bot.status.page) this.page = this.bot.status.page;
      })
      .then(() => {
        if (this.page) {
          this.BotService.testAdminChatButton(this.page, 'white', 'standard')
            .then(code => {
              if (code) this.showFooter = true;
              this.testAdminChatCode = this.$sce.trustAsHtml(code);
              this.$timeout(() => {
                window.FB && FB.XFBML.parse();
              });
            });
        } else {
          this.page = false;
          this.showFooter = true;
        }
      });
  }

  saveCommunication() {
    this.save();
  }

  save() {
    if (this.item.config.idle_time_in_hours < 0 || this.item.config.idle_time_in_hours === undefined) {
      this.item.config.idle_time_in_hours = 1;
    }
    super.save();
  }
}
