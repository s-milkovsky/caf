import CardPluginBaseController from '../card-plugin-base-controller/controller';
export default class CardRssController extends CardPluginBaseController {
  $onInit() {

    this.notSet = 'NOT SET';

    this.isNewArray = [];

    if (!this.item.id) {
      this.save(true);
      this.isNewArray.push(0);

      this.$timeout(() => {
        this.$element[0].querySelector('.svp-variables .cvp-input').focus();
      }, 100);

      this.hasValidationError = false;
    } else {
      this.item.config.items.forEach(item => {
        if (!('value' in item)) {
          item.value = null;
        }
      });

      this.validate();
    }


    this.updateVarsMap(true);
  }

  updateVarsMap(refresh) {
    this.BotService.parameterStats(refresh).then(res => {
      const lResult = {};
      const lKResult = {};

      angular.forEach(res.parameters, (value, key) => {
        let tmp = 0;
        lResult[key] = 0;
        angular.forEach(value, lValue => {
          lResult[key] += lValue;
        });

        if (value[this.notSet]) {
          tmp = Number(value[this.notSet]);
        }

        value[this.notSet] = 9999999999;

        lKResult[key] = Object.keys(value).sort((a, b) => value[b] - value[a]).reduce((acc, curr) => {
          acc[curr] = value[curr];
          return acc;
        }, {});

        value[this.notSet] = tmp;
        lKResult[key][this.notSet] = tmp;

      });
      this.parameterStats = {};
      this.parameterStats.parametersSort = Object.keys(lResult).sort((a, b) => lResult[b] - lResult[a]).reduce((acc, curr) => {
        acc[curr] = lResult[curr];
        return acc;
      }, {});
      this.parameterStats.parameters = lKResult;
    });
  }

  addVar() {
    this.isNewArray.push(this.item.config.items.length);
    this.item.config.items.push({
      variable_name: '',
      value: ''
    });
    this.save();

    this.$timeout(() => {
      const inp = this.$element[0].querySelectorAll('.svp-variables .cvp-input');
      inp[inp.length - 1].focus();
    }, 100);
  }

  removeVar($index) {
    this.item.config.items.splice($index, 1);
    this.save();
  }

  validate() {
    this.item.is_valid = true;
    this.hasValidationError = false;

    this.item.config.items.every(item => {
      if (!item.variable_name || item.value === '') {
        this.item.is_valid = false;
        this.hasValidationError = true;
        return false;
      }
      return true;
    });
  }

  save(noValidate) {
    if (!noValidate) {
      this.$timeout(() => {
        this.validate();
      });
    }
    this.$timeout.cancel(this.saveTimeout);
    this.saveTimeout = this.$timeout(() => {
      super.save();
    }, 1000);
  }
}
