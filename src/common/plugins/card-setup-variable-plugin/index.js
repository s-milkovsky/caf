import ng from 'angular';

import './assets/less/card-setup-variable-plugin.less';
import svpVariables from './svp-variables';
import svpValues from './svp-values';

import Component from './component';

export default ng.module('app.common.plugins.cardSetupVariablePlugin', [svpVariables, svpValues])
  .directive('pluginCardSetupVariable', Component).name;
