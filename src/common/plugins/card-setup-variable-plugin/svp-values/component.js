import template from './svp-values.html';
import controller from './controller';

export default () => ({
  template,
  controllerAs: '$svpValues',
  controller,
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    value: '=',
    vars: '=',
    isNew: '@',
    varName: '@',
    save: '&'
  }
});
