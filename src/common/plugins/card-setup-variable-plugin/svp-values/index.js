import './assets/less/svp-values.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.plugins.cardSetupVariablePlugin.svpValues', [])
  .directive('svpValues', Component)
  .name;
