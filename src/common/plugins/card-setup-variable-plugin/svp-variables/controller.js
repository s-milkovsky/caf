export default class SvpVariablesController {
  constructor($scope, $timeout, $element, $rootScope) {
    'ngInject';

    this.$rootScope = $rootScope;
    this.$element = $element;

    $scope.$watch('$svpVariables.value', () => {
      if (this.value && this.input.innerText.trim() !== this.value.trim()) {
        this.localValue = this.value !== null ? this.value : 'NOT SET';
      }
    });

    $scope.$on('$showPopUp', () => {
      this.popUpOpen = null;
    });

    $scope.$on('document.mouseup', () => {
      $timeout(() => {
        this.popUpOpen = null;
      });
    });

    this.input = $element[0].querySelector('.cvp-input');
    this.popupScrollBox = $element[0].querySelector('.svp-popup');

  }

  $onInit() {
    this.wasBlured = this.isNew === 'false';
  }

  $onDestroy() {

  }

  elClick($event) {
    $event.stopPropagation();
    this.input.focus();
    this.placeCaretAtEnd(this.input);
  }

  onFocusInput() {
    this.updateVarsMap();
    this.$rootScope.$broadcast('$showPopUp');
    this.popUpOpen = true;
    this.popupAct = 0;
  }

  onBlurInput() {
    this.wasBlured = true;
    this.update(true);
    this.save();
  }

  filterShow() {
    const result = {};
    const val = this.value.trim();
    this.emptyPopUp = false;

    angular.forEach(this.vars, (value, key) => {
      if (
        key.toLowerCase().indexOf('fb_') !== 0 &&
        val.toLowerCase() !== key.toLowerCase() &&
        key.toLowerCase().indexOf(val.toLowerCase()) !== -1 &&
        !this.items.find(item => item.variable_name === key)
      ) {
        result[key] = value;
        this.emptyPopUp = true;
      }
    });
    return result;
  }

  compileUserText(n) {
    return String(n).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,') + ' ' + (Number(n) === 1 ? 'user' : 'users');
  }

  onKeyDown(e) {
    const textVal = e.target.innerText.trim();

    if (textVal.length === 0) {
      this.localValue = '';
      e.target.innerText = '';
    }

    if (e.keyCode === 8) {
      if (textVal.length === 1) {
        this.localValue = '';
        e.target.innerText = '';
        this.update();
      }
    } else
    if (e.keyCode === 13) {
      e.stopPropagation();
      e.preventDefault();

      const pars = this.filterShow();

      let count = 0;
      for (const key in pars) {
        count ++;
        if ((count - 1) === this.popupAct) {
          this.localValue = key;
          break;
        }
      }

      if (count > 0) {
        e.target.innerText = this.localValue;
        this.update();
    //    angular.element(e.target).trigger('input');
      }

      this.popupAct = 0;

      this.setFocusToValue();
      this.save();

    } else
    if (e.keyCode === 38) {
      if (this.popupAct > 0) {
        this.popupAct --;
      }
      this.setPopUpScroll();
      e.stopPropagation();
      e.preventDefault();
    } else
    if (e.keyCode === 40) {
      if (this.popupAct < (Object.keys(this.filterShow()).length - 1)) {
        this.popupAct ++;
      }
      this.setPopUpScroll();
      e.stopPropagation();
      e.preventDefault();
    }
  }

  remove($event) {
    this.localValue = '';
    this.input.innerText = '';
    $event.stopPropagation();
    this.input.focus();
    this.update();
    this.save();
  }

  setVar(lVar) {
    this.value = lVar;
    this.setFocusToValue();
    this.save();
  }

  setFocusToValue() {
    setTimeout(() => {
      if (this.localValue.length > 0) {
        this.$element[0].parentNode.parentNode.querySelector('.svp-values .cvp-input').focus();
      }
    }, 100);
  }

  update(trim) {
    if (trim) {
      this.value = this.input.innerText.trim();
    } else {
      this.value = this.input.innerText;
    }
    this.items[Number(this.index)].value = '';
  }

  setPopUpScroll() {
    const actItemObj = this.popupScrollBox.querySelector('.act');
    if (actItemObj) {
      if ((actItemObj.offsetTop - 48) < this.popupScrollBox.scrollTop) {
        this.popupScrollBox.scrollTop = actItemObj.offsetTop - 48;
      } else
      if ((actItemObj.offsetTop + actItemObj.offsetHeight + 48) > (this.popupScrollBox.scrollTop + this.popupScrollBox.offsetHeight)) {
        this.popupScrollBox.scrollTop = actItemObj.offsetTop + actItemObj.offsetHeight - this.popupScrollBox.offsetHeight + 48;
      }
    }
  }

  placeCaretAtEnd(el) {
    if (typeof window.getSelection !== 'undefined'
      && typeof document.createRange !== 'undefined') {
      const range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(false);
      const sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    } else if (typeof document.body.createTextRange !== 'undefined') {
      const textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(false);
      textRange.select();
    }
  }
}
