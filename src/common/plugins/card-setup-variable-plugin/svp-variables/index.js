import './assets/less/svp-variables.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.plugins.cardSetupVariablePlugin.svpVariables', [])
  .directive('svpVariables', Component)
  .name;
