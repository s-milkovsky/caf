import ng from 'angular';

import Component from './component';

export default ng.module('app.common.plugins.cardIntegrationIftttPluginAuto', [])
  .directive('pluginCardIntegrationIftttAuto', Component).name;
