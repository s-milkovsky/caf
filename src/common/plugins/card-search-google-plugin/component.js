import template from './template.html';
import controller from '../card-plugin-search-common-controller/controller';

export default () => ({
  require: {
    PluginListController: '^^pluginList'
  },
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    item: '='
  }
});
