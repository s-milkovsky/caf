import ng from 'angular';

import Component from './component';

export default ng.module('app.common.plugins.cardSearchBing', [])
  .directive('pluginCardSearchBing', Component).name;
