
import _ from 'lodash';

export class PluginCardService {
  constructor($http, $rootScope, StoreService) {
    'ngInject';
    this.$http = $http;
    this.$rootScope = $rootScope;
    this.StoreService = StoreService;
  }

  save(data, async) {
    const ldata = _.cloneDeep(data);


    if (ldata.config.quick_replies && ldata.config.quick_replies.buttons && ldata.config.quick_replies.buttons.length) {
      ldata.config.quick_replies.buttons.forEach(item => {
        item.title = _.unescape(item.title);
      });
    }


    delete ldata.validation_details;


    if (ldata.plugin_id === 'search') {
      ldata.config.number_of_pages = Number(ldata.config.number_of_pages);
    }

    delete ldata.initParam;

    const cleaner = (obj) => {
      for (const key in obj) {
        if (obj[key] === '' && key !== 'postback_email') {
          delete obj[key];
        } else
        if (typeof obj[key] === 'object') {
          cleaner(obj[key]);
        }
      }
    };

    const cleanerNull = (obj) => {
      for (const key in obj) {
        if (obj[key] === null) {
          delete obj[key];
        } else
        if (typeof obj[key] === 'object') {
          cleanerNull(obj[key]);
        }
      }
    };

    if (ldata.plugin_id !== 'json_plugin' && ldata.plugin_id !== 'setup_variable_plugin') {
      cleaner(ldata);
    }
    if (ldata.plugin_id === 'setup_variable_plugin') {
      cleanerNull(ldata);
    }

    return this.$http({
      url: `${this.StoreService.getApiUrl()}/blocks/${ldata.blockId}/cards`,
      method: 'post',
  //    async,
      data: ldata
    }).then(res => (res.data));
  }

  remove(data) {
    return this.$http({
      url: `${this.StoreService.getApiUrl()}/cards/${data.id}/delete`,
      method: 'post'
    }).then(res => (res.data))
      .catch(e => {
      //  console.log(e);
      });
  }
}
