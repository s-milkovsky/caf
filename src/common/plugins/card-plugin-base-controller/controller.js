export default class CardPluginBaseController {
  constructor(
    $scope,
    $rootScope,
    $interpolate,
    ModalService,
    HeaderAlertService,
    BotService,
    BlockService,
    $sce,
    $timeout,
    $state,
    $stateParams,
    $element,
    StoreService,
    PluginCardService
  ) {
    'ngInject';
    this.ModalService = ModalService;
    this.HeaderAlertService = HeaderAlertService;
    this.BotService = BotService;
    this.PluginCardService = PluginCardService;
    this.BlockService = BlockService;
    this.StoreService = StoreService;


    this.$timeout = $timeout;
    this.$element = $element;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$sce = $sce;
    this.$interpolate = $interpolate;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$showSelect = false;
    this.API_URL = this.StoreService.getApiUrl();
    this.botId = this.$state.params.bot_id;
    this.page = null;
    this.showFooter = false;
    this.hasValidationError = false;

    this.validation = {
      title: true,
      subtitle: true,
      button: true
    };


    this.storiesNumbers = { 1: '1 story', 2: '2 stories', 3: '3 stories', 4: '4 stories', 5: '5 stories', 6: '6 stories', 7: '7 stories', 8: '8 stories', 9: '9 stories' };

    this.listeners = [];

    this.listeners.push($scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.$showSelect = false;
        this.$showHelp = false;
      });
    }));

    this.listeners.push($scope.$on('$pluginHasErrorValidation', (e, data) => {
      if (data.id === this.item.id) {
        this.hasValidationError = data.isError;
      }
    }));

    this.BlockService.list().then((res) => {
      this.blockList = res;
      if (this.item.config.subscriptions_block) {
        this.currentBlock = [this.blockList.find(block => block.id === this.item.config.subscriptions_block)];
      } else
      if (this.item.config.no_subscriptions_block_id) {
        this.currentBlock = [this.blockList.find(block => block.id === this.item.config.no_subscriptions_block_id)];
      } else {
        this.currentBlock = [];
        if (!this.item.id) {
          this.save(true);
        }
        if (!this.item.validation_details) {
          this.item.validation_details = {};
        }
        if (this.item.is_valid === false) {
          this.data = this.item.validation_details || {};
          this.data.card_id = this.item.id;
          this.$rootScope.$broadcast('$pluginSaveResultError', { data: this.data });
        } else {
          this.$rootScope.$broadcast('$pluginSaveResultOk', this.item);
        }
      }
    });
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  load($query) {
    return this.BlockService.list($query);
  }

  save() {
    this.PluginListController.savePlugin(this.item);
  }

  saveSearch() {
    this.PluginListController.savePlugin(this.item).then((res) => {
      this.saveError = null;
    }).catch(e => {
      if (
        e.data.message === 'Failed to check Google API with this key and cx: 400' ||
        e.data.message === 'Failed to check Bing API with this token: 401'
      ) {
        this.saveError = 'tokenError';
      }
    });
  }

  setNumberPages(n) {
    this.item.config.number_of_pages = parseInt(n, 10);
    this.save();
  }

  saveInSub() {
    if (this.currentBlock && this.currentBlock.length > 0) {
      this.item.config.no_subscriptions_block_id = this.currentBlock[0].id;
    }
    if (this.currentBlock && this.currentBlock.length > 0) {
      this.item.config.subscriptions_block = this.currentBlock[0].id;
    }
  }

  onTagRemoved() {
    this.$timeout(() => {
      const tInput = document.activeElement;
      tInput.blur();
      this.$timeout(() => {
        tInput.focus();
      });
    });
    this.saveInSub();
  }
}
