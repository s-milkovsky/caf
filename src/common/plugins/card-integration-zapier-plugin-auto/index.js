import ng from 'angular';

import Component from './component';

export default ng.module('app.common.plugins.cardIntegrationZapierPluginAuto', [])
  .constant('AUTOPOSTZAPIER', {
    'autoposting/zapier_plugin': {
      title: 'Zapier',
      link: 'https://zapier.com/developer/invite/41346/322590eb123637445beca8283698f5d3/'
    },
    'autoposting/zapier_rss_plugin': {
      title: 'RSS (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12725/add-cards-to-chatfuel-for-facebook-with-new-rss-feed-items/'
    },
    'autoposting/zapier_youtube_plugin': {
      title: 'YouTube (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12727/add-new-youtube-videos-as-cards-for-your-plugin-on-chatfuel-for-facebook/'
    },
    'autoposting/zapier_sheets_plugin': {
      title: 'Google Sheets (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12728/add-chatfuel-for-facebook-cards-with-new-google-sheets-rows/'
    },
    'autoposting/zapier_calendar_plugin': {
      title: 'Google Calendar (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12729/add-cards-to-chatfuel-for-facebook-when-new-events-start-on-google-calendar/'
    },
    'autoposting/zapier_wordpress_plugin': {
      title: 'WordPress (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12730/add-new-wordpress-posts-to-chatfuel-for-facebook-as-new-cards/'
    },
    'autoposting/zapier_slack_plugin': {
      title: 'Slack (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12731/add-cards-to-chatfuel-for-facebook-with-new-messages-from-a-slack-channel/'
    },
    'autoposting/zapier_twitter_plugin': {
      title: 'Twitter (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12724/add-new-twitter-mentions-to-chatfuel-for-facebook-as-new-cards/'
    },
    'autoposting/zapier_instagram_plugin': {
      title: 'Instagram  (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12726/add-new-media-you-post-to-instagram-as-a-new-card-on-chatfuel-for-facebook/'
    },
    'autoposting/zapier_vimeo_plugin': {
      title: 'Vimeo (Zapier)',
      link: 'https://zapier.com/zapbook/zaps/12732/add-new-vimeo-videos-to-chatfuel-for-facebook-as-new-cards/'
    }
  })
  .directive('pluginCardIntegrationZapierAuto', Component).name;
