import ng from 'angular';

import Component from './component';

export default ng.module('app.common.plugins.cardIntegrationYoutubePluginAuto', [])
  .directive('pluginCardIntegrationYoutubeAuto', Component).name;
