import ng from 'angular';

import Component from './component';

export default ng.module('app.common.plugins.cardVideoPlugin', [])
  .directive('pluginCardVideo', Component).name;
