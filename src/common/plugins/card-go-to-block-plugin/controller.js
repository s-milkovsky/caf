import _ from 'lodash';
import CardPluginBaseController from '../card-plugin-base-controller/controller';
export default class CardGoToBlockController extends CardPluginBaseController {
  constructor(
      $scope,
      $rootScope,
      $interpolate,
      ModalService,
      HeaderAlertService,
      BotService,
      BlockService,
      $sce,
      $timeout,
      $state,
      $stateParams,
      $element,
      StoreService,
      PluginCardService
  ) {
    'ngInject';
    super(
      $scope,
      $rootScope,
      $interpolate,
      ModalService,
      HeaderAlertService,
      BotService,
      BlockService,
      $sce,
      $timeout,
      $state,
      $stateParams,
      $element,
      StoreService,
      PluginCardService);

      this.blocksTitles = [];
      this.excludeBlocks = [];

      this.userFilter = _.cloneDeep(this.item.config.user_filter);
      this.blockItems = this.item.config.action.items;
      this.randomSwitchOn = this.item.config.action.random;

      $scope.$on('$saveUserFilter', () => {
        this.saveParameters();
      })
  }

  $onInit() {
    this.$scope.$on('$updateBlocksInSuggest', event => {
      event.stopPropagation();
      this.save();
    });
  }

  saveParameters() {
    this.$timeout(() => {
      let temp = _.cloneDeep(this.userFilter);
      if (temp.parameters && temp.parameters.length > 0) {
        const res = [];
        temp.parameters.forEach(item => {
          if (!(item.name === '' || item.values.length === 0)) {
            res.push(item);
          }
        });
        temp.parameters = res;
      }
      this.item.config.user_filter = temp;
      this.save();
    }, 100)
  }

  addBlockItems($event) {
    $event.preventDefault();
    this.blockItems.push({item_type: 'block', blocks: []});
    const index = this.blockItems.length - 1;
    this.$timeout(() => {
      this.$scope.$broadcast('$setFocusByIndex', index);
    })
  }
  removeBlockItems(index) {
    this.blockItems.splice(index, 1);
    this.save();
  }
  changeRandom() {
    this.blockItemsCleaner();
    this.save();
  }
  blockItemsCleaner() {
    let usedBlocks = [];
    const lIntent = {};

    lIntent.action = {
      random: this.randomSwitchOn,
      items: []
    };

    this.blockItems.every(item => {
      if (item.item_type === 'block') {
        lIntent.action.items.push({
          item_type: 'block',
          blocks: item.blocks ? item.blocks : []
        });

        usedBlocks = usedBlocks.concat(item.blocks);
      }

      return this.randomSwitchOn;
    });

    this.item.config.action = lIntent.action;
  }
  save() {
    this.blockItemsCleaner();
    super.save();
  }
}
