import ng from 'angular';

import Component from './component';

export default ng.module('app.common.plugins.cardSubscriber', [])
  .directive('pluginCardSubscriber', Component).name;
