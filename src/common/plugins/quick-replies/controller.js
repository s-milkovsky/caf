import _ from 'lodash';

export default class QuickRepliesController {
  constructor($scope, $rootScope, $timeout, $document, $element, BlockService, ModalService, BotService) {
    'ngInject';

    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$timeout = $timeout;
    this.$element = $element;
    this.$document = $document;
    this.BlockService = BlockService;
    this.ModalService = ModalService;
    this.BotService = BotService;

    this.editedItemIndex = null;
    this.editedItemIsValid = true;
    this.saveTimeout = null;
  }

  $onInit() {
    this.$sortable = {
      handle: '.h-drag',
      'ui-floating': true,
      axis: 'x',
      tolerance: 'pointer',
      containment: 'document',
      start: () => {
        this.$scope.$apply(() => {
          this.cancel();
        });
      },
      stop: (e, ui) => {
         this.save();
      }
    };

    this.$document.on('mousedown', () => {
      this.$scope.$apply(() => {
        this.toolTipOpen = null;
      });
    });

    this.listeners = [];

    this.listeners.push(this.$scope.$on('document.click', () => {
      this.$scope.$apply(() => {
        if (this.editedItemIndex !== null && this.editedItemIsValid) {
          this.done();
        }
        if (this.editedItemIndex !== null && !this.editedItemIsValid) {
          this.cancel();
        }
      });
    }));

    this.listeners.push(this.$scope.$on('$buttonUpdate', () => {
      this.validate();
    }));

    this.listeners.push(this.$scope.$on('$updateBlocksInSuggest', () => {
      this.validate();
    }));

    this.listeners.push(this.$scope.$on('$suggestBlured', () => {
      if (this.editedItemIndex !== null && this.editedItemIsValid) {
        (this.$element[0].querySelectorAll('.orange-rounded')[this.editedItemIndex]).focus();
      }
    }));

  }

  $onDestroy() {
    this.$document.off('mousedown');
    this.listeners.forEach(fn => fn.call());
  }

  validate() {
    if (!this.plugin.config.quick_replies || !this.plugin.config.quick_replies.buttons || !this.plugin.config.quick_replies.buttons[this.editedItemIndex]) {
      return;
    }
    this.editedItemIsValid = (this.plugin.config.quick_replies.buttons[this.editedItemIndex].title &&
                 this.plugin.config.quick_replies.buttons[this.editedItemIndex].title.trim().length > 0);
  }

  onKeyDownDone($event) {
    if ($event.keyCode === 13) {
      this.done();
    }
  }

  done($event) {
    if (!this.editedItemIsValid) {
      return;
    }

    this.editedItemIsNew = false;
    this.editedItemBackup = null;

    this.editedItemIndex = null;
    if ($event) {
      $event.stopPropagation();
    }

    this.$timeout(() => {
      const addButton = this.$element[0].querySelector('.items-list .add.item');
      if (addButton) {
        addButton.focus();
      }
    });

    this.save();

    if (document.activeElement) {
      document.activeElement.blur();
    }
  }

  cancel($event) {
    if (this.editedItemIsNew) {
      this.editedItemIsNew = false;
      this.plugin.config.quick_replies.buttons.pop();
    } else {
      this.plugin.config.quick_replies.buttons[this.editedItemIndex] = this.editedItemBackup;
    }

    this.editedItemBackup = null;
    this.editedItemIndex = null;

    if ($event) {
      $event.stopPropagation();
    }

    if (document.activeElement) {
      document.activeElement.blur();
    }
  }

  edit(index, $event) {
    if (this.editedItemIndex === index) {
      return false;
    }

    if ($event) {
      $event.stopPropagation();
    }

    this.editedItemIndex = index;

    this.editedItemBackup = _.cloneDeep(this.plugin.config.quick_replies.buttons[this.editedItemIndex]);

    this.validate();

    this.$timeout(() => {
      // if (
      //   this.plugin.config.quick_replies.buttons[this.editedItemIndex].title &&
      //   this.plugin.config.quick_replies.buttons[this.editedItemIndex].title.trim().length
      // ) {
      //   this.$scope.$broadcast('$setFocusByIndex', this.editedItemIndex);
      // } else {
        const currentInp = this.$element[0].querySelectorAll('.art-textarea-expand')[this.editedItemIndex];
        if (currentInp) {
          currentInp.focus();
// TODO make bested (no double focus)
          currentInp.focus();
        }
 //     }
    }, 100);
  }

  onKeyDownTitle($event) {
    if ($event.keyCode === 13) {
      $event.preventDefault();
      if (
        this.plugin.config.quick_replies.buttons[this.editedItemIndex].title &&
        this.plugin.config.quick_replies.buttons[this.editedItemIndex].title.trim().length
      ) {
        this.$scope.$broadcast('$setFocusByIndex', this.editedItemIndex);
      }
    }
  }

  onKeyDownAdd($event) {
    if ($event.keyCode === 13) {
      this.add();
    }
  }

  add() {
    if (!this.plugin.config.quick_replies) {
      this.plugin.config.quick_replies = {};
    }

    if (!this.plugin.config.quick_replies.buttons) {
      this.plugin.config.quick_replies.buttons = [];
    }

    if (this.plugin.config.quick_replies.buttons.length > 9) {
      return;
    }

    this.plugin.config.quick_replies.buttons.push({ title: '' });

    this.$scope.$emit('$buttonUpdate');

    this.testHelp(2);

    this.editedItemIsNew = true;

    this.$timeout(() => {
      this.edit(this.plugin.config.quick_replies.buttons.length - 1);
    });
  }

  remove(index, $event) {
    $event.stopPropagation();
    this.plugin.config.quick_replies.buttons.splice(index, 1);
    this.save();
    this.$scope.$emit('$buttonUpdate');
  }

  removeAll() {
    this.ModalService.confirm('Do you really want to delete quick reply?').then((res) => {
      if (res) {
        this.plugin.config.quick_replies = { buttons: []};
        this.inValid = [];
        this.save();
        this.$scope.$emit('$buttonUpdate');
      }
    });
  }

  qrMouseOver(flag) {
    this.$rootScope.$broadcast('$onQrMouseOver', { id: this.plugin.id, flag });
  //  this.testHelp(1);
  }

  save() {
    if (!this.plugin.id) {
      return;
    }

    this.$timeout.cancel(this.saveTimeout);

    this.saveTimeout = this.$timeout(() => {
      delete this.plugin.validation_details;
      if (
        this.plugin.config.quick_replies &&
        (!this.plugin.config.quick_replies.buttons ||
        this.plugin.config.quick_replies.buttons.length === 0)
      ) {
        delete this.plugin.config.quick_replies;
      } else {
        this.plugin.config.quick_replies.buttons.forEach(button =>{
          if (button.block_ids && button.block_ids.length === 0) {
            delete button.block_ids;
          }
        });
      }

      this.$rootScope.$broadcast('$onSaveQuickReplies', this.plugin);

      this.$timeout(() => {
        this.BotService.parameterStats(true);
      }, 100);
    }, 1000);
  }

  openPopUp(openHelp) {
    let varName;

    if (this.plugin.config.quick_replies && this.plugin.config.quick_replies.property) {
      varName = this.plugin.config.quick_replies.property;
    }

    this.ModalService.userProperty(varName, openHelp).then(res => {
      if (res) {
        if (res.varName && res.varName.length > 0) {
          this.plugin.config.quick_replies.property = res.varName;
        } else {
          delete this.plugin.config.quick_replies.property;
        }

        this.save();
      }
    });
  }

  removeProperty() {
    delete this.plugin.config.quick_replies.property;
    this.save();
  }

  onHelpClick() {
    if (this.plugin.config.quick_replies && this.plugin.config.quick_replies.buttons && this.plugin.config.quick_replies.buttons.length > 0) {
      this.openPopUp(true);
    } else {
      this.toolTipOpen = (this.toolTipOpen === 1) ? null : 1;
    }
  }

  testHelp(tt) {
    if (!window.localStorage.getItem('qr_tool_tip_showed_' + tt)) {
      if (tt === 1 && this.plugin.config.quick_replies && this.plugin.config.quick_replies.buttons && this.plugin.config.quick_replies.buttons.length) {
        return;
      }

      this.toolTipOpen = tt;
      window.localStorage.setItem('qr_tool_tip_showed_' + tt, 'true');
    }
  }
}
