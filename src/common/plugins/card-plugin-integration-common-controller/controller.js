import CardPluginBaseController from '../card-plugin-base-controller/controller';
export default class CardPluginController extends CardPluginBaseController {
  constructor(
    $scope,
    $rootScope,
    $interpolate,
    ModalService,
    HeaderAlertService,
    BotService,
    BlockService,
    $sce,
    $timeout,
    $state,
    $stateParams,
    $element,
    StoreService,
    PluginCardService,
    AUTOPOSTZAPIER,
    INTEGRATIONZAPIER) {
    'ngInject';
    super(
      $scope,
      $rootScope,
      $interpolate,
      ModalService,
      HeaderAlertService,
      BotService,
      BlockService,
      $sce,
      $timeout,
      $state,
      $stateParams,
      $element,
      StoreService,
      PluginCardService,
      AUTOPOSTZAPIER,
      INTEGRATIONZAPIER);
    this.autopostZapier = AUTOPOSTZAPIER;
    this.integrationZapier = INTEGRATIONZAPIER;
  }
  $onInit() {
    this.$timeout(() => {
      if (!this.item.id) {
        this.save();
      }
      if (!this.item.validation_details) {
        this.item.validation_details = {};
      }
      if (this.item.is_valid === false) {
        this.data = this.item.validation_details || {};
        this.data.card_id = this.item.id;
        this.$rootScope.$broadcast('$pluginSaveResultError', { data: this.data });
      } else {
        this.$rootScope.$broadcast('$pluginSaveResultOk', this.item);
      }
    });
  }
  getZapierTitle(pluginId, type) {
    if (type === 'autoposting') {
      return this.autopostZapier[pluginId].title;
    }
    if (type === 'integration') {
      return this.integrationZapier[pluginId].title;
    }
  }
  getZapierUrl(pluginId, type) {
    if (type === 'autoposting') {
      return this.autopostZapier[pluginId].link;
    }
    if (type === 'integration') {
      return this.integrationZapier[pluginId].link;
    }
  }
}
