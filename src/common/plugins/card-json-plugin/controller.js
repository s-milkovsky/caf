import CardPluginBaseController from '../card-plugin-base-controller/controller';
export default class CardRssController extends CardPluginBaseController {
  $onInit() {
    if (!this.item.config.url) {
      this.item.config.url = '';
    }
    if (!this.item.config.show_error_messages && this.item.config.show_error_messages !== false) {
      this.item.config.show_error_messages = true;
    }
    if (!this.item.id) {
      this.save();
    }
    if (!this.item.validation_details) {
      this.item.validation_details = {};
    }
    if (this.item.is_valid === false) {
      this.data = this.item.validation_details || {};
      this.data.card_id = this.item.id;
      this.$rootScope.$broadcast('$pluginSaveResultError', { data: this.data });
    } else {
      this.$rootScope.$broadcast('$pluginSaveResultOk', this.item);
    }
  }
}
