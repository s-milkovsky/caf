export default class CardImageController {
  constructor($timeout, ModalService, Upload, StoreService) {
    'ngInject';

    this.ModalService = ModalService;
    this.StoreService = StoreService;
    this.Upload = Upload;
    this.$timeout = $timeout;

  }

  $onInit() {
    this.$timeout(() => {
      if (this.item && !this.item.id) {
        this.save();
      }
    });
  }

  edit() {
    this.ModalService.imageResize(this.item.image_url).then(result => {
      if (result) {
        this.item.image_url = `http://${result}`;
        this.save();
      }
    });
  }

  save() {
    this.PluginController.save(this.item, this.index);
  }

  remove() {
    this.item.image_url = '';
    this.save();
  }

  upload(file, err) {
    if (err.length) {
      return this.ModalService.imageSize();
    }
    if (file) {
      this.loading = true;
      file.upload = this.Upload.upload({
        url: `${this.StoreService.getApiUrl()}/imgupload?max_size=1000`,
       // url: `${this.StoreService.getApiUrl()}/imgupload`,
        data: { file }
      }).then(response => {
        this.$timeout(() => {
          this.plugin.config.gallery_cards[this.index].image_url = `http://${response.data.result}`;
          this.CardController.save();
          this.loading = false;
        });
      }, response => {
      //  console.log(response);
      });
    }
  }
}
