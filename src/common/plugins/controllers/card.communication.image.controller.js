export default class CardCommunicationImageController {
  constructor(ModalService) {
    'ngInject';
    this.ModalService = ModalService;
  }

  edit() {
    this.ModalService.imageResize(this.item.config.image_url).then(result => {
      if (result) {
        this.item.config.image_url = `http://${result}`;
        this.save();
      }
    });
  }

  save() {
    this.CardCommunicationController.saveCommunication();
  }

  remove() {
    this.item.config.image_url = '';
    this.save();
  }
}
