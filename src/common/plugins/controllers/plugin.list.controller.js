export default class PluginListController {
  constructor($scope, $state, $stateParams, $rootScope, PluginCardService, BotService, StoreService) {
    'ngInject';
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.PluginCardService = PluginCardService;
    this.BotService = BotService;
    this.StoreService = StoreService;
    this.pluginCreateProc = false;

    this.listener = $scope.$on('$onSaveQuickReplies', ($event, data) => {
      this.savePlugin(data);
    });
  }

  $onDestroy() {
    this.listener();
  }

  addPlugin(type, param) {
    const data = { plugin_id: type, config: {}, position: this.block.cards.length };
    switch (type) {
      case 'text': {
        _.extend(data.config, {
          text: '',
          buttons: []
        });
        break;
      }
      case 'gallery':
        _.extend(data.config, {
          gallery_cards: [
            {
              title: '',
              subtitle: '',
              item_url: '',
              buttons: []
            }
          ]
        });
        break;
      case 'image':
        _.extend(data.config, {
          url: ''
        });
        break;
      case 'form':
        _.extend(data.config, {
          title: '',
          form_properties: [],
          postback_method: 'GET',
          postback_url: ''
        });
      case 'search/bing_plugin':
        _.extend(data.config, {
          query: '',
          title: '',
          number_of_pages: 3,
          subscriptions_available: true,
          share_available: false,
          api_token: ''
        });
        break;
      case 'search/swiftype_plugin':
        _.extend(data.config, {
          query: '',
          title: '',
          number_of_pages: 3,
          subscriptions_available: true,
          share_available: false,
          allow_read_in_bot: false,
          api_token: ''
        });
        break;
      case 'search/google_plugin':
        _.extend(data.config, {
          query: '',
          api_key: '',
          api_cx: '',
          title: '',
          number_of_pages: 3,
          subscriptions_available: true,
          share_available: false
        });
        break;
      case 'subscription_broadcasting_plugin':
        _.extend(data.config, {
          title: 'Here is a digest based on your subscriptions:',
          subscription_title: 'Here\'s what I\'ve found about'
        });
        break;
      case 'communication':
        _.extend(data.config, {
          image_url: null,
          title: 'You started chat session with our operator',
          subtitle: 'To stop this chat just press Stop Chat button, or send "stop chat" message.',
          button_text: 'Stop Chat',
          stop_message: 'Chat with manager is stopped. You can continue chatting with this Chatbot.',
          idle_time_in_hours: 12,
          send_admin_notifications: true
        });
        break;
      case 'integration/ifttt_instagram_plugin':
      case 'integration/ifttt_twitter_plugin':
      case 'integration/ifttt_youtube_plugin':
      case 'integration/zapier_plugin':
      case 'integration/zapier_rss_plugin':
      case 'integration/zapier_youtube_plugin':
      case 'integration/zapier_sheets_plugin':
      case 'integration/zapier_calendar_plugin':
      case 'integration/zapier_wordpress_plugin':
      case 'integration/zapier_slack_plugin':
      case 'integration/zapier_twitter_plugin':
      case 'integration/zapier_instagram_plugin':
      case 'integration/zapier_vimeo_plugin':
        _.extend(data.config, {
          view: 'all'
        });
        break;
      case 'autoposting/rss_plugin':
      case 'autoposting/facebook_plugin':
        _.extend(data.config, {
          url: ''
        });
        break;
      case 'autoposting/twitter_plugin':
        _.extend(data.config, {
          account_name: ''
        });
        break;
      case 'autoposting/ifttt_plugin':
        _.extend(data.config, {
          integration: null
        });
        break;
      case 'autoposting/ifttt_instagram_plugin':
        _.extend(data.config, {
          integration: 'instagram'
        });
        break;
      case 'autoposting/ifttt_twitter_plugin':
        _.extend(data.config, {
          integration: 'twitter'
        });
        break;
      case 'autoposting/ifttt_youtube_plugin':
        _.extend(data.config, {
          integration: 'youtube'
        });
        break;
      case 'integration':
      case 'plugin':
        if (param) {
          data.initParam = param;
        }
        break;
      case 'typing':
        _.extend(data.config, {
          timeout_milliseconds: 1200
        });
        break;
      case 'audio':
        _.extend(data.config, {
          url: ''
        });
        break;
      case 'video':
        _.extend(data.config, {
          url: ''
        });
        break;
      case 'setup_variable_plugin':
        _.extend(data.config, {
          items: [
            {
              variable_name: '',
              value: ''
            }
          ]
        });
        break;
        case 'go_to_block_plugin':
          _.extend(data.config, {
            user_filter: {
              operation: 'and',
              parameters: []
            },
            action: {
              random: false,
              items: [
                  {
                    item_type: "block",
                    blocks: []
                  }
              ]
            }
          });
          break;
        case 'chatroom':
          _.extend(data.config, {
          });
          break;
      default: {
        break;
      }
    }
    this.block.cards.push(data);
    if (type.indexOf('autoposting') !== -1 && this.block.cards.filter(item => item.plugin_id.indexOf('autoposting') !== -1).length <= 1) {
      this.$rootScope.$broadcast('autopost:title', this.block.cards);
    }
  }

  savePlugin(data) {
    const saveData = data;

    if (!data.id) {
      if (this.pluginCreateProc) {
        return;
      } else {
        this.pluginCreateProc = true;
      }
    }

    if (data.id === 'new') {
      delete data.id;
    }

    data.blockId = this.block.id;

    if (data.position === undefined) {
      data.position = this.block.cards.findIndex(item => item.id === data.id);
      if (data.position === undefined) {
        data.position = this.block.cards.length;
      }
    }

    return this.PluginCardService.save(data).then(response => {
      if (!saveData.id) {
        this.pluginCreateProc = false;
      }
      if (response !== undefined) {
        data.id = response && response.result.id;
        data.localization = response.result.localization;
        data.is_valid = true;
      }
      this.$rootScope.$broadcast('$pluginSaveResultOk', data);
      this.$rootScope.$broadcast('$pluginUpdated', data, this.block.cards);
      return data;
    }).catch(err => {
      if (!saveData.id) {
        this.pluginCreateProc = false;
      }
      data.is_valid = false;
      this.$rootScope.$broadcast('$pluginSaveResultError', err);
      this.$rootScope.$broadcast('$pluginUpdated', data, this.block.cards);
      data.id = err.data.card_id;
      data.localization = err.data.localization;
      return data;
    });
  }

  remove(data) {
    if (data.id) {
      const index = _.findIndex(this.block.cards, { id: data.id });
      this.block.cards.splice(index, 1);
      if (data.plugin_id.indexOf('autoposting') !== -1) {
        this.$rootScope.$broadcast('autopost:title', this.block.cards);
      }
      return this.PluginCardService.remove(data).then(() => {
        this.$rootScope.$broadcast('$buttonUpdate');
        this.$rootScope.$broadcast('$pluginUpdated', data, this.block.cards);
      });
    } else {
      const index = _.findIndex(this.block.cards, { $$hashKey: data.$$hashKey });
      if (index === -1) return this.removeEmpty(data);
      this.block.cards.splice(index, 1);
      this.$rootScope.$broadcast('$pluginUpdated', data, this.block.cards);
      return false;
    }
  }

  removeEmpty(data) {
    const removed = this.block.cards.splice(data.position, 1);
    if (removed.length === 0) this.block.cards.splice(-1, 1);
    this.$rootScope.$broadcast('$pluginUpdated', data, this.block.cards);
    return false;
  }
}
