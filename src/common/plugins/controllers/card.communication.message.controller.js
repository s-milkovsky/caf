import CardTextController from '../card-text-plugin/controller';

export default class CardCommunicationMessageController extends CardTextController {

  init() {
    this.discount = 80;
  }

  $onInit() {
    this.updateDiscount();
  }

  save() {
    this.CardCommunicationController.saveCommunication();
  }
}
