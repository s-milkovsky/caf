import _ from 'lodash';

export default class CardButtonsItemController {
  constructor($scope, $rootScope, $state, $stateParams, $timeout, $element, BlockService, BotService, UserService) {
    'ngInject';
    this.$scope = $scope;
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$stateParams = $stateParams;
    this.$timeout = $timeout;
    this.$element = $element;
    this.BotService = BotService;
    this.BlockService = BlockService;
    this.UserService = UserService;

    this.targetTypes = {
      blocks: 'blocks',
      url: 'url',
      phone: 'phone-call',
      share: 'share',
      buy: 'buy'
    };

    this.validatePatterns = {
      http: new RegExp('^(https?://)?(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\\.)+(?:[A-Z]{2,6}\\.?|[A-Z0-9-]{2,}\\.?)|localhost|\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(?::\\d+)?(?:/?|[/?][^ ]+)$', 'i'),
      phone: new RegExp('^\\+?[0-9]+$')
    };

    this.listeners = [];

    this.listeners.push($rootScope.$on('$closeOther', () => {
      if (this.$closeOtherEventSended) {
        this.$closeOtherEventSended = false;
      } else {
        this.closePopup();
      }
    }));

    this.listeners.push($rootScope.$on('$paymentsEnabled', () => {
      this.updateBotPaymentsStatus(true);
    }));

    this.listeners.push($scope.$on('gallery-sort-stop', () => {
      this.$timeout(() => {
        this.dataInit();
      });
    }));

    this.listeners.push($rootScope.$on('buttons-update', () => {
      this.$timeout(() => {
        this.dataInit();
      });
    }));

    this.listeners.push($scope.$on('buttons-sort-stop', () => {
      this.$timeout(() => {
        this.dataInit();
        if (this.index === 0) {
          this.save();
        }
      });
    }));

    this.listeners.push(this.$scope.$on('document.click', () => {
      this.$scope.$apply(() => {
        if (!this.wizardOpen) {
          this.closePopup();
        }
      });
    }));

    this.listeners.push(this.$scope.$on('$updateBlocksInSuggest', () => {
      this.testError();
    }));

    this.listeners.push(this.$scope.$on('$suggestBlured', () => {
      if (this.tmpItem.block_id && this.tmpItem.block_id.length > 0) {
        this.doneButton.focus();
      }
    }));

    this.listeners.push(this.$scope.$on('$setButtonData', (event, newItem) => {
      if (this.index === 0) {
        this.dataInit(newItem);
        this.$timeout(() => {
          this.animateOff = true;
          this.openPopup();
          this.$timeout(() => {
            this.animateOff = false;
          }, 100);
        });
      }
    }));

    this.listeners.push(this.$scope.$on('$openButtonPopUp', (event, index) => {
      if (this.index === index) {
        this.$timeout(() => {
          this.animateOff = true;
          this.openPopup();
          this.$timeout(() => {
            this.animateOff = false;
            const inp = this.$element[0].querySelector('.cost-item-value input');
            if (inp) {
              inp.focus();
            }
          }, 100);
        });
      }
    }));

    this.listeners.push(this.$scope.$on('$moveToFirst', () => {
      this.closePopup(null, true);
    }));

    this.webViewSizes = {
      full: '100% window size',
      tall: '70% window size',
      compact: '40% window size'
    };

    this.blocksTitles = [];
    this.doneFocused = true;
    this.wizardOpen = false;

  }

  $onInit() {
    this.$maxDiscount = 20;

    this.$timeout(() => {
      this.titleObj = this.$element[0].querySelector('.title');
      this.doneButton = this.$element[0].querySelector('.orange-rounded');
    });

    if (this.item.new) {
      this.$timeout(() => {
        this.openPopup();
      });
    }

    this.dataInit();
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
    this.backUp = null;
  }

  onKeyDownDone($event) {
    if ($event.keyCode === 13) {
      $event.preventDefault();

      if (this.testError().length === 0 && this.doneFocused) {
        this.donePopup($event);
      }
    }
  }

  onBlurTitle() {
  //  this.titleFocused = false;
    this.$timeout(() => {
      this.beenChange = true;
      this.$scope.$broadcast('$setFocusOutsideStatus');
    }, 200);
  }

  dataInit(data) {
    if (data) {
      this.tmpItem = data;
    } else {
      this.tmpItem = _.cloneDeep(this.item);
    }

    if (this.tmpItem.title) {
      this.tmpItem.title = _.escape(this.tmpItem.title).trim();
      this.$discount = this.$maxDiscount - this.tmpItem.title.length;
    } else {
      this.$discount = this.$maxDiscount;
    }

    this.typeDetect();

    this.updateBotPaymentsStatus();
  }

  typeDetect() {
    if (this.tmpItem.block_id && this.tmpItem.block_id.length > 0) {
      this.setTargetType(this.targetTypes.blocks);
    } else
    if (this.tmpItem.url && this.tmpItem.url.length > 0) {
      this.setTargetType(this.targetTypes.url);
    } else
    if (this.tmpItem.phone_number && this.tmpItem.phone_number.length > 0) {
      this.tmpItem.phone_number = this.tmpItem.phone_number.replace('+', '');
      this.setTargetType(this.targetTypes.phone);
    } else
    if (this.tmpItem.show_share_screen) {
      this.setTargetType(this.targetTypes.share);
    } else
    if (this.tmpItem.payment_summary) {
      this.setTargetType(this.targetTypes.buy);
    } else
    {
      this.setTargetType(this.targetTypes.blocks);
    }

    this.testError();
  }

  openPopup($event) {
    if (!this.open) {
      this.open = true;
      this.changed = false;
      this.setTitleFocus();
      this.$closeOtherEventSended = true;
      this.$rootScope.$broadcast('$closeOther');
      this.titleFocused = true;
      this.$rootScope.buttonEditorOpen = true;
    }

    $event && $event.stopPropagation();
  }

  closePopup($event, noremove) {
    $event && $event.stopPropagation();
    if (this.open) {
      this.open = false;
      this.$rootScope.buttonEditorOpen = false;
      this.$scope.$emit('$validateCard');

      if (!noremove && (this.testError().length >= 0 || this.targetType === this.targetTypes.share) && this.tmpItem.new) {
        this.$timeout(() => {
          this.remove();
        }, 50);
      } else {
        this.dataInit();
      }
    }

    this.backUp = null;
  }

  donePopup($event) {
    $event && $event.stopPropagation();
    this.open = false;
    this.$rootScope.buttonEditorOpen = false;
    this.save();
  }

  setTargetType(tt, sendEvent) {
    this.targetType = tt;
    this.$timeout(() => {
      this.testError();
    }, 200);


    switch (this.targetType) {
      case this.targetTypes.blocks:
        if (!this.tmpItem.block_id) {
          this.tmpItem.block_id = [];
        }
        if (sendEvent && this.tmpItem.block_id.length === 0) {
          this.$timeout(() => {
            this.$scope.$broadcast('$setFocus');
          });
        }
        break;
      case this.targetTypes.phone:
        this.setCursorToInput();
        break;
      case this.targetTypes.url:
        this.$timeout(() => {
          this.$scope.$broadcast('$setFocus');
        });
        break;
      case this.targetTypes.buy:
        if (!this.tmpItem.payment_summary) {
          this.tmpItem.payment_summary = {
            requested_user_info: ['shipping_address', 'contact_name', 'contact_phone', 'contact_email'],
            price_list: [{ amount: '', label: 'Price' }]
          };
          this.tmpItem.fallback_url = '';
          this.tmpItem.payment_blocks = [];
          this.tmpItem.variables = [];
        }
        break;
      case this.targetTypes.share:
        this.$timeout(() => {
          this.doneButton.focus();
        });
        break;
      default:
    }
  }

  setCursorToInput() {
    this.$timeout(() => {
      const el = this.$element[0].querySelector('.input');
      if (el && el.value.length === 0) {
        el.focus();
        el.selectionStart = el.selectionEnd = el.value.length;
      }
    }, 100);
  }

  setTitleFocus() {
    this.$timeout(() => {
      if (this.titleObj) {
        if (this.titleObj.innerText.length) {
          this.placeCaretAtEnd(this.titleObj);
        } else {
          this.titleObj.focus();
        }
      }
    }, 100);
  }

  testError() {
    this.error = [];
    const title = (this.tmpItem.title || '').replace(/( |&nbsp;)+/g, ' ').replace(/(<([^>]+)>)/ig, '').trim();

    if (this.targetType !== this.targetTypes.share && this.targetType !== this.targetTypes.buy && (!title || title.length === 0)) {
      this.error.push('title');
    }

    switch (this.targetType) {
      case this.targetTypes.blocks:
        if (!this.tmpItem.block_id || this.tmpItem.block_id.length === 0) {
          this.error.push(this.targetTypes.blocks);
        }
        break;
      case this.targetTypes.phone:
        if (!this.tmpItem.phone_number || this.tmpItem.phone_number.length === 0 || !this.validatePatterns.phone.test(this.tmpItem.phone_number)) {
          this.error.push(this.targetTypes.phone);
        }
        break;
      case this.targetTypes.url:
        if (!this.tmpItem.url || this.tmpItem.url.length === 0 || !this.validatePatterns.http.test(this.tmpItem.url)) {
          this.error.push(this.targetTypes.url);
        }
        break;
      case this.targetTypes.share:
        break;
      case this.targetTypes.buy:
        if (!(this.totalCost && this.totalCost > 50) || this.index !== 0 || this.tmpItem.payment_summary.requested_user_info.length === 0) {
          this.error.push(this.targetTypes.buy);
        }
        break;
      default:
    }

    return this.error;
  }

  updateDiscount($event) {
    this.$discount = this.$maxDiscount - $event.target.innerText.trim().length;

    if (this.$discount < 0) {
      $event.target.innerText = $event.target.innerText.trim().substring(0, this.$maxDiscount);
      this.$discount = 0;
    }
  }

  onTrim() {
    this.titleObj.innerText = this.titleObj.innerText.replace(/^\s+|\s+$/g,"");
  }

  onTitleKeyDown($event) {
    if ($event && ($event.keyCode === 13 || $event.keyCode === 9)) {
      if ($event.target.innerText.trim().length > 0) {
        switch (this.targetType) {
          case this.targetTypes.blocks:
            this.$scope.$broadcast('$setFocus');
            break;
          case this.targetTypes.phone:
            this.$element[0].querySelector('input').focus();
            break;
          case this.targetTypes.url:
            this.$element[0].querySelector('.art-textarea-expand').focus();
            break;
          case this.targetTypes.share:
            break;
          default:
        }
      }
      $event.preventDefault();
      $event.stopPropagation();
      return;
    }


    if (this.$discount < 1 && ($event.keyCode > 46 || $event.keyCode === 32)) {
      $event.preventDefault();
      $event.stopPropagation();
      $event.stopImmediatePropagation();
    }
    if (this.$discount < 0) {
      $event.target.innerText = $event.target.innerText.substr(0, this.$maxDiscount);
      this.tmpItem.title = $event.target.innerText;
    }

    // this.$timeout(() => {
    //   const childes = $event.target.querySelectorAll('*');
    //   if (childes && childes.length) {
    //     [].forEach.call(childes, item => {
    //       item.remove();
    //     });
    //   }
    // });
  }

  load($query) {
    return this.BlockService.list($query);
  }

  save() {
    if (this.testError().length === 0) {
      delete this.tmpItem.new;

      switch (this.targetType) {
        case this.targetTypes.blocks:
          delete this.tmpItem.phone_number;
          delete this.tmpItem.url;
          delete this.tmpItem.show_share_screen;
          delete this.tmpItem.webview_height_ratio;
          delete this.tmpItem.payment_summary;
          delete this.tmpItem.fallback_url;
          delete this.tmpItem.payment_blocks;
          delete this.tmpItem.variables;
          break;
        case this.targetTypes.phone:
          this.tmpItem.phone_number = '+' + this.tmpItem.phone_number.replace('+', '');
          delete this.tmpItem.url;
          delete this.tmpItem.show_share_screen;
          delete this.tmpItem.block_id;
          delete this.tmpItem.webview_height_ratio;
          delete this.tmpItem.payment_summary;
          delete this.tmpItem.fallback_url;
          delete this.tmpItem.payment_blocks;
          delete this.tmpItem.variables;
          break;
        case this.targetTypes.url:
          delete this.tmpItem.phone_number;
          delete this.tmpItem.show_share_screen;
          delete this.tmpItem.block_id;
          delete this.tmpItem.payment_summary;
          delete this.tmpItem.fallback_url;
          delete this.tmpItem.payment_blocks;
          delete this.tmpItem.variables;
          break;
        case this.targetTypes.share:
          this.tmpItem.show_share_screen = true;

          delete this.tmpItem.phone_number;
          delete this.tmpItem.url;
          delete this.tmpItem.block_id;
          delete this.tmpItem.webview_height_ratio;
          delete this.tmpItem.payment_summary;
          delete this.tmpItem.fallback_url;
          delete this.tmpItem.payment_blocks;
          delete this.tmpItem.variables;
          break;
        case this.targetTypes.buy:
          delete this.tmpItem.phone_number;
          delete this.tmpItem.url;
          delete this.tmpItem.block_id;
          delete this.tmpItem.webview_height_ratio;
          delete this.tmpItem.show_share_screen;
          this.tmpItem.payment_summary.price_list.forEach((price, i) => {
            if (!price.label || price.label.length === 0) {
              this.tmpItem.payment_summary.price_list[i] = null;
            } else
            if (!price.amount) {
              this.tmpItem.payment_summary.price_list[i].amount = '0.00';
            }
          });

          this.tmpItem.payment_summary.price_list = _.compact(this.tmpItem.payment_summary.price_list);
          break;
        default:
      }

      this.tmpItem.title = _.unescape(this.tmpItem.title).trim().replace('&nbsp;', ' ');


      delete this.item.title;
      delete this.item.phone_number;
      delete this.item.url;
      delete this.item.block_id;
      delete this.item.payment_summary;
      delete this.item.show_share_screen;
      delete this.item.new;
      delete this.item.webview_height_ratio;
      delete this.item.fallback_url;
      delete this.item.payment_blocks;
      delete this.item.variables;

      _.assign(this.item, this.tmpItem);

      this.ButtonsListController.save();
      this.$scope.$emit('$validateCard');
    }
  }

  remove($event) {
    if ($event) {
      $event.preventDefault();
      $event.stopImmediatePropagation();
    }
    this.ButtonsListController.remove(this.index);
    this.$scope.$emit('$validateCard');

    this.$timeout(() => {
      this.$rootScope.$broadcast('buttons-update');
    });
  }

  onKeyDownActionInput($event) {
    if ($event.keyCode === 13 && this.error.length === 0) {
      $event.preventDefault();
      $event.stopImmediatePropagation();
      if (!this.error.length) {
        this.donePopup($event);
      }
    }
  }

  placeCaretAtEnd(el) {
    if (!el || this.targetType === this.targetTypes.share || this.targetType === this.targetTypes.buy) {
      return;
    }
    el.focus();
    if (typeof window.getSelection !== 'undefined'
      && typeof document.createRange !== 'undefined') {
      const range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(false);
      const sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    } else if (typeof document.body.createTextRange !== 'undefined') {
      const textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(false);
      textRange.select();
    }
  }

  updateBotPaymentsStatus(paymentEnabled) {
    if (paymentEnabled) {
      this.botStatusPaymentEnabled = true;
      this.paymentsAccess = true;
      if (this.bot) {
        if (!this.bot.status) {
          this.bot.status = {};
        }
        this.bot.status.payments_status = 'enabled';
      }
    } else {
      this.BotService.cachedList().then(bots => {
        this.bot = bots.find(item => item.id === this.$stateParams.bot_id);
        if (this.bot) {
          this.botStatusConnected = Boolean(this.bot.status.page);
          this.botStatusPaymentEnabled = this.botStatusConnected && this.bot.status.payments_status === 'enabled';
// ///
  //         this.botStatusPaymentEnabled = false;
        }
      });

      this.UserService.show(true).then(res => {
        this.paymentsAccess = res.payments_access;
      });
    }
  }

  testShowBuy() {
    return Boolean(this.items.find(item => item.payment_summary));
  }

  getWarringTTText() {
    let out = '';
    if (this.botStatusConnected) {
      if (this.botStatusPaymentEnabled) {
        out = 'To enable Native Payments you\'ll need to grant Facebook Payment Permission in the payments wizard';
      } else {
        out = 'To enable Native Payments you\'ll need to complete the payment setup wizard';
      }
    } else {
      out = 'To enable Native Payments you\'ll need to make your bot public and complete the payment setup wizard';
    }
    return out;
  }
}
