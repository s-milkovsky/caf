export default class CardController {

  constructor($scope, $rootScope, $element, $state, $stateParams, $timeout) {
    'ngInject';
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.$element = $element;
    this.$timeout = $timeout;
  }

  $onInit() {
    this.$error = { message: 'It is required to fill or "Subtitle" field <br> or add a button or an image' };

    this.validate();
    // @todo: in better way
    // hide tooltip on focus any input
    this.$element.on('focus', 'input, textarea', () => {
      this.$error.show = false;
    });

    this.$scope.$on('$validateCard', () => {
      this.validate();
    })
  }

  $onDestroy() {
    this.$element.off('focus');
  }

  validate() {
    this.$timeout(() => {
      if (this.$rootScope.buttonEditorOpen) {
        return;
      }

      this.$error.show = false;
      if (this.item.title && !this.item.subtitle && !this.item.item_url && !this.item.buttons.length && !this.item.image_url) {
        if (this.item.plugin_id) return;
        this.$error.show = true;
      }
    }, 300);
  }

  save(item, index, event) {
    if (event && event.target === document.activeElement) {
      this.PluginController.save(this.item, this.index);
    } else {
      this.validate();
      this.PluginController.save(this.item, this.index);
    }
    // if (!this.validate()) {
    // //  return;
    // }
    // this.PluginController.save(this.item, this.index);
  }
}
