export default class PluginController {
  constructor($rootScope, $scope) {
    'ngInject';

    this.$rootScope = $rootScope;
    this.$scope = $scope;
  }

  // Save Plugin
  save(data, index) {
    if (index) {
      this.item.config.gallery_cards[index] = data;
    }
    if (!this.validate()) {
    //  return;
    }
    this.PluginListController.savePlugin(this.item);
  }

  validate() {
    switch (this.item.plugin_id) {
      case 'text':
        if (!this.item.config.text) {
          return false;
        }
        break;
      case 'gallery':
        const cards = this.item.config.gallery_cards.filter(card => (!!card.title));
        if (!cards.length || cards.length !== this.item.config.gallery_cards.length) {
          return false;
        }
        break;
      default:
        return true;
    }
    return true;
  }

  // Remove Row
  remove(index) {
    if (index !== undefined) {
      this.item.config.gallery_cards.splice(index, 1);
      if (!this.item.config.gallery_cards.length) {
        return this.PluginListController.remove(this.item);
      }
      return this.PluginListController.savePlugin(this.item);
    }
    this.PluginListController.remove(this.item);

    // const perm = this.PluginListController.remove(this.item);
    // if (perm) {
    //   perm.then(() => {
    //     this.$rootScope.$broadcast('$pluginUpdated', this.item);
    //   });
    // } else {
    //   this.$rootScope.$broadcast('$pluginUpdated', this.item);
    // }
  }

  broadcastEvent(eventName) {
    this.$scope.$broadcast(eventName);
  }
}
