import CardTextController from '../card-text-plugin/controller';

export default class CardCommunicationTitleController extends CardTextController {

  init() {
    this.discount = 80;
    this.$errors = {};
  }

  $onInit() {
    this.updateDiscount();
  }

  save() {
    if (!this.validate()) {
      this.CardCommunicationController.validation.title = false;
    //  return;
    }
    this.CardCommunicationController.validation.title = true;
    this.CardCommunicationController.saveCommunication();
  }
}
