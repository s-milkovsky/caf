

export default class CardCommunicationButtonsTopController {
  constructor($timeout, Upload, ModalService, StoreService) {
    'ngInject';
    this.$timeout = $timeout;
    this.Upload = Upload;
    this.ModalService = ModalService;
    this.StoreService = StoreService;
  }

  upload(file, err) {
    if (err.length) {
      return this.ModalService.imageSize();
    }
    if (file) {
      this.loading = true;
      file.upload = this.Upload.upload({
        url: `${this.StoreService.getApiUrl()}/imgupload?max_size=1000`,
      //  url: `${this.StoreService.getApiUrl()}/imgupload`,
        data: { file }
      }).then(response => {
        this.$timeout(() => {
          this.item.config.image_url = `http://${response.data.result}`;
          this.CardCommunicationController.saveCommunication();
          this.loading = false;
        });
      }, response => {
     //   console.log(response);
      });
    }
  }
}
