import _ from 'lodash';

export default class CardButtonsListController {
  constructor($scope, $rootScope, $timeout) {
    'ngInject';
    this.$scope = $scope;
    this.activeButton = {};


    $scope.$on('$moveToFirst', (event, index, newItem) => {
      newItem.new = true;

      this.items.splice(index, 1);
      this.items.unshift(newItem);

      $timeout(() => {
        $rootScope.$emit('buttons-update');
        this.$scope.$broadcast('$openButtonPopUp', 0);
      });
    });
  }
  save() {
    // const pluginId = this.item.plugin_id;
    // switch (pluginId) {
    //   case 'text': {
    //     this.item.config.buttons = this.item.config.buttons.filter(button => !!button.title);
    //     break;
    //   }
    //   default: {
    //     this.item.buttons = this.item.buttons.filter(button => !!button.title);
    //     break;
    //   }
    // }

    this.$scope.$emit('$buttonUpdate');
    this.PluginController.save(this.item, this.index);
  }

  onOver(flag) {
    this.$scope.$emit('$onActionListOver', { id: this.item.id, flag });
  }

  remove(index) {
    this.items.splice(index, 1);
    this.save();
  }
}
