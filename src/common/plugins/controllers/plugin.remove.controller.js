export default class PluginRemoveController {
  constructor($interpolate, ModalService) {
    'ngInject';
    this.$interpolate = $interpolate;
    this.ModalService = ModalService;
  }

  remove($event) {
    $event.preventDefault();
    const context = this.$interpolate(this.ModalService.templates.plugin())();
    this.ModalService
      .confirm(context)
      .then(isConfirmed => isConfirmed && this.PluginController.remove());
  }
}
