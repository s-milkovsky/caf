

export default class CardButtonsTopController {
  constructor($interpolate, $timeout, Upload, ModalService, StoreService) {
    'ngInject';
    this.$interpolate = $interpolate;
    this.$timeout = $timeout;
    this.Upload = Upload;
    this.ModalService = ModalService;
    this.StoreService = StoreService;
  }

  upload(file, err) {
    if (err.length) {
      return this.ModalService.imageSize();
    }
    if (file) {
      this.loading = true;
      file.upload = this.Upload.upload({
        //url: `${this.StoreService.getApiUrl()}/imgupload`,
        url: `${this.StoreService.getApiUrl()}/imgupload?max_size=1000`,
        data: { file }
      }).then(response => {
        this.$timeout(() => {
          this.item.config.gallery_cards[this.index].image_url = `http://${response.data.result}`;
          this.CardController.save();
          this.loading = false;
        });
      }, response => {
     //   console.log(response);
      });
    }
  }

  remove($event) {
    $event.preventDefault();
    const context = this.$interpolate(this.ModalService.templates.card())();
    this.ModalService
      .confirm(context)
      .then(isConfirmed => isConfirmed && this.PluginController.remove(this.index));
  }
}
