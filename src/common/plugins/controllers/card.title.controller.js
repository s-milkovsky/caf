import CardTextController from '../card-text-plugin/controller';

export default class CardTitleController extends CardTextController {
  init() {
    this.type = 'gallery';
    this.field = 'title';
    this.$errors = { message: 'Title is Empty' };
  }

  save(event) {
    if (!this.validate()) {
     // return;
    }
    this.CardController.save(this.item, this.index, event);
  }
}
