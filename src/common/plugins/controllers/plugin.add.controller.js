export default class PluginAddController {
  constructor($scope, $state, $stateParams, ModalService) {
    'ngInject';

    this.$state = $state;
    this.ModalService = ModalService;

    this.listener = $scope.$on('plugin:add', ($event, type) => {
      this.add($event, type);
    });
  }

  $onDestroy() {
    this.listener();
  }

  add($event, type) {
    const event = $event;
    event.preventDefault();
    this._addCard(type);
  }

  _addCard(type) {
    if (type === 'plugin' && this.$state.params.type === 'auto') {
      this.ModalService.pluginBrowserAutopost().then(result => {
        if (result) {
          this.PluginListController.addPlugin(result.type, null);
        }
      });
    } else if (type === 'plugin') {
      this.ModalService.pluginBrowser().then(result => {
        if (result) {
          if (!result.param) {
            this.PluginListController.addPlugin(result.type, null);
          } else {
            this.PluginListController.addPlugin('plugin', result);
          }
        }
      });
    } else {
      this.PluginListController.addPlugin(type, null);
    }
  }
}
