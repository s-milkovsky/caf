export default class CardAddController {
  constructor() {
    'ngInject';
  }

  add($event) {
    $event.preventDefault();
    this.items.push({
      title: '',
      subtitle: '',
      item_url: '',
      buttons: []
    });
  }
}
