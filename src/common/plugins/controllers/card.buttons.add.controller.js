export default class CardButtonsAddController {
  constructor($scope) {
    'ngInject';
    this.$scope = $scope;
  }

  add($event) {
    $event.preventDefault();
    this.items.push({ title: '', new: true });
    this.$scope.$emit('$buttonUpdate');
  }
}
