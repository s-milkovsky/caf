export default class CardUrlController {
  constructor() {
    this.validate();
  }

  validate() {
    this.$errors = {};
 //   if (this.item && !/^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/.test(this.item)) {
    if (this.item && !(new RegExp('^(https?://)?(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\\.)+(?:[A-Z]{2,6}\\.?|[A-Z0-9-]{2,}\\.?)|localhost|\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(?::\\d+)?(?:/?|[/?][^ ]+)$', 'i')).test(this.item)) {
      this.$errors = { message: 'Invalid URL', show: true };
      return false;
    }
    return true;
  }

  save(event) {
    if (!this.validate()) {
      //return;
    }
    this.CardController.save(this.item, this.index, event);
  }
}
