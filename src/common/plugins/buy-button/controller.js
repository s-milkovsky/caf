import _ from 'lodash';

export default class BuyButtonController {
  constructor($scope, $state, $stateParams, $timeout, $element, ModalService) {
    'ngInject';

    this.$scope = $scope;
    this.$stateParams = $stateParams;
    this.$state = $state;
    this.$timeout = $timeout;
    this.$element = $element;

    this.ModalService = ModalService;

    $scope.$on('gallery-sort-stop', () => {
      this.$timeout(() => {
        this.updateTotal();
      }, 10);
    });
  }

  $onInit() {
    this.updateTotal();
    this.$timeout(() => {
      const inp = this.$element[0].querySelector('.cost-item-value input');
      if (inp) {
        inp.focus();
      }
    });
  }

  $onDestroy() {

  }

  addCostItem() {
    this.item.payment_summary.price_list.push({ amount: '', label: '' });

    this.$timeout(() => {
      const pInp = this.$element[0].querySelectorAll('.cost-item-name input');
      if (pInp.length) {
        pInp[pInp.length - 1].focus();
      }
    });
  }

  removeCostItem($index) {
    this.item.payment_summary.price_list.splice($index, 1);
    this.updateTotal();
  }

  updateTotal() {
    this.totalCost = 0;
    this.item.payment_summary.price_list.forEach(lPrice => {
      const cp = parseFloat(lPrice.amount);
      if (!isNaN(cp)) {
        this.totalCost += Math.round(cp * 100);
      }
    });

    this.$timeout(() => {
      this.testError();
    });
  }

  goSettings() {
    this.$state.go('app.settings', { bot_id: this.$stateParams.bot_id });
  }

  moveToFirst() {
    this.wizardOpen = true;

    this.ModalService.confirmAdv('Are you sure you want to move Buy button to the first position?',
      { ok: 'MOVE', close: 'CANCEL' })
      .then((res) => {
        if (res) {
          // if (this.index === 0) {
          //   this.items.length = 1;
          // } else {

          // }

          delete this.item.phone_number;
          delete this.item.url;
          delete this.item.block_id;
          delete this.item.webview_height_ratio;
          delete this.item.show_share_screen;

          this.$scope.$emit('$moveToFirst', this.index, this.item);

          this.$timeout(() => {
            this.wizardOpen = false;
          });
        }
      });
  }

  onKeyDownPriceLabel($event, $index) {
    if ($event.keyCode === 13 && $event.target.value.trim().length > 0) {
      const pInp = this.$element[0].querySelectorAll('.cost-item-value input');
      if (pInp[$index]) {
        pInp[$index].focus();
      }
    }
  }

  onKeyDownPriceAmount($event, $index) {
    if ($event.keyCode === 13 && $event.target.value.trim().length > 1) {
      const pInp = this.$element[0].querySelectorAll('.cost-item-name input');
      if (pInp[$index + 1]) {
        pInp[$index + 1].focus();
      } else {
      //  this.addCostItem();
        this.donePopup();
      }
    }
  }

  validateURL(val) {
    return !val ||
      val.length === 0 ||
      new RegExp('^(https?://)?(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\\.)+(?:[A-Z]{2,6}\\.?|[A-Z0-9-]{2,}\\.?)|localhost|\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(?::\\d+)?(?:/?|[/?][^ ]+)$', 'i').test(val);
  }

  openWizard() {
    if (this.botStatusConnected) {
      this.wizardOpen = true;
      this.ModalService
        .wizardPayments()
        .then(res => {
          if (res) {
            this.updateBotPaymentsStatus();
          }
          this.$timeout(() => {
            this.wizardOpen = false;
          }, 500);
        });
    }
  }

  advToggle() {
    this.advOpen = !this.advOpen;
    if (this.advOpen) {
      this.aBoxShow = true;
    } else {
      this.$timeout(() => {
        this.aBoxShow = false;
      }, 100);
    }
  }
}
