import ng from 'angular';
import './assets/less/style.less';

import Component from './component';

export default ng.module('app.common.plugins.buyButton', [])
  .directive('buyButton', Component)
  .name;
