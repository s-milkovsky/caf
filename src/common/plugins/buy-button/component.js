import template from './template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'vm',
  bindToController: true,
  restrict: 'E',
  replace: true,
  transclude: true,
  scope: {
    item: '=',
    items: '=',
    index: '=',
    botStatusConnected: '=',
    botStatusPaymentEnabled: '=',
    paymentsAccess: '=',
    totalCost: '=',
    wizardOpen: '=',
    donePopup: '&',
    testError: '&',
    updateBotPaymentsStatus: '&'
  }
});
