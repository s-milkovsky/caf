
const TYPE_URL = 'url';
const TYPE_CARD = 'card';
const TYPE_EMAIL = 'email';
const TYPE_PARAM = 'postback_save_parameters';
import CardPluginBaseController from '../card-plugin-base-controller/controller';
export default class CardFormController extends CardPluginBaseController {
  constructor(
      $scope,
      $rootScope,
      $interpolate,
      ModalService,
      HeaderAlertService,
      BotService,
      BlockService,
      $sce,
      $timeout,
      $state,
      $stateParams,
      $element,
      StoreService,
      PluginCardService
  ) {
    'ngInject';
    super(
      $scope,
      $rootScope,
      $interpolate,
      ModalService,
      HeaderAlertService,
      BotService,
      BlockService,
      $sce,
      $timeout,
      $state,
      $stateParams,
      $element,
      StoreService,
      PluginCardService);

    this.$formProperties = [];

    this.listener = $scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.$typePopupShown = false;
        this.$methodPopupShown = false;
        this.$showHelp = false;
      });
    });
  }

  $onInit() {
    if (!this.item.config.form_properties.length) {
      this.addFormProperty();
    }
    if (!this.item.id) {
      this.save();
    }
    if (!this.item.validation_details) {
      this.item.validation_details = {};
    }
    if (this.item.is_valid === false) {
      this.data = this.item.validation_details || {};
      this.data.card_id = this.item.id;
      this.$rootScope.$broadcast('$pluginSaveResultError', { data: this.data });
    } else {
      this.$rootScope.$broadcast('$pluginSaveResultOk', this.item);
    }
  }

  $onDestroy() {
    this.listener();
  }

  removeFormProperty(index) {
    this.item.config.form_properties.splice(index, 1);
    this.$formProperties = [];
    this.save();
  }

  addFormProperty() {
    this.item.config.form_properties.push({
      caption: '',
      title: '',
      type: 'text'
    });
  }

  _fillFormProperty(index) {
    if (!this.$formProperties[index]) {
      this.$formProperties[index] = {
        title: {
          error: 'Should be filled in',
          touched: false,
          showTooltip: false
        }
      };
    }
  }

  hideTooltip(index, property) {
    this._fillFormProperty(index);
    this.$formProperties[index][property].showTooltip = false;
  }

  updateFormProperty(index, property) {
    this.save();
  }

  getType() {
    switch (true) {
      case ('postback_url' in this.item.config): return TYPE_URL;
      case ('postback_card' in this.item.config): return TYPE_CARD;
      case ('postback_email' in this.item.config): return TYPE_EMAIL;
      case ('postback_save_parameters' in this.item.config): return TYPE_PARAM;
      default: return TYPE_URL;
    }
  }

  getTypeLabel() {
    const labels = {
      [TYPE_URL]: 'Callback',
      [TYPE_CARD]: 'Show URL',
      [TYPE_EMAIL]: 'Email',
      [TYPE_PARAM]: 'Save user variables'
    };

    return labels[this.getType()];
  }

  setType(type) {
    if (this.getType() === type) {
      return;
    }

    switch (type) {
      case TYPE_URL: {
        delete this.item.config.postback_email;
        delete this.item.config.postback_card;
        delete this.item.config.postback_save_parameters;
        delete this.item.config.title;
        this.item.config.postback_url = '';
        this.item.config.postback_method = 'GET';
        break;
      }
      case TYPE_CARD: {
        delete this.item.config.postback_email;
        delete this.item.config.postback_method;
        delete this.item.config.postback_url;
        delete this.item.config.postback_save_parameters;
        delete this.item.config.title;
        this.item.config.postback_card = {
          buttons: [
            { title: '', url: '' }
          ],
          text: ''
        };
        break;
      }
      case TYPE_EMAIL: {
        delete this.item.config.postback_card;
        delete this.item.config.postback_method;
        delete this.item.config.postback_url;
        delete this.item.config.postback_save_parameters;
        this.item.config.postback_email = '';
        this.item.config.title = '';
        break;
      }
      case TYPE_PARAM: {
        delete this.item.config.postback_card;
        delete this.item.config.postback_method;
        delete this.item.config.postback_url;
        delete this.item.config.postback_email;
        delete this.item.config.title;
        this.item.config.postback_save_parameters = true;

        this.locSave();
        break;
      }
    }
  }

  setPostbackMethod(method) {
    this.item.config.postback_method = method;
    this.save();
  }

  locSave() {
    this.save();
    this.$timeout(() => {
      this.BotService.parameterStats(true);
    }, 100);
  }
}
