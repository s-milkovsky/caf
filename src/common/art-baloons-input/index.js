import './assets/less/art-baloons-input.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.artBaloonsInput', [])
  .directive('artBaloonsInput', Component)
  .name;
