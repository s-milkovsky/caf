import template from './art-baloons-input.html';
import controller from './controller';

export default () => ({
  template,
  controllerAs: '$artBaloonsInput',
  controller,
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    ngModel: '=',
    index: '=',
    itemMaxLength: '@'
  }
});
