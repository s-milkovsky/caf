import ng from 'angular';
import showFocusComponent from './component';

export default ng.module('app.common.focus', [])
  .directive('showFocus', showFocusComponent)
  .name;
