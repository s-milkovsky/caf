import ng from 'angular';
import suggestTemplateMultiline from './component';
import './assets/less/style.less';


export default ng.module('app.common.suggestTemplateMultiline', [])
  .directive('suggestTemplateMultiline', suggestTemplateMultiline)
  .name;
