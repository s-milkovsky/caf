import ng from 'angular';

import component from './component';
import service from './service';

export default ng.module('app.common.headerAlert', [])
  .factory('HeaderAlertService', service)
  .directive('headerAlert', component)
  .name;
