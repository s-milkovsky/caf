
export default ($timeout) => {
  'ngInject';
  return {
    text: null,

    setText(text) {


      this.resetNull = true;

      $timeout(() => {
        this.resetNull = false;
      }, 500);

      this.text = text;
    },

    clearText() {
      this.text = null;
    }
  };
}
