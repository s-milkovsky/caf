import template from './header-alert.template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'hac',
  restrict: 'E',
  replace: true
});
