export default class ArtBigDropdownController {
  constructor($scope, $timeout) {
    'ngInject';

    this.$timeout = $timeout;
    this.$scope = $scope;

    $scope.$on('document.mouseup', () => {
      $timeout(() => {
        this.open = false;
      });
    });
  }

  $onInit() {
    if (!this.ngModel) {
      for (const key in this.items) {
        this.ngModel = key;
        break;
      }
    }
  }

  showPopup() {
    if (!this.open) {
      this.$timeout(() => {
        this.open = true;
      });
    }
  }

  setValue(val, $event) {
    $event.stopPropagation();
    $event.stopImmediatePropagation();
    this.ngModel = val;
    this.$scope.$emit('$popupValueChange', { index: this.index, value: val });
    this.open = false;
  }
}
