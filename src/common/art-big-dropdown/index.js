import './assets/less/art-big-dropdown.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.artBigDropdown', [])
  .directive('artBigDropdown', Component)
  .name;
