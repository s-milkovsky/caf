import _ from 'lodash';

export default class BlocksSuggestController {
  constructor($scope, $state, $element, $document, $timeout, $stateParams, $window, StoreService, GroupService, BlockService) {
    'ngInject';
    this.$window = $window;
    this.$document = $document;
    this.$element = $element;
    this.$timeout = $timeout;
    this.$stateParams = $stateParams;
    this.$scope = $scope;
    this.$state = $state;
    this.$rootScope = $scope;

    this.GroupService = GroupService;
    this.BlockService = BlockService;
    this.StoreService = StoreService;

    this.groups = StoreService.get('groups');

    this.showError = false;
    this.focused = false;
  }

  $onInit() {

    this.$timeout(() => {
      this.input = this.$element[0].querySelector('.bs-input');
      this.scrollBox = this.$element[0].querySelector('.bs-content');
      this.popupScrollBox = this.$element[0].querySelector('.bs-popup ul');
    });

    if (!this.blocks) {
      this.blocks = [];
    }

    this.$scope.$on('$setFocus', () => {
      this.setFocusToInput();
      this.scrollToInput();
    });

    this.$scope.$on('$setFocusByIndex', (event, index) => {
      if (index === this.index) {
        this.setFocusToInput();
        this.scrollToInput();
      }
    });

    this.$scope.$on('$setFocusOutsideStatus', (event, val) => {
      this.showError = !val;
    });

    this.$scope.$watch('vm.blocks', () => {
      this.prepareData();
    });

    this.storageEventKey = 'updateBlocks';

    angular.element(this.$window).on('storage', event => {
      if (event.originalEvent.key === this.storageEventKey) {
        this.updateGroups();
      }
    });

    this.$timeout(() => {
      this.updateGroups();
    });
  }

  $onDestroy() {
    angular.element(this.$window).off('storage');
  }

  updateGroups() {
    const val = window.localStorage.getItem(this.storageEventKey);
    if (val && val.indexOf('event') === 0) {
      this.GroupService.list(this.$stateParams.bot_id).then(data => {
        this.groups.length = 0;
        data.forEach(gr => this.groups.push(gr));
        this.mapingBlocks();
        this.pupupUpdate();
        window.localStorage.removeItem(this.storageEventKey);
      });
    }
  }

  onFocusBlock($event, $index) {
    this.currentQueryTitle = null;
    this.currentQuery = null;

    this.currentIndex = $index;
    this.$timeout.cancel(this.popupHideTimout);
    this.hidePopup();
    this.$scope.$emit('$suggestFocused', { index: this.index });

    this.focused = true;
  }

  onBlurBlock($event) {
    if (this.preventBlur && $event) {
      this.preventBlur = null;
      $event.stopImmediatePropagation();
      $event.preventDefault();
      return false;
    }

    this.localBlocks[this.currentIndex] = this.blocks[this.currentIndex];
    this.updateBlocksTitles();

    this.popupHideTimout = this.$timeout(() => {
      $event.stopImmediatePropagation();
      this.hidePopup($event);
    }, 200);
    this.$scope.$emit('$suggestBlured');
    this.focused = false;
  }

  onChangeBlock($event, $index) {
    if ([38, 40].indexOf($event.keyCode) !== -1) {
      return;
    }

    this.pupupUpdate($event);

    if ((this.mapIdToTitle[this.localBlocks[$index]]).toLowerCase().trim() !== this.currentQuery) {
      this.setPopUpPosition($event.target.parentNode);
      this.showPopup($event);
    } else {
      this.hidePopup();
    }

    this.divInputClean($event.target);
  }

  onKeyDownBlock($event, $index) {
    if ([8, 9, 13, 37, 39, 46].indexOf($event.keyCode) !== -1) {
      if ($event.keyCode === 9 || ($event.keyCode === 13 && this.popupShowed)) {
        if ($index === (this.blocksObjList.length - 1)) {
          this.setFocusToInput($event);
        } else {
          this.placeCaretAtEnd(this.blocksObjList[$index + 1]);
        }
        $event.preventDefault();
        $event.stopImmediatePropagation();
      } else
      if ([8, 37].indexOf($event.keyCode) !== -1) {
        if (this.getCaretPosition(this.blocksObjList[$index]) === 0 && $index > 0) {
          this.placeCaretAtEnd(this.blocksObjList[$index - 1]);
          $event.preventDefault();
          $event.stopImmediatePropagation();
        }

        if ($event.keyCode === 8 && this.blocksObjList[$index].innerText.trim().length === 1) {
          this.preventBlur = true;
        }
      } else
      if ($event.keyCode === 46 && this.blocksObjList[$index].innerText.trim().length === 1) {
        this.preventBlur = true;
      } else
      if ($event.keyCode === 39 && this.getCaretPosition(this.blocksObjList[$index]) === (this.blocksObjList[$index].innerText.length) && $index <= (this.blocksObjList.length - 1)) {
        if ($index === (this.blocksObjList.length - 1)) {
          this.setFocusToInput($event);
        } else {
          this.blocksObjList[$index + 1].focus();
        }

        $event.preventDefault();
        $event.stopImmediatePropagation();
      }

      if ($event.keyCode !== 13 && this.blocksObjList[$index].innerText.trim().length === 0) {
        this.$timeout(() => {
          this.blocks.splice($index, 1);
          this.localBlocks.splice($index, 1);
          this.updateBlocksTitles();
        });
      }
    }

    this.onPopUpKeyEvent($event);
  }

  onKeyDownInput($event) {
    if ([8, 37].indexOf($event.keyCode) !== -1 && this.getCaretPosition(this.input) === 0 && this.blocksObjList.length > 0) {
      if ($event.keyCode === 8) {
        this.removeBlock($event, this.blocksObjList.length - 1);
      } else {
        this.placeCaretAtEnd(this.blocksObjList[this.blocksObjList.length - 1]);
      }
      $event.preventDefault();
      $event.stopImmediatePropagation();
    }

    this.onPopUpKeyEvent($event);
  }

  onPopUpKeyEvent($event) {
    if (this.popupShowed && [13, 38, 40].indexOf($event.keyCode) !== -1 && ((this.currentQueryTitle && !this.mapTitleToId[this.currentQueryTitle.toLowerCase()]) || this.outPopup.length)) {
      const index = this.activePopupItems.findIndex(item => item.pos === this.popupItemHover);

      if ($event.keyCode === 38 && index > 0) {
        this.popupItemHover = this.activePopupItems[index - 1].pos;
        this.setPopUpScroll();
      } else
      if ($event.keyCode === 40 && index < (this.activePopupItems.length - 1)) {
        this.popupItemHover = this.activePopupItems[index + 1].pos;
        this.setPopUpScroll();
      } else
      if ($event.keyCode === 13) {
        if (this.popupItemHover === -1) {
          this.createBlock();
        } else {
          this.setBlock($event, this.activePopupItems[index].id);
        }
      }

      $event.preventDefault();
      $event.stopImmediatePropagation();
    }

    if ($event.keyCode === 13) {
      $event.preventDefault();
    }
  }

  onFocusInput($event) {
    this.currentQueryTitle = null;
    this.currentQuery = null;
    this.currentIndex = null;
    this.showPopup($event);

    this.setPopUpPosition($event.target);
    this.scrollToInput();

    this.$timeout.cancel(this.popupHideTimout);
    this.$rootScope.$emit('$suggestFocusedGlobal');
    this.$scope.$emit('$suggestFocused', { index: this.index });

    this.focused = true;
  }

  onChangeInput($event) {
    if ([38, 40].indexOf($event.keyCode) !== -1) {
      return;
    }

    this.pupupUpdate($event);
    this.setPopUpPosition($event.target);
    this.divInputClean($event.target);
  }

  onBlurInput($event) {
    this.popupHideTimout = this.$timeout(() => {
      $event.stopImmediatePropagation();
      this.hidePopup($event);
    }, 300);

    this.$scope.$emit('$suggestBlured', { index: this.index });

    if (this.autoOnShowError === 'true') {
      this.showError = true;
    }

    this.focused = false;
  }

  setPopUpScroll() {
    const actItemObj = this.popupScrollBox.querySelector('.act');
    if (actItemObj) {
      if ((actItemObj.offsetTop - 40) < this.popupScrollBox.scrollTop) {
        this.popupScrollBox.scrollTop = actItemObj.offsetTop - 40;
      } else
      if ((actItemObj.offsetTop + actItemObj.offsetHeight + 40) > (this.popupScrollBox.scrollTop + this.popupScrollBox.offsetHeight)) {
        this.popupScrollBox.scrollTop = actItemObj.offsetTop + actItemObj.offsetHeight - this.popupScrollBox.offsetHeight + 40;
      }
    }
  }

  prepareData() {
    this.mapingBlocks();

    this.localBlocks = [];

    if (this.blocks && this.blocks.length > 0) {
      this.blocks.forEach(blockId => {
        if (this.mapIdToTitle[blockId]) {
          this.localBlocks.push(blockId);
        }
      });
    }

    this.updateBlocksTitles();
    this.$timeout(() => {
   //   this.scrollToInput();
    }, 10);
  }

  updateBlocksTitles() {
    this.localBlocksTitles = this.localBlocks.map(id => this.mapIdToTitle[id]);
    this.blocksTitles = _.cloneDeep(this.localBlocksTitles);

    this.$timeout(() => {
      this.blocksObjList = this.$element[0].querySelectorAll('.block-title');
    });
  }

  mapingBlocks() {
    this.mapIdToTitle = {};
    this.mapTitleToId = {};
    this.groups.forEach(group => {
      group.blocks.forEach(block => {
        this.mapIdToTitle[block.id] = block.title;
        this.mapTitleToId[block.title.toLowerCase()] = block.id;
      });
    });
  }

  pupupUpdate($event) {
    this.$timeout.cancel(this.updatePopupTimeout);

    this.updatePopupTimeout = this.$timeout(() => {

      const query = $event ? $event.target.innerText.toLowerCase().trim() : (this.currentQuery ? this.currentQuery : '');
      const foundPatReplace = str => (query.length ? str.replace(new RegExp('(' + query + ')', 'ig'), '<span>$1</span>') : str);

      if ($event) {
        this.currentQueryTitle = $event.target.innerText.trim();
      }

      this.currentQuery = (query && query.length) ? query : null;

      this.outPopup = [];

      if (this.currentQueryTitle && !this.mapTitleToId[this.currentQueryTitle.toLowerCase()]) {
        this.activePopupItems = [{ pos: -1, id: null }];
      } else {
        this.activePopupItems = [];
      }


      let bCount = 1;

      this.groups.forEach(group => {
        if (group.blocks.length === 0) {
          return;
        }

        const groupFound = (group.title.toLowerCase().indexOf(query) !== -1);
        const lFounds = [];

        group.blocks.forEach(block => {
          if ((
            groupFound ||
            block.title.toLowerCase().indexOf(query) !== -1) &&
            this.localBlocks.indexOf(block.id) === -1 &&
            (!this.excludeBlocks || this.excludeBlocks.indexOf(block.id) === -1
            )) {
            lFounds.push({
              type: 'block',
              title: foundPatReplace(block.title),
              id: block.id
            });

            this.activePopupItems.push({pos: bCount, id: block.id});

            bCount++;
          }
        });

        if (lFounds.length > 0) {
          this.outPopup.push({
            type: 'group',
            title: foundPatReplace(group.title)
          });
          this.outPopup = this.outPopup.concat(lFounds);
          bCount++;
        }
      });

      this.popupItemHover = this.outPopup.length ? 1 : -1;
      this.popupItemHoverId = this.outPopup.length > 1 ? this.outPopup[1].id : null;
    }, 10);
  }

  showPopup($event) {
    this.pupupUpdate($event);

    if (!this.popupShowed) {
      this.$timeout(() => {
        this.popupShowed = true;
      }, 200);
    }
  }

  hidePopup() {
    if (this.popupShowed) {
      this.popupShowed = false;
    }
  }

  setBlock($event, id) {
    if (id) {
      if (!this.blocks) {
        this.blocks = [];
      }

      if (this.currentIndex !== null) {
        this.localBlocks[this.currentIndex] = this.blocks[this.currentIndex] = id;
        this.currentIndex = null;
      } else {
        this.localBlocks.push(id);
        this.blocks.push(id);
        this.input.innerHTML = '';
        this.currentQuery = null;
        this.currentQueryTitle = null;
      }

      this.updateBlocksTitles();
  //    this.pupupUpdate();
      //this.setFocusToInput();


      this.$document[0].activeElement.blur();
    //  this.scrollToInput();

      this.$scope.$emit('$updateBlocksInSuggest', { index: this.index });
      this.$scope.$emit('$blockAdded', { index: this.index });

      this.hidePopup();
    }

    $event.stopImmediatePropagation();
  }

  removeBlock($event, $index) {
    this.localBlocks.splice($index, 1);
    this.blocks.splice($index, 1);

    this.updateBlocksTitles();
    $event.stopPropagation();
    this.setFocusToInput();
    this.$scope.$emit('$updateBlocksInSuggest', { index: this.index });
  }

  setFocusToInput($event) {
    this.$timeout(() => {
      this.setPopUpPosition(this.input);
      if (document.activeElement !== this.input) {
        this.placeCaretAtEnd(this.input);
      }
      this.scrollToInput();
    });
    $event && $event.stopImmediatePropagation();
  }

  scrollToInput() {
    if (this.input) {
      this.scrollBox.scrollLeft = this.input.offsetLeft - this.scrollBox.offsetWidth + this.input.offsetWidth;
      this.setPopUpPosition(this.input);
    }
  }

  setPopUpPosition(el) {
      this.$timeout(() => {
        this.popupLeft = el.offsetLeft - this.scrollBox.scrollLeft;
      }, this.popupShowed ? 10 : 200);
  }


  placeCaretAtEnd(el) {
  //  el.scrollIntoView(true);
    el.focus();
    if (typeof window.getSelection !== 'undefined'
      && typeof document.createRange !== 'undefined') {
      const range = document.createRange();
      range.selectNodeContents(el);
      range.collapse(false);
      const sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    } else if (typeof document.body.createTextRange !== 'undefined') {
      const textRange = document.body.createTextRange();
      textRange.moveToElementText(el);
      textRange.collapse(false);
      textRange.select();
    }
  }

  getCaretPosition(el) {
    let caretPos = 0,
      sel, range;
    if (window.getSelection) {
      sel = window.getSelection();
      if (sel.rangeCount) {
        range = sel.getRangeAt(0);
        if (range.commonAncestorContainer.parentNode === el) {
          caretPos = range.endOffset;
        }
        if (!caretPos) {
          caretPos = sel.anchorOffset;
        }
      }
    } else if (document.selection && document.selection.createRange) {
      range = document.selection.createRange();
      if (range.parentElement() === el) {
        const tempEl = document.createElement('span');
        el.insertBefore(tempEl, el.firstChild);
        const tempRange = range.duplicate();
        tempRange.moveToElementText(tempEl);
        tempRange.setEndPoint('EndToEnd', range);
        caretPos = tempRange.text.length;
        el.removeChild(tempEl);
      }
    }
    return caretPos;
  }

  createBlock() {
    let parentGroup;
    let blockIndex;
    let getNextGroup = false;
    this.groups = this.StoreService.get('groups');

    const currentBlockId = this.$stateParams.id;

    if (currentBlockId) {
      this.groups.every(group => {
        if (getNextGroup && !group.builtin) {
          parentGroup = group;
          blockIndex = 0;
          return false;
        }

        if (currentBlockId) {
          blockIndex = group.blocks.findIndex(block => block.id === currentBlockId);
          if (blockIndex !== -1) {
            blockIndex += 1;
            if (group.builtin) {
              getNextGroup = true;
            } else {
              parentGroup = group;
              return false;
            }
          }
        }

        parentGroup = group;
        blockIndex = group.blocks.length;
        return true;
      });
    } else {
      this.groups.every(group => {
        if (!group.builtin) {
          parentGroup = group;
          return false;
        }
        return true;
      });
      blockIndex = parentGroup.blocks.length;
    }



    const lTitle = this.currentQueryTitle;
    const lIndex = this.currentIndex;


    this.BlockService.create({
      parent_group: parentGroup.id,
      position: blockIndex,
      title: this.currentQueryTitle
    }).then(newBlock => {
      parentGroup.blocks.splice(blockIndex, 0, newBlock);
      this.$rootScope.$emit('block:update', newBlock.id, lTitle);

      if (lIndex !== null) {
        this.blocks[lIndex] = newBlock.id;
      } else {
        this.blocks.push(newBlock.id);
      }


      this.currentQueryTitle = null;
      this.currentQuery = null;
      this.currentIndex = null;
      this.input.innerHTML = '';
      this.prepareData();

      //this.setFocusToInput();

      this.$document[0].activeElement.blur();
      this.scrollToInput();

      this.$scope.$emit('$updateBlocksInSuggest', { index: this.index });
      this.$scope.$emit('$blockAdded', { index: this.index });
    });
  }

  divInputClean(el) {
    const childs = el.querySelectorAll('*');
    if (childs && childs.length) {
      [].forEach.call(childs, item => {
        item.remove();
      });
    }
  }

  go(id) {
    this.$state.go('app.structure.block', { id });
  }
}
