import ng from 'angular';
import './assets/less/blocks-suggest.less';

import component from './component';

export default ng.module('app.common.blocksSuggest', [])
  .directive('blocksSuggest', component)
  .name;
