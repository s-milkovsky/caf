import ng from 'angular';

export default ng.module('app.common.ngKey', [])
  .directive('ngKey', () => (
    (scope, element) => {
      const validKeyCodes = [27, 13]; // escape and enter

      element.bind('keydown keypress', event => {
        if (~validKeyCodes.indexOf(event.which)) {
          setTimeout(() => {
            event.target.blur();
          }, 10);
          event.preventDefault();
        }
      });

      scope.$on('$destroy', () => {
        element.unbind('keydown keypress');
      });
    }
  ))
  .name;
