import template from './template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: 'mob',
  restrict: 'E',
  replace: true
});
