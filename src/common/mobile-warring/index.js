import ng from 'angular';

import component from './component';
import service from './service';
import './assets/less/style.less';

export default ng.module('app.common.mobileWarring', [])
  .factory('MobileWarringService', service)
  .directive('mobileWarring', component)
  .name;
