
export default class MobileWarringController {
  constructor(MobileWarringService) {
    'ngInject';

    this.MobileWarringService = MobileWarringService;
  }

  hide() {
    this.MobileWarringService.hide();
  }
}
