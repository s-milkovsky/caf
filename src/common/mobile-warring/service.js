
export default ($window) => {
  'ngInject';
  return {
    isShow: false,

    show() {
      if (!localStorage.getItem('mobile-warring-closed')){
        this.isShow = true;
        angular.element(angular.element(document.getElementsByTagName('body')[0])).addClass('mobile-warring-show');
        angular.element($window).trigger('resize');
      }
    },

    hide() {
      this.isShow = false;
      angular.element(angular.element(document.getElementsByTagName('body')[0])).removeClass('mobile-warring-show');
      angular.element($window).trigger('resize');
      localStorage.setItem('mobile-warring-closed', 'true');
    }
  };
};
