
import ng from 'angular';
import smartSave from './component';



export default ng.module('app.common.smartSave', [])
  .directive('smartSave', smartSave)
  .name;
