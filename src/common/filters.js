
export function swapLink(StoreService) {
  'ngInject';
  let a = document.createElement('a');

  return function(input) {
    a.href = StoreService.getMainUrl();
    a.host = input + '.' + a.host;

    return a.href;
  };
}

export function ordinal() {
  return function(n) {
    let s = ['th', 'st', 'nd', 'rd']
      , v = n % 100;

    return n + (s[(v - 20) % 10] || s[v] || s[0]);
  };
}

export function removeTime() {
  return function(title) {
    return (title || '').replace(/ \d{1,2}:\d{2} (am|pm)$/i, '');
  };
}

export function inverseSpecialChars() {
  return (input) => {
    const pattern = /(&lt;)|(&gt;)|(&amp;)/g;
    return input.replace(pattern, (str) => (
      { '&lt;': '<',
        '&gt;': '>',
        '&amp;': '&' }[str]
    ));
  };
}
