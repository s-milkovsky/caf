

export class CommonValidationService {
  constructor($http, $log, StoreService) {
    'ngInject';
    this.$http = $http;
    this.$log = $log;
    this.StoreService = StoreService;
  }

  get() {
    if (!this.validation) {
      this.validation = this.fetch();
    }
    return this.validation;
  }

  fetch() {
    return this.$http({
      url: `${this.StoreService.getApiUrl()}/validator_config`,
      method: 'get'
    }).then(res => (res.data.result))
    .catch(e => {
      this.$log.error(e);
    });
  }
}
