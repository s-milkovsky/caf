import template from './user.template.html';
import controller from './controller';

export default function() {
  return {
    template,
    controller,
    controllerAs: 'userCtrl', //vm is overrided
    restrict: 'E',
    replace: true
  };
};
