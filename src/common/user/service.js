
export default ($http, $rootScope, StoreService) => {
  'ngInject';

  let cachedUserPromise;


  return {
    reshow() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl()  }/user`
      }).then(res => res.data.result);
    },

    show(refresh) {
      if (refresh) {
        cachedUserPromise = null;
      }
      if (!cachedUserPromise) {
        cachedUserPromise = this.reshow();
      }

      return cachedUserPromise;
    },


    subscribe(ok) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/user/email_subscription?subscribe=${ (ok?'true':'false') }`
      }).then(res => res.data.result);
    },

    setFlags(flagValue) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/user/add_flag?flag=${flagValue}`
      }).then(res => res.data.result);
    },

    acceptTerms(flagValue) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/accept_terms?subscribe=${flagValue}`
      }).then(res => res.data.result);
    }
  };
};
