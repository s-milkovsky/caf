
export default class UserControlController {
  constructor($scope, $state, $window, $timeout, $rootScope, UserService, ModalService, StoreService) {
    'ngInject';
    this.$scope = $scope;
    this.$state = $state;
    this.$timeout = $timeout;
    this.$window = $window;
    this.$rootScope = $rootScope;
    this.UserService = UserService;
    this.ModalService = ModalService;
    this.StoreService = StoreService;

  //  $scope.$on('$stateChangeSuccess', () => {
      // do not send request when route is auth.token
      // 'case when we redirected to this page from landing page
      // token cannot be set in time
      // if (!$state.is('auth.token')) {
      //   this.loadUser();
      // }
  //  });
  }

  $onInit() {
    if (!this.$state.is('auth.token')) {
      this.loadUser();
    }

    this.listeners = [];

    this.listeners.push(this.$rootScope.$on('$tos', (event, status) => {
      if (!status) {
        this.loadUser();
      }
    }));
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());
  }

  loadUser() {
    if (this.user) {
      return;
    }

    this.UserService.show()
      .then(user => {
        if (user) {
          // identify current user
          if (this.$window.__insp) {
            this.$window.__insp.push(['identify', user.name]);
          }
        }
        this.user = user;
      });
  }

  logout() {
    // this.$window.Intercom("shutdown");
    this.StoreService.setAutorizedCookie(false);
    localStorage.removeItem('token');
    this.$window.location = this.StoreService.getMainUrl();
  }
}
