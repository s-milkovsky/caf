import ng from 'angular';
import UserService from './service';
import userControlComponent from './component';

export default ng.module('app.common.userControl', [])
  .factory('UserService', UserService)
  .directive('userControl', userControlComponent)
  .name;
