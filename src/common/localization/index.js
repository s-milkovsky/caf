import './localization.less'
import ng from 'angular';
import localizationComponent from './component';

export default ng.module('app.common.localization', [])
  .directive('localization', localizationComponent)
  .name;
