export default class LocalizationController {
  constructor($scope) {
    'ngInject';
    this.$scope = $scope;
  }

  $onInit() {
    this.loading = true;
    this.watcher = this.$scope.$watch('vm.item.localization', (data) => {
      if (data) {
        this.watcher();
        this.show = data && Object.keys(data).length !== 0;
        this.localizationStorage = data;
        this.loading = false;
      }
    });
  }

  save(item) {
    this.item.localization = item;
    this.PluginListController.savePlugin(this.item);
  }
}
