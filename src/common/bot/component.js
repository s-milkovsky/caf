import template from './bot.template.html';
import controller from './controller';

export default () => {
  return {
    replace: true,
    restrict: 'E',
    scope: {
      bot: '=',
      bots: '='
    },
    template,
    controller,
    controllerAs: 'vm',
    bindToController: true
  };
};
