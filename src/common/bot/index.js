import ng from 'angular';
import BotService from './service';
import BotItemComponent from './component';

export default ng.module('app.common.bot', [])
  .factory('BotService', BotService)
  .directive('botItem', BotItemComponent)
  .name;
