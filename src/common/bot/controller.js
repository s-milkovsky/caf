export default class BotController {
  constructor(
    $scope,
    $rootScope,
    $state,
    $interpolate,
    $sce,
    $timeout,
    $element,
    BotService,
    ModalService,
    PageService,
    API_ERROR_MSG) {
    'ngInject';
    this.$sce = $sce;
    this.$rootScope = $rootScope;
    this.$state = $state;
    this.$interpolate = $interpolate;
    this.$timeout = $timeout;
    this.$element = $element;
    this.ModalService = ModalService;
    this.BotService = BotService;
    this.PageService = PageService;
    this.API_ERROR_MSG = API_ERROR_MSG;

    this.pageAvatar = '../../assets/images/av_def_bot.png';

    this.statusTitle = 'draft';

    this.updateBotStatus();

    this.listeners = [];

    this.listeners.push($scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.menuShowed = false;
      });
    }));
    this.listeners.push($rootScope.$on('$hideBotMenu', () => {
      this.menuShowed = false;
    }));

    this.listeners.push($scope.$on('$updateBotStatus', (e, data) => {
      if (this.bot.id === data.id) {
        this.bot.status.status = data.status;
        this.bot.status.read_only = data.read_only;
        this.bot.status.page = data.page;
        this.updateBotStatus();
      }
    }));

    // this.BotService.testPageButton(this.bot.id, 'blue', 'large')
    //   .then(code => {
    //     this.testPageCode = this.$sce.trustAsHtml(code);
    //     this.$timeout(() => {
    //       window.FB && FB.XFBML.parse();
    //     });
    //   });

    this._showPageAvatar();
    this.preventClickIfEdited();
  }

  $onDestroy() {
    this.listeners.forEach(fn => fn.call());

    this.$element.find('.content, .button').off('click');
  }

  preventClickIfEdited() {
    const elements = this.$element.find('.content, .button');

    elements.on('click', () => {
      if (this.$edited) return false;
    });
  }

  _showPageAvatar() {
    if (this.bot.status.page) {
      this.pageAvatar = this.bot.status.page_info.picture;
    }
  }

  updateBotStatus() {
    this.statusTitle = this.BotService.getStatusText(this.bot);
    this.buttonTitle = this.BotService.getWizardButtonText(this.bot);
  }

  $timeZone() {
    let timeZone = 'GMT+' + this.bot.timezone_offset / 3600;

    if (this.bot.timezone_name.toLowerCase() !== 'unknown') {
      timeZone += ' ' + this.bot.timezone_name;
    }

    return timeZone;
  }

  toClone(event) {
    this.menuShowed = false;
    event.preventDefault();

    this.ModalService
      .cloneBot(this.bot)
      .then(botTitle => {
        botTitle && this.clone(botTitle);
      });
  }

  clone(newTitle) {
    this.$rootScope.$emit('startCloning');
    this.BotService.create({ title: newTitle })
      .then(newBot => {
        return this.BotService.cloneFrom(newBot.id, this.bot.id);
      })
      .then(newBot => {
        this.bots.push(newBot);
        this.$rootScope.$emit('stopCloning');
      })
      .catch(err => {
        this.$rootScope.$emit('stopCloning');
      //  alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });
  }

  toMoveTo(event) {
    this.menuShowed = false;
    event.preventDefault();

    this.ModalService
      .moveToBot(this.bot)
      .then(botId => {
        botId && this.moveTo(botId);
      });
  }

  moveTo(botId) {
    this.menuShowed = false;
    this.BotService.cloneFrom(this.bot.id, botId)
      .catch(err => {
    //    alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });
  }

  toRemove(event) {
    this.menuShowed = false;
    event.preventDefault();

    const context = this.$interpolate(this.ModalService.templates.bot())({ bot: this.bot });

    this.ModalService
      .confirm(context)
      .then(isConfirmed => {
        isConfirmed && this.remove();
      });
  }

  remove() {
    this.BotService.remove(this.bot.id)
      .then(() => {
        let i = this.bots.findIndex(item => item === this.bot);
        if (i !== -1) {
          this.bots.splice(i, 1);
        }
      })
      .catch(err => {
   //     alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });
  }

  toEdit(event) {
    this.menuShowed = false;
    event.preventDefault();
    this.$edited = true;
  }

  edit(event) {
    this.BotService.update(this.bot.id, { title: this.bot.title })
      .catch(err => {
     //   alert(JSON.stringify(err.data || this.API_ERROR_MSG));
      });

    event.stopPropagation();

    this.$edited = false;
  }

  go() {
    if (!this.$edited) {
      this.$state.go('app.structure', { bot_id: this.bot.id });
    }
  }

  goWizard(event) {
    event.preventDefault();
    this.menuShowed = false;

    const target = this.BotService.getWizardButtonStep(this.bot);
    if (target > 0) {
      this.$state.go('wizard.page' + target, { bot_id: this.bot.id });
    } else {


      switch (target) {
        case -1:
          this.$state.go('app.structure', { bot_id: this.bot.id });

          this.$timeout(() => {
            this.$state.go('app.settings', { bot_id: this.bot.id });
          });
          break;
        case -2:
          this.$state.go('app.structure', { bot_id: this.bot.id });
          break;
        default:
      }
    }
  }

  goWizardEnd(event) {
    event.preventDefault();
    this.menuShowed = false;
    this.$state.go('wizard.page3', { bot_id: this.bot.id });
  }

  showMenu(event) {
    this.$rootScope.$emit('$hideBotMenu');
    event.preventDefault();
    event.stopPropagation();
    this.menuShowed = true;
  }

  onBotReadOnlyChange() {

    if (this.bot.status.read_only) {
      this.bot.status.status = 'pending';
    } else
    if (!this.bot.status.read_only && this.bot.status.status === 'pending') {
      this.bot.status.status = 'published';
    }

    this.BotService.update(this.bot.id, {
      timezone_offset: this.bot.timezone_offset,
      timezone_name: this.bot.timezone_name,
      title: this.bot.title,
      read_only: this.bot.status.read_only,
      status: this.bot.status.status
    });

    this.$rootScope.$broadcast('$updateBotStatus', { id: this.bot.id, status: this.bot.status.status, read_only: this.bot.status.read_only });
  }
}
