export default ($http, $stateParams, StoreService) => {
  'ngInject';
  let cachedListPromise;
  let cachedParameterStatsPromises = [];

  return {

    list() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots`
      }).then(res => {
        res.data.result.forEach(item => {
          if (!item.first_block.id) {
            item.first_block = { id: item.first_block };
          }
        });
        return res.data.result;
      });
    },

    cachedList(refresh) {
      if (refresh) {
        cachedListPromise = null;
      }
      if (!cachedListPromise) {
        cachedListPromise = this.list();
      }

      return cachedListPromise;
    },

    create(data) {
      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots`
      }).then(res => res.data.result);
    },

    update(id, data) {
      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ id }`
      }).then(res => res.data.result);
    },

    show() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }`
      }).then(res => {
        if (!res.data.result.first_block.id) {
          res.data.result.first_block = { id: res.data.result.first_block };
        }
        return res.data.result;
      });
    },

    remove(id) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ id }/delete`
      }).then(res => res.data.result);
    },

    setPayments(id, data) {
      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ id }/payment_props`
      }).then(res => res.data.result);
    },

    getPayments(id) {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ id }/payment_props`
      }).then(res => res.data);
    },

    cloneFrom(botId, fromBotId) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ botId }/clone_from/${ fromBotId }`
      }).then(res => {
        if (!res.data.result.first_block.id) {
          res.data.result.first_block = { id: res.data.result.first_block };
        }
        return res.data.result;
      });
    },

    appParams(id) {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ id }/app_params`
      }).then(res => res.data.result);
    },

    drafts() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/drafts`
      }).then(res => res.data.result);
    },

    testPageButton(botId, color, size) {
      return $http({
        method: 'get',
        params: {color, size},
        url: `${ StoreService.getApiUrl() }/bots/${ botId }/testpage`
      }).then(res => res.data.result.replace(/<script>.*?<\/script>/i, ''));
    },

    testAdminChatButton(pageId, color, size) {
      return $http({
        method: 'get',
        params: {color, size},
        url: `${ StoreService.getApiUrl() }/pages/${ pageId }/admin_button`
      }).then(res => res.data.result.replace(/<script>.*?<\/script>/i, ''));
    },

    messageUsButton(botId, color, size) {
      return $http({
        url: `${ StoreService.getApiUrl() }/bots/${ botId }/messageus`,
        params: {color, size},
        method: 'get'
      }).then(res => res.data.result);
    },

    getWizardButtonText(lbot) {
      let status = 'draft';
      let res;

      if (lbot && lbot.status && lbot.status.status) {
        status = lbot.status.status;
      }

      switch (status) {
        case 'connected':
          res = 'Submit for review';
          break;
        case 'pending':
          res = 'Check status';
          break;
        case 'published':
          res = 'Rerun wizard';
          break;
        default:
          res = 'connect to facebook';
      }

      return res;
    },

    getStatusText(lbot) {
      let status = 'draft';
      let readOnly = false;
      let res;

      if (lbot && lbot.status && lbot.status.status) {
        status = lbot.status.status;
        readOnly = lbot.status.read_only;
      }

      switch (status) {
        case 'connected':
          res = 'connected to the page';
          break;
        case 'pending':
          res = 'pending';
          break;
        case 'published':
          res = 'connected to the page';
          break;
        default:
          res = 'draft';
      }

      if (readOnly) {
        //  res += ', read-only';
      }

      return res;
    },

    getWizardButtonStep(lbot) {
      let status = 'draft';
      let res;

      if (lbot && lbot.status && lbot.status.status) {
        status = lbot.status.status;
      }

      switch (status) {
        case 'connected':
          res = 3;
          break;
        case 'pending':
          res = -2;
          break;
        case 'published':
          res = 1;
          break;
        default:
          res = -1;
      }

      return res;
    },


    getAiLanguages() {
      return $http({
        url: `${StoreService.getApiUrl()}/witai/languages`,
        method: 'get'
      }).then(res => (res.data.result))
        .catch(e => {
          //  console.log(e);
        });
    },

    getParams() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/public_parameters`
      }).then(res => res.data.result);
    },

    getMenu() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/menu`
      }).then(res => res.data.result);
    },

    setMenu(data) {
      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/menu`
      }).then(res => res.data.result);
    },

    getInvite() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/invite`
      }).then(res => res.data.result);
    },

    removeAdmin(uId) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/delete_admin/${ uId }`
      }).then(res => res.data.result);
    },

    unbindBot() {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/unbind`
      }).then(res => res.data.result);
    },

    userCount(data) {
      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/user_count`
      }).then(res => res.data.result);
    },

    parameterStats(refresh) {
      const botId = $stateParams.bot_id;

      if (refresh) {
        cachedParameterStatsPromises[botId] = null;
      }
      if (!cachedParameterStatsPromises[botId]) {
        cachedParameterStatsPromises[botId] = this.parameterStatsUpdate(botId);
      }

      return cachedParameterStatsPromises[botId];
    },

    parameterStatsUpdate() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/parameter_stats`
      }).then(res => res.data.result);
    },

    getSettingNotifications() {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/notifications`
      }).then(res => res.data.result);
    },

    getBroadcastStats(fromDate) {
      fromDate = fromDate ? fromDate : '';
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/broadcast_stats?limit=50&from=${ fromDate }`
      }).then(res => res.data.result);
    }
  };
};
