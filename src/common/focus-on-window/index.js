import ng from 'angular';

function focusOnWindow() {
  return (scope, element) => {
    let mouseOver = false;

    setTimeout(() => { window.focus(); }, 500);

    element.on('mouseenter', () => {
      mouseOver = true;
    });

    element.on('mouseleave', () => {
      mouseOver = false;
    });

    ng.element(window).on('blur', () => {
      if (mouseOver) setTimeout(() => { window.focus(); }, 500);
    });

    const listener = scope.$on('$destroy', () => {
      element.off('mouseenter');
      element.off('mouseleave');
      ng.element(window).off('blur');
      listener();
    });
  };
}

export default ng.module('app.common.focusOnWindow', [])
  .directive('focusOnWindow', focusOnWindow)
  .name;
