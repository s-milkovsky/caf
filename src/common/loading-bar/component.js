
export default function($http) {
  'ngInject';
  return {
    restrict: 'A',
    link(scope, element, attrs) {
      function isSaving() {
        return $http.pendingRequests.filter(item => item.method === 'POST').length > 0;
      }
      const watcher = scope.$watch(isSaving, function(value) {
        element.text(value ? 'Saving' : 'Saved');
        element.css('color', (value ? '#000' : '#35c67c'));
        if (value) {
          element.addClass('spinner');
        } else {
          element.removeClass('spinner');
        }
      });

      scope.$on('$destroy', () => {
        watcher();
      });
    }
  };
};
