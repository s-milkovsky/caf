import angular from 'angular';
import loadingBarComponent from './component';

export default angular
  .module('app.common.loadingBar', [])
  .directive('loadingBar', loadingBarComponent)
  .name;
