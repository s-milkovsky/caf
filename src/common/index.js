import ng from 'angular';
import Broadcast from './broadcast';
import Group from './group';
import Bot from './bot';
import Block from './block';
import Plugins from './plugins';
import Page from './page';
import Tutorial from './tutorial';
import ShowFocus from './show-focus';
import LoadingBar from './loading-bar';
import InputControl from './input-control';
import User from './user';
import Modal from './modal';
import ngKey from './ng-key';
import ngExpand from './ng-expand';
import ngAutosize from './ng-autosize';
import selectText from './select-text';
import fbIframeClick from './fb-iframe-click';
import noInputSelectText from './no-input-select-text';
import autoResize from './auto-resize';
import fbTestBotBtnClick from './fb-test-bot-btn-click';
import focusOnWindow from './focus-on-window';
import suggestTemplate from './suggest-template';
import suggestTemplateMultiline from './suggest-template-multiline';
import headerAlert from './header-alert';
import headerAlertErrors from './header-alert-errors';
import smartSave from './smart-save';
import autoSave from './auto-save';
import imageCrop from './image-crop';
import mobileWarring from './mobile-warring';
import localization from './localization';
import flatStripTags from './flat-strip-tags';
import draggableTextFields from './draggable-text-fields';
import textareaVExpand from './textarea-v-expand';
import safePaste from './safe-paste';
import artSwitcher from './art-switcher';
import artMiniDropdown from './art-mini-dropdown';
import artTextareaExpand from './art-textarea-expand';
import artBaloonsInput from './art-baloons-input';
import artBigDropdown from './art-big-dropdown';
import blocksSuggest from './blocks-suggest';
import artCheckbox from './art-checkbox';
import artOnlyNumber from './art-only-number';
import artOnlyNumberCost from './art-only-number-cost';
import artCurrency from './art-currency';
import titleSlicer from './title-slicer';
import artVarsSuggest from './art-vars-suggest';
import userFilter from './user-filter';

import { CommonValidationService } from './services';
import { swapLink, ordinal, removeTime, inverseSpecialChars } from './filters';

export default ng.module('app.common', [
  Broadcast,
  Group,
  Bot,
  Block,
  Plugins,
  Tutorial,
  ShowFocus,
  LoadingBar,
  InputControl,
  User,
  Page,
  Modal,
  ngKey,
  ngExpand,
  ngAutosize,
  selectText,
  fbIframeClick,
  noInputSelectText,
  autoResize,
  fbTestBotBtnClick,
  focusOnWindow,
  suggestTemplate,
  suggestTemplateMultiline,
  headerAlert,
  headerAlertErrors,
  smartSave,
  autoSave,
  imageCrop,
  mobileWarring,
  localization,
  flatStripTags,
  draggableTextFields,
  textareaVExpand,
  flatStripTags,
  artSwitcher,
  artMiniDropdown,
  artTextareaExpand,
  artBaloonsInput,
  artBigDropdown,
  blocksSuggest,
  safePaste,
  artCheckbox,
  artOnlyNumber,
  artOnlyNumberCost,
  artCurrency,
  titleSlicer,
  artVarsSuggest,
  userFilter
])
  .service('ValidationService', CommonValidationService)
  .filter('removeTime', removeTime)
  .filter('swapLink', swapLink)
  .filter('ordinal', ordinal)
  .filter('inverse', inverseSpecialChars)
  .name;
