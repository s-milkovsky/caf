export default ($http, $stateParams, StoreService) => {
  'ngInject';


  function mapItem(item) {
    item.deadline *= 1000;
    return item;
  }

  return {
    list(from, to) {
      from = from / 1000 | 0;
      to = to / 1000 | 0;

      return $http({
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/broadcast`,
        params: { from, to },
        method: 'get'
      })
        .then(res => res.data.result.map(mapItem));
    },

    show(id) {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/broadcasts/${ id }`
      })
        .then(res => mapItem(res.data.result));
    },

    showRecurrent(id) {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/recurrent_broadcasts/${ id }`
      })
        .then(res => mapItem(res.data.result));
    },

    remove(id) {
      return $http({
        url: `${ StoreService.getApiUrl() }/broadcasts/${ id }/delete`,
        method: 'post'
      })
        .then(res => res.data);
    },

    removeRecurrent(id) {
      return $http({
        url: `${ StoreService.getApiUrl() }/recurrent_broadcasts/${ id }/delete`,
        method: 'post'
      })
        .then(res => res.data);
    },

    update(id, data) {
      if (data.deadline) {
        data.deadline = data.deadline / 1000 | 0;
      }

      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/broadcasts/${ id }`
      })
        .then(res => res.data.result);
    },

    updateRecurrent(id, data) {

      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/recurrent_broadcasts/${ id }`
      })
        .then(res => res.data.result);
    },


    listRecurrent() {
      return $http({
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/recurrent_broadcast`,
        method: 'get'
      })
        .then(res => res.data.result.map(mapItem));
    },

    listAutopost() {
      return $http({
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/autoposting`,
        method: 'get'
      })
        .then(res => res.data.result.map(mapItem));
    }
  };
};
