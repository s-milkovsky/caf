import ng from 'angular';
import BroadcastService from './service';

export default ng.module('app.common.broadcast', [])
  .factory('BroadcastService', BroadcastService)
  .name;
