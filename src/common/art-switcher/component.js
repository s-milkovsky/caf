import template from './art-switcher.html';
import controller from './controller';

export default () => ({
  template,
  controllerAs: '$artSwitcher',
  controller,
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    ngModel: '='
  }
});
