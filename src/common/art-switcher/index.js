import './assets/less/art-switcher.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.artSwitcher', [])
  .directive('artSwitcher', Component)
  .name;
