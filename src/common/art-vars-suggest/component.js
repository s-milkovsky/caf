import template from './art-vars-suggest.html';
import controller from './controller';

export default () => ({
  template,
  controllerAs: '$artVarsSuggest',
  controller,
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    ngModel: '=',
    itemMaxLength: '@'
  }
});
