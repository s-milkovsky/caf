import _ from 'lodash';

export default class ArtVarsSuggestController {
  constructor($scope, $timeout, BotService) {
    'ngInject';

    if (!this.ngModel) {
      this.ngModel = [];
    }

    BotService.parameterStats().then(res => {
      const lResult = {};
      angular.forEach(res.parameters, (value, key) => {
        lResult[key] = 0;
        angular.forEach(value, lValue => {
          lResult[key] += lValue;
        });
      });
      this.parameterStats = Object.keys(lResult).sort((a,b) => lResult[b]-lResult[a]).reduce((acc, curr) => {
        acc[curr] = lResult[curr];
        return acc;
      }, {});
    });

    $scope.$on('document.mouseup', () => {
      $timeout(() => {
        this.valuesPopUpOpen = null;
      });
    });
  }

  $onInit() {



  }

  $onDestroy() {

  }

  valuesClick($event) {
    $event.stopPropagation();
    $event.currentTarget.querySelector('input').focus();
    this.valuesPopUpOpen = true;
    this.valuePopupAct = 0;
  }

  filterShow() {
    const result = {};
    angular.forEach(this.parameterStats, (value, key) => {
      if (
        this.ngModel.indexOf(key) === -1
        && (!this.inputModel || this.inputModel.length === 0 || key.toLowerCase().indexOf(this.inputModel.toLowerCase()) !== -1)
      ) {
        result[key] = value;
      }
    });
    return result;
  }

  compileUserText(n) {
    return String(n).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,') + ' ' + (Number(n) === 1 ? 'user' : 'users');
  }


  onInputFocus(e, index) {
    this.focused = true;
    angular.element(e.target).parent().parent().scrollLeft(0);
  }

  onInputBlur(e, index) {
    angular.element(e.target).parent().parent().scrollLeft(0);
    this.focused = false;
  }

  onKeyDown(e) {
    if (e.keyCode === 8) {
      if (!this.inputModel || this.inputModel.length === 0) {
        this.ngModel.pop();
        e.stopPropagation();
        e.preventDefault();
      }
    } else
    if (e.keyCode === 13) {
      e.stopPropagation();
      e.preventDefault();

      const pars = this.filterShow();

      let count = 0;
      for (const key in pars) {
        count ++;
        if ((count - 1) === this.valuePopupAct) {
          this.ngModel.push(key);
          break;
        }
      }

      if (count > 0) {
        e.target.value = '';
        angular.element(e.target).trigger('input');
      }

      this.valuePopupAct = 0;
    } else
    if (e.keyCode === 38) {
      if (this.valuePopupAct > 0) {
        this.valuePopupAct --;
      }
      e.stopPropagation();
      e.preventDefault();
    } else
    if (e.keyCode === 40) {
      if (this.valuePopupAct < (Object.keys(this.filterShow()).length - 1)) {
        this.valuePopupAct ++;
      }
      e.stopPropagation();
      e.preventDefault();
    }
  }

  removeVar(index, $event) {
    this.ngModel.splice(index, 1);
    $event.stopPropagation();
    this.valuesPopUpOpen = null;
  }

}
