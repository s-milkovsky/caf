import './assets/less/art-vars-suggest.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.artVarsSuggest', [])
  .directive('artVarsSuggest', Component)
  .name;
