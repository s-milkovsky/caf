export default class ImageCropController {
  constructor($scope, $element, $timeout, Cropper) {
    'ngInject';

    this.$element = $element;
    this.$timeout = $timeout;
    this.Cropper = Cropper;

    this.watcher = $scope.$watch('image', () => { this._initCrop(); });
  }

  $onDestroy() {
    this.watcher();
    this.destroyCrop();
  }

  _setCropperSettings() {
    return {
      aspectRatio: 16 / 9,
      viewMode: 1,
      zoomable: false,
      zoomOnTouch: false,
      zoomOnWheel: false
    };
  }

  destroyCrop() {
    if (this.croppingImage) {
      this.croppingImage.off('load');
      this.croppingImage.off('built');
      this.croppingImage.off('cropend');
      this.croppingImage = null;

      if (this.cropper) {
        this.cropper = null;
      }
    }
  }

  _initCrop() {
    this.croppingImage = this.$element.find('#cropping-image');

    this.croppingImage.on('load', () => {
      this.cropper = new this.Cropper(this.croppingImage[0], this._setCropperSettings());
    });
    this.croppingImage.on('built', () => {
      this.$timeout(() => { this.loaded = true; }, 10);
    });
    this.croppingImage.on('cropend', () => {
      this.croppedImage = this.cropper.getCroppedCanvas().toDataURL('image/png', 1.0);
    });
  }
}
