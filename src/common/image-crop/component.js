import template from './template.html';
import controller from './controller';

export default () => ({
  template,
  controller,
  controllerAs: '$imageCrop',
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    image: '=',
    croppedImage: '=',
    loaded: '='
  }
});
