import 'cropperjs/dist/cropper.css';
import Cropper from 'cropperjs/dist/cropper.js';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.imageCrop', [])
  .directive('imageCrop', Component)
  .constant('Cropper', Cropper)
  .name;
