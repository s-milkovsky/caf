<div class="content">

  <p class="title">
    Broadcast preferences
  </p>

  <div>
    Send using
    <div class="selector" ng-class="{act: vm.$showSelect == true}">
      <div class="label placeholder" ng-click="$event.stopPropagation(); vm.$showSelect = true" ng-bind="vm.useBotTimezone ? 'Bot\'s timezone' : 'User\'s timezone'"></div>
      <ul>
        <li class="disabled">Send using:</li>
        <li ng-click="vm.useBotTimezone = true">Bot's timezone</li>
        <li ng-click="vm.useBotTimezone = false">User's timezone</li>
      </ul>
    </div>
  </div>




</div>

<div class="footer">
  <div class="button b-true" ng-click="vm.close()">Cancel</div>
  <div class="button b-false" ng-click="vm.ok()">Save</div>
</div>

<div class="button close" ng-click="vm.close()"></div>
