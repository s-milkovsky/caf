
import ng from 'angular';

export default class ModalController {
  constructor($q, $http, $uibModalInstance, image, StoreService) {
    'ngInject';
    this.StoreService = StoreService;
    this.$q = $q;
    this.$http = $http;
    this.$modal = $uibModalInstance;
    this.image = image;
  }

  $onInit() {
    this.croppedImage = '';
    this.convert().then(result => result);
  }

  convert() {
  //  this.image = this.image.replace('imagizer.imageshack', 'img.chatfuel');

    this.image = this.image.replace('imagizer.imageshack.com', 'img.chatfuel.com');
    this.image = this.image.replace('imagizer.imageshack.us', 'img.chatfuel.com');

    return this.$q.when(this.image);
  }

  dataURItoBlob(dataURI) {
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], { type: mimeString });
  }

  save() {
    const blob = this.dataURItoBlob(this.croppedImage);
    const data = new FormData();
    data.append('file', blob);
    this.$http.post(`${this.StoreService.getApiUrl()}/imgupload?max_size=1000`, data, {
   // this.$http.post(`${this.StoreService.getApiUrl()}/imgupload`, data, {
      transformRequest: ng.identity,
      headers: {
        'Content-Type': undefined
      }
    }).success(response => {
      this.$modal.close(response.result);
    }).error(e => {
    //  console.log(e);
    });
  }

  close() {
    this.$modal.close(null);
  }
}
