<div class="title">Edit Image</div>

<div class="content" ng-class="{'crop-preloader': !vm.loaded}">
  <image-crop image="vm.image" cropped-image="vm.croppedImage" loaded="vm.loaded"></image-crop>
</div>

<div class="footer">
  <div class="button b-true" ng-click="vm.close()">Cancel</div>
  <div
    class="button b-false"
    ng-class="{'crop-btn-disabled': !vm.croppedImage}"
    ng-click="vm.croppedImage && vm.save()">Done</div>
</div>

<div class="button close" ng-click="vm.close()"></div>
