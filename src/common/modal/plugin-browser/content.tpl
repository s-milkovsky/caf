<div class="content">
  <h2>
    Chatfuel Plugins
  </h2>

  <h3>
    Most used:
  </h3>

  <ul>
    <li ng-click="vm.addPlugin('json_plugin',null)" class="json" ng-show="!vm.isBc">
      JSON API
      <div ng-click="vm.$showHelp = 11; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 11, right: vm.$showHelp == 11 }">
        <div class="popup-help">
          <h3>
            JSON API
          </h3>

          <div class="help-image">
            <div class="line right size-295">
              <div class="box">
                <div class="img msg-user1"></div>
              </div>
            </div>
            <div class="line left">
              <div class="box white">
                <div class="img top-img1"></div>
                <div class="img bottom-text1"></div>
              </div>
              <div class="box white">
                <div class="img top-img2"></div>
                <div class="img bottom-text1"></div>
              </div>
              <div class="box white">
                <div class="img top-img3"></div>
                <div class="img bottom-text1"></div>
              </div>
            </div>
          </div>

          <p>
            Plugin to connect your Chatfuel bot to your API. Shows your JSON as a gallery card. You can use static or dynamic URL with user variables saved in 'Data collection form' plugin.
          </p>
          <p>
            Features:
          </p>
          <ul>
            <li>
              Use of user variables is available in URL field.
            </li>
          </ul>
          <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
        </div>
      </div>
    </li>
    <li ng-click="vm.addPlugin('form', null)" class="form" ng-show="!vm.isBc">
      User input
      <div ng-click="vm.$showHelp = 8; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 8, bottom: vm.$showHelp == 8 }">
        <div class="popup-help">
          <h3>
            User input
          </h3>

          <div class="help-image">
            <div class="line left size-170">
              <div class="box">
                <div class="img msg-bot1"></div>
              </div>
            </div>
            <div class="line right size-170">
              <div class="box">
                <div class="img msg-user1"></div>
              </div>
            </div>
          </div>

          <p>
            It is a step-by-step guide that can save or send user inputs in several ways.
          </p>
          <p>
            Features:
          </p>
          <ul>
            <li>Sends user input as email, GET or POST callback.</li>
            <li>Saves user input to use it in dynamic URLs in plugins like JSON, RSS etc..</li>
          </ul>
          <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
        </div>
      </div>
    </li>
    <li ng-click="vm.addPlugin('communication', null)" class="contact" ng-show="!vm.isBc">
      Live chat
    </li>
    <li ng-click="vm.addPlugin('typing', null)" class="typing">
      Typing…
    </li>
    <li ng-click="vm.addPlugin('setup_variable_plugin', null)" class="setup-varibale" ng-show="!vm.isBc">
      Set up a variable
    </li>
    <li ng-click="vm.addPlugin('go_to_block_plugin', null)" class="go-to-block" ng-show="!vm.isBc">
      Go to block
    </li>
    <li ng-click="vm.addPlugin('audio', null)" class="audio-plugin" ng-show="!vm.isBc">
      Audio
    </li>
    <li ng-click="vm.addPlugin('video', null)" class="video-plugin" ng-show="!vm.isBc">
      Video
    </li>
    <li ng-click="vm.addPlugin('chatroom', null)" class="chatroom" ng-show="!vm.isBc">
      Chat room
    </li>
  </ul>

  <h3>
    Plugins with subscriptions:
  </h3>

    <ul>
      <li ng-click="vm.addPlugin('search/google_plugin', null)" class="google" ng-show="!vm.isBc">
        Google site search
        <div ng-click="vm.$showHelp = 5; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 5, right: vm.$showHelp == 5 }">
          <div class="popup-help">
            <h3>
              Google site search
            </h3>

            <div class="help-image">
              <div class="line right size-295">
                <div class="box">
                  <div class="img msg-user1"></div>
                </div>
              </div>
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img top-img2"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img top-img3"></div>
                  <div class="img bottom-text1"></div>
                </div>
              </div>
            </div>

            <p>
              Shows search results for a request. You can use a specific request or a user variables saved in a «User input» plugin.
            </p>
            <p>
              Features:
            </p>
            <ul>
              <li>
                Use of user variables is available in URL field.
              </li>
              <li>
                Users can subscribe to RSS.
              </li>
              <li>
                You can set limit of sent gallery cards.
              </li>
            </ul>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('search/bing_plugin',null)" class="bind" ng-show="!vm.isBc">
        Bing search
        <div ng-click="vm.$showHelp = 6; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 6, bottom: vm.$showHelp == 6 }">
          <div class="popup-help">
            <h3>
              Bing search
            </h3>
            <div class="help-image">
              <div class="line right size-295">
                <div class="box">
                  <div class="img msg-user1"></div>
                </div>
              </div>
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img top-img2"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img top-img3"></div>
                  <div class="img bottom-text1"></div>
                </div>
              </div>
            </div>
            <p>
              Shows search results for a request. You can use a specific request or a user variable saved in a «User input» plugin.
            </p>
            <p>
              Features:
            </p>
            <ul>
              <li>
                Use of user variables is available in URL field.
              </li>
              <li>
                Users can subscribe to RSS.
              </li>
              <li>
                You can set limit of sent gallery cards.
              </li>
            </ul>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>



      <li ng-click="vm.addPlugin('search/swiftype_plugin',null)" class="swiftype" ng-show="!vm.isBc">
        Swiftype search
        <div ng-click="vm.$showHelp = 36; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 36, bottom: vm.$showHelp == 36 }">
          <div class="popup-help">
            <h3>
              Swiftype search
            </h3>
            <div class="help-image">
              <div class="line right size-295">
                <div class="box">
                  <div class="img msg-user1"></div>
                </div>
              </div>
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img top-img2"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img top-img3"></div>
                  <div class="img bottom-text1"></div>
                </div>
              </div>
            </div>
            <p>
              Shows search results for a request. You can use a specific request or a user variable saved in a «User input» plugin.
            </p>
            <p>
              Features:
            </p>
            <ul>
              <li>
                Use of user variables is available in URL field.
              </li>
              <li>
                Users can subscribe to RSS.
              </li>
              <li>
                You can set limit of sent gallery cards.
              </li>
            </ul>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>



      <li ng-click="vm.addPlugin('search/rss_plugin', null)" class="rss" ng-show="!vm.isBc">
        RSS import
        <div ng-click="vm.$showHelp = 10; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 10, right: vm.$showHelp == 10 }">
          <div class="popup-help">
            <h3>
              RSS import
            </h3>

            <div class="help-image">
              <div class="line right size-295">
                <div class="box">
                  <div class="img msg-user1"></div>
                </div>
              </div>
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img middle-text1"></div>
                  <div class="img bottom-btnshow"></div>
                </div>
                <div class="box white">
                  <div class="img top-img2"></div>
                  <div class="img middle-text1"></div>
                  <div class="img bottom-btnshow"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                  <div class="img middle-btnsubscr"></div>
                </div>
              </div>
            </div>

            <p>
              Shows RSS content as a card gallery. You can use a static URL or a URL with a user variable saved in «User input» plugin.
            </p>
            <p>
              Features:
            </p>
            <ul>
              <li>
                Use of user variables is available in URL field.
              </li>
              <li>
                Users can subscribe to RSS.
              </li>
              <li>
                Users can read RSS content right in bot.
              </li>
            </ul>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('subscribe_user_plugin', null)" class="subscriber" ng-show="!vm.isBc">
        Subscriber
      </li>
      <li ng-click="vm.addPlugin('subscriptions_manager_plugin', null)" class="subscriptions-manager" ng-show="!vm.isBc">
        Subscriptions list
      </li>
      <li ng-click="vm.addPlugin('subscription_broadcasting_plugin', null)" class="subscriptions-publisher">
        Digest
      </li>
    </ul>

    <h3>
      IFTTT integrations:
    </h3>

    <ul>
      <li ng-click="vm.addPlugin('integration/ifttt_instagram_plugin', null)" class="instagram">
        Instagram
        <div ng-click="vm.$showHelp = 4; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 4, right: vm.$showHelp == 4 }">
          <div class="popup-help">
            <h3>
              Instagram
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Instagram.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/ifttt_twitter_plugin', null)" class="twitter">
        Twitter
        <div ng-click="vm.$showHelp = 2; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 2, right: vm.$showHelp == 2 }">
          <div class="popup-help">
            <h3>
              Twitter
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Publishes Twitter updates of specified username.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/ifttt_youtube_plugin', null)" class="youtube">
        YouTube
        <div ng-click="vm.$showHelp = 3; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 3, bottom: vm.$showHelp == 3 }">
          <div class="popup-help">
            <h3>
              YouTube
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Publishes YouTube updates from a specified channel.
            </p>
            <p>
              Features:
            </p>
            <ul>
              <li>Can publish a single card with current update.</li>
              <li>Can publish a gallery composed of current update and previous videos.</li>
            </ul>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
    </ul>

    <h3>
      Zapier integrations:
    </h3>

    <ul>
      <li ng-click="vm.addPlugin('integration/zapier_plugin', null)" class="zapier">
        Zapier
        <div ng-click="vm.$showHelp = 14; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 14, right: vm.$showHelp == 14 }">
          <div class="popup-help">
            <h3>
              Zapier
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Zapier.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_rss_plugin', null)" class="rss">
        RSS
        <div ng-click="vm.$showHelp = 21; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 21, right: vm.$showHelp == 21 }">
          <div class="popup-help">
            <h3>
              RSS
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from RSS.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_youtube_plugin', null)" class="youtube">
        YouTube
        <div ng-click="vm.$showHelp = 22; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 22, right: vm.$showHelp == 22 }">
          <div class="popup-help">
            <h3>
              YouTube
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from YouTube.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_sheets_plugin', null)" class="sheets">
        Google Sheets
        <div ng-click="vm.$showHelp = 23; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 23, right: vm.$showHelp == 23 }">
          <div class="popup-help">
            <h3>
              Google Sheets
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Google Sheets.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_calendar_plugin', null)" class="calendar">
        Google Calendar
        <div ng-click="vm.$showHelp = 24; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 24, right: vm.$showHelp == 24 }">
          <div class="popup-help">
            <h3>
              Google Calendar
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Google Calendar.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_wordpress_plugin', null)" class="wordpress">
        WordPress
        <div ng-click="vm.$showHelp = 25; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 25, right: vm.$showHelp == 25 }">
          <div class="popup-help">
            <h3>
              WordPress
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from WordPress.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_slack_plugin', slack)" class="slack">
        Slack
        <div ng-click="vm.$showHelp = 26; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 26, right: vm.$showHelp == 26 }">
          <div class="popup-help">
            <h3>
              Slack
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Slack.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_twitter_plugin', null)" class="twitter">
        Twitter
        <div ng-click="vm.$showHelp = 27; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 27, right: vm.$showHelp == 27 }">
          <div class="popup-help">
            <h3>
              Twitter
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Twitter.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_instagram_plugin', null)" class="instagram">
        Instagram
        <div ng-click="vm.$showHelp = 28; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 28, right: vm.$showHelp == 28 }">
          <div class="popup-help">
            <h3>
              Instagram
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Instagram.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
      <li ng-click="vm.addPlugin('integration/zapier_vimeo_plugin', null)" class="vimeo">
        Vimeo
        <div ng-click="vm.$showHelp = 29; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 29, right: vm.$showHelp == 29 }">
          <div class="popup-help">
            <h3>
              Vimeo
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Vimeo.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li>
    </ul>


</div>

<div class="button close" ng-click="vm.close()"></div>
