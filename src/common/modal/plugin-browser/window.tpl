<div
  class="popup plugin-browser"
  modal-render="{{ $isRendered }}"
  tabindex="-1"
  role="dialog"
  uib-modal-animation-class="fade"
  modal-in-class="in"
>
	<div class="back"></div>
  <div class="scroll-box">
    <div class="window" uib-modal-transclude></div>
  </div>
</div>
