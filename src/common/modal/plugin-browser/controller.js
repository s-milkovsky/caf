
export default class ModalController {
  constructor($state, $uibModalInstance, $stateParams, $scope, $timeout) {
    'ngInject';
    this.$modal = $uibModalInstance;
    this.$stateParams = $stateParams;


    this.isBc = ($state.current.name.indexOf('broadcast') !== -1);


    $scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.$showHelp = false;
      });
    });

    $timeout(() => {
      const windowHeight = angular.element(document.querySelector('.plugin-browser .window')).prop('offsetHeight') + 40;
      angular.element(document.querySelector('.plugin-browser .scroll-box')).css('min-height', windowHeight);
      angular.element(document.querySelector('.plugin-browser .back')).css('min-height', windowHeight);
    });

  }

  addPlugin(type, param) {
    this.$modal.close({ type, param });
  }

  close() {
    this.$modal.close(false);
  }
}
