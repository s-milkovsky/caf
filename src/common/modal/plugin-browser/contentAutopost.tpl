<div class="content content-autopost">
  <h2>
    Chatfuel Plugins
  </h2>

  <h3>
    Most used:
  </h3>

  <ul>
    <li ng-click="vm.addPlugin('typing', null)" class="typing">
      Typing…
    </li>
  </ul>
  <!-- <h3>
    Quick setup autoposting
  </h3>

    <ul>
      <li ng-click="vm.addPlugin('autoposting/rss_plugin', null)" class="rss">
        RSS -->
        <!-- <div ng-click="vm.$showHelp = 1; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 1, right: vm.$showHelp == 1 }">
          <div class="popup-help">
            <h3>
              RSS
            </h3>

            <div class="help-image">
              <div class="line right size-295">
                <div class="box">
                  <div class="img msg-user1"></div>
                </div>
              </div>
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img middle-text1"></div>
                  <div class="img bottom-btnshow"></div>
                </div>
                <div class="box white">
                  <div class="img top-img2"></div>
                  <div class="img middle-text1"></div>
                  <div class="img bottom-btnshow"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                  <div class="img middle-btnsubscr"></div>
                </div>
              </div>
            </div>

            <p>
              Shows RSS content as a card gallery. You can use a static URL or a URL with a user variable saved in «User input» plugin.
            </p>
            <p>
              Features:
            </p>
            <ul>
              <li>
                Use of user variables is available in URL field.
              </li>
              <li>
                Users can subscribe to RSS.
              </li>
              <li>
                Users can read RSS content right in bot.
              </li>
            </ul>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div> -->
      <!-- </li>
      <li ng-click="vm.addPlugin('autoposting/twitter_plugin', null)" class="twitter">
        Twitter -->
        <!-- <div ng-click="vm.$showHelp = 6; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 6, right: vm.$showHelp == 6 }">
          <div class="popup-help">
            <h3>
              Twitter
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Publishes Twitter updates of specified username.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div> -->
      <!-- </li>
      <li ng-click="vm.addPlugin('autoposting/facebook_plugin', null)" class="facebook">
        Facebook -->
        <!-- <div ng-click="vm.$showHelp = 7; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 7, bottom: vm.$showHelp == 7 }">
          <div class="popup-help">
            <h3>
              Facebook
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Publishes Facebook updates from a specified channel.
            </p>
            <p>
              Features:
            </p>
            <ul>
              <li>Can publish a single card with current update.</li>
              <li>Can publish a gallery composed of current update and previous videos.</li>
            </ul>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div> -->
      <!-- </li>
    </ul> -->

    <h3>
      IFTTT autoposting
    </h3>

    <ul>
      <li ng-click="vm.addPlugin('autoposting/ifttt_plugin', null)" class="ifttt">
        IFTTT
        <!-- <div ng-click="vm.$showHelp = 5; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 5, right: vm.$showHelp == 5 }">
          <div class="popup-help">
            <h3>
              IFTTT
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from IFTTT.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div> -->
      </li>
      <li ng-click="vm.addPlugin('autoposting/ifttt_instagram_plugin', null)" class="instagram">
        Instagram
        <!-- <div ng-click="vm.$showHelp = 2; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 2, right: vm.$showHelp == 2 }">
          <div class="popup-help">
            <h3>
              Instagram
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Instagram.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div> -->
      </li>
      <li ng-click="vm.addPlugin('autoposting/ifttt_twitter_plugin', null)" class="twitter">
        Twitter
        <!-- <div ng-click="vm.$showHelp = 3; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 3, right: vm.$showHelp == 3 }">
          <div class="popup-help">
            <h3>
              Twitter
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Twitter.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div> -->
      </li>
      <li ng-click="vm.addPlugin('autoposting/ifttt_youtube_plugin', null)" class="youtube">
        Youtube
        <!-- <div ng-click="vm.$showHelp = 4; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 4, right: vm.$showHelp == 4 }">
          <div class="popup-help">
            <h3>
              Youtube
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Youtube.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div> -->
      </li>
      <!-- <li ng-click="vm.addPlugin('integration/youtube_auto', null)" class="youtube">
        YouTube
        <div ng-click="vm.$showHelp = 3; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 3, bottom: vm.$showHelp == 3 }">
          <div class="popup-help">
            <h3>
              YouTube
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Publishes YouTube updates from a specified channel.
            </p>
            <p>
              Features:
            </p>
            <ul>
              <li>Can publish a single card with current update.</li>
              <li>Can publish a gallery composed of current update and previous videos.</li>
            </ul>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div>
      </li> -->
    </ul>

    <h3>
      Zapier autoposting
    </h3>

    <ul>
      <li ng-click="vm.addPlugin('autoposting/zapier_plugin', null)" class="zapier">
        Zapier
        <!-- <div ng-click="vm.$showHelp = 8; $event.stopPropagation();" ng-class="{ act: vm.$showHelp == 8, right: vm.$showHelp == 8 }">
          <div class="popup-help">
            <h3>
              Zapier
            </h3>

            <div class="help-image">
              <div class="line left">
                <div class="box white">
                  <div class="img top-img1"></div>
                  <div class="img bottom-text1"></div>
                </div>
                <div class="box white">
                  <div class="img card1-long"></div>
                </div>
                <div class="box white">
                  <div class="img card2-long"></div>
                </div>
              </div>
            </div>

            <p>
              Broadcasts content from Zapier.
            </p>
            <div class="button close db-sq" ng-click="vm.$showHelp = false; $event.stopPropagation();"></div>
          </div>
        </div> -->
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_rss_plugin', null)" class="rss">
        RSS
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_youtube_plugin', null)" class="youtube">
        YouTube
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_sheets_plugin', null)" class="sheets">
        Google Sheets
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_calendar_plugin', null)" class="calendar">
        Google Calendar
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_wordpress_plugin', null)" class="wordpress">
        WordPress
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_slack_plugin', null)" class="slack">
        Slack
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_twitter_plugin', null)" class="twitter">
        Twitter
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_instagram_plugin', null)" class="instagram">
        Instagram
      </li>
      <li ng-click="vm.addPlugin('autoposting/zapier_vimeo_plugin', null)" class="vimeo">
        Vimeo
      </li>
    </ul>
</div>

<div class="button close" ng-click="vm.close()"></div>
