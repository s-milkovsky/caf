<div class="content" ng-class="{'block-link-on': vm.active}">
  <p class="title">
    Block link <art-switcher ng-model="vm.active"></art-switcher>
  </p>
  <p class="modal-description">
    Setup and use link parameters to allow users to <b>start a chat session with the bot from this block</b>. This parameter will be written to <span class="ref-mask">ref</span> user variable.
  </p>
  <div class="modal-block-link" ng-if="vm.active">
    <div class="link-to-block">https://m.me/{{vm.content.page}}?ref=</div>
    <input uib-tooltip="{{vm.$refError}}"
           tooltip-is-open="!!vm.$refError"
           tooltip-class="referral-error"
           tooltip-trigger="none"
           tooltip-placement="top"
           type="text" ng-model="vm._content.block.referral" class="input selectInput" ng-change="vm.checkInputTitle($event)"/>
  </div>
</div>

<div class="footer" ng-if="vm.active">
  <input type="text" class="input-to-copy" value="{{vm.encodeLink()}}"><span ng-click="vm.copyLink($event)" class="button-ver2 white b-copy-invite"
                    ng-class="{disabled: vm.buttonDisabled}"
                    uib-tooltip="Copied to clipboard"
                    tooltip-is-open="vm.copied"
                    tooltip-trigger="none"
                    tooltip-placement="top"
                    tooltip-class="tooltip-copied">COPY URL</span>
  <div class="button b-false" ng-class="{disabled: vm.buttonDisabled}" ng-click="vm.ok()" ng-bind-html="vm.buttons.ok" ng-if="vm.buttons.ok"></div>
</div>

<div class="button close" ng-click="vm.close()"></div>
