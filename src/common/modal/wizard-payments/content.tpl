<div class="wizard-content">
  <div class="state state-step-0" ng-if="vm.currentState === vm.states.step0">
    <div class="title">
      <b>This wizard will help you activate Payments feature for your bot.</b>
    </div>

    <div class="h-box">
      <div class="h-line">
        <div class="h-text">
          Native Buy Button is currently available only for users from the USA, and for bots connected to US-based Pages.
        </div>
        <div class="img img-0-1"></div>
        <div class="img img-0-2"></div>
      </div>
      <div class="h-line">
        <div class="h-text">
          To allow non-US users to buy your goods, you can use<br/>Fallback URL (link to the product page in your online store)
        </div>
        <div class="img img-0-3"></div>
      </div>
    </div>
  </div>

  <div class="state state-step-1" ng-if="vm.currentState === vm.states.step1">
    <div class="title">
      Step 1 of 2. <b>Set up Facebook payments</b>
    </div>

    <p>
       <i>1.</i> <a ng-href="{{vm.fbPageSettings}}" target="_blank">Open your Facebook Page Settings</a> and check if you have Payments section<span class="red">*</span> in the left navigation panel.
    </p>
    <div class="note">
      If you don’t have that section or if you see «Sorry, something went wrong» error you’ll need to <a href="https://docs.google.com/forms/d/1pEK8RP0w6V_7pawxMCEe_1dZcMP0cgib0zKuwqIAql8/viewform?edit_requested=true" target="_blank">submit payment request</a>
    </div>
    <p>
      <i>2.</i> On the right choose preferred payment method: Stripe or Paypal.
    </p>
    <div class="img img1"></div>
    <div class="img img2"></div>
  </div>

  <div class="state state-whoops" ng-if="vm.currentState ===  vm.states.whoops">
    <div class="whoops-box">
      <p>
        Whoops!
      </p>
      <p>
        Looks like Facebook Page payments were not set up properly. Try again or <a href="mailto:team@chatfuel.com">contact us</a>
      </p>
    </div>
  </div>

  <div class="state state-step-2" ng-if="vm.currentState ===  vm.states.step2">
    <div class="title">
      Step 2 of 2. <b>Fill in your technical information</b>
    </div>
    <p>
      Set up your merchant information as well as Payment Privacy Policy to enable Native Payments for your chatbot.
    </p>
    <div class="form">
      <div class="line">
        <div class="control">
          <div class="label">
            PAYMENT POLICY URL<span class="req">*</span>
          </div>
          <div class="input-box">
            <input
              class="cf-input auto-error-off"
              ng-class="{error: vm.errors.indexOf('url') !== -1}"
              type="text"
              placeholder="Required"
              ng-model="vm.payment_props.terms_of_service_url"
              ng-blur="vm.validate()"
              ng-change="vm.validate()"
            />
          </div>
        </div>
        <!--a href="#" target="_blank">
          Where to get Terms and Policies?
        </a-->
      </div>
      <div class="line">
        <div class="button-ver2 white-rounded" ng-click="vm.setDefUrl()">
          Get Default Policy
        </div>
        <div class="r-col-note">
          By using <a href="https://chatfuel.com/Payment_privacy_policy.pdf" target="_blank">Default Policy</a> you're confirming that you've carefully reviewed it. Only you — and not Chatfuel — will be bound by the terms of the Default Policies.
        </div>
      </div>

      <div class="line">
        <div class="control">
          <div class="label">
            MERCHANT NAME
          </div>
          <div class="input-box">
            <input class="cf-input auto-error-off" type="text" placeholder="Required" ng-model="vm.payment_props.merchant_name"/>
          </div>
        </div>
        <div class="control">
          <div class="label">
            ADMIN EMAIL FOR ORDERS<span class="req">*</span>
          </div>
          <div class="input-box">
            <input
              class="cf-input auto-error-off"
              ng-class="{error: vm.errors.indexOf('email') !== -1}"
              type="text"
              placeholder="Required"
              ng-model="vm.payment_props.merchant_emails_str"
              ng-blur="vm.validate()"
              ng-change="vm.validate()"
            />
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer">
    <div class="button-ver2 white-rounded" ng-click="vm.close()" ng-if="vm.currentState !==  vm.states.step2">CANCEL</div>
    <div class="button-ver2 white-rounded" ng-click="vm.currentState =  vm.states.step1" ng-if="vm.currentState ===  vm.states.step2">BACK</div>
    <div class="button-ver2 orange-rounded" ng-if="vm.currentState ===  vm.states.step0" ng-click="vm.start()">START</div>
    <div class="button-ver2 orange-rounded" ng-if="vm.currentState ===  vm.states.step1" ng-click="vm.getPaymentsFields();vm.currentState =  vm.states.step2">NEXT</div>
    <div class="button-ver2 orange-rounded" ng-if="vm.currentState ===  vm.states.whoops" ng-click="vm.checkStatus()">TRY AGAIN</div>
    <div class="button-ver2 orange-rounded" ng-class="{disabled: vm.errors.length !== 0}" ng-if="vm.currentState ===  vm.states.step2" ng-click="vm.ok()">DONE</div>
  </div>

  <div class="loader" ng-class="{act: vm.showLoader}">
    <div class="loader-box">
      <p>
        Checking payments setup...
      </p>
      <p>
        Just to be sure that everything's alright, it will take a moment
      </p>
    </div>
  </div>
</div>

<div class="button close" ng-click="vm.close()"></div>
