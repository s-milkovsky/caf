import _ from 'lodash';

export default class ModalController {
  constructor($uibModalInstance, $timeout, $rootScope, content, buttons, BotService, $stateParams, UserService, PageService, StoreService) {
    'ngInject';
    this.$modal = $uibModalInstance;
    this.$timeout = $timeout;
    this.$rootScope = $rootScope;
    this.content = content;
    this.buttons = buttons;
    this.$stateParams = $stateParams;
    this.BotService = BotService;
    this.UserService = UserService;
    this.PageService = PageService;
    this.StoreService = StoreService;

    this.validatePatterns = {
      url: new RegExp('^(https?://)?(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\\.)+(?:[A-Z]{2,6}\\.?|[A-Z0-9-]{2,}\\.?)|localhost|\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})(?::\\d+)?(?:/?|[/?][^ ]+)$', 'i'),
      email: new RegExp('^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$', 'i')
    };

    this.BotService.cachedList().then(bots => {
      const bot = bots.find(lBot => lBot.id === this.$stateParams.bot_id);
   //   this.fbPageSettings = `https://www.facebook.com/${bot.status.page}/settings/?tab=payments`;
      this.fbPageSettings = `https://www.facebook.com/${bot.status.page}/settings/`;
    });

    this.UserService.show().then(res => {
      this.paymentsAccess = res.payments_access;

      this.paymentPropsDef = {
        merchant_name: res.name,
        merchant_emails_str: res.email,
        terms_of_service_url: ''
      };
    });

    this.states = {
      step0: 'step-0',
      step1: 'step-1',
      whoops: 'whoops',
      step2: 'step-2'
    };

    this.$modal.result.then(() => {
      this.stopWatch();
    });
  }

  $onInit() {
    this.payment_props = {
      merchant_name: '',
      merchant_emails_str: '',
      terms_of_service_url: ''
    };

    this.getPaymentsFields();

    this.currentState = this.states.step0;
    this.showLoader = false;
  }

  $onDestroy() {
    this.stopWatch();
  }

  checkStatus() {
    this.showLoader = true;
    let watchShow = 1000;

    this.$timeout(() => {
      watchShow = 0;
    }, 700);

    this.BotService.cachedList(true).then(bots => {
      this.$timeout(() => {
        this.showLoader = false;
        const bot = bots.find(lBot => lBot.id === this.$stateParams.bot_id);
        if (true || bot.status.payments_status === 'enabled') {
          this.currentState = this.states.step2;
          this.getPaymentsFields();
        } else {
          this.currentState = this.states.whoops;
        }
      }, watchShow);
    });
  }

  getPaymentsFields() {
    this.BotService.getPayments(this.$stateParams.bot_id)
    .then(res => {
      if (!_.isEmpty(res.result)) {
        this.payment_props = res.result;
        this.payment_props.merchant_emails_str = this.payment_props.merchant_emails ? this.payment_props.merchant_emails.join(', ') : '';
      } else {
        this.payment_props = this.paymentPropsDef;
      }
      this.$timeout(() => {
        const cfInput = document.querySelector('.wizard-content .cf-input');
        if (cfInput) {
          cfInput.focus();
        }
      });
      this.validate();
    });
  }

  setPaymentsFields() {
    const pProps = _.cloneDeep(this.payment_props);
    pProps.merchant_emails = _.compact(pProps.merchant_emails_str.split(/\s*,\s*/));
    delete pProps.merchant_emails_str;
    return this.BotService.setPayments(this.$stateParams.bot_id, pProps);
  }


  validate() {
    this.errors = [];

    this.payment_props.merchant_emails_str = this.payment_props.merchant_emails_str.trim();
    this.payment_props.terms_of_service_url = this.payment_props.terms_of_service_url.trim();

    if (this.payment_props.merchant_emails_str.length === 0 || !this.payment_props.merchant_emails_str.replace(/(^[ ,]+)|([ ,]+$)/, '').split(',').every(email => this.validatePatterns.email.test(email.trim()))) {
      this.errors.push('email');
    }

    if (this.payment_props.terms_of_service_url.length === 0 || !this.validatePatterns.url.test(this.payment_props.terms_of_service_url)) {
      this.errors.push('url');
    }

    return this.errors.length === 0;
  }

  setDefUrl() {
    this.payment_props.terms_of_service_url = 'https://chatfuel.com/Payment_privacy_policy.pdf';
    this.validate();
  }

  ok() {
    this.stopWatch();
    if (this.validate()) {
      this.setPaymentsFields().then(() => {
        this.$rootScope.$emit('$paymentsEnabled');
        this.$modal.close(true);
      });
    }
  }

  close() {
    this.stopWatch();
    this.$modal.close(false);
  }

  watchUserFbAccess() {
    this.UserService.show(true)
      .then((res) => {
        this.user = res;

        if (window.payments_access_test) {
          res.payments_access = true;
        }

        if (res.payments_access) {
          this.fbWindows.close();
          this.fbWindows = null;
          window.focus();
          this.paymentsAccess = true;
          this.PageService.cachedList(Math.random());
          this.currentState = this.states.step1;
        } else {
          if (this.fbWindows && !this.fbWindows.closed) {
            this.$timeout.cancel(this.fbWatchTimeout);
            this.fbWatchTimeout = this.$timeout(() => {
              this.watchUserFbAccess();
            }, 500);
          } else {
            this.fbWindows = null;
          }
        }
      });
  }

  start() {
    if (this.paymentsAccess) {
      this.PageService.cachedList(Math.random());
      this.currentState = this.states.step1;
    } else {
      if (!this.fbWindows || this.fbWindows.closed) {
        this.fbWindows = window.open(this.StoreService.getApiUrl().replace(/\/+$/, '') + '/access_payments?bot=' + this.$stateParams.bot_id, '_blank');
        this.fbWindows.focus();

        this.$timeout(() => {
          this.watchUserFbAccess();
        }, 1000);
      } else {
        this.fbWindows.focus();
      }
    }
  }

  stopWatch() {
    if (this.fbWindows) {
      this.fbWindows.close();
      this.fbWindows = null;
    }
    this.$timeout.cancel(this.fbWatchTimeout);
  }
}
