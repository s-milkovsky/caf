<div class="content">
  <p class="title">Get latest news</p>
  <p>
    Subscribe to our newsletter to receive news and important updates.
  </p>
</div>

<div class="footer">
  <div class="button b-true" ng-click="vm.close()">Cancel</div>
  <div class="button b-false" ng-click="vm.ok()">SUBSCRIBE</div>
</div>

<div class="button close" ng-click="vm.close()"></div>
