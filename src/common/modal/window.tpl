<div
  class="popup"
  modal-render="{{ $isRendered }}"
  tabindex="-1"
  role="dialog"
  uib-modal-animation-class="fade"
  modal-in-class="in"
  ng-style="{ 'z-index': 105000 + index*10, display: 'block' }"
>
	<div class="back"></div>
	<div class="window {{size ? 'window-' + size : ''}}" uib-modal-transclude></div>
</div>
