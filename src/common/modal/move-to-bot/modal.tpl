<div class="content">
  <p class="title">Move chatbot to</p>

    <div class="selector" ng-class="{act: vm.$showSelect}">
      <div class="label placeholder" ng-click="$event.stopPropagation(); vm.$showSelect = true" ng-bind="(vm.bot) ? vm.bot.title : 'Select a chatbot'"></div>
      <ul>
        <li class="disabled">Select a chatbot</li>
        <li ng-click="vm.bot = item;"  ng-repeat="item in vm.bots track by $index">{{item.title}}</li>
      </ul>
    </div>


  <p class="gray mini">Note that the selected chatbot will be replaced with <span class="red" ng-bind="vm.movedBot.title"></span> chatbot. This action is undoable.</p>

</div>

<div class="footer">
  <div class="button b-true" ng-click="vm.close()">Cancel</div>
  <div class="button b-false" ng-click="vm.ok()" ng-disabled="!vm.bot">Move</div>
</div>

<div class="button close" ng-click="vm.close()"></div>
