<div class="content">
  <p class="title">
    Invite an admin
  </p>
  <p class="modal-description">
    Copy and send this code to invite an admin. This link is active for <b>24 hours</b>.
  </p>
  <p>
    <input type="text" ng-model="vm.content" class="input selectInput" select-text/><span ng-click="vm.copyLink($event)" class="button-ver2 white b-copy-invite"
                    uib-tooltip="Copied to clipboard"
                    tooltip-is-open="vm.copied"
                    tooltip-trigger="none"
                    tooltip-placement="top"
                    tooltip-class="tooltip-copied">COPY</span>
  </p>
</div>

<div class="footer">
  <div class="button b-true" ng-click="vm.close()" ng-bind-html="vm.buttons.close" ng-if="vm.buttons.close"></div>
  <div class="button b-false" ng-click="vm.ok()" ng-bind-html="vm.buttons.ok" ng-if="vm.buttons.ok"></div>
</div>

<div class="button close" ng-click="vm.close()"></div>
