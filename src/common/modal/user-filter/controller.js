
export default class ModalController {
  constructor($uibModalInstance, $timeout, $scope, BotService, userFilter, userFilteredCount, openHelp, index) {
    'ngInject';
    this.$modal = $uibModalInstance;
    this.$timeout = $timeout;

    this.BotService = BotService;

    this.userFilter = userFilter;
    this.userFilteredCount = userFilteredCount;
    this.index = index;

    BotService.parameterStats().then(res => {
      this.parameterStats = res;
      this.userTotalCount = res.total_users;
    });

    if (openHelp) {
      this.openHelpGo(true);
    }

    this.resize();
    $scope.$on('$modalResize', () => {
      this.resize();
    });
  }

  resize() {
    this.$timeout(() => {
      const windowHeight = angular.element(document.querySelector('.user-filter .window')).prop('offsetHeight') + 40;
      angular.element(document.querySelector('.user-filter .scroll-box')).css('min-height', windowHeight);
      angular.element(document.querySelector('.user-filter .back')).css('min-height', windowHeight);
    }, 500);
  }

  openHelpGo(v) {
    this.openHelp = v;
    this.resize();
  }

  ok() {
    this.$modal.close(true);
  }

  close() {
    this.$modal.close(null);
  }
}
