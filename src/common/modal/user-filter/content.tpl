<div class="content">

  <p class="title">
    User filter
  </p>

  <div class="help" ng-class="{act : vm.openHelp}">
    <p>
      Choose which users will receive your message. Use one of the predefined filters, or your own filters in the dropdown below, to reach a specific group of people.
    </p>
    <p>
      <span class="img"></span>
    </p>
    <p>
      For example, you have a simple poll asking users which fruit they like more: Apples or Bananas. You set up their replies to be saved to the User Variable <span class="ico-var">fruit</span>.
    </p>
    <p>
      If you wish to broadcast a message containing "Interesting facts about apples," you would select the <span class="ico-var">fruit</span> User Variable from the dropdown menu and enter  <span class="ico-val">apple</span> value in the next field. Now all the people who like apples will receive the message.<br/>
      <!--a href="http://help.chatfuel.com/facebook-messenger/" target="_blank">Learn more about using the User Filter</a-->
    </p>
  </div>
  <div class="help-switch">
    <span class="hide" ng-if="vm.openHelp" ng-click="vm.openHelpGo(false);">Hide</span>
    <span class="show" ng-if="!vm.openHelp" ng-click="vm.openHelpGo(true);">How it works and how to use it?</span>
  </div>
  <div class="filters-title">
    Who will receive this broadcast:
  </div>
  <user-filter user-filter="vm.userFilter" user-filtered-count="vm.userFilteredCount" index="vm.index" first-add="true"></user-filter>
</div>

<div class="footer">
  <div class="total">
    Total recipients: <span class="green" ng-bind="vm.userFilteredCount > vm.parameterStats.total_users ? vm.parameterStats.total_users : vm.userFilteredCount"></span> of <span ng-bind="vm.parameterStats.total_users"></span>
  </div>
  <div class="button b-false" ng-click="vm.ok()">Done</div>
</div>

<div class="button close" ng-click="vm.ok()"></div>
