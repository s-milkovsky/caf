<div class="content" ng-bind-html="vm.content"></div>

<div class="footer">
  <div class="button b-true" ng-click="vm.close()" ng-bind-html="vm.buttons.close" ng-if="vm.buttons.close"></div>
  <div class="button b-false" ng-click="vm.ok()" ng-bind-html="vm.buttons.ok" ng-if="vm.buttons.ok"></div>
</div>

<div class="button close" ng-click="vm.close()"></div>
