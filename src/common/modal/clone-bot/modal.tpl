<div class="content">
  <p class="title">Clone chatbot</p>
  <p>
    <label>
      Choose destination chatbot name:
      <input ng-model="vm.botTitle" placeholder="Chatbot name"/>
    </label>
  </p>
</div>

<div class="footer">
  <div class="button b-true" ng-click="vm.close()">Cancel</div>
  <div class="button b-false" ng-click="vm.ok()" ng-disabled="!vm.botTitle">Clone</div>
</div>

<div class="button close" ng-click="vm.close()"></div>
