<div class="content" ng-bind-html="vm.content"></div>

<div class="footer">
  <div class="button b-true" ng-click="vm.close()">Cancel</div>
  <div class="button b-false" ng-click="vm.ok()">Delete</div>
</div>

<div class="button close" ng-click="vm.close()"></div>
