import './plugin-browser/style.less';
import './broadcast-preference/style.less';
import './user-property/style.less';
import './user-filter/style.less';
import './onboarding-bot/style.less';
import './wizard-payments/style.less';
import angular from 'angular';
import ngSanitize from 'angular-sanitize';
import modal from 'angular-ui-bootstrap/src/modal';
import position from 'angular-ui-bootstrap/src/position';
import ModalService from './modal.service';
import 'angular-img-cropper';
// import 'ng-img-crop-full-extended/compile/unminified/ng-img-crop';

export default angular
  .module('app.common.modal', [ngSanitize, modal, position, 'ngFileUpload', 'angular-img-cropper'])
  .service('ModalService', ModalService)
  .name;
