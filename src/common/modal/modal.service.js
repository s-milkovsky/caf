import templateUrl from './confirm.tpl';
import templateSettingsUrl from './confirm-adv/confirm.tpl';
import templateAlert from './alert/confirm.tpl';
import windowTemplateUrl from './window.tpl';
import block from './templates/block.html';
import group from './templates/group.html';
import groupMoreTen from './templates/groupMoreTen.html';
import plugin from './templates/plugin.html';
import card from './templates/card.html';
import bot from './templates/bot.html';
import controller from './modal.controller';

import subscriptionTemplateUrl from './subscription/modal.tpl';
import subscriptionController from './subscription/controller';

import cloneBotTemplateUrl from './clone-bot/modal.tpl';
import cloneBotController from './clone-bot/controller';

import moveToBotTemplateUrl from './move-to-bot/modal.tpl';
import moveToBotController from './move-to-bot/controller';

import inviteTemplateUrl from './confirm-adv-invite/confirm.tpl';
import inviteController from './confirm-adv-invite/controller';

import linkTemplateUrl from './confirm-block-link/confirm.tpl';
import linkController from './confirm-block-link/controller';

import wizardPaymentsTemplateUrl from './wizard-payments/content.tpl';
import wizardPaymentsController from './wizard-payments/controller';

import imageSizeTemplateUrl from './image-size/modal.tpl';
import imageSizeController from './image-size/controller';

import imageResizeTemplateUrl from './image-resize/modal.tpl';
import imageResizeController from './image-resize/controller';

import readOnlyWindowTemplate from './read-only/window.tpl';
import readOnlyTemplate from './read-only/templatereadonly.tpl';
import readOnlyController from './read-only/controller';

import pluginBrowserWindowTemplate from './plugin-browser/window.tpl';
import pluginBrowserTemplate from './plugin-browser/content.tpl';
import pluginBrowserAutopostTemplate from './plugin-browser/contentAutopost.tpl';
import pluginBrowserController from './plugin-browser/controller';


import broadcastPreferenceWindowTemplate from './broadcast-preference/window.tpl';
import broadcastPreferenceTemplate from './broadcast-preference/content.tpl';
import broadcastPreferenceController from './broadcast-preference/controller';

import userPropertyWindowTemplate from './user-property/window.tpl';
import userPropertyTemplate from './user-property/content.tpl';
import userPropertyController from './user-property/controller';


import userFilterWindowTemplate from './user-filter/window.tpl';
import userFilterTemplate from './user-filter/content.tpl';
import userFilterController from './user-filter/controller';


import onboardingBotWindowTemplate from './onboarding-bot/window.tpl';
import onboardingBotTemplate from './onboarding-bot/template.tpl';


const SHOW_MODAL_CLASS = 'show-modal-popup';

export default class ModalService {
  constructor($uibModal, $document) {
    'ngInject';
    this.$uibModal = $uibModal;
    this.$document = $document;

    this.templates = {
      block,
      group,
      plugin,
      card,
      bot,
      groupMoreTen
    };
  }

  onboardingBot() {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        content: () => '',
        buttons: () => ''
      },
      controller,
      templateUrl: onboardingBotTemplate,
      windowTemplateUrl: onboardingBotWindowTemplate
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  userProperty(varName, openHelp) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        varName: () => varName,
        openHelp: () => openHelp
      },
      controller: userPropertyController,
      templateUrl: userPropertyTemplate,
      windowTemplateUrl: userPropertyWindowTemplate
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  userFilter(userFilter, userFilteredCount, openHelp, index) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        userFilter: () => userFilter,
        userFilteredCount: () => userFilteredCount,
        openHelp: () => openHelp,
        index: () => index
      },
      controller: userFilterController,
      templateUrl: userFilterTemplate,
      windowTemplateUrl: userFilterWindowTemplate
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  broadcastPreference(useBotTimezone) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        useBotTimezone: () => useBotTimezone,
      },
      controller: broadcastPreferenceController,
      templateUrl: broadcastPreferenceTemplate,
      windowTemplateUrl: broadcastPreferenceWindowTemplate
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  pluginBrowser() {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      controller: pluginBrowserController,
      templateUrl: pluginBrowserTemplate,
      windowTemplateUrl: pluginBrowserWindowTemplate
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  pluginBrowserAutopost() {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      controller: pluginBrowserController,
      templateUrl: pluginBrowserAutopostTemplate,
      windowTemplateUrl: pluginBrowserWindowTemplate
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  subscription() {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      controller: subscriptionController,
      templateUrl: subscriptionTemplateUrl,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }


  readOnly() {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        content: () => ''
      },
      controller: readOnlyController,
      templateUrl: readOnlyTemplate,
      windowTemplateUrl: readOnlyWindowTemplate
    });

    this._bindClosingBackdrop(modal, false);
    return modal;
  }

  confirm(content) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        content: () => content,
        buttons: () => {}
      },
      controller,
      templateUrl,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, false);
    return modal.result;
  }

  alert(content) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        content: () => content,
        buttons: () => {}
      },
      controller,
      templateUrl: templateAlert,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, false);
    return modal.result;
  }

  confirmAdv(content, buttons) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        content: () => content,
        buttons: () => buttons
      },
      controller,
      templateUrl: templateSettingsUrl,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, false);
    return modal.result;
  }

  confirmAdvInvite(content, buttons) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      size: 'invite',
      resolve: {
        content: () => content,
        buttons: () => buttons
      },
      controller: inviteController,
      templateUrl: inviteTemplateUrl,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, false);
    return modal.result;
  }

  confirmBlockLink(content, buttons) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      size: 'block-link',
      resolve: {
        content: () => content,
        buttons: () => buttons
      },
      controller: linkController,
      templateUrl: linkTemplateUrl,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, false);
    return modal.result;
  }

  wizardPayments(content, buttons) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      size: 'payments',
      resolve: {
        content: () => content,
        buttons: () => buttons
      },
      controller: wizardPaymentsController,
      templateUrl: wizardPaymentsTemplateUrl,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, false);
    return modal.result;
  }

  cloneBot(clonedBot) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        clonedBot: () => clonedBot,
      },
      controller: cloneBotController,
      templateUrl: cloneBotTemplateUrl,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  moveToBot(movedBot) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      resolve: {
        movedBot: () => movedBot,
      },
      controller: moveToBotController,
      templateUrl: moveToBotTemplateUrl,
      windowTemplateUrl
    });

    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  imageSize() {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      controller: imageSizeController,
      templateUrl: imageSizeTemplateUrl,
      windowTemplateUrl
    });
    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  imageResize(image) {
    const modal = this.$uibModal.open({
      controllerAs: 'vm',
      bindToController: true,
      backdrop: 'static',
      size: 'crop',
      resolve: {
        image: () => image
      },
      controller: imageResizeController,
      templateUrl: imageResizeTemplateUrl,
      windowTemplateUrl
    });
    this._bindClosingBackdrop(modal, null);
    return modal.result;
  }

  _bindClosingBackdrop(modal, res) {
    modal.opened.then(() => {
      this.$document.find('body').addClass(SHOW_MODAL_CLASS);
      this.$document.on('click', handler);
    });

    modal.closed.then(() => {
      this.$document.find('body').removeClass(SHOW_MODAL_CLASS);
      this.$document.off('click', handler);
    });

    function handler(e) {
      e.target.classList.contains('back') && modal.close(res);
    }
  }
}
