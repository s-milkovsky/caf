
export default class ModalController {
  constructor($uibModalInstance, $timeout, $scope, varName, openHelp, BotService) {
    'ngInject';
    this.$modal = $uibModalInstance;
    this.$timeout = $timeout;
    this.varName = varName;

    // $timeout(() => {
    //   angular.element(document.activeElement).triggerHandler('focus');
    // }, 100);

    BotService.getParams().then(res => {
      this.botParamsCache = res.filter((value, index, self) => self.indexOf(value) === index);
    });

    $scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.$showSelect = false;
      });
    });

    if (openHelp) {
      this.openHelpGo(true);
    }
  }

  // resize() {
  //   $timeout(() => {
  //     const windowHeight = angular.element(document.querySelector('.plugin-browser .window')).prop('offsetHeight') + 40;
  //     angular.element(document.querySelector('.plugin-browser .scroll-box')).css('min-height', windowHeight);
  //     angular.element(document.querySelector('.plugin-browser .back')).css('min-height', windowHeight);
  //   });
  // }

  openHelpGo(v) {
    this.openHelp = v;
    const obj = angular.element(document.querySelector('.popup.user-property'));

    if (v) {
      obj.addClass('openHelp');
    } else {
      obj.removeClass('openHelp');
    }
  }

  ok() {
    this.$modal.close({ varName: this.varName });
  }

  close() {
    this.$modal.close(null);
  }

  inputFocus() {
    this.$timeout(() => {
      document.querySelector('.create input').focus();
    });
  }
}
