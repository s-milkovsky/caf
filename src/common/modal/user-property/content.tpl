<div class="content">

  <p class="title">
    Remember the user’s choices by saving replies to the user variable.
  </p>

  <div class="help" ng-class="{act : vm.openHelp}">
    <p>
      When a user clicks on a Quick Reply button, it's caption can be saved to User Variable.
    </p>
    <p>
      <span class="img"></span>
    </p>
    <p>
      For example, let’s take a set of Quick Reply buttons with fruits names. You can create a User Variable called <span class="ico-brown">fruit</span> next to it. When the user presses the <span class="ico-blue">APPLE</span> button, then the caption “Apple” will be saved to the <span class="ico-brown">fruit</span> variable for this user.
    </p>
    <p>
      Later on, this can be used in the Broadcasting section to send messages to specific users who have chosen “Apple” before.
      <!-- <a href="http://help.chatfuel.com/facebook-messenger/" target="_blank">Learn more about implementing User Variables</a> -->
    </p>
  </div>
  <div class="help-switch">
    <span class="hide" ng-if="vm.openHelp" ng-click="vm.openHelpGo(false);">Hide</span>
    <span class="show" ng-if="!vm.openHelp" ng-click="vm.openHelpGo(true);">How it works and how to use it?</span>
  </div>

  <div class="property-selector" ng-if="!vm.stateCreate" ng-class="{selected: vm.varNameArr.length > 0}">
    <div class="button-ver2 white go-create" ng-click="vm.stateCreate = true; vm.inputFocus()">Create new variable</div>

    <div class="or">
      OR
    </div>

    <div class="selector-ver2" ng-class="{act: vm.$showSelect == true}">
      <div class="label placeholder" ng-click="$event.stopPropagation(); vm.$showSelect = true"><span ng-class="{'variable-mask': vm.varName}" ng-bind="vm.varName ? vm.varName : 'Choose existing variable'"></span></div>
      <ul>
        <li class="disabled">Choose existing variable:</li>
        <li ng-click="vm.varName = param" ng-repeat="param in vm.botParamsCache track by $index"><span>{{param}}</span></li>
      </ul>
    </div>

  </div>
  <div ng-if="vm.stateCreate" class="create">
    <input type="text" placeholder="Enter new variable name" ng-model="vm.varName" autofocus="true" ng-blur="vm.showMask = true" ng-focus="vm.showMask = false"/>
    <div class="variable-mask-wrapper"><div class="variable-mask" ng-class="{'show-mask': vm.showMask && vm.varName}">{{vm.varName || 'Enter new variable name'}}</div></div>
  </div>

</div>

<div class="footer">
  <div class="button b-true" ng-click="vm.stateCreate = false" ng-if="vm.stateCreate">Back</div>
  <div class="button b-true" ng-click="vm.close()" ng-if="!vm.stateCreate">Cancel</div>
  <div class="button b-false" ng-click="vm.ok()" ng-if="!vm.stateCreate">Done</div>
  <div class="button b-false" ng-click="vm.ok()" ng-if="vm.stateCreate" ng-class="{ disabled: !vm.varName}">Create</div>
</div>

<div class="button close" ng-click="vm.close()"></div>
