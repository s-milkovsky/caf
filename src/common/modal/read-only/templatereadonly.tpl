<div class="content">
  <h2>Your chatbot was sent to review</h2>
  <p>It is now read-only. To check out your chatbot submition status click here:</p>
  <a ng-attr-href="{{vm.fbUrl}}" target="_blank" class="button db-blue">CHECK OUT SUBMISSION STATUS</a>
</div>

<div class="footer">

  <h2>If your chatbot is…</h2>

  <div class="tabs">
    <ul>
      <li ng-click="vm.tabN = 0" ng-class="{ 'act': vm.tabN == 0 }">
        Pending submission
      </li>
      <li ng-click="vm.tabN = 1" ng-class="{ 'act': vm.tabN == 1 }">
        Approved
      </li>
      <li ng-click="vm.tabN = 2" ng-class="{ 'act': vm.tabN == 2 }">
        Rejected
      </li>
    </ul>
    <div class="tabs-box">

      <div class="tab-item"  ng-class="{ 'act': vm.tabN == 0 }">
        <p class="title">
          If your chatbot is pending submission you can:
        </p>
        <ol>
          <li>
            Wait some more time until Facebook submits your chatbot.
          </li>
          <li>
            Skip chatbot submission on <a ng-attr-href="{{vm.fbUrl}}" target="_blank">App Review Page</a> and click <b>Skip Submission</b> here, make some changes you like and start from the very begining.
          </li>
        </ol>
        <div>
          <a ng-click="vm.skipSubmission()" class="button db-border">SKIP SUBMISSION</a>
          <a ui-sref="bots" class="button db-orange">BACK TO DASHBOARD</a>
        </div>
      </div>

      <div class="tab-item" ng-class="{ 'act': vm.tabN == 1 }">
        <div class="img"></div>
        <p>
          After successfull submission don’t forget to make your chatbot public on <a ng-attr-href="{{vm.fbUrl}}" target="_blank">App Review Page</a>, so everyone will be able to chat with it. Then press <b>Done</b> button.
        </p>
        <div>
          <a ng-click="vm.publishedDone()" class="button db-orange">Done</a>
        </div>
      </div>

      <div class="tab-item" ng-class="{ 'act': vm.tabN == 2 }">
        <p class="title">
          If your submission failed:
        </p>
        <ol>
          <li>
            Try to understand what fixes have to be made and click <b>Edit Chatbot</b>. <br/>Send to Review again.
          </li>
          <li>
            Try using our <a href="//help.chatfuel.com/facebook-messenger/" target="_blank">Help</a> section in the Chatfuel website header.
          </li>
        </ol>
        <p>
          If you have any other problems contact us via Messenger: &nbsp;<span class="fb-box"><span class='fb-messengermessageus' id='fb-messengermessageus' messenger_app_id='1678638095724206' page_id='791843450960342' color='blue' size='xlarge'></span></span>
        </p>
        <div>
          <a ng-click="vm.rejectedDone()" class="button db-orange">EDIT CHATBOT</a>
        </div>
      </div>





    </div>
  </div>

</div>
