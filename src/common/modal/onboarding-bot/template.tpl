<div class="title">Awesome, your bot is ready!</div>
<div class="content">
  <p>Now you can take a look around. Bot-assistant in Messenger will guide you about your next steps and send you valuable tips from time to time.</p>
  <div>
    <div class="button" ng-click="vm.ok()">GREAT! CONTINUE BUILDING MY BOT</div>
  </div>
</div>

<div class="button close" ng-click="vm.close()"></div>
