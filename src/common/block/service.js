import _ from 'lodash';

/* global StoreService.getApiUrl() */
export default ($http, $stateParams, $filter, StoreService) => {
  'ngInject';

  return {

    show(blockId) {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/blocks/${ blockId }`
      }).then(res => res.data.result);
    },

    list(query) {
      return $http({
        method: 'get',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/blocks`,
        params: { query }
      }).then(res => res.data.result);
    },

    save(data) {
      if (data.id) {
        return this.update(data);
      }

      return this.create(data);
    },

    /**
     * @param data {{ parent_group: string }}
     * @returns {Promise}
     */
    create(data) {
    //  data.group_id = data.parent_group;
    //  data.bot_id = $stateParams.bot_id;

      const parentGroup = data.parent_group;
      delete data.parent_group;

      if (!data.title) {
        data.title = null;
      }

      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/groups/${ parentGroup }/blocks`
      }).then(res => {
        data = res.data.result;
        return data;
      });
    },

    /**
     * @param data {{ id: string, title: string }}
     * @returns {Promise}
     */
    update(data) {
      // rename group id field
      // @todo: remove it

       const lData = _.cloneDeep(data);

      lData.parent_group && (lData.group_id = lData.parent_group);
      // data.position || (data.position = 0);
      const id = lData.id;

      delete lData.parent_group;
      delete lData.id;


      return $http({
        data: lData,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/blocks/${ id }`
      }).then(res => {
        return data;
      });
    },

    saveReferral(data, blockId) {
      return $http({
        data,
        method: 'post',
        url: `${ StoreService.getApiUrl() }/blocks/${blockId}/referral `
      }).then(res => {
        return data;
      });
    },

    createBroadcastBlock(send_on_update = false) {
      let params = {};
      if (send_on_update) {
        params.send_on_update = true;
      }

      return $http({
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/broadcast_blocks`,
        method: 'post',
        params
      }).then(res => res.data.result);
    },

    createBroadcastNowBlock() {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/send_now_block`
      }).then(res => res.data.result);
    },

    removeBroadcastNowBlock() {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/send_now_block/delete`
      }).then(res => res.data.result);
    },

    detail(id) {
      return $http({
        method: 'get',
        url: `${StoreService.getApiUrl()}/blocks/${id}`
      }).then(res => res.data.result);
    },

    remove(id) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/blocks/${ id }/delete`
      })
        .then(res => res.data);
    },

    // sendOnUpdateBlockList() {
    //   return $http({
    //     url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/autoposted_blocks`,
    //     method: 'get'
    //   })
    //     .then(res => res.data.result);
    // },

    broadcast(blockId, deadline) {
      deadline = deadline / 1000 | 0;

      return $http({
        method: 'post',
        data: { deadline },
        url: `${ StoreService.getApiUrl() }/blocks/${ blockId }/broadcast`
      }).then(res => {
        return { id: res.data.result };
      });
    },

    broadcastNow(blockId) {
      return $http({
        method: 'post',
        url: `${ StoreService.getApiUrl() }/blocks/${ blockId }/broadcast/now`
      }).then(res => res.data.result);
    },

    broadcastRecurrent(blockId, data) {
      return $http({
        method: 'post',
        data: data,
        url: `${ StoreService.getApiUrl() }/blocks/${ blockId }/broadcast/recurrent`
      }).then(res => res.data.result);
    },

    broadcastAutopost(data) {
      return $http({
        method: 'post',
        data,
        url: `${ StoreService.getApiUrl() }/bots/${ $stateParams.bot_id }/autoposting`
      }).then(res => res.data.result);
    },


    messageUsButton(blockId, color, size, noReplace) {
      return $http({
        url: `${ StoreService.getApiUrl() }/blocks/${ blockId }/messageus`,
        params: { color, size },
        method: 'get'
      }).then(res => {
        return noReplace ? res.data.result : res.data.result.replace(/<script>.*?<\/script>/i, '');
      });
    },

    webPluginButton(blockId, color, size, noReplace) {
      return $http({
        url: `${ StoreService.getApiUrl() }/blocks/${ blockId }/webplugin`,
        params: { color, size },
        method: 'get'
      }).then(res => {
        return noReplace ? res.data.result : res.data.result.replace(/<script>.*?<\/script>/i, '');
      });
    }
  };
};
