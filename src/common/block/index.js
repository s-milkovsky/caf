import ng from 'angular';
import BlockService from './service';

export default ng.module('app.common.block', [])
  .factory('BlockService', BlockService)
  .name;
