import template from './group.html';
import controller from './controller';

export default function() {
  return {
    restrict: 'E',
    scope: {
      group: '=',
      groups: '='
    },
    template,
    controller,
    controllerAs: 'vm',
    bindToController: true
  };
};
