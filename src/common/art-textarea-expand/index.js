import './assets/less/art-textarea-expand.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.artTextareaExpand', [])
  .directive('artTextareaExpand', Component)
  .name;
