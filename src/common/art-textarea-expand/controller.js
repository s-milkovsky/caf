export default class ArtTextareaExpandController {
  constructor($element, $scope) {
    'ngInject';
    this.$element = $element;
    this.$scope = $scope;

    this.showError = false;
    this.focused = false;

    $scope.$watch('$artTextareaExpand.value', () => {
      if (this.$element[0].innerText !== this.value) {
        this.$element[0].innerText = this.value ? this.value : '';
      }
    });

    $scope.$on('$setFocusByIndex', (event, index) => {
      if (index === this.index) {
        this.$element[0].focus();
      }
    });

    $scope.$on('$setFocus', () => {
      this.$element[0].focus();
    });

    $scope.$on('$setFocusOutsideStatus', (event, val) => {
      this.showError = !val;
    });

  }

  $onInit() {
    this.$element.on('keyup input', $event => {
      this.value = this.$element[0].innerText;
      this.$scope.$emit('$textareaChange');
      this.updateDiscount();

      if (this.ngKeyup) {
        this.$scope.$apply(() => {
          this.ngKeyup({ $event });
        });
      }
    });


    this.$element.on('keydown', $event => {
      if ([8, 9, 37, 39, 46].indexOf($event.keyCode) !== -1) {
      } else {
        if (this.maxLength > 0 && this.discount < 1) {
          $event.preventDefault();
        }
      }
      if (this.ngKeydown) {
        this.$scope.$apply(() => {
          this.ngKeydown({ $event });
        });
      }
    });

    this.$element.on('blur', () => {
      this.$element[0].innerText = this.value ? this.value.trim() : '';
      this.focused = false;
      if (!this.value && this.showError) {
        this.$element.addClass('error');
      }
    });

    this.$element.on('focus', () => {
      this.focused = true;
      this.updateDiscount();
      this.$element.removeClass('error');
    });

    this.$element.on('paste', (event) => {
      const replaceInvalidCharacters = (string) => {
        const specialCharacters = ['–', '’', '<.*?>', '\\r', '\\n', '\\s+'];
        const normalCharacters = ['-', '\'', '', ' ', ' ', ' '];
        let regEx;
        for (let x = 0; x < specialCharacters.length; x++) {
          regEx = new RegExp(specialCharacters[x], 'g');
          string = string.replace(regEx, normalCharacters[x]);
        }
        return string;
      };
      event.preventDefault();
      document.execCommand('inserttext', false, replaceInvalidCharacters(event.originalEvent.clipboardData.getData('text/plain')));
      return false;
    });
  }

  $onDestroy() {
    this.$element.off('keyup input');
    this.$element.off('keydown');
    this.$element.off('blur');
    this.$element.off('focus');
    this.$element.off('paste');
  }

  updateDiscount() {
    if (Number(this.maxLength) > 0) {
      this.$element.addClass('show-discount');
      this.discount = this.maxLength - this.$element[0].innerText.length;
      if (this.discount < 0) {
        this.$element[0].innerText = this.$element[0].innerText.substring(0, this.maxLength);
        this.discount = 0;
      }

      this.$element.attr('discount', this.discount);

      if (this.discount < 7) {
        this.$element.addClass('show-discount-red');
      } else {
        this.$element.removeClass('show-discount-red');
      }
    } else {
      this.$element.removeClass('show-discount');
    }
  }
}
