import './assets/less/style.less';

import ng from 'angular';
import Component from './component.js';

export default ng.module('app.common.userFilter', [])
  .directive('userFilter', Component)
  .name;
