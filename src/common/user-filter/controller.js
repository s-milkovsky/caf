export default class UserFilterController {
  constructor($scope, BotService, $timeout, $rootScope) {
    'ngInject';
    this.$timeout = $timeout;
    this.$scope = $scope;
    this.BotService = BotService;

    $scope.$on('document.click', () => {
      $scope.$apply(() => {
        this.$showSelect = false;
        this.valuesPopUpOpen = null;
      });
    });
    $rootScope.$on('$suggestFocusedGlobal', () => {
      this.$showSelect = false;
      this.valuesPopUpOpen = null;
    });

    this.inputModels = [];

    if (this.index) this.showValuesPopUp(this.index);

    if ((!this.userFilter.parameters || !this.userFilter.parameters.length) && this.firstAdd) {
      this.add();
    }

    BotService.parameterStats().then(res => {
      this.parameterStats = res;
      this.userTotalCount = res.total_users;

      const parametersTotalCounts = {};
      let localCount;

      for (const key in res.parameters) {
        localCount = 0;
        for (const subkey in res.parameters[key]) {
          if (subkey === 'NOT SET') {
            continue;
          }
          localCount += res.parameters[key][subkey];
        }
        parametersTotalCounts[key] = localCount;
      }

      this.parameterStats.parametersTotalCounts = parametersTotalCounts;
    });
  }

  compileUserText(n) {
    return String(n).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,') + ' ' + (Number(n) === 1 ? 'user' : 'users');
  }

  remove($index) {
    this.userFilter.parameters.splice($index, 1);

    if (this.userFilter.parameters.length === 0 && this.firstAdd) {
      this.add();
    }

    this.updateUserCount();

    this.$scope.$emit('$modalResize');
    this.$scope.$emit('$saveUserFilter');
  }

  add() {
    if (!this.userFilter.parameters) {
      this.userFilter.parameters = [];
    }

    this.userFilter.parameters.push({ name: '', values: [] });

    this.$scope.$emit('$modalResize');
  }
  updateUserCount() {
    this.BotService.userCount(this.userFilter).then(res => {
      this.userFilteredCount = Number(res);
    });
  }

  valuesClick($event, $index) {
    $event.stopPropagation();
    this.$showSelect = false;
    $event.currentTarget.querySelector('input').focus();
    this.valuesPopUpOpen = $index;
    this.popupAct = 0;
  }

  showValuesPopUp($index) {
    this.$timeout(() => {
      this.valuesPopUpOpen = $index;
      const inp = document.querySelectorAll('.filters-list input')[$index];
      if (inp) {
        inp.focus();
      }
    });
  }

  onVariableClick(key, filter, filterIndex) {
    filter.name = key;
    filter.values = [];
    this.showValuesPopUp(filterIndex);
    this.updateUserCount();
  }

  variablesFilter(key) {
    if (!this.userFilter || !this.userFilter.parameters) {
      return true;
    }

    return typeof(this.userFilter.parameters.find(item => item.name === key)) === 'undefined';
  }

  filterShow(filter, obj, filterIndex) {
    let result = {};
    angular.forEach(obj, (value, key) => {
      if (
        filter.values.indexOf(key) === -1
        && (!this.inputModels[filterIndex] || this.inputModels[filterIndex].length === 0 || key.toLowerCase().indexOf(this.inputModels[filterIndex].toLowerCase()) !== -1)
      ) {
        result[key] = value;
      }
    });
    return Object.keys(result).sort((a,b) => result[b]-result[a]).reduce((acc, curr) => {
      acc[curr] = result[curr];
      return acc;
    }, {});
  }

  onInputFocus(e, index) {
    if (!this.userFilter.parameters[index].name) {
      this.$showSelect = index;
      this.valuesPopUpOpen = null;
      e.target.blur();
    }

    angular.element(e.target).parent().parent().scrollLeft(0);
  }

  onInputBlur(e, index) {
    angular.element(e.target).parent().parent().scrollLeft(0);
    this.$scope.$emit('$saveUserFilter');
  }

  onKeyDown(e, filterIndex) {
    if (e.keyCode === 8) {
      if (!this.inputModels[filterIndex] || this.inputModels[filterIndex].length === 0) {
        this.userFilter.parameters[filterIndex].values.pop();
        e.stopPropagation();
        e.preventDefault();
        this.updateUserCount();
        this.$scope.$emit('$saveUserFilter');
      }
    } else
    if (e.keyCode === 13) {
      this.userFilter.parameters.push();
      e.stopPropagation();
      e.preventDefault();

      const filter = this.userFilter.parameters[filterIndex];
      const pars = this.filterShow(filter, this.parameterStats.parameters[filter.name], filterIndex);

      let count = 0;
      for (const key in pars) {
        count ++;
        if ((count - 1) === this.popupAct) {
          this.userFilter.parameters[filterIndex].values.push(key);
          break;
        }
      }

      if (count > 0) {
        e.target.value = '';
        angular.element(e.target).trigger('input');
      }

      this.updateUserCount();

      this.popupAct = 0;
    } else
    if (e.keyCode === 38) {
      if (this.popupAct > 0) {
        this.popupAct --;
      }
      e.stopPropagation();
      e.preventDefault();
    } else
    if (e.keyCode === 40) {
      const filter = this.userFilter.parameters[filterIndex];
      if (this.popupAct < (Object.keys(this.filterShow(filter, this.parameterStats.parameters[filter.name], filterIndex)).length - 1)) {
        this.popupAct ++;
      }
      e.stopPropagation();
      e.preventDefault();
    }
  }
  removeValueItem(event, index, filterIndex) {
    event.preventDefault();
    event.stopPropagation();
    this.userFilter.parameters[filterIndex].values.splice(index, 1);
    this.updateUserCount();
    this.$scope.$emit('$saveUserFilter');
  }


}
