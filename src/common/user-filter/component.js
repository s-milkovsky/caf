import template from './template.html';
import controller from './controller';

export default () => ({
  template,
  controllerAs: 'vm',
  controller,
  bindToController: true,
  restrict: 'E',
  replace: true,
  scope: {
    userFilter: '=',
    userFilteredCount: '=',
    index: '=',
    firstAdd: '='
  }
});
