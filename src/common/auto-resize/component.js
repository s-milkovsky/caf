import controller from './controller';

export default () => ({
  scope: false,
  restrict: 'A',
  controller
});
