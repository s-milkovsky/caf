import ng from 'angular';

import autoSizeComponent from './component';

export default ng.module('app.common.autoResize', [])
  .directive('autoResize', autoSizeComponent)
  .name;
