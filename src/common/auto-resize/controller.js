export default class AutoSizeController {
  constructor($scope, $element) {
    'ngInject';

    this.$element = $element;

    const watcher = $scope.$watch('bot.title', () => {
      const elemClientHeight = this.$element[0].clientHeight;
      const elemScrollHeight = this.$element[0].scrollHeight;
      const computedHeight = elemScrollHeight + 2 + 'px';
      if (elemClientHeight < elemScrollHeight) this.$element.css('height', computedHeight);
    });

    $element.on('keypress input', $element, function () {
      const elem = this;
      const offset = elem.offsetHeight - elem.clientHeight;
      $element.css('overflow', 'hidden')
        .css('height', 'auto')
        .css('height', elem.scrollHeight + offset);
    });

    const listener = $scope.$on('$destroy', () => {
      listener();
      watcher();
      $element.off('keypress input');
    });
  }
}
