import ng from 'angular';

import fbTestBotBtnClickComponent from './component';

export default ng.module('app.common.fbTestBotBtnClick', [])
  .directive('fbTestBotBtnClick', fbTestBotBtnClickComponent)
  .name;
