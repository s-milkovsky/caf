/* global DEBUG */

import { AuthInterceptor, SequenceInterceptor, PreloaderInterceptor } from './app.services';

export const AppConfig = ($stateProvider, $urlRouterProvider, $httpProvider, $logProvider) => {
  'ngInject';
  $logProvider.debugEnabled(DEBUG);
  $httpProvider.interceptors.push(AuthInterceptor);
  $httpProvider.interceptors.push(SequenceInterceptor);
  $urlRouterProvider.otherwise('/bots');
  $stateProvider
    .state('app', {
      url: '/bot/:bot_id',
      abstract: true
    })
    .state('auth', {
      url: '/auth',
      abstract: true
    })
    .state('auth.token', {
      url: '/token/{params:.*}',
      resolve: {
        token($q, $timeout, $state, $stateParams, $rootScope, StoreService) {
          'ngInject';

          const params = $stateParams.params.split('/');
          const token = params.shift();
          const paramsObj = {};

          for (let i = 0; i < params.length; i += 2) {
            paramsObj[params[i]] = params[i + 1];
          }

          if (!localStorage.getItem('token')) {
            if (paramsObj.is_new_user && paramsObj.is_new_user === 'true') {
              $rootScope.$emit('$statEvent', { event: 'newuser' });
              $timeout(() => {
                $rootScope.$emit('$newuser');
              }, 500);

              $rootScope.$broadcast('$tos', true);
              $rootScope.$showTos = true;
              $rootScope.$newuser = true;

              window.localStorage.setItem('$newuser', 'true');
            }
            $rootScope.$emit('$statEvent', { event: 'authorized' });
          }

          localStorage.setItem('token', token);
          StoreService.setAutorizedCookie(true);

          if (!$rootScope.$showTos) {
            $timeout(() => {
              // if (paramsObj.bot) {
              //   $state.go('app.structure', { bot_id: paramsObj.bot });
              // } else {
              //   $state.go('bots');
              // }
              $state.go('bots');
            });
          } else {
            $rootScope.$afterShowTosTarget = (paramsObj.bot ? (paramsObj.bot) : 'bots');
            $rootScope.$afterShowTosGoSettings = true;
           // $rootScope.$afterShowTosTarget = 'settings';
          }

          return $q.reject();
        }
      }
    });
};
