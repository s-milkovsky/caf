import template from './app.html';
import controller from './app.controller';

export const AppComponent = ($document) => {
  'ngInject';
  return {
    controller,
    template,
    restrict: 'E',
    controllerAs: 'vm',
    replace: true,
    link: (scope, element, attrs) => {
      $document.on('click', () => {
        scope.$broadcast('document.click');
      });
      $document.on('mousedown', () => {
        scope.$broadcast('document.mousedown');
      });
      $document.on('mouseup', () => {
        scope.$broadcast('document.mouseup');
      });
    }
  };
};
