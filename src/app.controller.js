export default class AppController {
  constructor($scope, $rootScope, $state) {
    'ngInject';

    $scope.showPreloader = true;
    
    this.rs = $rootScope;

    $scope.$on('$stateChangeSuccess', () => {
      this.className = '';
      this.showHeader = false;
      if ($state.is('app.aisetup')) {
        this.className = 'ai-setup';
      } else if ($state.includes('app.**')) {
        this.className = 'editor';
      } else if ($state.is('bots')) {
        this.className = 'dashboard';
        this.showHeader = true;
      }
    });

  }
}
