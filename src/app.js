/* global MOBILE_PAGE_URL, DEBUG */

import './assets/less/main.less';
import './assets2/less/main.less';

// import jQuery before angular and expose it
import $ from 'jquery';
import ng from 'angular';
import ngRouter from 'angular-ui-router';
import ngCookies from 'angular-cookies';
import MobileDetect from 'mobile-detect';

import Common from './common';
import Components from './components';
import { AppConfig } from './app.config';
import { AppComponent } from './app.component';
import { Store } from './app.services';
import TUTORIAL from './tutorials.json';


ng.module('app', [ngRouter, Common, Components, ngCookies])
  .directive('app', AppComponent)
  .service('StoreService', Store)
  .config(AppConfig)
  .constant('TUTORIAL', TUTORIAL)
  .constant('API_ERROR_MSG', 'Sorry, something went wrong. Please try again later.')
  .run(($window, $rootScope, UserService, MobileWarringService, StoreService) => {
    'ngInject';

    const md = new MobileDetect($window.navigator.userAgent);

    StoreService.setGoOutCookie(false);
    
    if (window.localStorage.getItem('token')) {
      StoreService.setAutorizedCookie(true);
    }

    if (md.mobile() || md.phone()) {
      const body = angular.element(document.getElementsByTagName('body')[0]);

      angular.element($window).on('resize', () => {
        const mws = body.hasClass('mobile-warring-show');
        const h = angular.element($window).height() + 64 - (mws ? 110 : 0);
        body.css('min-height', h);
        if (mws) {
          body.css('height', h);
        }
      });

      MobileWarringService.show();

      angular.element($window).trigger('resize');
    }

    if ($window.location.search) {
      try {
        localStorage.setItem('get-params', $window.location.search);
      } catch (err) {}
    }



    // intercom script
    // (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/mca3oa0n';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();

    // load intercom
    // UserService.show()
    //   .then(res => {
    //     $window.Intercom('boot', {
    //       app_id: 'mca3oa0n',
    //       name: res.name,
    //       email: res.email,
    //       user_id: res.id,
    //       created_at: res.date_added
    //     });
    //   });
  });
