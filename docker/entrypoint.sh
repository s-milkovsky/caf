#!/bin/sh

cd /opt

if [ ! $APP_ID ]; then
  rm -f dist/config.js
else
  echo $(envsubst < docker/config.js.template) > dist/config.js
fi

nginx -g "daemon off;"
