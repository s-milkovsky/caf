var path              = require('path');

var webpack           = require('webpack');
var CleanPlugin       = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var precss            = require('precss');
var autoprefixer      = require('autoprefixer');
var WebpackRobots     = require('@tanepiper/webpack-robotstxt');

const PATH = __dirname + '/src';

const DEBUG = (process.env.DEBUG || false) === 'true';

console.log("Debug mode: ", DEBUG);

const plugins = [
  new CleanPlugin(['dist']),
  new webpack.optimize.DedupePlugin(),
  new ExtractTextPlugin("[name]-[hash].css"),
  new webpack.DefinePlugin({
    DEBUG: DEBUG,
    API_URL: JSON.stringify(process.env.API_URL || ''),
    APP_ID: JSON.stringify(process.env.APP_ID || ''),
    MAIN_PAGE_URL: JSON.stringify(process.env.MAIN_PAGE_URL || 'http://dev.chatfuel.com'),
    MOBILE_PAGE_URL: JSON.stringify(process.env.MOBILE_PAGE_URL || 'http://dev.chatfuel.com/ru/m-dashboard.html')
  }),
  new HtmlWebpackPlugin({
    template: './index.html',
    inject: 'body'
  }),
  new webpack.ContextReplacementPlugin(/\/moment\/locale/, /en-gb/),
  new WebpackRobots({
    userAgents: [{
      name: '*',
      disallow: ['/']
    }]
  })
];


if (false && !DEBUG) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false }
    })
  )
}

module.exports = {
  devtool: 'source-map',
  context: PATH,

  entry: {
    app: ['./app.js']
  },

  output: {
    path: './dist',
    filename: 'bundle-[hash].js'
  },

  module: {
    loaders: [
      { test: require.resolve('jquery'), loader: 'expose?jQuery' },
      { test: /\.js$/, loader: 'ng-annotate!babel?presets=es2015', exclude: /node_modules/ },
      { test: /\.less/, loader: ExtractTextPlugin.extract('style', 'css!less?sourceMap') },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css') },
      { test: /\.woff2?(.*)?$/, loader: "file?name=fonts/[name].[ext]" },
      { test: /\.(ttf|eot|svg)(.*)?$/, loader: "file?name=fonts/[name].[ext]" },
			{ test: /\.tpl$/, loaders: [`ngtemplate?relativeTo=${ PATH }`, `html?root=${ PATH }`] },
    	{ test: /\.html$/, loader: 'underscore-template-loader', query: { attributes: ['img:src', 'link:href'] } },
      { test: /\.png|\.jpg|\.gif|images\/\.svg$/, loader: 'file?name=[path][name].[ext]' },
      { test: /\.json$/, loader: 'json' }
    ]
  },

  resolve: {
    root: [
      path.resolve('./node_modules'),
      path.resolve('./src/vendors')
    ]
  },

  postcss: function () {
    return [precss, autoprefixer];
  },

  plugins: plugins,

  devServer: {
    contentBase: "./dist",
    historyApiFallback: true,
    colors: true,
    hot: true,
    port: 80
  }
};
